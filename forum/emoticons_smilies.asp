<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

Response.Buffer = True 

'Declare variables
Dim intIndexPosition		'Holds the idex poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Emoticon Smilies</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
		
<script  language="JavaScript">

//Function to add the code to the message for the smileys
function AddSmileyIcon(iconCode) {
 	var txtarea = window.opener.document.frmAddMessage.message;
 	iconCode = ' ' + iconCode + ' ';
 	if (txtarea.createTextRange && txtarea.caretPos) {
  		var caretPos = txtarea.caretPos;
  		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? iconCode + ' ' : iconCode;
  		txtarea.focus();
 	} else {
  		txtarea.value  += iconCode;
  		txtarea.focus();
 	}
}

</script>
<!--#include file="includes/skin_file.asp" -->
</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center"><span class="heading"><% = strTxtEmoticonSmilies %></span></td>
  </tr>
</table>
<br />
  <table width="350" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% = strTableBorderColour %>" height="138">
  <tr> 
      <td height="174"> 
        
      <table border="0" align="center" cellpadding="4" cellspacing="1" width="350">
        <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"> 
          <td colspan="2" class="text" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr> 
                <td align="center" class="text"><% = strTxtClickOnEmoticonToAdd %></td>
              </tr>
            </table>
            <table width="340" border="0" cellspacing="0" cellpadding="4"><%

'Intilise the index position (we are starting at 1 instead of position 0 in the array for simpler calculations)
intIndexPosition = 1

'Calcultae the number of outer loops to do
intNumberOfOuterLoops = UBound(saryEmoticons) / 2

'If there is a remainder add 1 to the number of loops
If UBound(saryEmoticons) MOD 2 > 0 Then intNumberOfOuterLoops = intNumberOfOuterLoops + 1

'Loop throgh th list of emoticons
For intLoop = 1 to intNumberOfOuterLoops
      
%>
             <tr><%

	'Loop throgh th list of emoticons
	For intInnerLoop = 1 to 2  
	
		'If there is nothing to display show an empty box
		If intIndexPosition > UBound(saryEmoticons) Then 
			Response.Write("<td width=""17"" class=""text"">&nbsp;</td>") 
			Response.Write("<td width=""62"" class=""text"">&nbsp;</td>")
			Response.Write("<td width=""64"" class=""text"">&nbsp;</td>")
		'Else show the emoticon
		Else 
			Response.Write("<td width=""17"" class=""text""><a href=""JavaScript:AddSmileyIcon('" & saryEmoticons(intIndexPosition,2) & "')""><img src=""" & saryEmoticons(intIndexPosition,3) & """ border=""0"" alt=""" & saryEmoticons(intIndexPosition,2) & """></a></td>")
                	Response.Write("<td width=""62"" class=""text"">" & saryEmoticons(intIndexPosition,1) & "</td>")
                	Response.Write("<td width=""64"" class=""text"">" & saryEmoticons(intIndexPosition,2) & "</td>")
              	End If
              
              'Minus one form the index position
              intIndexPosition = intIndexPosition + 1 
	Next            
%></tr><%
Next             
%></table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center" height="34"><a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a></td>
  </tr>
</table>
<div align="center">
<% 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</div>
</body>
</html>