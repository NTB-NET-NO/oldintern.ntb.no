<%@ Control Language="vb" AutoEventWireup="false" Inherits="intranett.internavis_list" CodeFile="internavis_list.ascx.vb" %>

<!-- Lists items from the forum -->

<asp:datalist CSSClass="artikkel_tekst" id="dlItems" runat="server" CellPadding="0" CellSpacing="0"
	RepeatLayout="Table">
	<ItemTemplate>
			<%# dateheader%>
			<%# Format(dr("start_date"), "dd.MM.yyyy HH:mm") %><br>
			<b><A href='view_avisitem.aspx?TID=<%# dr("topic_id") %>' target="internavis_main" >
				<%# dr("Subject") %>
			</A></b>
			<br>( <i>
			<asp:Label ID="name" Runat="server" Visible=True>
			</asp:Label>
			</i> )
			<br><br>
	</ItemTemplate>
</asp:datalist>
