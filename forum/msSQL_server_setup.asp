<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="admin/SQL_server_connection.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide ASP Discussion Forum
'**
'**  Copyright 2001 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can redistribute it and/or modify
'**  it under the terms of the GNU General Public License as published by
'**  the Free Software Foundation; either version 2 of the License, or
'**  any later version.
'**
'**  All copyright notices must remain intacked in the scripts and the
'**  outputted HTML.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide must remain in place and the powered by
'**  logo with link back to Web Wiz Guide must remain visiable when the pages
'**  are viewed.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'**  GNU General Public License for more details.
'**
'**  You should have received a copy of the GNU General Public License
'**  along with this program; if not, write to the Free Software
'**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.com
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'****************************************************************************************

'Set the response buffer to true as we maybe redirecting and setting a cookie
Response.Buffer = False


'Set up the database table name prefix and stored procedure prefix
'(This is useful if you are running multiple forums from one database)
' - make sure you also change this in the common.asp files
Const strDbTable = "tbl"
Const strDbProc = "wwfSp"

%>
<html>
<head>
<title>MS SQL Server Database Setup</title>
<meta name="copyright" content="Copyright (C) 2001-2003 Bruce Corkhill" />

<!-- The Web Wiz Guide Login Script is written by Bruce Corkhill �2001
     	If you want your own  Login Script then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a Username
	if (document.frmLogin.name.value==""){
		errorMsg += "\n\tUsername \t- Enter a Username with tbale\creation rights on the database";
	}

	//Check for a Password
	//if (document.frmLogin.password.value==""){
		//errorMsg += "\n\tPassword \t- Enter a Password with tbale\creation rights on the database";
	//}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "_____________________________________________________________________\n\n";
		msg += "Your Login to the SQL Server has failed because there are problem(s) with the form.\n";
		msg += "Please correct the problem(s) and re-submit the form.\n";
		msg += "_____________________________________________________________________\n\n";
		msg += "The following field(s) need to be corrected: -\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
// -->
</script>

</head>
<body bgcolor="#FFFFFF" text="#000000">
<table width="584" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center">
      <h1>Web Wiz Forums Database Microsoft SQL Server Setup</h1>
    </td>
  </tr>
</table>
<div align="center"><br />
 <font face="Verdana, Arial, Helvetica" size="2">Before you can use this page to create the tables, defualt values, stored procedures, etc. in the <br>
 SQL Server Database you first need to create an empty database on the Microsoft SQL Server.<font face="Verdana, Arial, Helvetica" size="2"><br />
 <br>
 <strong>Please Read!!</strong><br />
 Enter a username and password in the box below that has table creation/modification <br />
 rights at the SQL Server database you are using.<br>
 <br />
 <b>This may not be the same user that you have used in your SQL Server connection string!</b></font></font><br />
</div>
<form name="frmLogin" method="post" action="msSQL_server_setup.asp" onSubmit="return CheckForm();">
  
 <table width="400" border="0" cellspacing="0" cellpadding="1" align="CENTER" bgcolor="#000000">
  <tr>
      <td>
        <table width="400" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#CCCCCC">
     <tr>
            
      <td align="right" height="47" valign="bottom" width="190">SQL Server Username: </td>
            <td height="47" valign="bottom" width="208">
              <input type="text" name="name">
            </td>
          </tr>
          <tr>
            
      <td align="right" width="190">SQL Server Password: </td>
            <td width="208">
              <input type="password" name="password">
            </td>
          </tr>
          <tr>
            <td align="right" height="44" width="190">&nbsp;</td>
            <td height="44" width="208">
              <input type="submit" name="Submit" value="Enter">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="reset" name="Submit" value="Reset">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
<table width="702" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><font face="Verdana, Arial, Helvetica, sans-serif" size="2">&nbsp;
      <%

Dim adoCon 			'Database Connection Variable
Dim strCon			'Holds the Database driver and the path and name of the database
Dim strSQL			'Holds the SQL query for the database
Dim intBadWordLoopCounter	'Holds the bad word filter writing to db counter
Dim blnErrorOccured		'Set to true if an error occurs


'Resume on all errors
On Error Resume Next


'intialise variables
blnErrorOccured = False

'If a username and password is entred then start the ball rolling
If NOT Request.Form("name") = "" Then

	'Create database connection
	'Create a connection odject
	Set adoCon = Server.CreateObject("ADODB.Connection")

	'Build a database driver connection string to the SQL server
	strCon = "Provider=SQLOLEDB;Server=" & strSQLServerName & ";User ID=" & Request.Form("name") & ";Password=" & Request.Form("password") & ";Database=" & strSQLDBName & ";"

	'Set the connection string to the database
	adoCon.connectionstring = strCon

	'Set an active connection to the Connection object
	adoCon.Open

	'If an error has occured write an error to the page
	If Err.Number <> 0 Then
		Response.Write("<br /><b>Error Connecting to database on SQL Server</b><br /><br />Check the following is set up and correct:- <br /><br />SQL Server User Name<br />SQL Server Password<br />")
		Response.Write("Name of the SQL Server<br />Database name set up by you on the server<br />Empty Database set up by you on the SQL Server<br /><br />Check also that you have entered the correct details in the file 'SQL_server_connection.asp' found in the admin directory.<br /><br />")
	Else





'******************************************
'***  		Create the tables     *****
'******************************************

		'Create the Category Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Category] ("
		strSQL = strSQL & "[Cat_ID] [smallint] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Cat_name] [nvarchar] (60) NOT NULL ,"
		strSQL = strSQL & "[Cat_order] [smallint] NOT NULL DEFAULT (1) "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then

			'Write an error message
			Response.Write("<br />Error Creating the Table " & strDbTable & "Category (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		'Create the Forum Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Forum] ("
		strSQL = strSQL & "[Forum_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Cat_ID] [smallint] NULL ,"
		strSQL = strSQL & "[Forum_Order] [smallint] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Forum_name] [nvarchar] (70) NULL ,"
		strSQL = strSQL & "[Forum_description] [nvarchar] (200) NULL ,"
		strSQL = strSQL & "[Date_Started] [datetime] NOT NULL DEFAULT (getdate()),"
		strSQL = strSQL & "[No_of_topics] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[No_of_posts] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Password] [nvarchar] (50) NULL ,"
		strSQL = strSQL & "[Forum_code] [nvarchar] (40) NULL ,"
		strSQL = strSQL & "[Locked] [bit] NOT NULL ,"
		strSQL = strSQL & "[Read] [int] NULL ,"
		strSQL = strSQL & "[Post] [int] NULL ,"
		strSQL = strSQL & "[Reply_posts] [int] NULL ,"
		strSQL = strSQL & "[Edit_posts] [int] NULL ,"
		strSQL = strSQL & "[Delete_posts] [int] NULL ,"
		strSQL = strSQL & "[Priority_posts] [int] NULL ,"
		strSQL = strSQL & "[Poll_create] [int] NULL ,"
		strSQL = strSQL & "[Vote] [int] NULL ,"
		strSQL = strSQL & "[Attachments] [int] NULL ,"
		strSQL = strSQL & "[Image_upload] [int] NULL ,"
		strSQL = strSQL & "[Hide] [bit] NOT NULL ,"
		strSQL = strSQL & "[Show_topics] [smallint] NULL "
		strSQL = strSQL & ") ON [PRIMARY]"
		
		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then

			'Write an error message
			Response.Write("<br />Error Creating the Table " & strDbTable & "Forum (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		'Create the Topic Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Topic] ("
		strSQL = strSQL & "[Topic_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Forum_ID] [int] NULL ,"
		strSQL = strSQL & "[Poll_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Moved_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Subject] [nvarchar] (70) NOT NULL ,"
		strSQL = strSQL & "[Start_date] [datetime] NOT NULL DEFAULT (getdate()),"
		strSQL = strSQL & "[Last_entry_date] [datetime] NULL ,"
		strSQL = strSQL & "[No_of_views] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Locked] [bit] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Priority] [smallint] NOT NULL "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Topic (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		'Create the Thread Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Thread] ("
		strSQL = strSQL & "[Thread_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Topic_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[Author_ID] [int] NULL ,"
		strSQL = strSQL & "[Message] [text] NULL ," 'This is set to a text datatype inorder to hold large posts as nvarchar has 4,000 limit and varchar an 8,000 limit
		strSQL = strSQL & "[Message_date] [datetime] NOT NULL  DEFAULT (getdate()) ,"
		strSQL = strSQL & "[Show_signature] [bit] NOT NULL DEFAULT (0), "
		strSQL = strSQL & "[IP_addr] [nvarchar] (30) NULL "
		strSQL = strSQL & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
		
		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Thread (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Create the Author Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & "[Author_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Group_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[Username] [nvarchar] (25) NOT NULL ,"
		strSQL = strSQL & "[Real_name] [nvarchar] (40) NULL ,"
		strSQL = strSQL & "[User_code] [nvarchar] (50) NOT NULL ,"
		strSQL = strSQL & "[Password] [nvarchar] (50) NOT NULL ,"
		strSQL = strSQL & "[Salt] [nvarchar] (30) NULL ,"
		strSQL = strSQL & "[Author_email] [nvarchar] (75) NULL ,"
		strSQL = strSQL & "[Show_email] [bit] NOT NULL ,"
		strSQL = strSQL & "[Homepage] [nvarchar] (50) NULL ,"
		strSQL = strSQL & "[Location] [nvarchar] (60) NULL ,"
		strSQL = strSQL & "[MSN] [nvarchar] (75) NULL ,"
		strSQL = strSQL & "[Yahoo] [nvarchar] (75) NULL ,"
		strSQL = strSQL & "[ICQ] [nvarchar] (20) NULL ,"
		strSQL = strSQL & "[AIM] [nvarchar] (75) NULL ,"
		strSQL = strSQL & "[Occupation] [nvarchar] (60) NULL ,"
		strSQL = strSQL & "[Interests] [nvarchar] (160) NULL ,"
		strSQL = strSQL & "[DOB] [datetime] NULL ,"
		strSQL = strSQL & "[Signature] [ntext] NULL ,"
		strSQL = strSQL & "[Attach_signature] [bit] NOT NULL ,"
		strSQL = strSQL & "[No_of_posts] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Join_date] [datetime] NOT NULL DEFAULT (getdate()),"
		strSQL = strSQL & "[Active] [bit] NOT NULL ,"
		strSQL = strSQL & "[Avatar] [nvarchar] (100) NULL ,"
		strSQL = strSQL & "[Avatar_title] [nvarchar] (70) NULL ,"
		strSQL = strSQL & "[Last_visit] [datetime] NOT NULL DEFAULT (getdate()),"
		strSQL = strSQL & "[Time_offset] [nvarchar] (1) NOT NULL ,"
		strSQL = strSQL & "[Time_offset_hours] [smallint] NOT NULL ,"
		strSQL = strSQL & "[Date_format] [nvarchar] (10) NULL ,"
		strSQL = strSQL & "[Rich_editor] [bit] NOT NULL ,"
		strSQL = strSQL & "[Reply_notify] [bit] NOT NULL ,"
		strSQL = strSQL & "[PM_notify] [bit] NOT NULL "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Author (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Create the Private messages table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "PMMessage] ("
		strSQL = strSQL & "[PM_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Author_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[From_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[PM_Tittle] [nvarchar] (70) NOT NULL ,"
		strSQL = strSQL & "[PM_Message] [text] NOT NULL ," 'This is set to a text datatype inorder to hold large posts as nvarchar has 4,000 limit and varchar an 8,000 limit
		strSQL = strSQL & "[PM_Message_Date] [datetime] NOT NULL  DEFAULT (getdate()),"
		strSQL = strSQL & "[Read_Post] [bit] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Email_notify] [bit] NOT NULL DEFAULT (0)"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "PMMessage (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Create the Buddy List table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "BuddyList] ("
		strSQL = strSQL & "[Address_ID] [int] IDENTITY (1, 1) PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Author_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[Buddy_ID] [int] NOT NULL ,"
		strSQL = strSQL & "[Description] [nvarchar] (60) NOT NULL ,"
		strSQL = strSQL & "[Block] [bit] NOT NULL DEFAULT (0) "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "BuddyList (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If





		'Create the Configuration Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Configuration] ("
		strSQL = strSQL & "[website_name] [nvarchar] (50)  NOT NULL  PRIMARY KEY  CLUSTERED ,"
		strSQL = strSQL & "[forum_name] [nvarchar] (50)  NULL, "
		strSQL = strSQL & "[forum_path] [nvarchar] (70)  NULL ,"
		strSQL = strSQL & "[website_path] [nvarchar] (70)  NULL ,"
		strSQL = strSQL & "[forum_email_address] [nvarchar] (50)  NULL ,"
		strSQL = strSQL & "[email_notify] [bit] NOT NULL ,"
		strSQL = strSQL & "[mail_component] [nvarchar] (10)  NULL ,"
		strSQL = strSQL & "[mail_server] [nvarchar] (60)  NULL ,"
		strSQL = strSQL & "[IE_editor] [bit] NOT NULL ,"
		strSQL = strSQL & "[L_code] [nvarchar] (15) NULL ,"
		strSQL = strSQL & "[Topics_per_page] [smallint] NULL ,"
		strSQL = strSQL & "[Title_image] [nvarchar] (70)  NULL ,"
		strSQL = strSQL & "[Text_link] [bit] NOT NULL ,"
		strSQL = strSQL & "[Emoticons] [bit] NOT NULL ,"
		strSQL = strSQL & "[Avatar] [bit] NOT NULL, "
		strSQL = strSQL & "[Email_activate] [bit] NOT NULL, "
		strSQL = strSQL & "[Email_post] [bit] NOT NULL, "
		strSQL = strSQL & "[Hot_views] [smallint] NULL, "
		strSQL = strSQL & "[Hot_replies] [smallint] NOT NULL, "
		strSQL = strSQL & "[Private_msg] [bit] NOT NULL, "
		strSQL = strSQL & "[No_of_priavte_msg] [smallint] NULL, "
		strSQL = strSQL & "[Threads_per_page] [smallint] NULL, "
		strSQL = strSQL & "[Spam_seconds] [smallint] NULL, "
		strSQL = strSQL & "[Spam_minutes] [smallint] NULL, "
		strSQL = strSQL & "[Vote_choices] [smallint] NULL, "
		strSQL = strSQL & "[Email_sys] [bit] NOT NULL, "
		strSQL = strSQL & "[Upload_img_path] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_img_types] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_img_size] [smallint] NULL, "
		strSQL = strSQL & "[Upload_files_path] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_files_type] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_files_size] [smallint] NULL, "
		strSQL = strSQL & "[Upload_avatar] [bit] NOT NULL, "
		strSQL = strSQL & "[Upload_avatar_path] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_avatar_types] [nvarchar] (50) NULL, "
		strSQL = strSQL & "[Upload_avatar_size] [smallint] NULL, "
		strSQL = strSQL & "[Upload_component] [nvarchar] (10) NULL, "
		strSQL = strSQL & "[Active_users] [bit] NOT NULL, "
		strSQL = strSQL & "[Forums_closed] [bit] NOT NULL, "
		strSQL = strSQL & "[Show_edit] [bit] NOT NULL, "
		strSQL = strSQL & "[Process_time] [bit] NOT NULL, "
		strSQL = strSQL & "[Flash] [bit] NOT NULL, "
		strSQL = strSQL & "[Show_mod] [bit] NOT NULL, "
		strSQL = strSQL & "[Reg_closed] [bit] NOT NULL "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Configuration (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Create the Date Time Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "DateTimeFormat] ("
		strSQL = strSQL & "[Date_format] [nvarchar] (10)  NOT NULL  PRIMARY KEY  CLUSTERED ,"
		strSQL = strSQL & "[Year_format] [nvarchar] (6)  NULL ,"
		strSQL = strSQL & "[Seporator] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month1] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month2] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month3] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month4] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month5] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month6] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month7] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month8] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month9] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month10] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month11] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Month12] [nvarchar] (15)  NULL ,"
		strSQL = strSQL & "[Time_format] [smallint] NULL ,"
		strSQL = strSQL & "[am] [nvarchar] (6)  NULL ,"
		strSQL = strSQL & "[pm] [nvarchar] (6)  NULL "
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "DateTimeFormat (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		'Create the Group Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Group_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Name] [nvarchar] (40) NULL ,"
		strSQL = strSQL & "[Minimum_posts] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Special_rank] [bit] NOT NULL ,"
		strSQL = strSQL & "[Stars] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Custom_stars] [nvarchar] (80) NULL ,"
		strSQL = strSQL & "[Starting_group] [bit] NOT NULL DEFAULT (0)"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Group (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		'Create the Poll Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Poll] ("
		strSQL = strSQL & "[Poll_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Poll_question] [nvarchar] (90) NULL ,"
		strSQL = strSQL & "[Multiple_votes] [bit] NOT NULL ,"
		strSQL = strSQL & "[Reply] [bit] NOT NULL ,"
		strSQL = strSQL & "[Author_ID] [int] NULL"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Poll (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		'Create the Poll Choices Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "PollChoice] ("
		strSQL = strSQL & "[Choice_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Poll_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Choice] [nvarchar] (80) NULL ,"
		strSQL = strSQL & "[Votes] [int] NOT NULL DEFAULT (0)"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "PollChoice (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		'Create the Email Notify Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "EmailNotify] ("
		strSQL = strSQL & "[Watch_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Author_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Forum_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Topic_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "EmailNotify (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		'Create the Permissions Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Permissions] ("
		strSQL = strSQL & "[Group_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Author_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Forum_ID] [int] NOT NULL DEFAULT (0),"
		strSQL = strSQL & "[Read] [bit] NOT NULL ,"
		strSQL = strSQL & "[Post] [bit] NOT NULL ,"
		strSQL = strSQL & "[Reply_posts] [bit] NOT NULL ,"
		strSQL = strSQL & "[Edit_posts] [bit] NOT NULL ,"
		strSQL = strSQL & "[Delete_posts] [bit] NOT NULL ,"
		strSQL = strSQL & "[Priority_posts] [bit] NOT NULL ,"
		strSQL = strSQL & "[Poll_create] [bit] NOT NULL ,"
		strSQL = strSQL & "[Vote] [bit] NOT NULL ,"
		strSQL = strSQL & "[Attachments] [bit] NOT NULL ,"
		strSQL = strSQL & "[Image_upload] [bit] NOT NULL ,"
		strSQL = strSQL & "[Moderate] [bit] NOT NULL"
		strSQL = strSQL & ") ON [PRIMARY]"
		
		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Permissions (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		


		'Create the GuestName Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "GuestName] ("
		strSQL = strSQL & "[Guest_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Thread_ID] [int] NULL ,"
		strSQL = strSQL & "[Name] [nvarchar] (30)  NULL"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "GuestName (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If





		'Create the Smut Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "Smut] ("
		strSQL = strSQL & "[ID_no] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[Smut] [nvarchar] (50)  NULL ,"
		strSQL = strSQL & "[Word_replace] [nvarchar] (50)  NULL"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "Smut (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		'Create the Ban Table
		strSQL = "CREATE TABLE [dbo].[" & strDbTable & "BanList] ("
		strSQL = strSQL & "[Ban_ID] [int] IDENTITY (1, 1)  PRIMARY KEY  CLUSTERED  NOT NULL ,"
		strSQL = strSQL & "[IP] [nvarchar] (30) NULL ,"
		strSQL = strSQL & "[Email] [nvarchar] (60) NULL"
		strSQL = strSQL & ") ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Table " & strDbTable & "BanList (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If





'******************************************
'***  		 Create indexes	      *****
'******************************************


		strSQL = "CREATE  INDEX [Ban_ID] ON [dbo].[" & strDbTable & "BanList]([Ban_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  UNIQUE  INDEX [Cat_ID] ON [dbo].[" & strDbTable & "Category]([Cat_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Group_ID] ON [dbo].[" & strDbTable & "Group]([Group_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Poll_ID] ON [dbo].[" & strDbTable & "Poll]([Poll_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Author_ID] ON [dbo].[" & strDbTable & "Author]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Group_ID] ON [dbo].[" & strDbTable & "Author]([Group_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Group" & strDbTable & "Author] ON [dbo].[" & strDbTable & "Author]([Group_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  UNIQUE  INDEX [User_code] ON [dbo].[" & strDbTable & "Author]([User_code]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  UNIQUE  INDEX [Username] ON [dbo].[" & strDbTable & "Author]([Username]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Cat_ID] ON [dbo].[" & strDbTable & "Forum]([Cat_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Forum_code] ON [dbo].[" & strDbTable & "Forum]([Forum_code]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Categories" & strDbTable & "Forum] ON [dbo].[" & strDbTable & "Forum]([Cat_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Choice_ID] ON [dbo].[" & strDbTable & "PollChoice]([Choice_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Poll_ID] ON [dbo].[" & strDbTable & "PollChoice]([Poll_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Polls" & strDbTable & "PollChoice] ON [dbo].[" & strDbTable & "PollChoice]([Poll_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Address_ID] ON [dbo].[" & strDbTable & "BuddyList]([Address_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Author_ID] ON [dbo].[" & strDbTable & "BuddyList]([Buddy_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Buddy_ID] ON [dbo].[" & strDbTable & "BuddyList]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Author" & strDbTable & "BuddyList] ON [dbo].[" & strDbTable & "BuddyList]([Buddy_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Author_ID] ON [dbo].[" & strDbTable & "EmailNotify]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Forum_ID] ON [dbo].[" & strDbTable & "EmailNotify]([Forum_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Author" & strDbTable & "TopicWatch] ON [dbo].[" & strDbTable & "EmailNotify]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Toipc_ID] ON [dbo].[" & strDbTable & "EmailNotify]([Topic_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Watch_ID] ON [dbo].[" & strDbTable & "EmailNotify]([Watch_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Auhor_ID] ON [dbo].[" & strDbTable & "PMMessage]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [From_ID] ON [dbo].[" & strDbTable & "PMMessage]([From_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Message_ID] ON [dbo].[" & strDbTable & "PMMessage]([PM_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Author" & strDbTable & "PMMessage] ON [dbo].[" & strDbTable & "PMMessage]([From_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Forum_ID] ON [dbo].[" & strDbTable & "Permissions]([Forum_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Forum" & strDbTable & "Permissions] ON [dbo].[" & strDbTable & "Permissions]([Forum_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Group_ID] ON [dbo].[" & strDbTable & "Permissions]([Group_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Forum_ID] ON [dbo].[" & strDbTable & "Topic]([Forum_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Poll_ID] ON [dbo].[" & strDbTable & "Topic]([Poll_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Forum" & strDbTable & "Topic] ON [dbo].[" & strDbTable & "Topic]([Forum_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Topic_ID] ON [dbo].[" & strDbTable & "Topic]([Topic_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Message_date] ON [dbo].[" & strDbTable & "Thread]([Message_date]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Message_ID] ON [dbo].[" & strDbTable & "Thread]([Thread_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Author" & strDbTable & "Thread] ON [dbo].[" & strDbTable & "Thread]([Author_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [" & strDbTable & "Topic" & strDbTable & "Thread] ON [dbo].[" & strDbTable & "Thread]([Topic_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
		
		strSQL = "CREATE  INDEX [Topic_ID] ON [dbo].[" & strDbTable & "Thread]([Topic_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)
				
		strSQL = "CREATE  INDEX [Guest_ID] ON [dbo].[" & strDbTable & "GuestName]([Guest_ID]) ON [PRIMARY] "
		
		'Write to the database
		adoCon.Execute(strSQL)
		
 		strSQL = "CREATE  INDEX [" & strDbTable & "Thread" & strDbTable & "GuestName] ON [dbo].[" & strDbTable & "GuestName]([Thread_ID]) ON [PRIMARY]"

		'Write to the database
		adoCon.Execute(strSQL)

 		strSQL = "CREATE  INDEX [Thread_ID] ON [dbo].[" & strDbTable & "GuestName]([Thread_ID]) ON [PRIMARY]"
		
		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating one or more Indexs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If







'******************************************
'***  	Create relationships	      *****
'******************************************

		'Create relations between the tblAuthor and the tblGroup tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "Author] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Author_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Group_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & vbCrLf & "[Group_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "Author and " & strDbTable & "Group tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		'Create relations between the tblForum and the tblCategory tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "Forum] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Forum_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Cat_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Category] ("
		strSQL = strSQL & vbCrLf & "[Cat_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "Forum and " & strDbTable & "Category tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
			
		
		'Create relations between the tblPollChoice and the tblPoll tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "PollChoice] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "PollChoice_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Poll_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Poll] ("
		strSQL = strSQL & vbCrLf & "[Poll_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "PollChoice and " & strDbTable & "Poll tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		'Create relations between the tblBuddyList and the tblAuthor tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "BuddyList] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "BuddyList_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Buddy_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "BuddyList and " & strDbTable & "Author tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		'Create relations between the tblEmailNotify and the tblAuthor tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "EmailNotify] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "EmailNotify_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "EmailNotify and " & strDbTable & "Author tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		'Create relations between the PMMessage and the Author tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "PMMessage] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "PMMessage_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[From_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "PMMessage and " & strDbTable & "Author tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		'Create relations between the Permissions and the Forum tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "Permissions] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Permissions_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Forum_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Forum] ("
		strSQL = strSQL & vbCrLf & "[Forum_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "Permissions and " & strDbTable & "Forum tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		'Create relations between the tblTopic and the tblForum tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "Topic] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Topic_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Forum_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Forum] ("
		strSQL = strSQL & vbCrLf & "[Forum_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "Topic and " & strDbTable & "Forum tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		'Create relations between the GuestName and the Thread tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "GuestName] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "GuestName_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Thread_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Thread] ("
		strSQL = strSQL & vbCrLf & "[Thread_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "GuestName and " & strDbTable & "Thread tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		


		'Create relations between the tblThread, tblTopic and the tblAuthor tables
		strSQL = "ALTER TABLE [dbo].[" & strDbTable & "Thread] ADD "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Thread_FK00] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & vbCrLf & "[Author_ID]"
		strSQL = strSQL & vbCrLf & "), "
		strSQL = strSQL & vbCrLf & "CONSTRAINT [" & strDbTable & "Thread_FK01] FOREIGN KEY "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "[Topic_ID]"
		strSQL = strSQL & vbCrLf & ") REFERENCES [dbo].[" & strDbTable & "Topic] ("
		strSQL = strSQL & vbCrLf & "[Topic_ID]"
		strSQL = strSQL & vbCrLf & ")"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Relationship between the " & strDbTable & "Thread, " & strDbTable & "Topic and " & strDbTable & "Author tables (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If







'******************************************
'***  	Insert default values	      *****
'******************************************
		
		
		'Enter the default values in the UserGroup Table
		'Admin Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Admin Group', "
		strSQL = strSQL & "'-1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'5', "
		strSQL = strSQL & "'0')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
		
		'Guest Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Guest Group', "
		strSQL = strSQL & "'-1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
		
		'Moderator Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Moderator Group', "
		strSQL = strSQL & "'-1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'4', "
		strSQL = strSQL & "'0')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
		
		'Newbie Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Newbie', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'1')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
		
		'Groupie Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Groupie', "
		strSQL = strSQL & "'40', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'2', "
		strSQL = strSQL & "'0')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
		
		'Full Member Group
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Group] ("
		strSQL = strSQL & "[Name], "
		strSQL = strSQL & "[Minimum_posts], "
		strSQL = strSQL & "[Special_rank], "
		strSQL = strSQL & "[Stars], "
		strSQL = strSQL & "[Starting_group] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('Senior Member', "
		strSQL = strSQL & "'100', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'3', "
		strSQL = strSQL & "'0')"
		
		'Write to the database
		adoCon.Execute(strSQL)
		
			
		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error entering default values in the Table " & strDbTable & "Group<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		'Enter the default values in the Author Table
		'Enter the admin account into db
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & "[Group_ID], "
		strSQL = strSQL & "[Username], "
		strSQL = strSQL & "[User_code], "
		strSQL = strSQL & "[Password], "
		strSQL = strSQL & "[Salt], "
		strSQL = strSQL & "[Show_email], "
		strSQL = strSQL & "[Attach_signature], "
		strSQL = strSQL & "[Time_offset], "
		strSQL = strSQL & "[Time_offset_hours], "
		strSQL = strSQL & "[Rich_editor], "
		strSQL = strSQL & "[Date_format], "
		strSQL = strSQL & "[Active], "
		strSQL = strSQL & "[Reply_notify], "
		strSQL = strSQL & "[PM_notify], "
		strSQL = strSQL & "[No_of_posts] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('1', "
		strSQL = strSQL & "'administrator', "
		strSQL = strSQL & "'administrator2FC73499BAC5A41', "
		strSQL = strSQL & "'A85B3E67CFA695D711570FB9822C0CF82871903B', "
		strSQL = strSQL & "'72964E7', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'+', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'dd/mm/yy', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0')"

		'Write to the database
		adoCon.Execute(strSQL)

		'Enter the Guest account into db
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Author] ("
		strSQL = strSQL & "[Group_ID], "
		strSQL = strSQL & "[Username], "
		strSQL = strSQL & "[User_code], "
		strSQL = strSQL & "[Password], "
		strSQL = strSQL & "[Salt], "
		strSQL = strSQL & "[Show_email], "
		strSQL = strSQL & "[Attach_signature], "
		strSQL = strSQL & "[Time_offset], "
		strSQL = strSQL & "[Time_offset_hours], "
		strSQL = strSQL & "[Rich_editor], "
		strSQL = strSQL & "[Date_format], "
		strSQL = strSQL & "[Active], "
		strSQL = strSQL & "[Reply_notify], "
		strSQL = strSQL & "[PM_notify], "
		strSQL = strSQL & "[No_of_posts] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('2', "
		strSQL = strSQL & "'Guests', "
		strSQL = strSQL & "'Guest48CEE9Z2849AE95A6', "
		strSQL = strSQL & "'6734DDD7A6A6C9F4D34945B0C9CF9677F3221EC9', "
		strSQL = strSQL & "'E4AC', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'+', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'dd/mm/yy', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'0')"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error entering default values in the Table " & strDbTable & "Author<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		

		'Enter the default values in the Configuration Table
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "Configuration] ("
		strSQL = strSQL & "[website_name], "
		strSQL = strSQL & "[forum_name], "
		strSQL = strSQL & "[forum_path], "
		strSQL = strSQL & "[website_path], "
		strSQL = strSQL & "[forum_email_address], "
		strSQL = strSQL & "[email_notify], "
		strSQL = strSQL & "[mail_component], "
		strSQL = strSQL & "[mail_server], "
		strSQL = strSQL & "[IE_editor], "
		strSQL = strSQL & "[L_code], "
		strSQL = strSQL & "[Topics_per_page], "
		strSQL = strSQL & "[Title_image], "
		strSQL = strSQL & "[Text_link], "
		strSQL = strSQL & "[Emoticons], "
		strSQL = strSQL & "[Avatar], "
		strSQL = strSQL & "[Email_activate], "
		strSQL = strSQL & "[Email_post], "
		strSQL = strSQL & "[Hot_views], "
		strSQL = strSQL & "[Hot_replies], "
		strSQL = strSQL & "[Private_msg], "
		strSQL = strSQL & "[No_of_priavte_msg], "
		strSQL = strSQL & "[Threads_per_page], "
		strSQL = strSQL & "[Spam_seconds], "
		strSQL = strSQL & "[Spam_minutes], "
		strSQL = strSQL & "[Vote_choices], "
		strSQL = strSQL & "[Email_sys], "
		strSQL = strSQL & "[Upload_img_path], "
		strSQL = strSQL & "[Upload_img_types], "
		strSQL = strSQL & "[Upload_img_size], "
		strSQL = strSQL & "[Upload_files_path], "
		strSQL = strSQL & "[Upload_files_type], "
		strSQL = strSQL & "[Upload_files_size], "
		strSQL = strSQL & "[Upload_avatar], "
		strSQL = strSQL & "[Upload_avatar_path], "
		strSQL = strSQL & "[Upload_avatar_types], "
		strSQL = strSQL & "[Upload_avatar_size], "
		strSQL = strSQL & "[Upload_component], "
		strSQL = strSQL & "[Active_users], "
		strSQL = strSQL & "[Forums_closed], "
		strSQL = strSQL & "[Show_edit], "
		strSQL = strSQL & "[Process_time], "
		strSQL = strSQL & "[Flash], "
		strSQL = strSQL & "[Show_mod], "
		strSQL = strSQL & "[Reg_closed] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('My Website', "
		strSQL = strSQL & "'Web Wiz Forums', "
		strSQL = strSQL & "'http://www.myweb.com/forum', "
		strSQL = strSQL & "'http://www.webwizforums.com', "
		strSQL = strSQL & "'forum@myweb.coo', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'CDONTS', "
		strSQL = strSQL & "'', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'16', "
		strSQL = strSQL & "'forum_images/web_wiz_forums.gif', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'0', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'50', "
		strSQL = strSQL & "'10', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'40', "
		strSQL = strSQL & "'10', "
		strSQL = strSQL & "'20', "
		strSQL = strSQL & "'7', "
		strSQL = strSQL & "'5', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'uploads', "
		strSQL = strSQL & "'jpg;jpeg;gif;png', "
		strSQL = strSQL & "'15', "
		strSQL = strSQL & "'uploads', "
		strSQL = strSQL & "'zip;rar', "
		strSQL = strSQL & "'500', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'uploads', "
		strSQL = strSQL & "'jpg;jpeg;gif;png', "
		strSQL = strSQL & "'15', "
		strSQL = strSQL & "'AspUpload', "
		strSQL = strSQL & "'1', "
		strSQL = strSQL & "'0',"
		strSQL = strSQL & "'1',"
		strSQL = strSQL & "'1',"
		strSQL = strSQL & "'0',"
		strSQL = strSQL & "'1',"
		strSQL = strSQL & "'0')"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error entering default values in the Table " & strDbTable & "Configuration<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Enter the default values in the date time table
		strSQL = "INSERT INTO [dbo].[" & strDbTable & "DateTimeFormat] ("
		strSQL = strSQL & "[Date_format], "
		strSQL = strSQL & "[Year_format], "
		strSQL = strSQL & "[Seporator], "
		strSQL = strSQL & "[Month1], "
		strSQL = strSQL & "[Month2], "
		strSQL = strSQL & "[Month3], "
		strSQL = strSQL & "[Month4], "
		strSQL = strSQL & "[Month5], "
		strSQL = strSQL & "[Month6], "
		strSQL = strSQL & "[Month7], "
		strSQL = strSQL & "[Month8], "
		strSQL = strSQL & "[Month9], "
		strSQL = strSQL & "[Month10], "
		strSQL = strSQL & "[Month11], "
		strSQL = strSQL & "[Month12], "
		strSQL = strSQL & "[Time_format], "
		strSQL = strSQL & "[am], "
		strSQL = strSQL & "[pm] "
		strSQL = strSQL & ") "
		strSQL = strSQL & "VALUES "
		strSQL = strSQL & "('dd/mm/yy', "
		strSQL = strSQL & "'long', "
		strSQL = strSQL & "' ', "
		strSQL = strSQL & "'January', "
		strSQL = strSQL & "'February', "
		strSQL = strSQL & "'March', "
		strSQL = strSQL & "'April', "
		strSQL = strSQL & "'May', "
		strSQL = strSQL & "'June', "
		strSQL = strSQL & "'July', "
		strSQL = strSQL & "'August', "
		strSQL = strSQL & "'September', "
		strSQL = strSQL & "'October', "
		strSQL = strSQL & "'November', "
		strSQL = strSQL & "'December', "
		strSQL = strSQL & "'12', "
		strSQL = strSQL & "'am', "
		strSQL = strSQL & "'pm')"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error entering default values in the Table " & strDbTable & "DateTimeFormat<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		'Enter the default values in the smut table
		For intBadWordLoopCounter = 1 to 14

			'Write the SQL
			strSQL = "INSERT INTO [dbo].[" & strDbTable & "Smut] ([Smut], [Word_replace]) "
			strSQL = strSQL & "VALUES ("

			Select Case intBadWordLoopCounter
				Case 1
					strSQL = strSQL & "'cunt', 'c**t'"
				Case 2
					strSQL = strSQL & "'cunting', 'c**ting'"
				Case 3
					strSQL = strSQL & "'fuck', 'f**k'"
				Case 4
					strSQL = strSQL & "'fucker', 'f**ker'"
				Case 5
					strSQL = strSQL & "'fucking', 'f**king'"
				Case 6
					strSQL = strSQL & "'fuck-off', 'f**k-off'"
				Case 7
					strSQL = strSQL & "'fuckoff', 'f**ker'"
				Case 8
					strSQL = strSQL & "'motherfucker', 'motherf**k'"
				Case 9
					strSQL = strSQL & "'shit', 'sh*t'"
				Case 10
					strSQL = strSQL & "'shiting', 'sh*ting'"
				Case 11
					strSQL = strSQL & "'slag', 'sl*g'"
				Case 12
					strSQL = strSQL & "'tosser', 't**ser'"
				Case 13
					strSQL = strSQL & "'wanker', 'w**ker'"
				Case 14
					strSQL = strSQL & "'wanking', 'w**king'"
			End Select

			strSQL = strSQL & ")"

			'Write to database
			adoCon.Execute(strSQL)
		Next

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error entering default values in the Table " & strDbTable & "Smut<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If




'******************************************
'***  		Stored Procedures      *****
'******************************************

		'Create relations between the " & strDbTable & "Topic, " & strDbTable & "Thread and " & strDbTable & "Author tables
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "SelectConfiguration] AS "
		strSQL = strSQL & "SELECT " & strDbTable & "Configuration.* From " & strDbTable & "Configuration;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "SelectConfiguration (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		'Create relations between the " & strDbTable & "Topic, " & strDbTable & "Thread and " & strDbTable & "Author tables
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ChkUserID]"
		strSQL = strSQL & vbCrLf & "( "
		strSQL = strSQL & vbCrLf & "@strUserID VarChar(50) "
		strSQL = strSQL & vbCrLf & ") "
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Group_ID, " & strDbTable & "Author.Active, " & strDbTable & "Author.Signature, " & strDbTable & "Author.Author_email, " & strDbTable & "Author.Date_format, " & strDbTable & "Author.Time_offset, " & strDbTable & "Author.Time_offset_hours, " & strDbTable & "Author.Reply_notify, " & strDbTable & "Author.Attach_signature, " & strDbTable & "Author.Rich_editor, " & strDbTable & "Author.Last_visit "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Author "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Author.User_code = @strUserID;"


		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ChkUserID (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If

		'Create relations between the " & strDbTable & "Topic, " & strDbTable & "Thread and " & strDbTable & "Author tables
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "CategoryAll] AS "
		strSQL = strSQL & "SELECT " & strDbTable & "Category.Cat_name, " & strDbTable & "Category.Cat_ID FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "CategoryAll (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumsAllWhereCatIs] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intCatID smallint"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = @intCatID ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumsAllWhereCatIs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumTopicCount] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT Count(" & strDbTable & "Topic.Forum_ID) AS Topic_Count From " & strDbTable & "Topic WHERE " & strDbTable & "Topic.Forum_ID = @intForumID"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumTopicCount (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "LastForumPostEntry] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT Top 1 " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID, " & strDbTable & "Thread.Topic_ID, " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Message_date "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Author, " & strDbTable & "Thread  "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Thread.Topic_ID IN "
		strSQL = strSQL & vbCrLf & "     (SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
		strSQL = strSQL & vbCrLf & "     FROM " & strDbTable & "Topic "
		strSQL = strSQL & vbCrLf & "     WHERE " & strDbTable & "Topic.Forum_ID = @intForumID "
		strSQL = strSQL & vbCrLf & "     ORDER BY " & strDbTable & "Topic.Last_entry_date DESC) "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Thread.Message_date DESC;"



		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "LastForumPostEntry (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If

		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ModeratorGroup] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Group.Group_ID, " & strDbTable & "Group.Name "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Group, " & strDbTable & "Permissions "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Group.Group_ID = " & strDbTable & "Permissions.Group_ID AND " & strDbTable & "Permissions.Moderate = 1 AND " & strDbTable & "Permissions.Forum_ID = @intForumID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ModeratorGroup (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "Moderators] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Permissions, " & strDbTable & "Author "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Permissions.Author_ID AND " & strDbTable & "Permissions.Moderate = 1 AND " & strDbTable & "Permissions.Forum_ID = @intForumID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ModeratorGroup (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumPermissions] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int, "
		strSQL = strSQL & vbCrLf & "@intGroupID int, "
		strSQL = strSQL & vbCrLf & "@intAuthorID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Permissions.* "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Permissions "
		strSQL = strSQL & vbCrLf & "WHERE  (" & strDbTable & "Permissions.Group_ID = @intGroupID OR " & strDbTable & "Permissions.Author_ID = @intAuthorID) AND " & strDbTable & "Permissions.Forum_ID = @intForumID "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Permissions.Author_ID DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumPermissions (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumThreadCount] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT Count(" & strDbTable & "Thread.Thread_ID) AS Thread_Count "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Topic INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID "
		strSQL = strSQL & vbCrLf & "GROUP BY " & strDbTable & "Topic.Forum_ID "
		strSQL = strSQL & vbCrLf & "HAVING (((" & strDbTable & "Topic.Forum_ID)=@intForumID));"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumThreadCount (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "UpdateLasVisit] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngUserID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "UPDATE " & strDbTable & "Author SET " & strDbTable & "Author.Last_visit=GetDate() WHERE " & strDbTable & "Author.Author_ID=@lngUserID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "UpdateLasVisit (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "AuthorDesc] AS "
		strSQL = strSQL & "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID FROM " & strDbTable & "Author ORDER BY " & strDbTable & "Author.Author_ID DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "AuthorDesc (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "TimeAndDateSettings] AS "
		strSQL = strSQL & "SELECT " & strDbTable & "DateTimeFormat.* FROM " & strDbTable & "DateTimeFormat;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "TimeAndDateSettings (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "CountOfPMs] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngLoggedInUserID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & "  AS "
		strSQL = strSQL & vbCrLf & "SELECT Count(" & strDbTable & "PMMessage.PM_ID) AS CountOfPM FROM " & strDbTable & "PMMessage "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "PMMessage.Read_Post = 0 AND " & strDbTable & "PMMessage.Author_ID = @lngLoggedInUserID "

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "CountOfPMs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "DateOfLastUnReadPM] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngLoggedInUserID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & "  AS "
		strSQL = strSQL & vbCrLf & "SELECT TOP 1 " & strDbTable & "PMMessage.PM_ID FROM " & strDbTable & "PMMessage "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "PMMessage.Read_Post = 0 AND " & strDbTable & "PMMessage.Author_ID =  @lngLoggedInUserID "
		strSQL = strSQL & vbCrLf & "ORDER BY PM_Message_Date DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "DateOfLastUnReadPM (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "LastAndFirstThreadAuthor] "
		strSQL = strSQL & vbCrLf & "( "
		strSQL = strSQL & vbCrLf & "@lngTopicID int "
		strSQL = strSQL & vbCrLf & ") "
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Author_ID, " & strDbTable & "Thread.Message, " & strDbTable & "Thread.Message_date, " & strDbTable & "Author.Username "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Author, " & strDbTable & "Thread "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Thread.Topic_ID = @lngTopicID "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Thread.Message_date ASC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "LastAndFirstThreadAuthor (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "UpdateViewPostCount] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngNumberOfViews int, "
		strSQL = strSQL & vbCrLf & "@lngTopicID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "UPDATE " & strDbTable & "Topic SET "
		strSQL = strSQL & vbCrLf & "" & strDbTable & "Topic.No_of_views=@lngNumberOfViews "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Topic.Topic_ID=@lngTopicID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "UpdateViewPostCount (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumsAllWhereForumIs] "
		strSQL = strSQL & vbCrLf & "( "
		strSQL = strSQL & vbCrLf & "@intForumID int "
		strSQL = strSQL & vbCrLf & ") "
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = @intForumID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumsAllWhereForumIs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "TopicDetialsInTheLastXX] "
		strSQL = strSQL & vbCrLf & "( "
		strSQL = strSQL & vbCrLf & "@intForumID int, "
		strSQL = strSQL & vbCrLf & "@intShowTopicsFrom int "
		strSQL = strSQL & vbCrLf & ") "
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Moved_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.Locked, " & strDbTable & "Topic.Priority FROM " & strDbTable & "Topic "
		strSQL = strSQL & vbCrLf & "WHERE (((" & strDbTable & "Topic.Forum_ID = @intForumID) OR (" & strDbTable & "Topic.Moved_ID = @intForumID)) AND ((" & strDbTable & "Topic.Last_entry_date > GetDate() - @intShowTopicsFrom) OR (" & strDbTable & "Topic.Priority > 0))) OR  (" & strDbTable & "Topic.Priority = 3) "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Topic.Priority DESC, " & strDbTable & "Topic.Last_entry_date DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "TopicDetialsInTheLastXX (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "TopicDetialsAll] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@intForumID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Moved_ID, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.Locked, " & strDbTable & "Topic.Priority FROM " & strDbTable & "Topic "
		strSQL = strSQL & vbCrLf & "WHERE (" & strDbTable & "Topic.Forum_ID = @intForumID) OR  (" & strDbTable & "Topic.Priority = 3) OR (" & strDbTable & "Topic.Moved_ID = @intForumID) "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Topic.Priority DESC, " & strDbTable & "Topic.Last_entry_date DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "TopicDetialsAll (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ThreadDetails] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngTopicID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT  " & strDbTable & "Topic.*, " & strDbTable & "Thread.*, " & strDbTable & "Author.Username, " & strDbTable & "Author.Homepage, " & strDbTable & "Author.Location, " & strDbTable & "Author.No_of_posts, " & strDbTable & "Author.Join_date, " & strDbTable & "Author.Signature, " & strDbTable & "Author.Active, " & strDbTable & "Author.Avatar, " & strDbTable & "Author.Avatar_title, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Topic, " & strDbTable & "Thread, " & strDbTable & "Author, " & strDbTable & "Group "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID AND " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Topic.Topic_ID = @lngTopicID "
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Thread.Message_date ASC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ThreadDetails (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "PollDetails] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngPollID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT  " & strDbTable & "Poll.*, " & strDbTable & "PollChoice.* "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Poll INNER JOIN " & strDbTable & "PollChoice ON " & strDbTable & "Poll.Poll_ID = " & strDbTable & "PollChoice.Poll_ID "
		strSQL = strSQL & vbCrLf & "WHERE (((" & strDbTable & "Poll.Poll_ID)=@lngPollID));"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "PollDetails (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "AuthorLastPostDate] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngUserID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & "As "
		strSQL = strSQL & vbCrLf & "SELECT TOP 1 " & strDbTable & "Thread.Message_date, " & strDbTable & "Thread.Author_ID FROM " & strDbTable & "Thread  WHERE " & strDbTable & "Thread.Author_ID = @lngUserID ORDER BY " & strDbTable & "Thread.Message_date DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "AuthorLastPostDate (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ActiveTopics] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@AuthorID int, "
		strSQL = strSQL & vbCrLf & "@GroupID int, "
		strSQL = strSQL & vbCrLf & "@GroupPerm int, "
		strSQL = strSQL & vbCrLf & "@dblActiveFrom datetime"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum.Password, " & strDbTable & "Forum.Forum_code, " & strDbTable & "Topic.* "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "Category, " & strDbTable & "Forum, " & strDbTable & "Topic "
		strSQL = strSQL & vbCrLf & "WHERE ((" & strDbTable & "Category.Cat_ID = " & strDbTable & "Forum.Cat_ID AND " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID) AND (" & strDbTable & "Topic.Last_entry_date > GetDate() - @dblActiveFrom)) "
		strSQL = strSQL & vbCrLf & "AND (" & strDbTable & "Forum.[Read] <= @GroupPerm OR (" & strDbTable & "Topic.Forum_ID IN ("
		strSQL = strSQL & vbCrLf & "	SELECT " & strDbTable & "Permissions.Forum_ID "
		strSQL = strSQL & vbCrLf & "	FROM " & strDbTable & "Permissions "
		strSQL = strSQL & vbCrLf & "	WHERE " & strDbTable & "Permissions.Author_ID = @AuthorID OR " & strDbTable & "Permissions.Group_ID = @GroupID AND " & strDbTable & "Permissions.[Read]=1))"
		strSQL = strSQL & vbCrLf & "	)"
		strSQL = strSQL & vbCrLf & "ORDER BY " & strDbTable & "Category.Cat_order ASC, " & strDbTable & "Forum.Forum_Order ASC, " & strDbTable & "Topic.Last_entry_date DESC;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ActiveToipcs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If


		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "TopicEmailNotify] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngAuthorID int, "
		strSQL = strSQL & vbCrLf & "@lngTopicID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "EmailNotify.* "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "EmailNotify "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "EmailNotify.Author_ID = @lngAuthorID AND " & strDbTable & "EmailNotify.Topic_ID = @lngTopicID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "TopicEmailNotify (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "ForumEmailNotify] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngAuthorID int, "
		strSQL = strSQL & vbCrLf & "@intForumID int "
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & " AS "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "EmailNotify.* "
		strSQL = strSQL & vbCrLf & "FROM " & strDbTable & "EmailNotify "
		strSQL = strSQL & vbCrLf & "WHERE " & strDbTable & "EmailNotify.Author_ID = @lngAuthorID AND " & strDbTable & "EmailNotify.Forum_ID = @intForumID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "ForumEmailNotify (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "BannedIPs] AS "
		strSQL = strSQL & "SELECT " & strDbTable & "BanList.IP FROM " & strDbTable & "BanList WHERE " & strDbTable & "BanList.IP Is Not Null;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "BannedIPs (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "AuthorDetails] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngUserID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & "As "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "Author.* FROM " & strDbTable & "Author WHERE " & strDbTable & "Author.Author_ID = @lngUserID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "AuthorDetails (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If



		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "GuestPoster] "
		strSQL = strSQL & vbCrLf & "("
		strSQL = strSQL & vbCrLf & "@lngThreadID int"
		strSQL = strSQL & vbCrLf & ")"
		strSQL = strSQL & vbCrLf & "As "
		strSQL = strSQL & vbCrLf & "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = @lngThreadID;"

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "AuthorLastPostDate (it may already exsist)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If
		
		
		
		strSQL = "CREATE PROCEDURE [dbo].[" & strDbProc & "DBinfo] "
		strSQL = strSQL & "As "
		strSQL = strSQL & vbCrLf & "Declare @low int"
		strSQL = strSQL & vbCrLf & "Declare @dbsize dec(10,2)"
		strSQL = strSQL & vbCrLf & "Declare @dbpath nvarchar(100)"
		strSQL = strSQL & vbCrLf & "Declare @logpath nvarchar(100)"
		strSQL = strSQL & vbCrLf & "Declare @dbfilesize dec(10,2)"
		strSQL = strSQL & vbCrLf & "Declare @logfilesize dec(10,2)"
		strSQL = strSQL & vbCrLf & "Declare @maxdbfilesize dec(10,2)"
		strSQL = strSQL & vbCrLf & "Declare @maxlogfilesize dec(10,2)"
		strSQL = strSQL & vbCrLf & " "
		strSQL = strSQL & vbCrLf & "--Minimum Database Size"
		strSQL = strSQL & vbCrLf & "select @low = low from master.dbo.spt_values"
		strSQL = strSQL & vbCrLf & "			where type = N'E' and number = 1"
		strSQL = strSQL & vbCrLf & "--Calculation of current Database Size in MB"
		strSQL = strSQL & vbCrLf & "select @dbsize = (convert(dec(15),sum(size)) *  @low  / 1048576)"
		strSQL = strSQL & vbCrLf & "from [sysfiles]"
		strSQL = strSQL & vbCrLf & "--Actual File Size of Log and Data File"
		strSQL = strSQL & vbCrLf & "Select TOP 1 @dbpath = [filename] from [sysfiles] where groupid = 1"
		strSQL = strSQL & vbCrLf & "Select TOP 1 @logpath = [filename] from [sysfiles] where groupid = 0"
		strSQL = strSQL & vbCrLf & "Select @dbfilesize =  convert(dec(10,2),sum([size]))/128 from [sysfiles] where [filename] = @dbpath     --in MB"
		strSQL = strSQL & vbCrLf & "Select @logfilesize = convert(dec(10,2),sum([size]))/128 from [sysfiles] where [filename] = @logpath   --in MB"
		strSQL = strSQL & vbCrLf & "Select TOP 1 @maxdbfilesize = convert(dec(10,2),[maxsize])/128 from [sysfiles] where [filename] = @dbpath"
		strSQL = strSQL & vbCrLf & "Select TOP 1 @maxlogfilesize = convert(dec(10,2),[maxsize])/128 from [sysfiles] where [filename] = @logpath"
		strSQL = strSQL & vbCrLf & "If @maxdbfilesize = (-.01) Set @maxdbfilesize = -1"
		strSQL = strSQL & vbCrLf & "If @maxlogfilesize = (-.01) Set @maxlogfilesize = -1"
		strSQL = strSQL & vbCrLf & " "
		strSQL = strSQL & vbCrLf & "---Creating Output Table"
		strSQL = strSQL & vbCrLf & "select 	@dbsize Databasesize, @dbpath DataLocation , @logpath LogLocation, "
		strSQL = strSQL & vbCrLf & "	@dbfilesize DatabaseFileSize, @logfilesize Logfilesize,"
		strSQL = strSQL & vbCrLf & "	@maxdbfilesize MaxDBSize, @maxlogfilesize MazLogSize,"
		strSQL = strSQL & vbCrLf & "	ServerProperty('edition') Edition, "
		strSQL = strSQL & vbCrLf & "	CASE ServerProperty('IsCluster') "
		strSQL = strSQL & vbCrLf & "	 WHEN 0 THEN 'No Cluster'"
		strSQL = strSQL & vbCrLf & "	 WHEN 1 THEN 'Cluster'"
		strSQL = strSQL & vbCrLf & " 	 ELSE  'No Cluster/Unknown' "
		strSQL = strSQL & vbCrLf & "	END Cluster,"
		strSQL = strSQL & vbCrLf & "	CASE ServerProperty('License_Type')"
		strSQL = strSQL & vbCrLf & "	 WHEN 'PER_SEAT' THEN 'Seat Licensing (' + Convert(nvarchar(5),ServerProperty('NumLicenses')) + ')'"
		strSQL = strSQL & vbCrLf & "	 WHEN 'Per_Processor' THEN 'Processor Licensing (' + Convert(nvarchar(5),ServerProperty('NumLicenses')) + ')'"
		strSQL = strSQL & vbCrLf & "	 ELSE 'Licensing Disabled / Unknown'"
		strSQL = strSQL & vbCrLf & "	END Licensing,"
		strSQL = strSQL & vbCrLf & "	ServerProperty('ProductLevel') PLevel"
	

		'Write to the database
		adoCon.Execute(strSQL)

		'If an error has occured write an error to the page
		If Err.Number <> 0 Then
			Response.Write("<br />Error Creating the Stored Procedure " & strDbProc & "DBinfo (it may already exsist). If you are using MS SQL Server 7 ignore this error (You will not be able to view SQL Server stats in the admin area)<br />")

			'Reset error object
			Err.Number = 0

			'Set the error boolean to True
			blnErrorOccured = True
		End If








		'Display a message to say the database is created
		If blnErrorOccured = True Then
			Response.Write("<br /><b>SQL Server database is set up, but with Error!</b>")
		Else
			Response.Write("<br /><center><font size=""4""><b>Congratulations, the SQL Server Forum database is set up and Ready to Go :)</b></font></center>")
		End If
	End If

	'Reset Server Variables
	Set adoCon = Nothing
	Set adoCon = Nothing
End If
%>
      </font></td>
  </tr>
</table>
<br />
</body>
</html>