﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="search" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form defaultbutton="btnSearch" method="post" id="Search" runat="server">
    <div>
        <asp:TextBox ID="txtSearch" runat="server" Width="250px"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" AccessKey="S" 
            onclick="btnSearch_Click" PostBackUrl="~/search.aspx" Text="Søk" />
    </div>
    </form>
</body>
</html>
