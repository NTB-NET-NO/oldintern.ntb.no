﻿Imports System.IO
Imports System.Web.Configuration
Imports System.Web.UI.WebControls

Partial Class showpicture
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoServerCaching()
        Dim ts As New TimeSpan(0, 0, 0)
        Response.Cache.SetMaxAge(ts)
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim strImage As String = ""
        Dim htmlimg As New Image()
        htmlimg.Width = 775
        strImage = Request.QueryString("file")
        htmlimg.ImageUrl = "dokumenter/Ansattbilder/" & strImage


        form1.Controls.Add(htmlimg)
    End Sub
End Class
