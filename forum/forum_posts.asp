<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="functions/functions_format_post.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"



'******************************************
'***     Search Word Highlighter	***
'******************************************

'Function to highlight search words if coming from search page
Private Function searchHighlighter(ByVal strMessage, ByVal sarySearchWord)

	Dim intHighlightLoopCounter	'Loop counter to loop through words and hightlight them
	Dim strTempMessage		'Temporary message store
	Dim lngMessagePosition		'Holds the message position
	Dim intHTMLTagLength		'Holds the length of the HTML tags
	Dim intSearchWordLength		'Holds the length of teh search word
	Dim blnTempUpdate		'Set to true if the temp message variable is updated


	'Loop through each character in the post message
	For lngMessagePosition = 1 to Len(strMessage)

		'Initilise for each pass
		blnTempUpdate = False

		'If an HTML tag is found then move to the end of it so that no words in the HTML are highlighted
		If Mid(strMessage, lngMessagePosition, 1) = "<" Then

			'Get the length of the HTML tag
			intHTMLTagLength = (InStr(lngMessagePosition, strMessage, ">", 1) - lngMessagePosition)

			'Place the HTML tag back into the tempary message store
			strTempMessage = strTempMessage & Mid(strMessage, lngMessagePosition, intHTMLTagLength)

			'Add the length of the HTML tag to the post message position variable
			lngMessagePosition = lngMessagePosition + intHTMLTagLength
		End If

		'Loop through the search words to see if they are in the message post
		For intHighlightLoopCounter = 0 to UBound(sarySearchWord)

			'If there is a search word in the array position check it
			If sarySearchWord(intHighlightLoopCounter) <> "" Then

				'Get the length of the search word
				intSearchWordLength = Len(sarySearchWord(intHighlightLoopCounter))

				'If the next XX characters are the same as the search word then highlight them
				If LCase(Mid(strMessage, lngMessagePosition, intSearchWordLength)) = LCase(sarySearchWord(intHighlightLoopCounter)) Then

					'Highlight the search word
					strTempMessage = strTempMessage & "<span class=""highlight"">" & Mid(strMessage, lngMessagePosition, intSearchWordLength) & "</span>"

					'Add the length of the replaced search word to the post message position variable
					lngMessagePosition = lngMessagePosition + intSearchWordLength - 1

					'Set the changed boolean to true
					blnTempUpdate = True
				End If
			End If
		Next

		'If a search word is not highlighted then add the character from the post message being checked to the temp variable
		If blnTempUpdate = False Then
			strTempMessage = strTempMessage & Mid(strMessage, lngMessagePosition, 1)
		End If
	Next

	'Return the function
	searchHighlighter = strTempMessage
End Function





'Dimension variables
Dim rsTopic 			'Holds the Database Recordset Variable for the topic details
Dim rsPost			'Holds the database recordset variable for the thread
Dim intForumID			'Holds the forum ID number
Dim strForumName		'Holds the forum name
Dim lngTopicID			'Holds the topic number
Dim strSubject			'Holds the topic subject
Dim strUsername 		'Holds the Username of the thread
Dim lngUserID			'Holds the ID number of the user
Dim dtmTopicDate		'Holds the date the thread was made
Dim strMessage			'Holds the message body of the thread
Dim lngMessageID		'Holds the message ID number
Dim strAuthorHomepage		'Holds the homepage of the Username if it is given
Dim strAuthorLocation		'Holds the location of the user if given
Dim strAuthorAvatar		'Holds the authors avatar
Dim strAuthorSignature		'Holds the authors signature
Dim lngAuthorNumOfPosts		'Holds the number of posts the user has made to the forum
Dim dtmAuthorRegistration	'Holds the registration date of the user
Dim lngNumberOfViews		'Holds the number of times the topic has been viewed to save back to the database
Dim intStatus			'Holds the users interger status
Dim strStatus			'Holds the users status
Dim strMode			'Holds the mode of the page that is being passed
Dim intTopicPageNumber		'Holds the topic page position to link back to
Dim blnNoThread			'Set to true if there is no thread to view
Dim blnIsModerator		'Set to true if the user who posted the message is a forum moderator
Dim blnForumLocked		'Set to true if the forum is locked
Dim blnTopicLocked		'set to true if the topic is locked
Dim intThreadNo			'Holds the number of threads in the topic
Dim intPriority			'Holds the priority level of the topic
Dim strPostPage 		'Holds the page the form is posted to
Dim intRecordPositionPageNum	'Holds the recorset page number to show the Threads for
Dim intTotalNumOfPages		'Holds the number of pages
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intTopicPageLoopCounter	'Loop counter for other thread page link
Dim intTotalNumOfThreads	'Holds the total number of therads in this topic
Dim strAuthorIP			'Holds the authors IP
Dim strSearchKeywords		'Holds the keywords to search for
Dim sarySearchWord		'Array to hold the search words
Dim strGroupName		'Holds the authors group name
Dim intRankStars		'Holds the number of stars for the group
Dim strRankCustomStars		'Holds custom stars for the user group
Dim lngPollID			'Holds the poll ID
Dim blnPollNoReply		'Set to true if users can't reply to a poll
Dim blnBannedIP			'Set to true if the user is using a banned IP
Dim dtmLastEntryDate		'Holds the date of the last post entry to the topic
Dim intIndexPosition		'Holds the idex poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns
Dim strMemberTitle		'Holds the members title
Dim blnTopicWatched		'Set to true if this topic is being watched
Dim blnIsUserOnline		'Set to true if the user is online


'Initialise variables
strMode = "reply"
lngMessageID = 0
intForumID = 0
lngTopicID = 0
intThreadNo = 0
blnNoThread = False
blnIsModerator = False
blnPollNoReply = False
blnBannedIP = False
blnTopicWatched = False


'See if the user is using a banned IP address
If bannedIP() Then
	'If the user is using a banned IP then set the banned IP variable to true and active member variable to false
	blnBannedIP = True
End If


'If this is the first time the page is displayed then the Forum Thread record position is set to page 1
If Request.QueryString("TPN") = "" Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the Forum Thread record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("TPN"))
End If


'Read in the Topic ID for the topic to display and page number
lngTopicID = CLng(Request.QueryString("TID"))
intTopicPageNumber = CInt(Request.QueryString("PN"))


'If there is no Topic ID then redirect the user to the main forum page
If lngTopicID = 0 Then

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect "default.asp"
End If




'Read in the keywords if comming from a search
strSearchKeywords = Trim(Mid(Request.QueryString("KW"), 1, 35))


'Split up the keywords to be searched
sarySearchWord = Split(Trim(strSearchKeywords), " ")




'Get the posts from the database
'Create a record set object to the Threads held in the database
Set rsPost = Server.CreateObject("ADODB.Recordset")

'Initalise the strSQL variable with an SQL statement to query the database get the thread details
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ThreadDetails @lngTopicID = " & lngTopicID
Else
	strSQL = "SELECT  " & strDbTable & "Topic.*, " & strDbTable & "Thread.*, " & strDbTable & "Author.Username, " & strDbTable & "Author.Homepage, " & strDbTable & "Author.Location, " & strDbTable & "Author.No_of_posts, " & strDbTable & "Author.Join_date, " & strDbTable & "Author.Signature, " & strDbTable & "Author.Active, " & strDbTable & "Author.Avatar, " & strDbTable & "Author.Avatar_title, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
	strSQL = strSQL & "FROM " & strDbTable & "Topic, " & strDbTable & "Thread, " & strDbTable & "Author, " & strDbTable & "Group "
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID AND " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Topic.Topic_ID = " & lngTopicID & " "
	strSQL = strSQL & "ORDER BY " & strDbTable & "Thread.Message_date ASC;"
End If

'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
rsPost.CursorType = 1

'Query the database
rsPost.Open strSQL, adoCon

'Set the number of records to display on each page
rsPost.PageSize = intThreadsPerPage


'If there is no topic in the database then display the appropraite mesasage
If rsPost.EOF Then

	'If there are no thread's to display then display the appropriate error message
	strSubject = strTxtNoThreads
	blnNoThread = True


'Else there are records returned by the database
Else
	'Count the number of pages there are in the recordset calculated by the PageSize attribute set by admin
	intTotalNumOfPages = rsPost.PageCount

	'Get the total amount of threads in the topic
	intTotalNumOfThreads = rsPost.RecordCount

	'Get the record poistion to display from
	'If the page number to show from is higher than the last page number then the last page number is the highers page
	If (intRecordPositionPageNum > intTotalNumOfPages) OR (Request.QueryString("get") = "last") Then
		
		'Set the page number to show from
		rsPost.AbsolutePage = intTotalNumOfPages
		
		'Set the page position number to the highest page number
		intRecordPositionPageNum = intTotalNumOfPages
	
	'Else the page number to show from is the requested page number
	Else
		rsPost.AbsolutePage = intRecordPositionPageNum
	End If


	'Read in the number of views for the page form the database
	lngNumberOfViews = CLng(rsPost("No_of_views"))

	'Add 1 to the number of views the Topic has had
	lngNumberOfViews = lngNumberOfViews + 1

	'Write the number of times the Topic has been viewed back to the database
	'Initalise the strSQL variable with the SQL string
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "UpdateViewPostCount @lngNumberOfViews = " & lngNumberOfViews & ", @lngTopicID= " & lngTopicID
	Else
		strSQL = "UPDATE " & strDbTable & "Topic SET "
		strSQL = strSQL & "" & strDbTable & "Topic.No_of_views=" & lngNumberOfViews
		strSQL = strSQL & " WHERE (((" & strDbTable & "Topic.Topic_ID)=" & lngTopicID & "));"
	End If

	'Write to the database
	adoCon.Execute(strSQL)


	'Read in the thread subject forum ID and where the topic is locked
	intForumID = Cint(rsPost("Forum_ID"))
	lngPollID = CLng(rsPost("Poll_ID"))
	strSubject = rsPost("Subject")
	blnTopicLocked = CBool(rsPost("Locked"))
	intPriority = CInt(rsPost("Priority"))
	dtmLastEntryDate = CDate(rsPost("Last_entry_date"))

	'If this is a unread new post set a cookie so that it is not shown as a new post
	If(CDate(Session("dtmLastVisit")) < dtmLastEntryDate) AND (Request.Cookies("RT" & lngTopicID) = "") Then

		Response.Cookies("RT")("TID" & lngTopicID) = 1
	End If

End If



'If this is a top priority post across all forums then read in teh forum ID form the querystring and ingnore the real topic forum ID
If Request.QueryString("PR") = "3" Then intForumID = Cint(Request.QueryString("FID"))


'Create a recordset to get the forum details
Set rsTopic = Server.CreateObject("ADODB.Recordset")

'Read in the forum name and forum permssions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Forum_ID = " & intForumID & ";"
End If

'Query the database
rsTopic.Open strSQL, adoCon


'If there is a record returned by the recordset then check to see if you need a password to enter it
If NOT rsTopic.EOF Then

	'Read in forum details from the database
	strForumName = rsTopic("Forum_name")

	'Read in wether the forum is locked or not
	blnForumLocked = CBool(rsTopic("Locked"))

	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsTopic("Read")), CInt(rsTopic("Post")), CInt(rsTopic("Reply_posts")), CInt(rsTopic("Edit_posts")), CInt(rsTopic("Delete_posts")), 0, CInt(rsTopic("Poll_create")), CInt(rsTopic("Vote")), CInt(rsTopic("Attachments")), CInt(rsTopic("Image_upload")))

	'If the user has no read writes then kick them
	If blnRead = False Then

		'Reset Server Objects
		rsPost.Close
		Set rsPost = Nothing
		rsTopic.Close
		Set rsTopic = Nothing
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing


		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "insufficient_permission.asp"
	End If

	'If the forum requires a password and a logged in forum code is not found on the users machine then send them to a login page
	If rsTopic("Password") <> "" AND Request.Cookies(strCookieName)("Forum" & intForumID) <> rsTopic("Forum_code") Then

		'Reset Server Objects
		rsPost.Close
		Set rsPost = Nothing
		rsTopic.Close
		Set rsTopic = Nothing
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing

		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "forum_password_form.asp?RP=PT&FID=" & intForumID & "&TID=" & lngTopicID
	End If
End If

'Write the HTML head of the page
Response.Write("<html>" & _
vbCrLf & "<head>" & _
vbCrLf & "<title>" & strMainForumName & ": " & strSubject & "</title>" & _
vbCrLf & "<meta name=""copyright"" content=""Copyright (C) 2001-2004 Bruce Corkhill"" />" & _
vbCrLf & vbCrLf & "<!-- Web Wiz Forums ver. " & strVersion & " is written and produced by Bruce Corkhill �2001-2004" & _
vbCrLf & "     	If you want your own FREE Forum then goto http://www.webwizforums.com -->" & _
vbCrLf & vbCrLf & "<script language=""JavaScript"">" & _
vbCrLf & "function CheckForm() {" & _
vbCrLf & "	var errorMsg = '';")

		
'If Gecko Madis API (RTE) need to strip default input from the API
If RTEenabled = "Gecko" Then Response.Write(vbCrLf & "	//For Gecko Madis API (RTE)" & vbCrLf & "	if (document.frmAddMessage.message.value.indexOf('<br>') > -1 && document.frmAddMessage.message.value.length == 8) document.frmAddMessage.message.value = '';" & vbCrLf)


'If this is a guest posting check that they have entered their name
If blnPost And lngLoggedInUserID = 2 Then
	
	Response.Write(vbCrLf & "	//Check for a name" & _
	vbCrLf & "	if (document.frmAddMessage.Gname.value==''){" & _
	vbCrLf & "		errorMsg += '\n\t" & strTxtNoNameError & "';" & _
	vbCrLf & "	}")

End If

	
Response.Write(vbCrLf & vbCrLf & "	//Check for message" & _
vbCrLf & "	if (document.frmAddMessage.message.value==''){" & _
vbCrLf & "		errorMsg += '\n\t" & strTxtNoMessageError & "';" & _
vbCrLf & "	}" & _
vbCrLf & "" & _
vbCrLf & "	//If there is aproblem with the form then display an error" & _
vbCrLf & "	if (errorMsg != ''){" & _
vbCrLf & "		msg = '" & strTxtErrorDisplayLine & "\n\n';" & _
vbCrLf & "		msg += '" & strTxtErrorDisplayLine1 & "\n';" & _
vbCrLf & "		msg += '" & strTxtErrorDisplayLine2 & "\n';" & _
vbCrLf & "		msg += '" & strTxtErrorDisplayLine & "\n\n';" & _
vbCrLf & "		msg += '" & strTxtErrorDisplayLine3 & "\n';" & _
vbCrLf & "" & _
vbCrLf & "		errorMsg += alert(msg + errorMsg + '\n\n');" & _
vbCrLf & "		return false;" & _
vbCrLf & "	}" & _
vbCrLf & "" & _
vbCrLf & "	//Reset the submition action" & _
vbCrLf & "	document.frmAddMessage.action = 'post_message.asp?PN=" & CInt(Request.QueryString("PN")) & "'" & _
vbCrLf & "	document.frmAddMessage.target = '_self';")
If RTEenabled() <> "false" AND blnRTEEditor AND blnWYSIWYGEditor Then Response.Write(vbCrLf & "	document.frmAddMessage.Submit.disabled=true;")		
Response.Write(vbCrLf & "	return true;" & _	
vbCrLf & "}" & _
vbCrLf & "</script>")


%><!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" --><%


Response.Write(vbCrLf & "<a name=""top""></a>" & _
vbCrLf & "<table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""0"" cellpadding=""3"" align=""center""><tr>" & _
vbCrLf & "  <td align=""left"" class=""heading"">")

'Display the forum name
Response.Write(strForumName)

'If the forum is locked show a locked pad lock icon
If blnForumLocked = True Then
	Response.Write ("  <span class=""smText"">(<img src=""" & strImagePath & "forum_locked_icon.gif"" align=""baseline"" alt=""" & strTxtForumLocked & """> " & strTxtForumLocked & ")</span>")
End If 

Response.Write("</td>" & _
vbCrLf & "</tr>" & _
vbCrLf & " <tr>" & _
vbCrLf & "  <td align=""left"" width=""95%"" class=""bold""><img src=""" & strImagePath & "open_folder_icon.gif"" border=""0"" align=""absmiddle"">&nbsp;<a href=""default.asp"" target=""_self"" class=""boldLink"">" & strMainForumName & "</a>")
  
Response.Write(strNavSpacer)

'Check there are forum's to display
If rsTopic.EOF Then

	'If there are no forum's to display then display the appropriate error message
	Response.Write "<span class=""bold"">" & strTxtNoForums & "</span>"

'Else there the are forum's to write the HTML to display it the forum names and a discription
Else
	'Write the HTML of the forum descriptions as hyperlinks to the forums
	Response.Write ("<a href=""forum_topics.asp?FID=" & intForumID & "&PN=" & intTopicPageNumber & """ target=""_self"" class=""boldLink"">" & strForumName & "</a>")
End If


'Clean Up
rsTopic.Close


Response.Write(vbCrLf & "</td>" & vbCrLf & "  <td align=""right"" width=""5%"">")


   	'If the user is the forum admin or a moderator then give them admin functions on this topic
	If blnAdmin  OR blnModerator Then

		Response.Write(vbCrLf & "     <a href=""javascript:openWin('pop_up_topic_admin.asp?TID=" & lngTopicID & "','admin','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')""><img src=""" & strImagePath & "admin.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtTopicAdmin & """></a>")

	End If

Response.Write(vbCrLf & "</td>" & _
vbCrLf & " </tr>" & _
vbCrLf & "</table>" & _
vbCrLf & "      <table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""0"" cellpadding=""4"" align=""center"">" & _
vbCrLf & "        <tr>" & _
vbCrLf & "         <td width=""70%""><span class=""lgText""><img src=""" & strImagePath & "subject_folder.gif"" alt=""" & strTxtSubjectFolder & """ align=""absmiddle""> " & strTxtTopic & ": " & strSubject & "</span>")
 
'If the topic is locked then have a locked icon
If blnTopicLocked = True Then
	Response.Write ("  <span class=""smText"">(<img src=""" & strImagePath & "forum_locked_icon.gif"" align=""baseline"" alt=""" & strTxtTopicLocked & """> " & strTxtTopicLocked & ")</span>")
End If

Response.Write(vbCrLf & "	</td>")  
    
'If the user can reply and they are not suspened then display real reply links
If blnReply = True AND blnActiveMember = True Then

	'If the reply box is on the same page shorten the reply link
	If intRecordPositionPageNum = intTotalNumOfPages Then
		'Display images with links to reply to post or post a new topic
		Response.Write (vbCrLf & "<td align=""right""><a href=""#reply"" target=""_self""><img src=""" & strImagePath & "post_reply.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtPostReply & """></a>")
	Else
		'Display images with links to reply to post or post a new topic
		Response.Write (vbCrLf & "<td align=""right""><a href=""forum_posts.asp?TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intTotalNumOfPages & "#reply"" target=""_self""><img src=""" & strImagePath & "post_reply.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtPostReply & """></a>")
	End If

'Else the user can not reply
Else

    	'Display images with links to reply to post or post a new topic but get redirected to a login screen if user is not logged in
	Response.Write (vbCrLf & "<td align=""right""><a href=""edit_post.asp?M=R&FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "post_reply.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtPostReply & """></a>")

End If

'Display new topic link
Response.Write ("<a href=""post_message_form.asp?FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "new_post.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtNewTopic & """></a>")

'If the user can create a poll disply a create poll link
If blnPollCreate = True Then
	Response.Write ("<a href=""poll_create_form.asp?FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "new_poll.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtCreateNewPoll & """></a>")
End If

Response.Write(vbCrLf & "	</td>" & _
vbCrLf & "        </tr>" & _
vbCrLf & "      </table>")


'If there is a poll then display the poll include
If lngPollID > 0 Then
	%><!--#include file="includes/poll_display_inc.asp" --><%
End If



'If there are threads display them
If NOT rsPost.EOF Then



	'Check to see if user is watching this topic or not
	If blnEmail AND intGroupID <> 2 Then
	
		'Initalise the SQL string with a query to see if this person is watching the topic
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "TopicEmailNotify @lngAuthorID = " & lngLoggedInUserID & ", @lngTopicID= " & lngTopicID
		Else
			strSQL = "SELECT " & strDbTable & "EmailNotify.*  "
			strSQL = strSQL & "FROM " & strDbTable & "EmailNotify "
			strSQL = strSQL & "WHERE " & strDbTable & "EmailNotify.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "EmailNotify.Topic_ID=" & lngTopicID & ";"
		End If
	
		'Query the database
		rsTopic.Open strSQL, adoCon
	
		'If a record is return the user is watching this topic
		If NOT rsTopic.EOF Then 
			blnTopicWatched = True
			blnReplyNotify = True
		End If
	
		'Clean up
		rsTopic.Close
	End If




	'Create the HTML for the top of the table
	Response.Write("<table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""0"" cellpadding=""1"" bgcolor=""" & strTablePostsBorderColour & """ align=""center"">" & _
	vbCrLf & " <tr>" & _
	vbCrLf & "  <td>" & _
	vbCrLf & "  <table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" bgcolor=""" & strTablePostsBgColour & """>" & _
	vbCrLf & "    <tr>" & _
	vbCrLf & "     <td bgcolor=""" & strTablePostsBgColour & """>" & _
	vbCrLf & "      <table width=""100%"" border=""0"" cellspacing=""1"" cellpadding=""3"">" & _
	vbCrLf & "       <tr>" & _
	vbCrLf & "        <td bgcolor=""" & strTablePostsTitleColour & """ width=""140"" class=""tHeading"" background=""" & strTablePostsTitleBgImage & """ nowrap=""nowrap"">" & strTxtAuthor & "</td>" & _
	vbCrLf & "        <td bgcolor=""" & strTablePostsTitleColour & """ width=""82%"" class=""tHeading"" background=""" & strTablePostsTitleBgImage & """ nowrap=""nowrap""><table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & _
	vbCrLf & "          <tr>" & _
	vbCrLf & "           <td width=""20%"" class=""tHeading"">" & strTxtMessage & "</td>" & _
	vbCrLf & "           <td width=""80%"" align=""right"" nowrap=""nowrap""><a href=""get_topic.asp?FID=" & intForumID & "&TID=" & lngTopicID & "&DIR=P"" target=""_self"" class=""npLink"">&lt;&lt; " & strTxtPrevTopic & "</a> | <a href=""get_topic.asp?FID=" & intForumID & "&TID=" & lngTopicID & "&DIR=N"" target=""_self"" class=""npLink"">" & strTxtNextTopic & "&nbsp;&gt;&gt;</a></td>" & _
	vbCrLf & "          </tr>" & _
	vbCrLf & "         </table></td>" & _
	vbCrLf & "         </tr>")


	'Calculate the post number
	intThreadNo = (intRecordPositionPageNum - 1) * intThreadsPerPage

	
	'Loop round to read in all the thread's in the database
	For intRecordLoopCounter = 1 to intThreadsPerPage

		'Initilise moderator variable
		blnIsModerator = False

		'If there are no reacrds left exit for loop
		If rsPost.EOF Then Exit For

		'Calculate the thread number
		intThreadNo = intThreadNo + 1

		'Read in threads details for the topic from the database
		lngMessageID = CLng(rsPost("Thread_ID"))
		strMessage = rsPost("Message")
		strUsername = rsPost("Username")
		lngUserID = CLng(rsPost("Author_ID"))
		dtmTopicDate = CDate(rsPost("Message_date"))
		strAuthorHomepage = rsPost("Homepage")
		strAuthorLocation = rsPost("Location")
		dtmAuthorRegistration = CDate(rsPost("Join_date"))
		lngAuthorNumOfPosts = CLng(rsPost("No_of_posts"))
		strAuthorAvatar = rsPost("Avatar")
		strMemberTitle = rsPost("Avatar_title")
		strAuthorSignature = rsPost("Signature")
		strAuthorIP = rsPost("IP_addr")
		strGroupName = rsPost("Name")
		intRankStars = CInt(rsPost("Stars"))
		strRankCustomStars = rsPost("Custom_stars")
		
		
		
		'If the poster is a guest see if they have entered their name in the GuestName table and get it
		If lngUserID = 2 Then
			
			'Initalise the strSQL variable with an SQL statement to query the database
			If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "GuestPoster @lngThreadID = " & lngMessageID
			Else
				strSQL = "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = " & lngMessageID & ";"
			End If
			
			'Query the database
			rsTopic.Open strSQL, adoCon
			
			'Read in the guest posters name
			If NOT rsTopic.EOF Then strUsername = rsTopic("Name")
			
			'Close the recordset
			rsTopic.Close
		End If


		'If the post contains a quote or code block then format it
		If InStr(1, strMessage, "[QUOTE=", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatUserQuote(strMessage)
		If InStr(1, strMessage, "[QUOTE]", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatQuote(strMessage)
		If InStr(1, strMessage, "[CODE]", 1) > 0 AND InStr(1, strMessage, "[/CODE]", 1) > 0 Then strMessage = formatCode(strMessage)


		'If the post contains a flash link then format it
		If blnFlashFiles Then
			If InStr(1, strMessage, "[FLASH", 1) > 0 AND InStr(1, strMessage, "[/FLASH]", 1) > 0 Then strMessage = formatFlash(strMessage)
			If InStr(1, strAuthorSignature, "[FLASH", 1) > 0 AND InStr(1, strAuthorSignature, "[/FLASH]", 1) > 0 Then strAuthorSignature = formatFlash(strAuthorSignature)
		End If
		
		
		'If the message has been edited parse the 'edited by' XML into HTML for the post
		If InStr(1, strMessage, "<edited>", 1) Then strMessage = editedXMLParser(strMessage)


		'Call the function to highlight search words if coming froma search page
		If strSearchKeywords <> "" Then strMessage = searchHighlighter(strMessage, sarySearchWord)


		'If the user wants there signature shown then attach it to the message
		If rsPost("Show_signature") AND strAuthorSignature <> "" Then strMessage = strMessage & "<!-- Signature --><br /><br />__________________<br />" & strAuthorSignature & "<!-- Signature -->"

		


		Response.Write(vbCrLf & "      <tr>" & _
		vbCrLf & "        <td valign=""top"" background=""" & strTablePostsBgImage & """ bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePostsSideEvenRowColour) Else Response.Write(strTablePostsSideOddRowColour)
		Response.Write(""" class=""smText"">" & _
		vbCrLf & "         <a name=""" & lngMessageID & """></a>" & _
		vbCrLf & "         <span class=""bold"">" & strUsername & "</span><br />" & strGroupName & "<br />" & _
		vbCrLf & "         <img src=""")
		If strRankCustomStars <> "" Then Response.Write(strRankCustomStars) Else Response.Write(strImagePath & intRankStars & "_star_rating.gif")
		Response.Write(""" alt=""" & strGroupName & """><br />")


	        'If the user has an avatar then display it
	        If blnAvatar = True AND strAuthorAvatar <> "" Then Response.Write("<img src=""" & strAuthorAvatar & """ width=""" & intAvatarWidth & """ height=""" & intAvatarHeight & """ alt=""" & strTxtAvatar & """ vspace=""5"" OnError=""this.src='" & strImagePath & "blank.gif', height='0';"">")
	       	
	       	'If there is a title for this member then display it
	       	If strMemberTitle <> "" Then Response.Write(vbCrLf & "<br />" & strMemberTitle)

         	Response.Write("<br /><br />" & strTxtJoined & ": " & DateFormat(dtmAuthorRegistration, saryDateTimeData))

         	'If the is a location display it
         	If strAuthorLocation <> "" Then Response.Write("<br />" & strTxtLocation & ": " & strAuthorLocation)

		
		'If active users is enabled see if the user is online
		If blnActiveUsers Then 

			'Display if the user is online
			blnIsUserOnline = False
	
			'Loop through the active users array
			For intArrayPass = 1 To UBound(saryActiveUsers, 2)
				If saryActiveUsers(1, intArrayPass) = lngUserID Then blnIsUserOnline = True
			Next
	
			'Display if the user is online
			If blnIsUserOnline Then Response.Write(vbCrLf & "<br />" & strTxtOnlineStatus & ": " & strTxtOnLine2) Else Response.Write(vbCrLf & "<br />" & strTxtOnlineStatus & ": " & strTxtOffLine)
		End If
			

         	'Display the num of posts
         	Response.Write("<br />" & strTxtPosts & ": " & lngAuthorNumOfPosts)

		'Create the table for the main post
		Response.Write(vbCrLf & "         </td>")
		Response.Write(vbCrLf & "        <td valign=""top"" background=""" & strTablePostsBgImage & """ bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePostsEvenRowColour) Else Response.Write(strTablePostsOddRowColour)
		Response.Write(""" class=""text"">")
		Response.Write(vbCrLf & "         <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">")
		Response.Write(vbCrLf & "          <tr>")
		Response.Write(vbCrLf & "           <td width=""80%"" class=""smText"">" & strTxtPosted & " " & DateFormat(dtmTopicDate, saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(dtmTopicDate, saryDateTimeData))


                'If the user is the admin or moderatir then display the authors IP
                If (blnAdmin OR blnModerator) AND strAuthorIP <> "" Then
                	Response.Write(" | " & strTxtIP & " <a href=""javascript:openWin('pop_up_IP_blocking.asp?IP=" & strAuthorIP & "&TID=" & lngTopicID & "','move','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=425,height=425')"" class=""smLink"">" & strAuthorIP & "</a>")
                Else
                	Response.Write(" | " & strTxtIPLogged)
        	End If
        	
        	

		Response.Write(VbCrLf & "		    </td>")
		Response.Write(VbCrLf & "                    <td width=""20%"" align=""right"" nowrap=""nowrap"">")
		
		
		'Display the report post feature if email is enabled.
        	If blnEmail AND intGroupID <> 2 AND lngUserID <> lngLoggedInUserID Then Response.Write(VbCrLf & "<a href=""report_post.asp?PID=" & lngMessageID & "&FID=" & intForumID & "&TID=" & lngTopicID & "&TPN=" & intRecordPositionPageNum & """ target=""_self"" class=""smLink""><img src=""" & strImagePath & "report_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtReportPost & """></a>")

                'If the topic is not locked put in a link for someone to quote this message
      		If (blnTopicLocked = False) AND blnPollNoReply = False Then

			Response.Write(vbCrLf & "            		 <a href=""edit_post.asp?M=Q&PID=" & lngMessageID & "&TPN=" & intTotalNumOfPages & """><img src=""" & strImagePath & "quote_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtQuote & "&nbsp;" & strUsername & """></a>")

            	'Else put in a non breakin space for netscape 4 bug
            	Else
            		Response.Write("&nbsp;")
            	End If


		Response.Write(vbCrLf & "          </td>" & _
		vbCrLf & "          </tr>" & _
		vbCrLf & "          <tr>" & _
		vbCrLf & "           <td colspan=""2""><hr /></td>" & _
		vbCrLf & "          </tr>" & _
		vbCrLf & "         </table>" & _
		vbCrLf & "<!-- Message body -->" & vbCrLf & strMessage & vbCrLf &  "<!-- Message body ''"""" -->" & _
		vbCrLf & "        </td>" & _
		vbCrLf & "       </tr>" & _
		vbCrLf & "       <tr>" & _
		vbCrLf & "        <td bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePostsSideEvenRowColour) Else Response.Write(strTablePostsSideOddRowColour)
		Response.Write(""" background=""" & strTablePostsBgImage & """><a href=""#top"" target=""_self"" class=""npLink"">" & strTxtBackToTop & "</a></td>")
		Response.Write(vbCrLf & "        <td bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePostsEvenRowColour) Else Response.Write(strTablePostsOddRowColour)
		Response.Write(""" background=""" & strTablePostsBgImage & """ class=""text""><a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & lngUserID & "&FID=" & intForumID & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')""><img src=""" & strImagePath & "profile_icon.gif"" border=""0"" alt=""" & strTxtView & "&nbsp;" & strUsername & "'s&nbsp;" & strTxtProfile & """ align=""absmiddle""></a>")
		Response.Write(vbCrLf & "         <a href=""search_form.asp?KW=" & Server.URLEncode(strUsername) & "&SI=AR&FM=" & intForumID & "&FID=" & intForumID & "&TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum & """><img src=""" & strImagePath & "search_sm.gif"" border=""0"" alt=""" & strTxtSearchForPosts & "&nbsp;" & strUsername & """ align=""absmiddle""></a>")


            	'If the user has a hompeage put in a link button
      		If strAuthorHomepage <> "" Then Response.Write(vbCrLf & "         <a href=""" & strAuthorHomepage & """ target=""_blank""><img src=""" & strImagePath & "home_icon.gif"" border=""0"" alt=""" & strTxtVisit & " " & strUsername & "'s " & strTxtHomepage & """ align=""absmiddle""></a>")


            	'If the private msg's are on then display a link to enable use to send them a msg
            	If blnPrivateMessages AND intGroupID <> 2 Then

			Response.Write(vbCrLf & "         <a href=""pm_new_message_form.asp?name=" & Server.URLEncode(strUsername) & """ target=""_self""><img src=""" & strImagePath & "pm_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtSendPrivateMessage & """></a>")
			Response.Write(vbCrLf & "         <a href=""pm_buddy_list.asp?name=" & Server.URLEncode(strUsername) & """ target=""_self""><img src=""" & strImagePath & "add_buddy_sm.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtAddToBuddyList & """></a>")

      		End If

	      	'If the logged in user is the person who posted the message or the forum administrator/moderator then allow them to edit or delete the message
	      	If (lngLoggedInUserID = lngUserID AND blnForumLocked = False AND blnActiveMember AND blnTopicLocked = False) OR (blnAdmin OR blnModerator) Then

	      	   	'Only let the user edit the post if they have edit rights
	      	   	If blnEdit OR blnAdmin Then Response.Write(vbCrLf & "         <a href=""edit_post.asp?PID=" & lngMessageID & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum & """ target=""_self""><img src=""" & strImagePath & "edit_post_icon.gif"" border=""0"" align=""absmiddle"" alt=""" & strTxtEditPost & """></a>")

			'Only let a normal user delete there post if someone hasn't posted a reply
      			If (intTotalNumOfThreads = intThreadNo) OR (blnAdmin OR blnModerator) Then

	            		'Only let the user delete the post if they have delete rights
	            		If blnDelete OR blnAdmin Then Response.Write(vbCrLf & "         <a href=""delete_post.asp?PID=" & lngMessageID & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum & """ target=""_self"" OnClick=""return confirm('" & strTxtDeletePostAlert & "')""><img src=""" & strImagePath & "delete_sm.gif"" align=""absmiddle"" alt=""" & strTxtDeletePost & """ border=""0""></a>")
	      		End If
	      	End If


   	      	'If the user is the forum admin or a moderator then let them move the topic to another forum
	      	If blnAdmin OR blnModerator Then Response.Write(vbCrLf & "         <a href=""javascript:openWin('move_post_form.asp?PID=" & lngMessageID & "','move','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')""><img src=""" & strImagePath & "move_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtMovePost & """></a>")


		Response.Write(vbCrLf & "        </td>" & vbCrLf & "       </tr>" & vbCrLf & "       </tr>" & vbCrLf & "       <tr>")
		Response.Write(vbCrLf & "        <td colspan=""2"" align=""center"" background=""" & strTablePostsBgImage & """ bgcolor=""" & strTablePostsSeporatorColour & """><span style=""font-size: 2px;"">&nbsp;</span></td>")

		'Move to the next database record
		rsPost.MoveNext
	Next
End If

Response.Write("     </tr>" & _
vbCrLf & "    </table></td>" & _
vbCrLf & "   </tr>" & _
vbCrLf & "  </table>" & _
vbCrLf & " </td></tr>" & _
vbCrLf & "</table>" & _
vbCrLf & "<br />" & _
vbCrLf & "<div align=""center"">")



'Clean up
rsPost.Close
Set rsPost = Nothing




'Set up an achor
Response.Write("<a name=""reply""></a>")

'Display a message if the users IP is banned
If blnBannedIP Then

	Response.Write(vbCrLf & "	<p class=""text"">" & strTxtSorryYouDoNotHavePerimssionToReplyIPBanned & "</p>")

'Display message if the users forum membership is suspended
ElseIf blnActiveMember = False AND (intRecordPositionPageNum = intTotalNumOfPages) Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtSorryNoReply & "<br />")
	
	'If mem suspended display message
	If  InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) Then
		Response.Write(strTxtForumMemberSuspended)
	'Else account not yet active
	Else
		Response.Write(strTxtForumMembershipNotAct)
	End If
	'If email is on then place a re-send activation email link
	If InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) = False AND blnEmailActivation AND blnLoggedInUserEmail Then Response.Write("<br /><a href=""JavaScript:openWin('resend_email_activation.asp','actMail','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=475,height=200')"">" & strTxtResendActivationEmail & "</a>")
	
	Response.Write("</p>")

'Display message if the forum is locked
ElseIf blnForumLocked AND (intRecordPositionPageNum = intTotalNumOfPages) Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtSorryNoReply & "<br />" & strTxtThisForumIsLocked & "</p>")

'Display message if the user does not have permisison to post in this forum
ElseIf blnReply = False AND intGroupID <> 2 Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtSorryYouDoNotHavePerimssionToReplyToPostsInThisForum & "</p>")

'Display message if the topic is locked
ElseIf (blnTopicLocked = True) AND (intRecordPositionPageNum = intTotalNumOfPages) Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtSorryNoReply & "<br />" & strTxtThisTopicIsLocked & "</p>")

'Display message if this is a poll only
ElseIf blnPollNoReply Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtThisIsAPollOnlyYouCanNotReply & "</p>")

'Display message if the user is a guest or not logged in
ElseIf blnReply = False AND (intRecordPositionPageNum = intTotalNumOfPages) Then

	Response.Write(vbCrLf & "	<p class=""text"">" &  strTxtPostAReplyRegister & "&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self"">" & strTxtLoginSm & "</a><br />" & strTxtNeedToRegister & "&nbsp;<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self"">" & strTxtSmRegister & "</a></p>")

'Else disply the reply post box
ElseIf intRecordPositionPageNum = intTotalNumOfPages Then

	Response.Write(vbCrLf & "	<span class=""heading"">" & strTxtPostReply & "</span><br />")

	'See if the users browser is RTE enabled
	If RTEenabled() <> "false" AND blnRTEEditor AND blnWYSIWYGEditor Then

		'Open the message form for RTE enabled browsers
		%><!--#include file="includes/RTE_message_form_inc.asp" --><%
	Else
		'Open up the mesage form for non RTE enabled browsers
		%><!--#include file="includes/message_form_inc.asp" --><%
	End If

End If



Response.Write(vbCrLf & "	  <table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""0"" cellpadding=""3"">" & _
vbCrLf & "            <tr><form>")



'If there is more than 1 page of topics then dispaly drop down list to the other threads
If intTotalNumOfPages > 1 Then

	'Display an image link to the last topic
	Response.Write (vbCrLf & "		<td colspan=""3"" align=""right"" class=""text"" nowrap=""nowrap"">")
	
	
	'Display a prev link if previous pages are available
	If intRecordPositionPageNum > 1 Then
		If Request.QueryString("KW") <> "" Then 
			Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")
		Else
			Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")
		End If
	End If
	
	Response.Write (strTxtPage & " " & _
	vbCrLf & "		 <select onChange=""ForumJump(this)"" name=""SelectTopicPage"">")

	'Loop round to display links to all the other pages
	For intTopicPageLoopCounter = 1 to intTotalNumOfPages

		'Display a link in the link list to the another threads pages
		'If it's a search result page then place the highlighted keywords into the link
		If Request.QueryString("KW") <> "" Then
			Response.Write (vbCrLf & "		  <option value=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&PN=" & intTopicPageNumber & "&TPN=" & intTopicPageLoopCounter & """")
		Else
			Response.Write (vbCrLf & "		  <option value=""forum_posts.asp?TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intTopicPageLoopCounter & """")
		End If

		'If this page number to display is the same as the page being displayed then make sure it's selected
		If intTopicPageLoopCounter = intRecordPositionPageNum Then
			Response.Write (" selected")
		End If

		'Display the link page number
		Response.Write (">" & intTopicPageLoopCounter & "</option>")

	Next

	'End the drop down list
	Response.Write (vbCrLf & "		</select> " & strTxtOf & " " & intTotalNumOfPages )
	
	'Display a next link if needed
	If intRecordPositionPageNum <> intTotalNumOfPages Then 
		If Request.QueryString("KW") <> "" Then 
			Response.Write("&nbsp;<a href=""forum_posts.asp?TID=" & lngTopicID & "&KW=" & Server.URLEncode(Request.QueryString("KW")) & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")
		Else
			Response.Write("&nbsp;<a href=""forum_posts.asp?TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")
		End If
	End If
		
		
		
	Response.Write("</td>")
End If



Response.Write("</form></tr>" & _
vbCrLf & "             <tr>" & _
vbCrLf & "              <td>")



'Display a link to watch or un-watch this topic if email notification is enabled
If blnEmail AND intGroupID <> 2 Then

	'Create link
	Response.Write("<a href=""email_notify.asp?FID=" & intForumID & "&TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intTotalNumOfPages & """ target=""_self"">")

	'If topic is watched allow unwatch link display
	If blnTopicWatched Then Response.Write(strTxtUn)

	'Display link to watch the topic
	Response.Write(strTxtWatchThisTopic & "</a>")


'Display a non breaking space for Netscrape 4 bug
Else
	Response.Write("&nbsp;")

End If
 
 
Response.Write(vbCrLf & "             </td>" & _
vbCrLf & "              <td align=""right"">")



'If the user is not suspened and can reply then have links to reply etc.
If blnReply AND blnActiveMember Then

	'Only show the post reply link button on pages without the reply box as some people seem to think this button should actually post the forum!!!
	If intRecordPositionPageNum <> intTotalNumOfPages Then
		Response.Write (vbCrLf & "<a href=""forum_posts.asp?TID=" & lngTopicID & "&PN=" & intTopicPageNumber & "&TPN=" & intTotalNumOfPages & "#reply"" target=""_self""><img src=""" & strImagePath & "post_reply.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtPostReply & """></a>")
	End If

'Else the user is not logged
Else

    	'Display images with links to reply to post or post a new topic but get redirected to a login screen if user is not logged in
	Response.Write (vbCrLf & "<a href=""edit_post.asp?M=R&FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "post_reply.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtPostReply & """></a>")
End If

'Display new topic link
Response.Write ("<a href=""post_message_form.asp?FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "new_post.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtNewTopic & """></a>")

'If the user can create a poll disply a create poll link
If blnPollCreate = True Then
	Response.Write ("<a href=""poll_create_form.asp?FID=" & intForumID & """  target=""_self""><img src=""" & strImagePath & "new_poll.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtCreateNewPoll & """></a>")
End If


Response.Write(vbCrLf & "		</td>" & _
vbCrLf & "	    </tr>" & _
vbCrLf & "         </table>" & _
vbCrLf & " <table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""0"" cellpadding=""1"">" & _
vbCrLf & "  <tr>" & _
vbCrLf & "   <td width=""30%"">" & _
vbCrLf & "    <table width=""380"" border=""0"" cellspacing=""0"" cellpadding=""2"">" & _
vbCrLf & "     <tr>" & _
vbCrLf & "      <td>" & _
vbCrLf & "      <table width=""300"" border=""0"" cellspacing=""0"" cellpadding=""1"" align=""center"" bgcolor=""" & strTableBorderColour & """>" & _
vbCrLf & "       <tr>" & _
vbCrLf & "        <td>" & _
vbCrLf & "         <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""4"" align=""center"" bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """>" & _
vbCrLf & "          <tr>" & _
vbCrLf & "           <td align=""center""> <a href=""JavaScript:openWin('printer_friendly_posts.asp?TID=" & lngTopicID & "','printer_friendly','toolbar=0,location=0,status=0,menubar=1,scrollbars=1,resizable=1,width=640,height=390')""><img src=""" & strImagePath & "print_version.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtPrintVersion & """></a>" & _
vbCrLf & "           <a href=""JavaScript:openWin('printer_friendly_posts.asp?FID=" & intForumID & "&TID=" & lngTopicID & "','printer_friendly','toolbar=0,location=0,status=0,menubar=1,scrollbars=1,resizable=1,width=640,height=390')"" class=""smLink"">" & strTxtPrintVersion & "</a>")

'If the user has logged in then the Logged In and the e-mail is on then display a link to allow the user to e-mail topic to a friend
If intGroupID <> 2 AND blnEmail AND blnActiveMember Then

	Response.Write("         &nbsp;<a href=""JavaScript:openWin('email_topic.asp?TID=" & lngTopicID & "','email_friend','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=450')""><img src=""" & strImagePath & "e-mail_topic.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtEmailTopic & """></a>" & _
	vbCrLf & "         <a href=""JavaScript:openWin('email_topic.asp?TID=" & lngTopicID & "','email_friend','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=450')"" class=""smLink"">" & strTxtEmailTopic & "</a>")

End If

Response.Write("	 </td>" & _
vbCrLf & "        </tr>" & _
vbCrLf & "       </table>" & _
vbCrLf & "      </tr>" & _
vbCrLf & "     </table><br /><form>") 
%><!-- #include file="includes/forum_jump_inc.asp" --><% 
Response.Write("</form></td>" & _
vbCrLf & "    </table></td>" & _
vbCrLf & "   <td width=""70%"" align=""right"" class=""smText"" nowrap=""nowrap"">")
%><!-- #include file="includes/show_forum_permissions_inc.asp" --><%
Response.Write("</td>" & _
vbCrLf & "  </tr>" & _
vbCrLf & " </table>" & _
vbCrLf & "  <br />")


'Clear server objects
Set rsTopic = Nothing
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode Then
	If blnTextLinks Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******



'Display the process time
If blnShowProcessTime Then Response.Write("<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>")


Response.Write("</div>")

'Display an alert message if the user is watching this topic for email notification
If Request.QueryString("EN") = "TS" Then
	Response.Write("<script language=""JavaScript"">")
	Response.Write("alert('" & strTxtYouWillNowBeNotifiedOfAllReplies & "');")
	Response.Write("</script>")
End If

'Display an alert message if the user is not watching this topic for email notification
If Request.QueryString("EN") = "TU" Then
	Response.Write("<script language=""JavaScript"">")
	Response.Write("alert('" & strTxtYouWillNowNOTBeNotifiedOfAllReplies & "');")
	Response.Write("</script>")
End If

%>
<!-- #include file="includes/footer.asp" -->