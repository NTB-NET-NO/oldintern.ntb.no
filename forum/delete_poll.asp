<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/admin_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim lngTopicID 		'Holds the topic ID number to return to
Dim intForumID		'Holds the forum ID number
Dim lngPollID		'Holds the poll ID
Dim strMode		'Holds the page mode
Dim intPollLoopCounter	'Holds the poll loop counter
Dim strPollQuestion	'Holds the poll question
Dim blnMultipleVotes	'Set to true if multiple votes are allowed
Dim blnPollNoReply	'Set to true if this is a no reply poll
Dim strPollChoice	'Holds the poll choice


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")
End If



'Read in the message ID number to be deleted
lngTopicID = CLng(Request("TID"))


'If the person is not an admin or a moderator then send them away
If lngTopicID = "" Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If




'Initliase the SQL query to get the topic details from the database
strSQL = "SELECT " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Poll_ID "
strSQL = strSQL & "FROM " & strDbTable & "Topic "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"

'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
rsCommon.CursorType = 2

'Set the Lock Type for the records so that the record set is only locked when it is updated
rsCommon.LockType = 3

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
	lngPollID = CLng(rsCommon("Poll_ID"))
End If





'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)



'Check to make sure the user is deleting the post is a moderator or the forum adminstrator
If (blnAdmin = True OR blnModerator = True) AND lngPollID > 0 Then
	
	'Update the poll id with 0
	rsCommon.Fields("Poll_ID") = 0
	
	'Update the recordset
	rsCommon.Update
	
	'Delete the Poll choices
	strSQL = "DELETE FROM " & strDbTable & "PollChoice WHERE " & strDbTable & "PollChoice.Poll_ID="  & lngPollID & ";"

	'Write to database
	adoCon.Execute(strSQL)

	'Delete the Poll
	strSQL = "DELETE FROM " & strDbTable & "Poll WHERE " & strDbTable & "Poll.Poll_ID="  & lngPollID & ";"

	'Write to database
	adoCon.Execute(strSQL)
End If


'Clean up
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<script language="JavaScript">
	window.opener.location.href = "forum_posts.asp?TID=<% = lngTopicID %>"
	window.close();
</script>
</head>
</html>