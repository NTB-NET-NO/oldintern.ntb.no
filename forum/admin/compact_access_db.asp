<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true
Response.Buffer = False 


'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing




'Check to see if the user is using an access database
If strDatabaseType <> "Access" Then
	
	'Display message to sql server usres
	Response.Write "This page only works on Access but your Database Type is set to SQL Server"
	Response.End
End If

'Dimension variables
Dim objJetEngine		'Holds the jet database engine object
Dim objFSO			'Holds the FSO object
Dim strCompactDB		'Holds the destination of the compacted database


%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Compact and Repair Access Database</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">Compact and Repair Access Database</span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br />
  From here you can Compact and Repair the Access database used for the Forum making the database size smaller and the forum perform faster.<br>
  This feature can also repair a damaged or corrupted database.<br>
 </p>
 <%

'If this is a post back run the compact and repair
If Request.Form("postBack") Then %>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
   <td class="bold"><ol><%
 
 	'Create an intence of the FSO object
 	Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
 	
 	'Back up the database
 	objFSO.CopyFile strDbPathAndName, Replace(strDbPathAndName, ".mdb", "-backup.mdb", 1, -1, 1)
 	
 	Response.Write("	<li class=""bold"">Database backed up to:-<br/><span class=""smText"">" & Replace(strDbPathAndName, ".mdb", "-backup.mdb", 1, -1, 1) & "</span><br /><br /></li>")




	'Create an intence of the JET engine object
	Set objJetEngine = Server.CreateObject("JRO.JetEngine")

	'Get the destination and name of the compacted database
	strCompactDB = Replace(strDbPathAndName, ".mdb", "-tmp.mdb", 1, -1, 1)

	'Compact database
	objJetEngine.CompactDatabase strCon, "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strCompactDB
	
	'Display text that new compact db is created
	Response.Write("	<li class=""bold"">New compacted database:-<br/><span class=""smText"">" & strCompactDB & "</span><br /><br /></li>")
	
	'Release Jet object
	Set objJetEngine = Nothing
	
	
	
	
	'Delete old database
	objFSO.DeleteFile strDbPathAndName
	
	'Display text that that old db is deleted
	Response.Write("	<li class=""bold"">Old uncompacted database deleted:-<br/><span class=""smText"">" & strDbPathAndName & "</span><br /><br /></li>")
	
	
	
	'Rename temporary database to old name
	objFSO.MoveFile strCompactDB, strDbPathAndName
	
	'Display text that that old db is deleted
	Response.Write("	<li class=""bold"">Rename compacted database from:-<br/><span class=""smText"">" & strCompactDB & "</span><br />To:-<br /><span class=""smText"">" & strDbPathAndName & "</span><br /><br /></li>")
	

	'Release FSO object
	Set objFSO = Nothing
	
	
	Response.Write("	The Forums Access database is now compacted and repaired")

%></ol></td>
  </tr>
 </table>
<%
Else

%>
 <p class="text"> Please note: If the 'Compact and Repair' procedure fails a backup of your database will be created ending with '-backup.mdb'.<br />
 </p>
</div>
<form action="compact_access_db.asp" method="post" name="frmCompact" id="frmCompact">
 <div align="center"><br />
  <input name="postBack" type="hidden" id="postBack" value="true">
  <input type="submit" name="Submit" value="Compact and Repair Database">
 </div>
</form><%

End If

%>
<br />
</body>
</html>
