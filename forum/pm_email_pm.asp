<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<!--#include file="functions/functions_format_post.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"

'Declare variables
Dim lngPmMessageID		'Private message id
Dim strPmSubject 		'Holds the subject of the private message
Dim strUsername 		'Holds the Username of the thread
Dim strEmailBody		'Holds the body of the e-mail message
Dim blnEmailSent		'set to true if an e-mail is sent
Dim intForumID			'Holds the forum ID
Dim strPrivateMessage		'Holds the private message


'Raed in the pm mesage number to display
lngPmMessageID = CLng(Request.QueryString("ID"))

'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then Response.Redirect("default.asp")

'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then Response.Redirect("insufficient_permission.asp")




	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "PMMessage.*, " & strDbTable & "Author.Username "
strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "PMMessage "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "PMMessage.From_ID AND " & strDbTable & "PMMessage.PM_ID=" & lngPmMessageID & " "
'If this is a link from the out box then check the from author ID to check the user can view the message
If Request.QueryString("M") = "OB" Then
	strSQL = strSQL & " AND " & strDbTable & "PMMessage.From_ID=" & lngLoggedInUserID & ";"
'Else use the to author ID to check the user can view the message
Else
	strSQL = strSQL & " AND " & strDbTable & "PMMessage.Author_ID=" & lngLoggedInUserID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon



'If a mesage is found then send a mail
If blnLoggedInUserEmail AND blnEmail AND NOT rsCommon.EOF Then 
	
	'Read in some of the details
	strPmSubject = rsCommon("PM_Tittle")
	strUsername = rsCommon("Username")
	strPrivateMessage = rsCommon("PM_Message")
	
	'Change	the path to the	emotion	symbols	to include the path to the images
	strPrivateMessage = Replace(strPrivateMessage, "src=""smileys/smiley", "src=""" & strForumPath & "/smileys/smiley", 1, -1, 1)
	
	'Initailise the e-mail body variable with the body of the e-mail
	strEmailBody = strTxtHi & " " & decodeString(strLoggedInUsername) & ","
	strEmailBody = strEmailBody & "<br /><br />" & strTxtEmailBelowPrivateEmailThatYouRequested & ":-"
	strEmailBody = strEmailBody & "<br /><br /><hr />"
	strEmailBody = strEmailBody & "<br /><b>" & strTxtPrivateMessage & " :</b> " & strPmSubject
	strEmailBody = strEmailBody & "<br /><b>" & strTxtSentBy & " :</b> " & decodeString(strUsername) 
	strEmailBody = strEmailBody & "<br /><b>" & strTxtSent & " :</b> " & DateFormat(rsCommon("PM_Message_Date"), saryDateTimeData) & " at " & TimeFormat(rsCommon("PM_Message_Date"), saryDateTimeData) & "<br /><br />"
	strEmailBody = strEmailBody & strPrivateMessage
		
		
	'Call the function to send the e-mail
	blnEmailSent = SendMail(strEmailBody, decodeString(strLoggedInUsername), decodeString(strLoggedInUserEmail), decodeString(strLoggedInUsername), decodeString(strLoggedInUserEmail), decodeString(strPmSubject), strMailComponent, true)
End If



'Clear server objects
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

Response.Redirect("pm_show_message.asp?ES=" & blnEmailSent & "&" & Request.QueryString)
%>