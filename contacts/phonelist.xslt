<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="iso-8859-1" standalone="yes"/>

<!-- 
	XSLT Stylesheet by NTB Roar Vestre 
	Last Change 16.04.2002 by RoV
-->

<xsl:param name="sortering">
<xsl:text>navn</xsl:text>
</xsl:param>

<xsl:param name="baseurl">
<xsl:text>section.aspx?section=TELEFONLISTE</xsl:text>
</xsl:param>

<xsl:template match="/telefonliste">

	<script type="text/javascript" src="includes/overlib/overlibmws.js"></script>
	<script type="text/javascript" src="includes/overlib/overlibmws_hide.js"></script>
	<script type="text/javascript" src="includes/overlib/overlibmws_iframe.js"></script>
	<script type="text/javascript" src="includes/overlib/overlibmws_shadow.js"></script>

	<script language="javascript">
	&lt;!--

	var ol_width= 350;
	var ol_hidebyidall = 'flashDiv';

	var ol_fgcolor = '#FFFFFF';
	var ol_bgcolor = '#8888ff';
	var ol_cgcolor = '#8888ff';

	var ol_textcolor = '#000000';
	var ol_capcolor = '#FFFFFF';

	var ol_textsize = '10px'
	var ol_captionsize = '10px'

	//--&gt;
	</script>
	
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

	<table cellspacing="3" cellpadding="1" border="0">
	
	<tr bgcolor="#dddddd" align="left">

	<th class="artikkel_tekst" width="25">Msn</th>
	<th class="artikkel_tekst" width="150">
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="$baseurl"/>
				<xsl:text>&amp;sort=navn</xsl:text>
			</xsl:attribute>
		Navn</a>
	</th>
	<th class="artikkel_tekst" width="50">
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="$baseurl"/>
				<xsl:text>&amp;sort=signatur</xsl:text>
			</xsl:attribute>
		Sign.</a>
	</th>
	<th class="artikkel_tekst" width="120">
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="$baseurl"/>
				<xsl:text>&amp;sort=avdeling</xsl:text>
			</xsl:attribute>
		Avdeling</a>
	</th>
	<th class="artikkel_tekst" width="120">Telefon</th>
	<th class="artikkel_tekst" width="120">Mobil</th>
	</tr>

		<xsl:choose>
		
			<xsl:when test="$sortering = 'avdeling'">
				<xsl:apply-templates select="person">
				<xsl:sort select="avdeling" order="ascending"/>
				</xsl:apply-templates>				
			</xsl:when>
		
			<xsl:when test="$sortering = 'signatur'">
				<xsl:apply-templates select="person">
				<xsl:sort select="signatur" order="ascending"/>
				</xsl:apply-templates>				
			</xsl:when>

			<xsl:otherwise>
				<xsl:apply-templates select="person">
				<xsl:sort select="navn" order="ascending"/>
				</xsl:apply-templates>				
			</xsl:otherwise>

		</xsl:choose>

	<tr bgcolor="#dddddd" align="left">
	<td colspan="6" class="artikkel_tekst">
	<b>Sist oppdatert: </b><xsl:value-of select="timestamp"/>
	</td>
	</tr>
	
	</table>
</xsl:template>

<!-- Templates -->
<xsl:template match="person">
	<xsl:choose>
		<xsl:when test="position() mod 2 = 0">
			<tr bgcolor="#ddddff">	
			<td class="artikkel_tekst" align="center" valign="middle">
				<xsl:if test="messenger != ''">
					<img src="images/chaticon.gif" height="13" width="15" border="0" align="middle">
						<xsl:attribute name="onmouseover">
						<xsl:text>return overlib('&lt;div style="font-size: 10px; font-family: Verdana"&gt;</xsl:text>
						<xsl:text>MSN Messenger: &lt;b&gt;</xsl:text>
						<xsl:value-of select="messenger"/>

						<xsl:text>&lt;b&gt;</xsl:text>
						<xsl:text>&lt;/span&gt;', WIDTH, 320);</xsl:text>
						</xsl:attribute>
			
						<xsl:attribute name="onmouseout">
						<xsl:text>return nd();</xsl:text>
						</xsl:attribute>
					</img>
				</xsl:if>
			</td>
			<td class="artikkel_tekst">
				<span>
					<xsl:attribute name="onmouseover">
					<xsl:text>return overlib('&lt;table border=0 cellpadding=0 cellspacing=0 style="font-size: 10px; font-family: Verdana"&gt;</xsl:text>
					
					<xsl:text>&lt;tr&gt;&lt;td width=100&gt;Stilling:&lt;/td&gt;&lt;td width=250&gt;</xsl:text>
					<xsl:value-of select="stilling"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Privat telefon:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="privat"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Privat adresse:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="adresse_gate"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&amp;nbsp;&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="adresse_postnr"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="adresse_by"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;MSN Messenger:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="messenger"/>
					
					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;', CAPTION, '</xsl:text>
					<xsl:value-of select="navn"/>
					<xsl:text>');</xsl:text>
					</xsl:attribute>
					
					<xsl:attribute name="onmouseout">
					<xsl:text>return nd();</xsl:text>
					</xsl:attribute>
					
					<xsl:value-of select="navn"/>
				</span>
			</td>
			<td class="artikkel_tekst"><xsl:value-of select="signatur"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="avdeling"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="telefon"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="mobil"/></td>
			</tr>
		</xsl:when>

		<xsl:otherwise>
			<tr>	
			<td class="artikkel_tekst" align="center">
				<xsl:if test="messenger != ''">
					<img src="images/chaticon.gif" height="13" width="15" border="0" align="middle">
						<xsl:attribute name="onmouseover">
						<xsl:text>return overlib('&lt;div style="font-size: 10px; font-family: Verdana"&gt;</xsl:text>
						<xsl:text>MSN Messenger: &lt;b&gt;</xsl:text>
						<xsl:value-of select="messenger"/>

						<xsl:text>&lt;b&gt;</xsl:text>
						<xsl:text>&lt;/span&gt;', WIDTH, 320);</xsl:text>
						</xsl:attribute>
			
						<xsl:attribute name="onmouseout">
						<xsl:text>return nd();</xsl:text>
						</xsl:attribute>
					</img>
				</xsl:if>
			</td>
			<td class="artikkel_tekst">
				<span>
					<xsl:attribute name="onmouseover">
					<xsl:text>return overlib('&lt;table border=0 cellpadding=0 cellspacing=0 style="font-size: 10px; font-family: Verdana"&gt;</xsl:text>
					
					<xsl:text>&lt;tr&gt;&lt;td width=100&gt;Stilling:&lt;/td&gt;&lt;td width=250&gt;</xsl:text>
					<xsl:value-of select="stilling"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Privat telefon:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="privat"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Privat adresse:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="adresse_gate"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&amp;nbsp;&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="adresse_postnr"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="adresse_by"/>

					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;MSN Messenger:&lt;/td&gt;&lt;td&gt;</xsl:text>
					<xsl:value-of select="messenger"/>
					
					<xsl:text>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;', CAPTION, '</xsl:text>
					<xsl:value-of select="navn"/>
					<xsl:text>');</xsl:text>
					</xsl:attribute>
					
					<xsl:attribute name="onmouseout">
					<xsl:text>return nd();</xsl:text>
					</xsl:attribute>
					
					<xsl:value-of select="navn"/>
				</span>
			</td>
			<td class="artikkel_tekst"><xsl:value-of select="signatur"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="avdeling"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="telefon"/></td>
			<td class="artikkel_tekst"><xsl:value-of select="mobil"/></td>
			</tr>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>

  