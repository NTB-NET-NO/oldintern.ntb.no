Imports System.Data.SqlClient
Imports System.Xml
Imports intranett.Utils
Imports System.Web.Configuration


Namespace intranett


Partial Class article
    Inherits System.Web.UI.Page
    Protected dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblArticle As System.Web.UI.WebControls.Label
    Protected WithEvents lblArt As System.Web.UI.WebControls.Label
    Protected WithEvents pMenu As System.Web.UI.WebControls.Panel
    Protected WithEvents pMenuBody As System.Web.UI.WebControls.Panel


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub DoIplogin()
        Dim iploggedinuser As String = ""
        iploggedinuser = CheckIP(Request.UserHostAddress, Session("CurrentMainGroup"))
        If iploggedinuser <> "" Then
            Session("UserId") = iploggedinuser

            AddUserHit(Session("UserID"), True, False, Session("SiteCode"))

            Dim arr As Array = Split(GetUserData(Session("UserId"), "MainGroups"), ",")
            Dim i As Integer
            For i = 0 To arr.Length - 1
                If arr(i) <> "" Then Session("MainGroup" & CStr(arr(i))) = "1"
            Next
        End If
    End Sub

        Function GetMainArticle(ByVal keyword As String) As Integer
            '         Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
            Dim cd As New SqlCommand("select RefId from Articles where Keyword like 'AU_-" & keyword.Substring(2) & "'", dbNTB)
            dbNTB.Open()
            Try
                GetMainArticle = cd.ExecuteScalar
            Finally
                dbNTB.Close()
            End Try
        End Function

        Private Sub ShowArticle()
            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
            Dim cd As New SqlCommand
            Dim refid As Integer = CInt(Request.QueryString("RefID"))
            Dim i As Integer = 0
            Dim section As String = ""
            Dim s As String = ""
            Dim IsProtected As Boolean = True
            Dim ShowMenu As Boolean = True
            Dim IsTeaser As Boolean = False
            Dim maingroup As Integer = 0
            Dim maincat As Integer = 0
            Dim qmaingroup As Integer = 0
            Dim mainrefid As Integer = 0
            Dim keyword As String = ""
            Dim DoGif As Boolean = False
            ' Dim GifGroups As Array = Split(ConfigurationSettings.AppSettings("maingroupgif"), ",")
            Dim GifGroups As Array = Split(WebConfigurationManager.AppSettings("maingroupgif"), ",")
            Dim arrmg As Array

            section = Request.QueryString("Section")
            qmaingroup = CInt(Request.QueryString("MainGroup"))
            cd.Connection = dbNTB
            dbNTB.Open()
            cd.CommandText = "select MainGroup, Name, IsProtected, ShowMenu from Sections where Name = '" + section + "'"
            Try
                dr = cd.ExecuteReader
                If dr.Read Then
                    IsProtected = (dr("IsProtected") = 1)
                    maingroup = dr("MainGroup")
                    ShowMenu = (dr("ShowMenu") = 1)
                End If
            Finally
                dr.Close()
            End Try

            'Sjekk for om noen har pr�vd � "tukle" med URL'en for � f� tilgang til saker de ikke skal :)
            arrmg = Split(BitCommaText(maingroup), ",")
            If Array.IndexOf(arrmg, CStr(qmaingroup)) = -1 Then
                dbNTB.Close()
                Server.Transfer("noaccess.html")
            End If

            Session("CurrentArticleId") = CStr(refid)
            Session("CurrentMaingroup") = CStr(maingroup)

            If Session("UserId") = "" Then DoIplogin()

            cd.CommandText = "_GetArticle"
            cd.CommandType = CommandType.StoredProcedure
            cd.Parameters.Add(New SqlParameter("@refid", SqlDbType.Int))
            cd.Parameters.Item("@refid").Value = refid
            cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.Int))
            cd.Parameters.Item("@maingroup").Value = qmaingroup
            Try
                dr = cd.ExecuteReader
                If dr.Read Then

                    maincat = dr("Categories")

                    Session("CurrentArticleMainGroup") = dr("MainGroup")

                    'FIX: K&U gives maincat K&U for INN/UTE also
                    If (maincat And 16) > 0 And (dr("MainGroup") = 1 Or dr("MainGroup") = 2) Then
                        Session("CurrentArticleMainGroup2") = "8"
                    Else
                        Session("CurrentArticleMainGroup2") = ""
                    End If

                    'pMenu.Visible = ShowMenu
                    'Fjerner body-backgroundgif n�r meny ikke skal vises
                    'If Not ShowMenu Then body.Attributes.Add("background", "")

                    keyword = dr("Keyword")
                    H.DocumentContent = dr("ArticleXML")
                    ' DoGif = GifGroups.IndexOf(GifGroups, CStr(dr("MainGroup"))) <> -1
                    DoGif = Array.IndexOf(GifGroups, CStr(dr("MainGroup"))) <> -1
                    ' IsTeaser = (dr("Subgroup") = CInt(ConfigurationSettings.AppSettings("teaserid")))
                    IsTeaser = (dr("Subgroup") = CInt(WebConfigurationManager.AppSettings("teaserid")))

                    'FIX: K&U gives maincat K&U for INN/UTE also
                    If Session("MainGroup8") = "1" And (maincat And 16) > 0 And (dr("MainGroup") = 1 Or dr("MainGroup") = 2) Then
                        IsProtected = False
                    End If

                    If (IsProtected And Not IsTeaser) And (Session("MainGroup" & dr("MainGroup")) = "") And Session("MainGroup-1") <> "1" Then
                        dr.Close()
                        dbNTB.Close()
                        Session("TargetURL") = Request.RawUrl
                        If Not ShowMenu Then s = "&ShowMenu=0"
                        Server.Transfer("login.aspx?Section=" & section & s)
                    End If

                    'Log user hit
                    If Session("UserID") <> "" Then
                        AddUserHit(Session("UserID"), False, True, Session("SiteCode"))
                    End If

                    If Not DoGif Or IsTeaser Then
                        pArticle.Visible = True
                        pGifArticle.Visible = False
                        If IsTeaser Then 'artikkel er teaser, fjern nedlastnigsmuligheter og legg til link til hele saken
                            lblDownload.Visible = False
                            hLink.Visible = True
                            mainrefid = GetMainArticle(keyword)
                            hLink.NavigateUrl = "article.aspx?Section=" & section & "&RefId=" + CStr(mainrefid) & "&Maingroup=" & dr("MainGroup")
                            ' hLink.Text = ConfigurationSettings.AppSettings("featurelinktext")
                            hLink.Text = WebConfigurationManager.AppSettings("featurelinktext")
                            hLink.Target = "mainart"
                            ' H.TransformSource = ConfigurationSettings.AppSettings("viewTeaserXSLT")
                            H.TransformSource = WebConfigurationManager.AppSettings("viewTeaserXSLT")
                        Else
                            lblDownload.Visible = True
                            hLink.Visible = False

                            'If (CInt(ConfigurationSettings.AppSettings("AllowPicsDownload")) And CInt(Session("CurrentArticleMainGroup"))) > 0 Then
                            '    H.TransformArgumentList = New System.Xml.Xsl.XsltArgumentList
                            '    H.TransformArgumentList.AddParam("downPics", "", "True")
                            'End If

                            'H.TransformSource = ConfigurationSettings.AppSettings("viewXSLT")

                            If (CInt(WebConfigurationManager.AppSettings("AllowPicsDownload")) And CInt(Session("CurrentArticleMainGroup"))) > 0 Then
                                H.TransformArgumentList = New System.Xml.Xsl.XsltArgumentList
                                H.TransformArgumentList.AddParam("downPics", "", "True")
                            End If

                            H.TransformSource = WebConfigurationManager.AppSettings("viewXSLT")

                        End If
                    Else
                        'Do gifarticle (and not teaser)
                        pArticle.Visible = False
                        pGifArticle.Visible = True
                        ' imgArticle.ImageUrl = MakeGifArticle(refid, dr("ArticleXML"), MapPath(ConfigurationSettings.AppSettings("gifarticlexslt")), MapPath("temp"))
                        imgArticle.ImageUrl = MakeGifArticle(refid, dr("ArticleXML"), MapPath(WebConfigurationManager.AppSettings("gifarticlexslt")), MapPath("temp"))
                        If dr("HasPictures") > 0 Then
                            Pictures.Visible = True
                            Pictures.DocumentContent = dr("ArticleXML")
                            ' Pictures.TransformSource = ConfigurationSettings.AppSettings("viewPicArtXSLT")
                            Pictures.TransformSource = WebConfigurationManager.AppSettings("viewPicArtXSLT")
                        End If
                    End If
                Else
                    dr.Close()
                    dbNTB.Close()
                    Server.Transfer("noaccess.html")
                End If
            Finally
                dr.Close()
            End Try

            ' Subarticles
            If Not IsTeaser And keyword.Length > 3 Then ' finn underartikler (feature)
                cd.CommandText = "select articlexml, keyword, Refid, HasPictures from Articles where keyword like 'U_-" & keyword.Substring(4) & "' order by Keyword"
                cd.CommandType = CommandType.Text
                dr = cd.ExecuteReader
                'Satt av plass til maks 5 underartikler, kan endres her og i aspx-filen
                For i = 1 To 5
                    Dim n As String = "U" & CStr(i)
                    Dim xml As System.Web.UI.WebControls.Xml
                    Dim img As System.Web.UI.WebControls.Image
                    xml = Page.FindControl(n)
                    img = Page.FindControl("imgArticle" & n)
                    If dr.Read Then
                        If Not DoGif Then
                            xml.DocumentContent = dr("ArticleXML")
                            ' xml.TransformSource = ConfigurationSettings.AppSettings("viewSubArtXSLT")
                            xml.TransformSource = WebConfigurationManager.AppSettings("viewSubArtXSLT")
                        Else
                            img.Visible = True
                            ' img.ImageUrl = MakeGifArticle(dr("Refid"), dr("ArticleXML"), MapPath(ConfigurationSettings.AppSettings("gifarticlexslt")), MapPath("temp"))
                            img.ImageUrl = MakeGifArticle(dr("Refid"), dr("ArticleXML"), MapPath(WebConfigurationManager.AppSettings("gifarticlexslt")), MapPath("temp"))
                            If dr("HasPictures") > 0 Then
                                Pictures.Visible = True
                                Pictures.DocumentContent = dr("ArticleXML")
                                ' Pictures.TransformSource = ConfigurationSettings.AppSettings("viewPicArtXSLT")
                                Pictures.TransformSource = WebConfigurationManager.AppSettings("viewPicArtXSLT")
                            End If
                        End If
                    End If
                Next
                dr.Close()
            End If
            dbNTB.Close()

            ' Download links
            Dim str As String = ""
            Dim arr, tmp As Array
            ' arr = Split(ConfigurationSettings.AppSettings("downloadXSLT"), ",")
            arr = Split(WebConfigurationManager.AppSettings("downloadXSLT"), ",")
            For i = 0 To CType(arr, Array).Length - 1
                tmp = Split(arr(i), ";")
                str += "<a target='_new' href='convertxml.aspx?RefId=" & CStr(refid) & "&XSLT=" & tmp(1) & "'>" & tmp(0) & "</a>&nbsp;&nbsp;"
            Next
            ' lblDownload.Text = str
        End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then ShowArticle()
    End Sub

End Class

End Namespace
