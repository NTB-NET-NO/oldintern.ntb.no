<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the timeout of the forum
Server.ScriptTimeout = 90

'Set the date time format to your own if you are getting a CDATE error
'Session.LCID = 1033

Dim adoCon 			'Database Connection Variable Object
Dim strCon			'Holds the string to connect to the db
Dim rsCommon			'Database recordset
Dim lngLoggedInUserID		'Holds the logginned in user ID
Dim strUsername			'Holds the users username
Dim strPassword			'Holds the usres password
Dim strUserCode			'Holds the users ID code
Dim strLoggedInUserCode		'Holds the loggin in user ID
Dim strSQL			'Holds the SQL query
Dim strSalt			'Holds salt values
Dim strCode			'Holds the page code
Dim strCode2			'Holds the page code
Dim strDatabaseDateFunction	'Holds a different date function for Access or SQL server
Dim strDatabaseType		'Holds the type of database used
Dim strDbPathAndName		'Holds the path and name of the database
Dim intGroupID			'Holds the group ID of the user


'Intialise variables
Const strVersion = "7.8"
strSalt = "5CB237B1D85"
Const strCodeField = "&#076;_&#099;&#111;&#100;&#101;"
lngLoggedInUserID = 0
intGroupID = 0
Const blnMassMailier = True




'Database Type
'strDatabaseType = "Access"
strDatabaseType = "SQLServer"

'Set up the database table name prefix and stored procedure prefix
'(This is useful if you are running multiple forums from one database)
' - make sure you also change this in the msSQL_server_setup.asp file if setting up an ms SQL server database)
Const strDbTable = "tbl"
Const strDbProc = "wwfSp"


'Set up the forum cookie name
'(This is useful if you run multiple copies of Web Wiz Forums on the same site so that cookies don't interfer with each other)
Const strCookieName = "WWF"


'Encrypted passwords
'This will make your forum unsecure from hackers if you disable this!!!!!
'This can NOT be changed once your forum is in use!!!
'You will also need to directly edit the database to type in the admin password to the Password field in the tblAuthor table at record position 1
'also edit both common.asp files to change this variable
Const blnEncryptedPasswords = true




'Create database connection
'Create a connection odject
Set adoCon = Server.CreateObject("ADODB.Connection")

'If this is access set the access driver
If strDatabaseType = "Access" Then



	'--------------------- Set the path and name of the database --------------------------------------------------------------------------------
	
	'Virtual path to database
	strDbPathAndName = Server.MapPath("database/wwForum.mdb")  'This is the path of the database from this files location on the server
	
	'Physical path to database
	'strDbPathAndName = "" 'Use this if you use the physical server path, eg:- C:\Inetpub\private\WebWizForum.mdb
	
	
	'BRINKSTER USERS (Web Wiz Forums only works with free Brinkster accounts, not for the paid accounts)
	'Brinkster users remove the ' single quote mark from infront of the line below and replace USERNAME with your Brinkster uersname
	
	'strDbPathAndName = Server.MapPath("/USERNAME/db/wwForum.mdb")
	
	'PLEASE NOTE: - For extra security it is highly recommended you change the name of the database, wwForum.mdb, to another name and then 
	'replace the wwForum.mdb found above with the name you changed the forum database to.
	
	'---------------------------------------------------------------------------------------------------------------------------------------------
	
	
	
	
	'------------- If you are having problems with the script then try using a diffrent driver or DSN by editing the lines below --------------
				 
	'Database connection info and driver (if this driver does not work then comment it out and use one of the alternative drivers)
	'strCon = "DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=" & strDbPathAndName
	
	'Alternative drivers faster than the basic one above
	'strCon = "Provider=Microsoft.Jet.OLEDB.3.51; Data Source=" & strDbPathAndName 'This one is if you convert the database to Access 97
	strCon = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strDbPathAndName  'This one is for Access 2000/2002
	
	'If you wish to use DSN then comment out the driver above and uncomment the line below (DSN is slower than the above drivers)
	'strCon = "DSN=DSN_NAME" 'Place the DSN where you see DSN_NAME
	
	'---------------------------------------------------------------------------------------------------------------------------------------------




	
	'The now() function is used in Access for dates
	strDatabaseDateFunction = "Now()"


'Else set the MS SQL server stuff
Else 	
	
	%><!--#include file="SQL_server_connection.asp" --><%
	
	'The GetDate() function is used in SQL Server
	strDatabaseDateFunction = "GetDate()"
End If


'Set the connection string to the database
adoCon.connectionstring = strCon

'Set an active connection to the Connection object
adoCon.Open






'Read in the users details from the form if user is logging in
strUsername = Trim(Mid(Request.Form("name"), 1, 15))
strPassword = LCase(Trim(Mid(Request.Form("password"), 1, 15)))


'Intialise the ADO recordset object
Set rsCommon = Server.CreateObject("ADODB.Recordset")


'If a username has been entered check that the password is correct
If strUsername <> "" AND Session("lngSecurityCode") = Trim(Mid(Request.Form("securityCode"), 1, 6)) Then
	
	
	'Check the users session ID for security from hackers
	Call checkSessionID(Request.Form("sessionID"))
	
	
	'Take out parts of the username that are not permitted
	strUsername = Replace(strUsername, "password", "", 1, -1, 1)
	strUsername = Replace(strUsername, "salt", "", 1, -1, 1)
	strUsername = Replace(strUsername, "author", "", 1, -1, 1)
	strUsername = Replace(strUsername, "code", "", 1, -1, 1)
	strUsername = Replace(strUsername, "username", "", 1, -1, 1)
	
	'Replace harmful SQL quotation marks with doubles
	strUsername = formatSQLInput(strUsername)
	
	'Read the various forums from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Password, " & strDbTable & "Author.Salt, " & strDbTable & "Author.Group_ID, " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.User_code "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Username = '" & strUsername & "';"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'If the query has returned a value to the recordset then check the password is correct
	If NOT rsCommon.EOF Then
		
		
		'Only encrypt password if this is enabled
		If blnEncryptedPasswords Then
			'Encrypt password so we can check it against the encypted password in the database
			'Read in the salt
			strPassword = strPassword & rsCommon("Salt")
	
			'Encrypt the entered password
			strPassword = HashEncode(strPassword)
		End If
		
	
		'Check the encrypted password is correct, if it is get the user ID and set a cookie
		If strPassword = rsCommon("Password") Then
			
			'Read in the users ID number and whether they want to be automactically logged in when they return to the forum
			strUsername = rsCommon("Username")
			lngLoggedInUserID = CLng(rsCommon("Author_ID"))
			strUserCode = rsCommon("User_code")
			intGroupID = CInt(rsCommon("Group_ID"))
			
			
			'Write a cookie with the User ID number so the user logged in throughout the forum	
			'Write the cookie with the name Forum containing the value UserID number
			Response.Cookies(strCookieName)("UID") = strUserCode
		End If
	End If
	
	'Reset Server Objects
	rsCommon.Close
End If
	



'Read in users ID number from the cookie
strLoggedInUserCode = Trim(Mid(Request.Cookies(strCookieName)("UID"), 1, 44))


'If a cookie exsists on the users system then read in there username from the database
If strLoggedInUserCode <> "" Then
	
	'Make the usercode SQL safe
	strLoggedInUserCode = formatSQLInput(strLoggedInUserCode)
	
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username, " & strDbTable & "Author.Group_ID "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE User_code = '" & strLoggedInUserCode & "';"
		
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'If there is a user with the ID number read in from the cookie then
	If NOT rsCommon.EOF Then
	
		'Read in the users details from the recordset
		strUsername = rsCommon("Username")
		lngLoggedInUserID = CLng(rsCommon("Author_ID"))
		intGroupID = CInt(rsCommon("Group_ID"))
	
	'Otherwise the username is not correct or the user has been barred so set there User ID to 0
	Else
		lngLoggedInUserID = 0
		intGroupID = 0
	End If
	
	'Reset server objects
	rsCommon.Close

End If


'If the user is not the admin or not logged in send them away
If intGroupID <> 1 Then 
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect "../insufficient_permission.asp"
End If




'Check license is agreed to
'Initialise the SQL variable with an SQL statement to get the configuration details from the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "SelectConfiguration"
Else
	strSQL = "SELECT TOP 1 " & strDbTable & "Configuration.* From " & strDbTable & "Configuration;"
End If
	
'Query the database
rsCommon.Open strSQL, adoCon

'If the license is not agreed to the  redirect
If Cbool(rsCommon("L_Code")) = true and Session("blnLicense") = false Then
	
	'Clean up
	rsCommon.Close
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect "webwizforums_license.asp"
End If

'Clean up
rsCommon.Close
%>
<!--#include file="functions/functions_filters.asp" -->
<!--#include file="functions/functions_common.asp" -->
<!--#include file="functions/functions_hash1way.asp" -->