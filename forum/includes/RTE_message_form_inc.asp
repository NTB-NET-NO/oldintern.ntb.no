<!--#include file="RTE_setup.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'If a private message go to pm post message page otherwise goto post message page
If strMode = "PM" Then
	strPostPage = "pm_post_message.asp"
Else
	strPostPage = "post_message.asp?PN=" & Trim(Mid(Request.Form("PN"), 1, 8))
End If


Response.Write(vbCrLf & "<script language=""JavaScript"" src=""RTE_message_form_js.asp"" type=""text/javascript""></script>" & _
vbCrLf & "<form method=""post"" name=""frmAddMessage"" action=""" & strPostPage & """ onSubmit=""return CheckForm();"" onReset=""return ResetForm();"">" & _
vbCrLf & " <table width=""610"" border=""0"" cellspacing=""0"" cellpadding=""1"" bgcolor=""" & strTableBorderColour & """ height=""230"" align=""center"">" & _
vbCrLf & "  <tr>" & _
vbCrLf & "   <td>" & _
vbCrLf & "    <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ height=""201"">" & _
vbCrLf & "     <tr>" & _
vbCrLf & "      <td>" & _
vbCrLf & "       <table width=""100%"" border=""0"" align=""center"" height=""233"" cellpadding=""2"" cellspacing=""0"">" & _
vbCrLf & "        <tr>" & _
vbCrLf & "         <td colspan=""2"" height=""30"" class=""text"" align=""left"">*" & strTxtRequiredFields & "</td>" & _
vbCrLf & "        </tr>")


'If the poster is in a guest then get them to enter a name
If lngLoggedInUserID = 2 AND (strMode <> "edit" AND strMode <> "editTopic") Then

	Response.Write(vbCrLf & "        <tr>" & _
	vbCrLf & "	 <td align=""right"" width=""15%"" class=""text"">" & strTxtName & "*:</td>" & _
	vbCrLf & "	 <td width=""70%"">" & _
	vbCrLf & "	  <input type=""text"" name=""Gname"" size=""20"" maxlength=""20"" />" & _
	vbCrLf & "	 </td>" & _
	vbCrLf & "	</tr>")
End If

'If this is a private message display the username box
If strMode = "PM" Then

	Response.Write(vbCrLf & "        <tr>" & _
	vbCrLf & "         <td align=""right"" width=""15%"" class=""text"">" & strTxtToUsername & "*:</td>" & _
	vbCrLf & "         <td width=""70%"" class=""text"">")


         'Get the users buddy list if they have one

	'Initlise the sql statement
	strSQL = "SELECT " & strDbTable & "Author.Username "
	strSQL = strSQL & "FROM " & strDbTable & "Author INNER JOIN " & strDbTable & "BuddyList ON " & strDbTable & "Author.Author_ID = " & strDbTable & "BuddyList.Buddy_ID "
	strSQL = strSQL & "WHERE " & strDbTable & "BuddyList.Author_ID=" & lngLoggedInUserID & " AND " & strDbTable & "BuddyList.Buddy_ID <> 2 "
	strSQL = strSQL & "ORDER By " & strDbTable & "Author.Username ASC;"

	'Query the database
	rsCommon.Open strSQL, adoCon

	Response.Write(vbCrLf & "          <input type=""text"" name=""member"" size=""15"" maxlength=""15"" value=""" & Server.HTMLEncode(strBuddyName) & """")
	If NOT rsCommon.EOF Then Response.Write(" onChange=""document.frmAddMessage.selectMember.options[0].selected = true;""")
	Response.Write(" />")
	Response.Write(vbCrLf & "          <a href=""JavaScript:openWin('pop_up_member_search.asp','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=255')""><img src=""" & strImagePath & "search.gif"" alt=""" & strTxtMemberSearch & """ border=""0"" align=""absmiddle""></a>")

	'If there are records returned then display the users buddy list
        If NOT rsCommon.EOF Then

	Response.Write(vbCrLf & "          <span class=""text"">" & strSelectFormBuddyList & ":</span>" & _
	vbCrLf & "          <select name=""selectMember"" onChange=""member.value=''"">" & _
	vbCrLf & "            <option value="""">-- " & strTxtNoneSelected & " --</option>")

          	'Loop throuhgn and display the buddy list
          	Do While NOT rsCommon.EOF

          		Response.Write("<option value=""" & rsCommon("Username") & """>" & rsCommon("Username") & "</option>")

           		'Move to next record in rs
           		rsCommon.MoveNext
           	Loop

	Response.Write(vbCrLf & "          </select>")

	Else
		Response.Write(vbCrLf & "	<input type=""hidden"" name=""selectMember"" value="""" />")
        End If

        'Reset server variables
	rsCommon.Close

	Response.Write(vbCrLf & "       </td>" & _
	 	       vbCrLf & "        </tr>")

End If

'If this is a new post or editing the first thread then display the subject text box
If strMode = "new" or strMode="editTopic" or strMode = "PM" or strMode = "poll" Then

	Response.Write("        <tr>" & _
	vbCrLf & "         <td align=""right"" class=""text"">" & strTxtSubjectFolder & "*:</td>" & _
	vbCrLf & "         <td width=""70%"">" & _
	vbCrLf & "          <input type=""text"" name=""subject"" size=""30"" maxlength=""41""")
	If strMode="editTopic" or strMode="PM" Then Response.Write(" value=""" & strTopicSubject & """")
	Response.Write(" />")

        'If this is the forums moderator or forum admim then let them slect the priority level of the post
	If (blnAdmin = True OR blnPriority = True) AND (strMode = "new" or strMode="editTopic" or strMode = "poll") Then

		Response.Write("          <span class=""text"">&nbsp;" & strTxtPriority & ":" & _
		vbCrLf & "          <select name=""priority"">" & _
		vbCrLf & "           <option value=""0""")
		If intTopicPriority = 0 Then Response.Write(" selected")
		Response.Write(">" & strTxtNormal & "</option>" & _
		vbCrLf & "           <option value=""1""")
		If intTopicPriority = 1 Then Response.Write(" selected")
		Response.Write(">" & strTxtPinnedTopic & "</option>")

         	'If this is the forum admin or moderator let them post an annoucment to this forum
         	If blnAdmin = True OR blnModerator Then

			Response.Write(vbCrLf & "           <option value=""2""")
			If intTopicPriority = 2 Then Response.Write(" selected")
			Response.Write(""">" & strTopThisForum & "</option>")

        	End If

         	'If this is the forum admin let them post a priority post to all forums
         	If blnAdmin = True Then

			Response.Write(vbCrLf & "           <option value=""3""")
			If intTopicPriority = 3 Then Response.Write(" selected")
			Response.Write(""">" & strTxtTopAllForums & "</option>")

		End If

		Response.Write("          </select>" & _
		vbCrLf & "          </span>")

	End If

	Response.Write("         </td>" & _
	vbCrLf & "        </tr>")

End If

'If this is a new poll then display space to enter the poll
If strMode = "poll" Then

	%><!--#include file="poll_form_inc.asp" --><%

End If


Response.Write("	<tr>" & _
vbCrLf & "         <td valign=""bottom"" align=""right"" width=""15%"">&nbsp;</td>" & _
vbCrLf & "         <td width=""70%"" valign=""bottom"">" & _
vbCrLf & "          <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & _
vbCrLf & "           <tr>" & _
vbCrLf & "            <td>" & _
vbCrLf & "             <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""2"">" & _
vbCrLf & "              <tr>" & _
vbCrLf & "               <td>")


'RTE Tool Bar 1
'---------------------------------------------------------------------------

%><!--#include file="RTE_toolbar_1.asp" --><%

'---------------------------------------------------------------------------

Response.Write("               </td>" & _
vbCrLf & "              </tr>" & _
vbCrLf & "              <tr>" & _
vbCrLf & "               <td>")


'RTE Tool Bar 2
'---------------------------------------------------------------------------

%><!--#include file="RTE_toolbar_2.asp" --><%

'---------------------------------------------------------------------------



Response.Write(vbCrLf & "		  <iframe width=""260"" height=""165"" id=""colourPalette"" src=""RTE_colour_palette.asp"" style=""visibility:hidden; position: absolute; left: 0px; top: 0px;"" frameborder=""0"" scrolling=""no""></iframe>" & _
vbCrLf & "	       </td>" & _
vbCrLf & "	      </tr>" & _
vbCrLf & "             </table>" & _
vbCrLf & "            </td>" & _
vbCrLf & "           </tr>" & _
vbCrLf & "          </table>" & _
vbCrLf & "         </td>" & _
vbCrLf & "        </tr>" & _
vbCrLf & "        <tr>" & _
vbCrLf & "         <td valign=""top"" align=""right"" height=""61"" width=""15%"" class=""text"">" & strTxtMessage & "*:")


'*************** Emoticons *******************

'If emoticons are enabled show them next to the post window
If blnEmoticons Then

Response.Write(vbCrLf & "         <table border=""0"" cellspacing=""0"" cellpadding=""4"" align=""center"">" & _
vbCrLf & "          <tr><td class=""smText"" colspan=""3"" align=""center""><br />" & strTxtEmoticons & "</td></tr>")

	'Intilise the index position (we are starting at 1 instead of position 0 in the array for simpler calculations)
	intIndexPosition = 1

	'Calcultae the number of outer loops to do
	intNumberOfOuterLoops = 4

	'If there is a remainder add 1 to the number of loops
	If UBound(saryEmoticons) MOD 2 > 0 Then intNumberOfOuterLoops = intNumberOfOuterLoops + 1

	'Loop throgh th list of emoticons
	For intLoop = 1 to intNumberOfOuterLoops


	        Response.Write("<tr>")

		'Loop throgh th list of emoticons
		For intInnerLoop = 1 to 3

			'If there is nothing to display show an empty box
			If intIndexPosition > UBound(saryEmoticons) Then
				Response.Write(vbCrLf & "		<td class=""text"">&nbsp;</td>")

			'Else show the emoticon
			Else
				Response.Write(vbCrLf & "		<td><img src=""" & saryEmoticons(intIndexPosition,3) & """ border=""0"" alt=""" & saryEmoticons(intIndexPosition,1) & """ OnClick=""AddSmileyIcon('" & saryEmoticons(intIndexPosition,3) & "')"" style=""cursor: pointer;""></td>")
	              	End If

	              'Minus one form the index position
	              intIndexPosition = intIndexPosition + 1
		Next

		Response.Write("</tr>")

	Next

	Response.Write(vbCrLf & "         <tr><td colspan=""3"" align=""center""><a href=""javascript:openWin('RTE_emoticons_smilies.asp','emot','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=400,height=400')"" class=""smLink"">" & strTxtMore & "</a></td></tr>")
	Response.Write(vbCrLf & "         </table></td>")
End If

'******************************************



Response.Write("         <td height=""61"" width=""70%"" valign=""top"">")




'This bit creates a random number to add to the end of the Iframe link as IE will cache the page
'Randomise the system timer
Randomize Timer

Response.Write(vbCrLf & "          <script language=""javascript"">" & _
vbCrLf & "		  //Create an iframe" & _
vbCrLf & "		  document.write ('<iframe id=""message"" src=""RTE_textbox.asp?mode=" & strMode & "&POID=" & lngMessageID & "&ID=" & CInt(RND * 2000) & """ width=""700"" height=""400"" onMouseOver=""hideColourPallete()""></iframe>')")

If RTEenabled = "winIE" OR RTEenabled = "winIE5" Then Response.Write(vbCrLf & "		  frames.message.document.designMode = 'On';")

Response.Write(vbCrLf & "          </script>" & _
vbCrLf & "          <!-- Display a message for RTE users with JavaScript turned off -->" & _
vbCrLf & "          <noscript><span class=""bold""><br /><br />" & strTxtJavaScriptEnabled & "</span></noscript></td>" & _
vbCrLf & "        </tr>" & _
vbCrLf & "        <tr>" & _
vbCrLf & "	 <td align=""right"" width=""92"">&nbsp;</td>" & _
vbCrLf & "	 <td width=""508"" valign=""bottom"" class=""text"">&nbsp;<input type=""checkbox"" name=""forumCodes"" value=""True"" checked />" & strTxtEnable & " <a href=""JavaScript:openWin('forum_codes.asp','codes','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=550,height=400')"">" & strTxtForumCodes & "</a> " & strTxtToFormatPosts & _
vbCrLf & "	 </td></tr>")





'If not PM then display another row
If strMode <> "PM" Then

	'If signature of e-mail notify then display row to show
	If (blnLoggedInUserEmail = True AND blnEmail = True) OR blnLoggedInUserSignature = True Then

		Response.Write(vbCrLf & "        <tr>" & _
		vbCrLf & "         <td align=""right"" height=""7"" width=""92"">&nbsp;</td>" & _
		vbCrLf & "         <td height=""7"" width=""508"" valign=""bottom"" class=""text"">")

		'If the user has a signature offer them the chance to show it
		If blnLoggedInUserSignature Then

			Response.Write(vbCrLf & "		&nbsp;<input type=""checkbox"" name=""signature"" value=""True""")
			If blnAttachSignature = True Then Response.Write(" checked")
			Response.Write(" />" & strTxtShowSignature & "&nbsp;")

		End If

		'Display e-mail notify of replies option
		If blnEmail AND blnLoggedInUserEmail Then

			Response.Write(vbCrLf & "		&nbsp;<input type=""checkbox"" name=""email"" value=""True""")
			If blnReplyNotify Then Response.Write(" checked")
			Response.Write(" />" & strTxtEmailNotify & "&nbsp;")

		End If


	Response.Write(vbCrLf & "         </td>" & _
	vbCrLf & "        </tr>")


	End If

'If this is a private e-mail and e-mail is on and the user gave an e-mail address let them choose to be notified when pm msg is read
ElseIf strMode = "PM" AND blnEmail AND blnLoggedInUserEmail Then

	Response.Write(vbCrLf & "	<tr>" & _
	vbCrLf & "         <td align=""right"" width=""92"">&nbsp;</td>" & _
	vbCrLf & "         <td width=""508"" valign=""bottom"" class=""text"">&nbsp;<input type=""checkbox"" name=""email"" value=""True""><span class=""text"">" & strTxtEmailNotifyWhenPMIsRead & "</span></td>" & _
	vbCrLf & "        </tr>")
End If

Response.Write(vbCrLf & "        <td>" & _
vbCrLf & "         <input type=""hidden"" name=""message"" value="""">")

If strMode <> "PM" Then

         Response.Write(vbCrLf & "	<input type=""hidden"" name=""mode"" value=""" & strMode & """ />" & _
	 vbCrLf & "	<input type=""hidden"" name=""FID"" value=""" & intForumID & """ />" & _
	 vbCrLf & "	<input type=""hidden"" name=""TID"" value=""" & lngTopicID & """ />" & _
	 vbCrLf & "	<input type=""hidden"" name=""PID"" value=""" & lngMessageID & """ />" & _
	 vbCrLf & "	<input type=""hidden"" name=""TPN"" value=""" & intRecordPositionPageNum & """ />")
         'If reply get the thread position number in the topic
         If strMode = "reply" Then
         	Response.Write(vbCrLf & "		<input type=""hidden"" name=""ThreadPos"" value=""" & (intTotalNumOfThreads + 1) & """ />")
	End If
End If

         Response.Write(vbCrLf & "	<input type=""hidden"" name=""browser"" value=""RTE"" />" & _
         vbCrLf & "	<input type=""hidden"" name=""sessionID"" value=""" & Session.SessionID & """ />&nbsp;" & _
         vbCrLf & "    </td>" & _
         vbCrLf & "    <td height=""2"" width=""70%"" align=""left"">" & _
         vbCrLf & "	<p>")

Dim strGetMessageBoxHTML

'Set how to get the HTML form the message box for Win IE5 and then for other RTE browsers
If RTEenabled = "winIE5" Then strGetMessageBoxHTML = "frames.message.document.body.innerHTML;" Else strGetMessageBoxHTML = "document.getElementById('message').contentWindow.document.body.innerHTML;"

'Select the correct button for the page
If strMode="edit" OR strMode = "editTopic" Then

	Response.Write(vbCrLf & "          <input type=""submit"" name=""Submit"" value=""" & strTxtUpdatePost & """ OnClick=""document.frmAddMessage.message.value = " & strGetMessageBoxHTML & """ tabindex=""30"" />")

ElseIf strMode = "new" OR strMode = "poll" Then

	Response.Write(vbCrLf & "          <input type=""submit"" name=""Submit"" value=""" & strTxtNewTopic & """ OnClick=""document.frmAddMessage.message.value = " & strGetMessageBoxHTML & """ tabindex=""30"" />")

ElseIf strMode = "PM" Then

	Response.Write(vbCrLf & "          <input type=""submit"" name=""Submit"" value=""" & strTxtPostMessage & """ OnClick=""document.frmAddMessage.message.value = " & strGetMessageBoxHTML & """ tabindex=""30"" />")
Else

	Response.Write(vbCrLf & "          <input type=""submit"" name=""Submit"" value=""" & strTxtPostReply & """ OnClick=""document.frmAddMessage.message.value = " & strGetMessageBoxHTML & """ tabindex=""30"" />")
End If

Response.Write(vbCrLf & "          <input type=""button"" name=""Preview"" value=""" & strTxtPreviewPost & """ onClick=""document.frmAddMessage.message.value = " & strGetMessageBoxHTML & " OpenPreviewWindow('post_preview.asp', document.frmAddMessage);"" />" & _
vbCrLf & "          <input type=""reset"" name=""Reset"" value=""" & strTxtClearForm & """ />" & _
vbCrLf & "         </p>" & _
vbCrLf & "        </td>" & _
vbCrLf & "        </tr>" & _
vbCrLf & "       </table>" & _
vbCrLf & "      </td>" & _
vbCrLf & "     </tr>" & _
vbCrLf & "    </table>" & _
vbCrLf & "   </td>" & _
vbCrLf & "  </tr>" & _
vbCrLf & " </table>" & _
vbCrLf & "</form>")
%>