<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<!--#include file="functions/functions_format_post.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"

'Declare variables
Dim rsPmMessage			'ADO recordset object holding the users private messages
Dim lngPmMessageID		'Private message id
Dim strPmSubject 		'Holds the subject of the private message
Dim strMessage			'Holds the message body of the thread
Dim lngMessageID		'Holds the message ID number
Dim lngFromUserID		'Holds the from user ID
Dim lngToUserID			'Holds the to user ID
Dim dtmTopicDate		'Holds the date the thread was made
Dim strUsername 		'Holds the Username of the thread
Dim strAuthorHomepage		'Holds the homepage of the Username if it is given
Dim strAuthorLocation		'Holds the location of the user if given
Dim strAuthorAvatar		'Holds the authors avatar	
Dim lngAuthorNumOfPosts		'Holds the number of posts the user has made to the forum
Dim dtmAuthorRegistration	'Holds the registration date of the user
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intTopicPageLoopCounter	'Holds the number of pages there are of pm messages
Dim strEmailBody		'Holds the body of the e-mail message
Dim strEmailSubject		'Holds the subject of the e-mail
Dim blnEmailSent		'set to true if an e-mail is sent
Dim intForumID			'Holds the forum ID
Dim strGroupName		'Holds the authors group name
Dim intRankStars		'Holds the number of stars for the group
Dim strMemberTitle		'Holds the members title
Dim strRankCustomStars		'Holds custom stars for the user group

'Raed in the pm mesage number to display
lngPmMessageID = CLng(Request.QueryString("ID"))

'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then Response.Redirect("default.asp")

'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then Response.Redirect("insufficient_permission.asp")



'Intialise the ADO recordset object
Set rsPmMessage = Server.CreateObject("ADODB.Recordset")
	
'Initlise the sql statement
strSQL = "SELECT " & strDbTable & "PMMessage.*, " & strDbTable & "Author.Username, " & strDbTable & "Author.Homepage, " & strDbTable & "Author.Location, " & strDbTable & "Author.Author_email, " & strDbTable & "Author.No_of_posts, " & strDbTable & "Author.Join_date, " & strDbTable & "Author.Signature, " & strDbTable & "Author.Active, " & strDbTable & "Author.Avatar, " & strDbTable & "Author.Avatar_title, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "PMMessage, " & strDbTable & "Group "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "PMMessage.From_ID AND " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "PMMessage.PM_ID=" & lngPmMessageID & " "
'If this is a link from the out box then check the from author ID to check the user can view the message
If Request.QueryString("M") = "OB" Then
	strSQL = strSQL & " AND " & strDbTable & "PMMessage.From_ID=" & lngLoggedInUserID & ";"
'Else use the to author ID to check the user can view the message
Else
	strSQL = strSQL & " AND " & strDbTable & "PMMessage.Author_ID=" & lngLoggedInUserID & ";"
End If

'Query the database
rsPmMessage.Open strSQL, adoCon


'If a mesage is found then send a mail if the sender wants notifying
If NOT rsPmMessage.EOF Then 
	
	'Read in some of the details
	strPmSubject = rsPmMessage("PM_Tittle")
	strUsername = rsPmMessage("Username")
	
	'If the sender wants notifying then send a mail as long as e-mail notify is on and the message hasn't already been read
	If CBool(rsPmMessage("Email_notify")) AND rsPmMessage("Author_email") <> "" AND blnEmail AND CBool(rsPmMessage("Read_Post")) = False AND Request.QueryString("M") <> "OB" Then
		
		'Set the subject
		strEmailSubject = strMainForumName & " " & strTxtNotificationPM
	
		'Initailise the e-mail body variable with the body of the e-mail
		strEmailBody = strTxtHi & " " & decodeString(strUsername) & ","
		strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtThisIsToNotifyYouThat & " " & strLoggedInUsername & " " & strTxtHasReadPM & ", '" & decodeString(strPmSubject) & "', " & strTxtYouSentToThemOn & " " & strMainForumName & "."
		
		'Call the function to send the e-mail
		blnEmailSent = SendMail(strEmailBody, decodeString(strUsername), decodeString(rsPmMessage("Author_email")), strMainForumName, decodeString(strForumEmailAddress), strEmailSubject, strMailComponent, false)
	
	End If
End If

'If this is not from the outbox then update the read field
If Request.QueryString("M") <> "OB" Then
	'Inittilise the sql veriable to update the database
	strSQL = "UPDATE " & strDbTable & "PMMessage SET " & strDbTable & "PMMessage.Read_Post = 1 WHERE " & strDbTable & "PMMessage.PM_ID=" & lngPmMessageID & ";"

	'Execute the sql statement to set the pm to read
	adoCon.Execute(strSQL)
End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Private Messenger: <% = strPmSubject %></title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtPrivateMessenger %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtPrivateMessenger %><br /></td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="2" align="center">
 <tr> 
  <td width="60%"><span class="lgText"><img src="<% = strImagePath %>subject_folder.gif" alt="<% = strTxtSubjectFolder %>" align="absmiddle"> <% = strTxtPrivateMessenger %></span></td>
  <td align="right" width="40%" nowrap="nowrap"><a href="pm_inbox.asp" target="_self"><img src="<% = strImagePath %>inbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtInbox %>" border="0"></a><a href="pm_outbox.asp" target="_self"><img src="<% = strImagePath %>outbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtOutbox %>" border="0"></a><a href="pm_buddy_list.asp" target="_self"><img src="<% = strImagePath %>buddy_list.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtBuddyList %>" border="0"></a><a href="pm_new_message_form.asp" target="_self"><img src="<% = strImagePath %>new_private_message.gif" alt="<% = strTxtNewPrivateMessage %>" border="0"></a></td>
 </tr>
 <tr> 
  <td colspan="2"><span class="lgText"><% = strTxtSubjectFolder & ": " & strPmSubject %></span></td>
 </tr>
</table><%

'If no private message display an error
IF rsPmMessage.EOF Then
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="166">
 <tr>
  <td align="center" class="heading" height="120"><% = strTxtYouDoNotHavePermissionViewPM %></td>
 </tr>
</table><%

'Else display the message
Else
	'Read in threads details for the topic from the database
	strMessage = rsPmMessage("PM_Message")
	lngFromUserID = CLng(rsPmMessage("From_ID"))
	lngToUserID = CLng(rsPmMessage("Author_ID"))
	dtmTopicDate = CDate(rsPmMessage("PM_Message_Date")) 
	strAuthorHomepage = rsPmMessage("Homepage")
	strAuthorLocation = rsPmMessage("Location")
	dtmAuthorRegistration = CDate(rsPmMessage("Join_date"))
	lngAuthorNumOfPosts = CLng(rsPmMessage("No_of_posts"))
	strAuthorAvatar = rsPmMessage("Avatar")
	strGroupName = rsPmMessage("Name")
	intRankStars = CInt(rsPmMessage("Stars"))
	strMemberTitle = rsPmMessage("Avatar_title")
	strRankCustomStars = rsPmMessage("Custom_stars")
	
	
	'If the pm contains a quote or code block then format it
	If InStr(1, strMessage, "[QUOTE=", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatUserQuote(strMessage)
	If InStr(1, strMessage, "[QUOTE]", 1) > 0 AND InStr(1, strMessage, "[/QUOTE]", 1) > 0 Then strMessage = formatQuote(strMessage)
	If InStr(1, strMessage, "[CODE]", 1) > 0 AND InStr(1, strMessage, "[/CODE]", 1) > 0 Then strMessage = formatCode(strMessage)


	'If the pm contains a flash link then format it
	If blnFlashFiles Then
		If InStr(1, strMessage, "[FLASH", 1) > 0 AND InStr(1, strMessage, "[/FLASH]", 1) > 0 Then strMessage = formatFlash(strMessage)
	End If

%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTablePostsBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTablePMBgColour %>">
    <tr> 
     <td bgcolor="<% = strTablePMBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14">
       <tr> 
        <td bgcolor="<% = strTablePMTitleColour %>" width="145" class="tHeading" background="<% = strTablePMTitleBgImage %>" nowrap="nowrap"><% = strTxtAuthor %></td>
        <td bgcolor="<% = strTablePMTitleColour %>" width="82%" class="tHeading" background="<% = strTablePMTitleBgImage %>" nowrap="nowrap"><% = strTxtMessage %></td>
       </tr>
       <tr> 
        <td valign="top" background="<% = strTablePMBgImage %>" bgcolor="<% = strTablePMBoxSideBgColour %>" class="smText"> 
         <span class="bold"><a name="<% = lngMessageID %>"></a><% = strUsername %></span><br /> <span class="smText"><% = strGroupName %><br /><%
         	
         	Response.Write(vbCrLf & "         <img src=""")
		If strRankCustomStars <> "" Then Response.Write(strRankCustomStars) Else Response.Write(strImagePath & intRankStars & "_star_rating.gif")
		Response.Write(""" alt=""" & strGroupName & """><br />")
         
         
	        'If the user has an avatar then display it
	        If blnAvatar = True AND strAuthorAvatar <> "" Then 
	        	
	        	Response.Write("<img src=""" & strAuthorAvatar & """ width=""" & intAvatarWidth & """ height=""" & intAvatarHeight & """ alt=""" & strTxtAvatar & """ OnError=""this.src='avatars/blank.gif', height='0';"">")
	       	End If
	       	
	       	'If there is a title for this member then display it
	       	If strMemberTitle <> "" Then Response.Write(vbCrLf & "<br />" & strMemberTitle)
	       	
          		Response.Write("<br /><br />")
          		Response.Write(strTxtJoined & ": " & DateFormat(dtmAuthorRegistration, saryDateTimeData))
          		Response.Write("<br />")
          		Response.Write(strTxtPosts & ": " & lngAuthorNumOfPosts) 
         
         	'If the is a location display it
         	If strAuthorLocation <> "" Then
         		
         		Response.Write("<br />" & strTxtLocation & ": " & strAuthorLocation)
         	End If
         	
         	%></span></td>
        <td valign="top" background="<% = strTablePMBgImage %>" bgcolor="<% = strTablePMBoxBgColour %>" class="text"> 
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
           <td width="60%" class="smText"><% Response.Write(strTxtSent & ": " & DateFormat(dtmTopicDate, saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(dtmTopicDate, saryDateTimeData)) %></td>
           <td width="30%" align="right"><%
           	
           	'If the person reading the pm is the recepient disply delete and reply buttons
      		If lngToUserID = lngLoggedInUserID Then
      			
      			%>
           <a href="pm_delete_message.asp?pm_id=<% = rsPmMessage("PM_ID") %>" OnClick="return confirm('<% = strTxtDeletePrivateMessageAlert %>')"><img src="<% = strImagePath %>delete_sm.gif" border="0" align="absmiddle" alt="<% = strTxtDelete %>"></a> 
            <a href="pm_new_message_form.asp?code=reply&pm=<% = rsPmMessage("PM_ID") %>"><img src="<% = strImagePath %>pm_reply.gif" align="absmiddle" border="0" alt="<% = strTxtReplyToPrivateMessage %>"></a><%
            
        	End If
            %></td>
          </tr>
          <tr> 
           <td colspan="2"><hr /></td>
          </tr>
         </table>
         <!-- Message body -->
         <% = strMessage %>
         <!-- Message body ''"" -->
        </td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTablePMBoxSideBgColour %>" background="<% = strTablePMBgImage %>">&nbsp;</td>
        <td bgcolor="<% = strTablePMBoxBgColour %>" background="<% = strTablePMBgImage %>" class="text"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngFromUserID %>&FID=<% = intForumID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"><img src="<% = strImagePath %>profile_icon.gif" border="0" alt="<% = strTxtView %>&nbsp;<% = strUsername %>'s&nbsp;<% = strTxtProfile %>" align="absmiddle"></a> 
         <a href="search_form.asp?KW=<% = Server.URLEncode(strUsername) %>&SI=AR&FID=0"><img src="<% = strImagePath %>search_sm.gif" border="0" alt="<% = strTxtSearchForPosts %>&nbsp;<% = strUsername %>" align="absmiddle"></a><% 
         
            	'If the user has a hompeage put in a link button
      		If strAuthorHomepage <> "" Then
	%>
         <a href="<% = strAuthorHomepage %>" target="_blank"><img src="<% = strImagePath %>home_icon.gif" border="0" alt="<% = strTxtVisit & " " & strUsername & "'s " & strTxtHomepage %>" align="absmiddle"></a> 
         <%
      		
      		End If
      
            	'If the private msg's are on then display a link to enable use to send them a msg 
            	If blnPrivateMessages = True Then 
        
         %>
         <a href="pm_buddy_list.asp?name=<% = Server.URLEncode(strUsername) %>" target="_self"><img src="<% = strImagePath %>add_buddy_sm.gif" align="absmiddle" border="0" alt="<% = strTxtAddToBuddyList %>"></a> 
         <% 
      		
      		End If
      		
      		'If the person reading the pm is the recepient disply delete and reply buttons
      		If lngToUserID = lngLoggedInUserID Then
%>
         <a href="pm_delete_message.asp?pm_id=<% = rsPmMessage("PM_ID") %>" OnClick="return confirm('<% = strTxtDeletePrivateMessageAlert %>')"><img src="<% = strImagePath %>delete_sm.gif" border="0" alt="<% = strTxtDelete %>" align="absmiddle"></a>
	 <a href="pm_new_message_form.asp?code=reply&pm=<% = rsPmMessage("PM_ID") %>"><img src="<% = strImagePath %>pm_reply.gif" align="absmiddle" border="0" alt="<% = strTxtReplyToPrivateMessage %>"></a><%
	 
		End If
	 %></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table><%

	'If the user has an email address and emailing is enabled then allow user to receive this pm by email
	If blnLoggedInUserEmail AND blnEmail Then
	
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td><a href="pm_email_pm.asp?ID=<% = lngPmMessageID %><% If Request.QueryString("M") = "OB" Then Response.Write("&M=OB")%>"><% = strTxtEmailThisPMToMe %></a></td>
</tr>
</table><%

	End If

Response.Write("<br />")


End If

Response.Write(vbCrLf & "<div align=""center""><br />")

'Clear server objects
rsPmMessage.Close
Set rsPmMessage = Nothing
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
 <br />
</div>
<%
'Display a msg letting the user know they have been emailed a private message
If Request.QueryString("ES") = "True" Then
	Response.Write("<script  language=""JavaScript"">")
	Response.Write("alert('" & strTxtAnEmailWithPM & " " & strTxtBeenSent & ".');")
	Response.Write("</script>")
ElseIf Request.QueryString("ES") = "False" Then
	Response.Write("<script  language=""JavaScript"">")
	Response.Write("alert('" & strTxtAnEmailWithPM & " " & strTxtNotBeenSent & ".');")
	Response.Write("</script>")
End If
%>
<!-- #include file="includes/footer.asp" -->