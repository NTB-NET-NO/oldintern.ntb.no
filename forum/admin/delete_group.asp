<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Set the timeout of the page
Server.ScriptTimeout =  1000


'Dimension variables
Dim intStartGroupID		'Holds the strating group ID
Dim intDeleteUserGroupID	'Holds the group ID to be deleted

'Get the forum ID to delete
intDeleteUserGroupID = CInt(Request.QueryString("GID"))


'If there is a group ID to delete then do teh job
If intDeleteUserGroupID <> "" Then
	
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "SELECT " & strDbTable & "Group.Group_ID FROM " & strDbTable & "Group WHERE " & strDbTable & "Group.Starting_group = 1;"
	Else
		strSQL = "SELECT " & strDbTable & "Group.Group_ID FROM " & strDbTable & "Group WHERE " & strDbTable & "Group.Starting_group = True;"
	End If
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'Read in the strating group ID
	intStartGroupID = CInt(rsCommon("Group_ID"))




	'Initalise the SQL string with an SQL update command to update the starting group
	strSQL = "UPDATE " & strDbTable & "Author SET "_
	 	 & "" & strDbTable & "Author.Group_ID = " & intStartGroupID & " "_
	         & "WHERE " & strDbTable & "Author.Group_ID = " & intDeleteUserGroupID & ";"

	'Write the updated number of posts to the database
	adoCon.Execute(strSQL)



	'Delete the group permissions for forums form the database
	strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE " & strDbTable & "Permissions.Group_ID ="  & intDeleteUserGroupID & ";"

	'Write to database
	adoCon.Execute(strSQL)
	


	'Delete the group form the database
	strSQL = "DELETE FROM " & strDbTable & "Group WHERE " & strDbTable & "Group.Group_ID ="  & intDeleteUserGroupID & ";"

	'Write to database
	adoCon.Execute(strSQL)


	'Reset Server Objects
	rsCommon.Close
End If



'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'Return to the forum categories page
Response.Redirect("view_groups.asp")
%>