<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>

<xsl:template match="/nitf">
<html>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<body class='viewBody'>

	<div class="viewHeadline"><xsl:value-of select="body/body.head/hedline/hl1"/></div>
	<xsl:apply-templates select="body/body.content/media[@media-type='audio']"/>

	<xsl:apply-templates select="head/docdata/ed-msg"/>
	
	<xsl:apply-templates select="body/body.head/byline"/>

	<div class="viewDate">
		Publisert:
		<xsl:value-of select="substring(head/pubdata/@date.publication, 7, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 5, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 1, 4)"/>
		&#160;<xsl:value-of select="substring(head/pubdata/@date.publication, 10, 2)"/>:<xsl:value-of select="substring(head/pubdata/@date.publication, 12, 2)"/>
	</div>
	
	<xsl:if test="body/body.content/media[@media-type='image' and not(@class)]">
		<table>
			<tr>
				<xsl:apply-templates select="body/body.content/media[@media-type='image' and not(@class)]"/>
			</tr>
		</table>
	</xsl:if>

	<xsl:apply-templates select="body/body.content"/>
	
	<xsl:apply-templates select="body/body.end/tagline"/>

</body>
</html>
</xsl:template>


<!-- Templates -->

<xsl:template match="head/docdata/ed-msg">
	<xsl:if test="normalize-space(@info)!=''">
	<div class="viewEdMsg">
	Til red:
	<xsl:value-of select="@info"/>
	</div>
	</xsl:if>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:choose>
	<xsl:when test="p">
		<xsl:apply-templates select="p | hl2 | table | br"/>
	</xsl:when>	
	<xsl:otherwise>
		<div class='viewNews'><xsl:value-of select="."/></div>
	</xsl:otherwise>
	</xsl:choose>
		
</xsl:template>

<xsl:template match="body/body.head/byline/*">
	<div class='viewByline'><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[.!='']">
	<!-- Normal paragraphs -->
	<div class="viewNews"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[.='']">
	<!-- Empty paragraphs -->
	<br/>
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
	<!-- Paragraph of "ingress" -->
	<div class="viewIngress"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[@innrykk='true']">
	<!-- Paragraph of "Br�dtekst innrykk" -->
	<div class="viewIndent"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[@style='tabellkode']">
	<!-- Paragraph of "tabellkode" -->
	<div class="viewTableHead"><xsl:value-of select="."/></div>
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
	<!-- Normal paragraphs -->
	<div class="viewNews"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="hl2">
	<!-- Mellomtittel -->
	<div class="viewHl2"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="table">
	<!-- Tabeller -->
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>

<xsl:template match="tagline">
	<!-- Article author e-mail (in NTB used as signature) -->
	<div class="viewTagline">
	<xsl:choose>
	<xsl:when test="a/@href">
	<a><xsl:attribute name="href">mailto:<xsl:value-of select="a"/>
		?subject=Ang�ende: <xsl:value-of select="//body/body.head/hedline/hl1"/>
		</xsl:attribute>
		<xsl:value-of select="a"/></a>
	</xsl:when>		
	<xsl:otherwise>
	<xsl:value-of select="a"/>		
	</xsl:otherwise>
	</xsl:choose>
	</div>
</xsl:template> 

<xsl:template match="body/body.content/media[@media-type='image' and not(@class)]">
	<!-- Template for Scanpix media	-->
		<td>
			<img border="0"><xsl:attribute name="src">http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_<xsl:value-of select="media-reference/@source"/></xsl:attribute></img>
		</td>
</xsl:template>

<xsl:template match="body/body.content/media[@media-type='audio']">
	<!--
		Hardcoded paths for Image and JavaScript (complete URL is included in NTB's NITF)
	-->
<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js"></script>
<script language="javascript">var lyd = new FlashSound();</script>
	<a href="javascript://">
		<xsl:attribute name="onmouseover">lyd.TGotoAndPlay('/','start')</xsl:attribute>
		<xsl:attribute name="onmouseout">lyd.TGotoAndPlay('/','stop')</xsl:attribute>
		<img src="../images/sound_large.gif" border="0" align="right"/>
	</a>
<script>lyd.embedSWF("<xsl:value-of select="media-reference[@mime-type='application/x-shockwave-flash']/@source"/>");</script>

</xsl:template>


</xsl:stylesheet>