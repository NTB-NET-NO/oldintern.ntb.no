<%@ Outputcache Duration="30" VaryByParam="section"%>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="intranett.leftmenu2" CodeFile="leftmenu.ascx.vb" %>
<script type="text/javascript" src="includes/Menu.js"></script>

<!-- this is the submenus -->
<%= subitems %>
<!-- end submenus -->

<!-- this is the menu -->
<asp:datalist CSSClass="artikkel_tekst" id="dlMenu" runat="server" CellPadding="0" CellSpacing="0"
	RepeatLayout="Table">
	<ItemTemplate>
		<asp:label ID="label" Visible="false" runat="server" onmouseover="ShowLayer()">
			<%# lmdr("LinkName") %>
		</asp:label>
		<asp:label ID="link" Visible="false" runat="server">
			<A class=leftmenu href='<%# lmdr("URL") %>' target='<%# lmdr("Target") %>' onmouseover="ShowLayer('sub<%# lmdr("SubMenu") %>')">
				<%# lmdr("LinkName") %>
			</A>&nbsp;<FONT color="red"><B>
					<%# lmdr("IsNewText")%>
				</B></FONT>
		</asp:label>
	</ItemTemplate>
</asp:datalist>

