<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_hash1way.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True


'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"




'Dimension variables
Dim strUsername			'Holds the users username
Dim strPassword			'Holds the usres password
Dim blnAutoLogin		'Holds whether the user wnats to be automactically logged in
Dim lngUserID			'Holds the users Id number
Dim strUserCode			'Holds the users ID code
Dim intForumID			'Holds the forum ID
Dim lngLoopCounter		'Holds the loop counter
Dim blnIncorrectLogin		'Set to true if login is incorrect
Dim blnSecurityCodeOK		'Set to false if the security is not OK
Dim strReferer			'Holds the page to return to



'Intialise variables
blnAutoLogin = false
blnIncorrectLogin = false
blnSecurityCodeOK = true



'read in the forum ID number
If isNumeric(Request.QueryString("FID")) Then
	intForumID = CInt(Request.QueryString("FID"))
Else
	intForumID = 0
End If



'Read in the users details from the form
strUsername = Trim(Mid(Request.Form("name"), 1, 15))
strPassword = LCase(Trim(Mid(Request.Form("password"), 1, 15)))
blnAutoLogin = CBool(Request.Form("AutoLogin"))


'Take out parts of the username that are not permitted
strUsername = Replace(strUsername, "password", "", 1, -1, 1)
strUsername = Replace(strUsername, "salt", "", 1, -1, 1)
strUsername = Replace(strUsername, "author", "", 1, -1, 1)
strUsername = Replace(strUsername, "code", "", 1, -1, 1)
strUsername = Replace(strUsername, "username", "", 1, -1, 1)

'Replace harmful SQL quotation marks with doubles
strUsername = formatSQLInput(strUsername)




'If a username has been entered check that the password is correct
If (strUsername <> "" AND Request.Form("QUIK") = false) OR (Request.Form("QUIK") AND blnLongSecurityCode = false AND strUsername <> "") Then
	
	'Check the users session ID for security from hackers if the user code has been disabled
	If blnLongSecurityCode = False Then Call checkSessionID(Request.Form("sessionID"))
	
	'Check security code to pervent hackers
	If Session("lngSecurityCode") <> Trim(Mid(Request.Form("securityCode"), 1, 6)) AND blnLongSecurityCode Then blnSecurityCodeOK = False

	'Read the various forums from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Password, " & strDbTable & "Author.Salt, " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.User_code "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Username = '" & strUsername & "';"

	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3


	'Query the database
	rsCommon.Open strSQL, adoCon
	
	
	'If no record is returned then the login is incorrect
	If rsCommon.EOF Then blnIncorrectLogin = true

	
	'If the query has returned a value to the recordset then check the password is correct
	If NOT rsCommon.EOF AND blnSecurityCodeOK Then

		'Only encrypt password if this is enabled
		If blnEncryptedPasswords Then
			
			'Encrypt password so we can check it against the encypted password in the database
			'Read in the salt
			strPassword = strPassword & rsCommon("Salt")
	
			'Encrypt the entered password
			strPassword = HashEncode(strPassword)
		End If


		'Check the encrypted password is correct, if it is get the user ID and set a cookie
		If strPassword = rsCommon("Password") Then

			'Read in the users ID number and whether they want to be automactically logged in when they return to the forum
			lngUserID = CLng(rsCommon("Author_ID"))
			strUsername = rsCommon("Username")
			
			
			'For extra security create a new user code for the user
			strUserCode = userCode(strUsername)
			
			'Save the new usercode back to the database
			rsCommon.Fields("User_code") = strUserCode
			rsCommon.Update

			
			'Write a cookie with the User ID number so the user logged in throughout the forum
			'Write the cookie with the name Forum containing the value UserID number
			Response.Cookies(strCookieName)("UID") = strUserCode
   			
			
			
			'Write a cookie saying if the user is browsing anonymously, 1 = Anonymous, 0 = Shown
			If CBool(Request.Form("NS")) = False Then
				
				Response.Cookies(strCookieName)("NS") = "1" 'Anonymous 
			Else
				Response.Cookies(strCookieName)("NS") = "0" 'Shown
			End If


			'If the user has selected to be remebered when they next login then set the expiry date for the cookie for 1 year
			If blnAutoLogin = True Then

				'Set the expiry date for 1 year
				'If no expiry date is set the cookie is deleted from the users system 20 minutes after they leave the forum
				Response.Cookies(strCookieName).Expires = DateAdd("yyyy", 1, Now())
			End If


			'Reset Server Objects
			rsCommon.Close
			Set rsCommon = Nothing
			adoCon.Close
			Set adoCon = Nothing


			'Go to the login test to make sure the user has cookies enabled on their browser
			'If this is a redierect form the email notify unsubscribe page to get the user to log in then redirct back there
			If Request.QueryString("M") = "Unsubscribe" Then
				
				Response.Redirect("login_user_test.asp?TID=" & Request.QueryString("TID") & "&FID=" & intForumID & "&M=Unsubscribe")
			
			'Redirect the user back to the forum they have just come from
			ElseIf intForumID > 0 Then
				
				Response.Redirect("login_user_test.asp?FID=" & intForumID)
			'Return to forum homepage
			Else
				Response.Redirect("login_user_test.asp")
			End If
		
		'Else the login was incorrect
		Else
			blnIncorrectLogin = true
		End If
	End If

	'Reset Server Objects
	rsCommon.Close
End If


'If not quick login empty variables
If Request.Form("QUIK") OR blnSecurityCodeOK = false Then
	strUsername = Replace(strUsername, "''", "'")
	strPassword = Replace(strPassword, "''", "'")
Else
	strUsername = ""
	strPassword = ""
End If



'Create security code
If blnLongSecurityCode Then 
	
	'Initliase variable
	Session("lngSecurityCode") = ""
	        
	'Create a new session security code
	For lngLoopCounter = 1 to 6
	        	
		'Randomise the system timer
		Randomize Timer
			
		'Place the random number onto the end of teh security code session variable
		Session("lngSecurityCode") = Session("lngSecurityCode") & CStr(CInt(Rnd * 9))
	Next
End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Login Member</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a Username
	if (document.frmLogin.name.value==""){
		errorMsg += "\n\t<% = strTxtErrorUsername %>";
	}

	//Check for a Password
	if (document.frmLogin.password.value==""){
		errorMsg += "\n\t<% = strTxtErrorPassword %>";
	}<%

If blnLongSecurityCode Then 
	
	%>
	
	//Check for a security code
        if (document.frmLogin.securityCode.value == ''){
                errorMsg += "\n\t<% = strTxtErrorSecurityCode %>";
        }<%
End If

%>

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}

</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtLoginUser %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtLoginUser %><br /></td>
  </tr>
</table><br /><%

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'If the user has unsuccesfully tried logging in before then display a password incorrect error
If blnIncorrectLogin OR blnSecurityCodeOK = False Then
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" class="error"><%
	
	'If the login has failed
	If blnIncorrectLogin Then Response.Write(strTxtSorryUsernamePasswordIncorrect & "<br />" & strTxtPleaseTryAgain & "<br /><br />")
	
	'If the security code is incorrect
        If blnSecurityCodeOK = False Then Response.Write(Replace(strTxtSecurityCodeDidNotMatch, "\n\n", "<br />") & "<br /><br />")
	
	%></td>
  </tr>
</table><%

End If
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr><form method="post" name="frmLogin" action="login_user.asp?FID=<% = intForumID %><% If Request.QueryString("M") = "Unsubscribe" Then Response.Write("&TID=" & CLng(Request.QueryString("TID")) & "&M=Unsubscribe") %>" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>">
      <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<% = strTableBgColour %>">
    <tr bgcolor="<% = strTableTitleColour %>">
      <td colspan="2" background="<% = strTableTitleBgImage %>" class="tHeading"><% = strTxtLoginUser %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td colspan="2" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text">*<% = strTxtRequiredFields %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" >
         <td width="50%"  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtUsername %>*</td>
         <td width="50%" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><input type="text" name="name" id="name" size="15" maxlength="15" value="<% = strUsername %>" /></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%"  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtPassword %>*</td>
         <td width="50%" valign="top" background="<% = strTableBgImage %>"><input type="password" name="password" id="password" size="15" maxlength="15" value="<% = strPassword %>" /><%
    	
'If email notification is enabled then also show the forgotten password link
If blnEmail = True Then
	
	%> <a href="JavaScript:openWin('forgotten_password.asp','forgot_pass','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=570,height=350')"><% = strTxtClickHereForgottenPass %></a><%
      
End If
	  
	  %></td>
     </tr>   
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%" class="text" background="<% = strTableBgImage %>"><% = strTxtAutoLogin %></td>
         <td width="50%" valign="top" class="text" background="<% = strTableBgImage %>"><% = strTxtYes %><input type="radio" name="AutoLogin" value="true" checked />&nbsp;&nbsp;<% = strTxtNo %><input type="radio" name="AutoLogin" value="false" /></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%" class="text" background="<% = strTableBgImage %>"><% = strTxtAddMToActiveUsersList %></td>
         <td width="50%" valign="top" class="text" background="<% = strTableBgImage %>"><% = strTxtYes %><input type="radio" name="NS" value="true" checked />&nbsp;&nbsp;<% = strTxtNo %><input type="radio" name="NS" value="false" /></td>
     </tr><%

'If the image security codeis enabled then show this in the form         
If blnLongSecurityCode Then 
	
	%>
     <tr bgcolor="<% = strTableTitleColour %>">
      <td colspan="2" background="<% = strTableTitleBgImage %>" class="tHeading"><% = strTxtSecurityCodeConfirmation %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%" class="text" background="<% = strTableBgImage %>"><% = strTxtUniqueSecurityCode %><br /><span class="smText"><% = strTxtCookiesMustBeEnabled %></span></td>
         <td width="50%" valign="top" background="<% = strTableBgImage %>"><img src="security_image.asp?I=1&<% = hexValue(4) %>" /><img src="security_image.asp?I=2&<% = hexValue(4) %>" /><img src="security_image.asp?I=3&<% = hexValue(4) %>" /><img src="security_image.asp?I=4&<% = hexValue(4) %>" /><img src="security_image.asp?I=5&<% = hexValue(4) %>" /><img src="security_image.asp?I=6&<% = hexValue(4) %>" /></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%" class="text" background="<% = strTableBgImage %>"><% = strTxtConfirmSecurityCode %><br /><span class="smText"><% = strTxtEnter6DigitCode %></span></td>
         <td width="50%" valign="top" background="<% = strTableBgImage %>"><input type="text" name="securityCode" size="12" maxlength="12" autocomplete="off" /></td>
     </tr><%
     
End If

%>
     <tr bgcolor="<% = strTableBottomRowColour %>" background="<% = strTableBgImage %>">
       <td valign="top" height="2" colspan="2" align="center" background="<% = strTableBgImage %>">
        <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
        <input type="submit" name="Submit" value="<% = strTxtLoginUser %>" />
        <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
      </td>
     </tr>
    </table>
      </td>
    </tr>
  </table>
  </td>
 </form></tr>
</table>
<br /><br />
<table width="63%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" class="text"><a href="registration_rules.asp?FID=<% = intForumID %>" target="_self"><% = strClickHereIfNotRegistered %></a><br />
      <br />
    </tr>
  </table>
  <br />
  <script>document.frmLogin.<% If Request.Form("QUIK") And blnLongSecurityCode Then Response.Write("securityCode") Else Response.Write("name") %>.focus()</script>
 <div align="center"><%
 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"

%></div><%


'If the user has unsuccesfully tried logging in before then display a password incorrect error
If blnIncorrectLogin Then
        Response.Write("<script  language=""JavaScript"">")
        Response.Write("alert('" & strTxtSorryUsernamePasswordIncorrect & "\n\n" &  strTxtPleaseTryAgain & "');")
        Response.Write("</script>")

End If

'If the security code did not match
If blnSecurityCodeOK = False Then
        Response.Write("<script  language=""JavaScript"">")
        Response.Write("alert('" & strTxtSecurityCodeDidNotMatch & ".');")
        Response.Write("</script>")
End If
%>
<!-- #include file="includes/footer.asp" -->