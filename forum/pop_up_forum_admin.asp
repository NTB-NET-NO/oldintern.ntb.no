<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/admin_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim intForumID		'Holds the forum ID number
Dim blnLockedStatus	'Holds the lock status of the topic
Dim strForumName	'Holds the name of the forum
Dim strForumDescription	'Holds the description of the forum


'Read in the forum ID number
intForumID = CLng(Request("FID"))


'If the person is not an admin then send them away
If intForumID = "" OR  blnAdmin = False Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If



'Initliase the SQL query to get the topic details from the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
End If


'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
rsCommon.CursorType = 2

'Set the Lock Type for the records so that the record set is only locked when it is updated
rsCommon.LockType = 3

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
	strForumName = rsCommon("Forum_name")
	strForumDescription = rsCommon("Forum_description")
	blnLockedStatus = CBool(rsCommon("Locked"))
	
End If


'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)




'If this is a post back then  update the database
If (Request.Form("postBack")) AND (blnAdmin OR blnModerator) Then

	strForumName = Trim(Mid(Request.Form("forumName"), 1, 70))
	strForumDescription = Trim(Mid(Request.Form("description"), 1, 190))
	blnLockedStatus = CBool(Request.Form("locked"))

	'Update the recordset
	With rsCommon

		'Update	the recorset
		.Fields("Forum_name") = strForumName
		.Fields("Forum_description") = strForumDescription
		.Fields("Locked") = blnLockedStatus

		'Update db
		.Update

		'Re-run the query as access needs time to catch up
		.ReQuery

	End With

End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Forum Admin</title>

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001-2004
    	 If you want your forum then goto http://www.webwizforums.com -->

<script language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a forum name
	if (document.frmForumAdmin.forumName.value==""){
		errorMsg += "\n\t<% = strTxtErrorForumName %>";
	}
	
	//Check for a description
	if (document.frmForumAdmin.description.value==""){
		errorMsg += "\n\t<% = strTxtErrorForumDescription %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
</script>

<!--#include file="includes/skin_file.asp" -->

</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<div align="center" class="heading"><% = strTxtForumAdmin %></div>
    <br /><%

'If there is no forum info returned by the rs then display an error message
If rsCommon.EOF OR (blnAdmin = False AND blnModerator = False) OR bannedIP() OR blnActiveMember = False Then

	'Close the rs
	rsCommon.Close

	Response.Write("<div align=""center"">")
	Response.Write("<span class=""lgText"">" & strTxtForumNotFoundOrAccessDenied & "</span><br /><br /><br />")
	Response.Write("</div>")

'Else display a form to allow updating of the topic
Else

	'Close the rs
	rsCommon.Close
%>
<form name="frmForumAdmin" method="post" action="pop_up_forum_admin.asp" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="350" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td>
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="350">
     <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>">
      <td colspan="2" bgcolor="<% = strTableTitleColour %>" background="<% = strTableTitleBgImage %>" class="text">*<% = strTxtRequiredFields %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtForumName %>*:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="text" name="forumName" maxlength="70" size="30" value="<% = strForumName %>"/></td>
     </tr>
     
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtForumDiscription %>*:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="text" name="description" maxlength="190" size="30" value="<% = strForumDescription %>" /></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtForumLocked %>:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="checkbox" name="locked" value="true" <% If blnLockedStatus = True Then Response.Write(" checked") %> /></td>
     </tr>     
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" colspan="2" align="center"><a href="resync_post_count.asp?FID=<% = intForumID %>" target="_self"><% = strTxtResyncTopicPostCount %></a></td>
     </tr>     
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" align="center" />
      <td valign="top" colspan="2" background="<% = strTableBgImage %>" />
        <input type="hidden" name="FID" value="<% = intForumID %>" />
        <input type="hidden" name="postBack" value="true" />
        <input type="submit" name="Submit" value="<% = strTxtUpdateForum %>" />
        <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
</form><%

End If

%>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr>
      <td align="center">
        <a href="JavaScript:onClick=window.opener.location.href = window.opener.location.href; window.close();"><% = strTxtCloseWindow %></a>
      </td>
    </tr>
  </table>
<br /><br />
<div align="center">
<%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</div>
</body>
</html>