Imports System.Data.SqlClient
Imports System.Web.Configuration


Namespace intranett


    Partial Class internavis_list

        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Protected dr As SqlDataReader

        Protected olddate, dateheader As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            BindItems(dlItems)
        End Sub

        Private Sub BindItems(ByVal list As DataList)

            ' Dim dbForum As New SqlConnection(ConfigurationSettings.AppSettings("forumConString"))
            Dim dbForum As New SqlConnection(WebConfigurationManager.AppSettings("forumConString"))
            Dim cd As New SqlCommand

            cd.Connection = dbForum
            cd.CommandText = "internTopicItems"
            cd.CommandType = CommandType.StoredProcedure

            cd.Parameters.Add(New SqlParameter("@forumID", SqlDbType.Int))
            cd.Parameters.Item("@forumID").Value = 6

            cd.Parameters.Add(New SqlParameter("@count", SqlDbType.Int))
            cd.Parameters.Item("@count").Value = 15

            cd.Parameters.Add(New SqlParameter("@topicOnly", SqlDbType.Int))
            cd.Parameters.Item("@topicOnly").Value = 1

            cd.Parameters.Add(New SqlParameter("@sortDesc", SqlDbType.Int))
            cd.Parameters.Item("@sortDesc").Value = 1

            dbForum.Open()
            dr = cd.ExecuteReader()

            list.DataSource = dr
            list.DataBind()

            dr.Close()
            dbForum.Close()
        End Sub

        Private Sub dlItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlItems.ItemDataBound
            Dim name As String = ""
            Dim ctrl As System.Web.UI.WebControls.Label = e.Item.FindControl("name")

            If Not dr.IsDBNull(dr.GetOrdinal("real_name")) Then
                name = dr("real_name")
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("name")) And name = "" Then
                name = dr("name")
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("username")) And name = "" Then
                name = dr("username")
            End If

            ctrl.Text = name

        End Sub

        Private Sub dlItems_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlItems.ItemCreated

            If olddate <> Format(CType(sender, DataList).DataSource("start_date"), "yyyyMM") Then
                dateheader = "<p class=""artikkel_tekst""><b>" & Format(CType(sender, DataList).DataSource("start_date"), "MMMM, yyyy").ToUpper & "</b></p>"
            Else
                dateheader = ""
            End If

            olddate = Format(CType(sender, DataList).DataSource("start_date"), "yyyyMM")

        End Sub
    End Class

End Namespace
