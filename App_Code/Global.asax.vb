Imports System.Web
Imports System.Web.SessionState
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Diagnostics


Namespace intranett


    Public Class [Global]

        Inherits System.Web.HttpApplication
        Private Shadows Site As String
        Public Shared GlobalMaingroups As Hashtable
        Public Shared GlobalMainCats As Hashtable

#Region " Component Designer Generated Code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Component Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub

#End Region

        Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

            ' Fires when the application is started
            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
            dbNTB.Open()
            Application("db") = dbNTB

            Dim cd As New SqlCommand
            Dim dr As SqlDataReader

            GlobalMaingroups = New Hashtable
            GlobalMainCats = New Hashtable

            cd.Connection = Application("db")
            cd.CommandText = "select Category, Descriptivename from Bitnames (nolock) where typeofname = 0 order by Displaysort"
            dr = cd.ExecuteReader
            While dr.Read
                GlobalMaingroups.Add(dr("Category"), dr("Descriptivename"))
            End While
            dr.Close()
            cd.CommandText = "select Category, Descriptivename from Bitnames (nolock) where typeofname = 3 order by Displaysort"
            dr = cd.ExecuteReader
            While dr.Read
                GlobalMainCats.Add(dr("Category"), dr("Descriptivename"))
            End While
            dr.Close()
        End Sub

        Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)

            ' Fires when the session is started
            ' Session("SiteCode") = ConfigurationSettings.AppSettings("SiteCode")
            Session("SiteCode") = WebConfigurationManager.AppSettings("SiteCode")
            Site = Session("SiteCode")

            Session.Timeout = 20

            'If ConfigurationSettings.AppSettings("livestat") = "1" Then
            '    AddHit(True, Site)
            'End If

            If WebConfigurationManager.AppSettings("livestat") = "1" Then
                AddHit(True, Site)
            End If


        End Sub

        Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires at the beginning of each request
        End Sub

        Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires upon attempting to authenticate the use
        End Sub

        Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)


            Try

                Dim LogName As String = "ASPDOTNET_Errors"
                Dim LogSource As String = "NTB.NET"
                Dim LogErrorMessage As String = Context.Server.GetLastError.Message & " " & Context.Server.GetLastError.StackTrace

                If Not EventLog.SourceExists(LogSource) Then
                    EventLog.CreateEventSource(LogSource, LogName)
                    EventLog.WriteEntry(LogSource, LogErrorMessage.ToString())
                End If

                If WebConfigurationManager.AppSettings("EmailErrors") <> "" Then
                    SendEmail("weberror@ntb.no", WebConfigurationManager.AppSettings("EmailErrors"), "Error at website", LogErrorMessage)
                End If
                'Context.Server.ClearError()
            Catch ex As Exception
                ' Do something here with the error

            End Try

        End Sub

        Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)

            ' Fires when the session ends
        End Sub

        Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)

            ' Fires when the application ends
            Application("db").Close()
        End Sub

        Sub Application_OnEndRequest(ByVal sender As Object, ByVal e As EventArgs)
            Try

                Dim LogErrorMessage As String = Context.Server.GetLastError.Message & " " & Context.Server.GetLastError.StackTrace

                ' Fires at the end of each request
                'If ConfigurationSettings.AppSettings("livestat") = "1" Then
                '    AddHit(False, Site)
                'End If

                If WebConfigurationManager.AppSettings("EmailErrors") <> "" Then
                    SendEmail("weberror@ntb.no", WebConfigurationManager.AppSettings("EmailErrors"), "Error at website", LogErrorMessage)
                End If
            Catch ex As Exception

                ' Do something here if something is wrong
            End Try


        End Sub

    End Class

End Namespace
