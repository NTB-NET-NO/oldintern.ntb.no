/*
 overlibmws_crossframe.js plug-in module - Copyright Foteos Macrides 2003-2004
   For support of the FRAME feature.
   Initial: August 3, 2003 - Last Revised: November 27, 2003
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License agreement for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=0;
registerCommands('frame');

// INIT
// For commandline parser.
function parseFrameExtras(pf,i,ar){
var k=i,v;
if(k<ar.length){
if(ar[k]==FRAME){v=ar[++k];if(pf=='ol_'&&compatibleFrame(v))ol_frame=v;
else opt_FRAME(v);return k;}}
return -1;
}

/////////
// FRAME SUPPORT FUNCTIONS
/////////
function getFrameRef(thisFrame,ofrm){
var retVal='';
for(var i=0;i<thisFrame.length;i++){
if(thisFrame[i].length>0){
retVal=getFrameRef(thisFrame[i],ofrm);
if(retVal=='')continue;}
else if(thisFrame[i]!=ofrm)continue;
retVal='['+i+']'+retVal;
break;}
return retVal;
}

// Makes sure target frame has overLIB
function compatibleFrame(frameid){
if((OLns4&&typeof frameid.document.overDiv=='undefined')||
(document.all&&typeof frameid.document.all['overDiv']=='undefined')||
(document.getElementById&&frameid.document.getElementById('overDiv')==null))return false;
return true;
}

// Defines which frame we should point to.
function opt_FRAME(frm){
o3_frame=compatibleFrame(frm)?frm:ol_frame;
if(o3_frame!=ol_frame){
var tFrm=getFrameRef(top.frames,o3_frame);
var sFrm=getFrameRef(top.frames,ol_frame);
if(sFrm.length==tFrm.length){
l=tFrm.lastIndexOf('[');
if(l){
while(sFrm.substring(0,l)!=tFrm.substring(0,l))
l=tFrm.lastIndexOf('[',l-1);
tFrm=tFrm.substr(l);
sFrm=sFrm.substr(l);}}
var cnt=0,p='',str=tFrm;
while((k=str.lastIndexOf('['))!= -1){
cnt++;
str=str.substring(0,k);}
for(var i=0;i<cnt;i++) p=p+'parent.';
fnRef=p+'frames'+sFrm+'.';}
if(OLie4)over=o3_frame.document.all['overDiv'];
else if(OLns6)over=o3_frame.document.getElementById("overDiv");
else if(OLns4)over=o3_frame.document.overDiv;
return 0;
}

////////
// PLUGIN REGISTRATIONS
////////
registerCmdLineFunction(parseFrameExtras);

OLcrossframePI=1;
OLloaded=1;
