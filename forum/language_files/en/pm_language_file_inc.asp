<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by email ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'pm_welcome.asp
'---------------------------------------------------------------------------------
Const strTxtToYourPrivateMessenger = "to your Private Messenger"
Const strTxtPmIntroduction = "From your Private Messenger you can send and receive Private Messages between yourself and other forum members, knowing that your messages won't be viewed by other forum members."
Const strTxtInboxStatus = "Inbox Status"
Const strTxtGoToYourInbox = "Go to your inbox"
Const strTxtNoNewMsgsInYourInbox = "You have no new messages in your inbox."
Const strTxtYourLatestPrivateMessageIsFrom = "Your latest Private Message is from"
Const strTxtSentOn = "sent on"
Const strTxtPrivateMessengerOverview = "Private Messenger Overview"
Const strTxtInboxOverview = "This is where all incoming Private Messages are stored and from where you can view or delete any Private Messages you have received. It works very much like your email 'inbox'."
Const strTxtOutboxOverview = "This is where outgoing Private Messages are stored and from where you can view Private Messages that you have sent. Private Messages remain in your Outbox until the recipient deletes the message."
Const strTxtBuddyListOverview = "This is like an address book. You can add or delete forum members to your Buddy List for quick reference. You can also block those members you don't wish to receive Private Messages from."
Const strTxtNewMsgOverview = "This is where you can compose new Private Messages and send them to other forum members."

'pm_inbox.asp
'---------------------------------------------------------------------------------

Const strTxtInbox = "Inbox"
Const strTxtBuddyList = "Buddy List"
Const strTxtNewPrivateMessage = "New Private Message"
Const strTxtNoPrivateMessages = "You have no Private Messages in your"
Const strTxtRead = "Read"
Const strTxtMessageTitle = "Message Title"
Const strTxtMessageFrom = "Message From"
Const strTxtDate = "Date"
Const strTxtBlock = "block"
Const strTxtSentBy = "Sent by"
Const strTxtDeletePrivateMessageAlert = "Are you sure you want to delete the Private Message(s)?"
Const strTxtPrivateMessagesYouCanReceiveAnother = "Private Messages, you can receive another"
Const strTxtOutOf = "out of"
Const strTxtPreviousPrivateMessage = "Previous Private Message"
Const strTxtMeassageDeleted = "The Private Message(s) have been deleted from your Inbox"

'pm_show_message.asp
'---------------------------------------------------------------------------------
Const strTxtSorryYouDontHavePermissionsPM = "Sorry, you don't have permission to view this private message"
Const strTxtYouDoNotHavePermissionViewPM = "You do not have permission to view this Private Message."
Const strTxtNotificationReadPM = "Read Private Message Notification"
Const strTxtReplyToPrivateMessage = "Reply To Private Message"
Const strTxtAddToBuddy = "Add To Buddy List"
Const strTxtThisIsToNotifyYouThat = "This is to notify you that"
Const strTxtHasReadPM = "has read the Private Message"
Const strTxtYouSentToThemOn = "you sent to them on"


'pm_new_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtSendNewMessage = "Send New Message"
Const strTxtPostMessage = "Post Message"
Const strTxtEmailNotifyWhenPMIsRead = "Email Notify me when message is read"
Const strTxtToUsername = "To&nbsp;Username"
Const strSelectFormBuddyList = "or select from Buddy List"
Const strTxtNoPMSubjectErrorMsg = "Subject \t\t- Enter a Subject for your new Private Message"
Const strTxtNoToUsernameErrorMsg = "To Username \t- Enter or Select a Username to send you Private Message to"
Const strTxtNoPMErrorMsg = "Message \t\t- Enter a Private Message to send"
Const strTxtSent = "Sent"

'pm_post_message.asp
'---------------------------------------------------------------------------------
Const strTxtAPrivateMessageHasBeenPosted = "A Private Message has been posted for you on"
Const strTxtClickOnLinkBelowForPM = "Click on the link below to go to the forum to view the Private Message"
Const strTxtNotificationPM = "Private Message Notification"
Const strTxtTheUsernameCannotBeFound = "The Username you have entered can not be found."
Const strTxtYourPrivateMessage = "Your Private Message"
Const strTxtHasNotBeenSent = "has not been sent!"
Const strTxtAmendYourPrivateMessage = "Amend your Private Message"
Const strTxtReturnToYourPrivateMessenger = "Return to your Private Messenger"
Const strTxtYouAreBlockedFromSendingPMsTo = "You are blocked you from sending Private Messages to"
Const strTxtHasExceededMaxNumPPMs = "has exceeded the maximum number of Private Messages they are allowed to receive"
Const strTxtHasSentYouPM = "has sent you a Private Message with the following subject"
Const strTxtToViewThePrivateMessage = "To view the Private Message"


'pm_buddy_list.asp
'---------------------------------------------------------------------------------
Const strTxtNoBuddysInList = "You have no Buddies in your Buddy List"
Const strTxtDeleteBuddyAlert = "Are you sure you want to Delete this Buddy from your Buddy List?"
Const strTxtNoBuddyErrorMsg = "Member Name \t- Enter a Forum Member to add to your Buddy List"
Const strTxtBuddy = "Buddy"
Const strTxtDescription = "Description"
Const strTxtContactStatus = "Contact Status"
Const strTxtThisPersonCanNotMessageYou = "This person can not message you"
Const strTxtThisPersonCanMessageYou = "This person can message you"
Const strTxtAddNewBuddyToList = "Add New Buddy to List"
Const strTxtMemberName = "Member Name"
Const strTxtAllowThisMemberTo = "Allow this member to"
Const strTxtMessageMe = "Message me"
Const strTxtNotMessageMe = "Not message me"
Const strTxtHasNowBeenAddedToYourBuddyList = "has now been added to your Buddy List"
Const strTxtIsAlreadyInYourBuddyList = "is already in your Buddy List"
Const strTxtUserCanNotBeFoundInDatabase = "can not be found in the database.\n\nCheck you havn\'t misspelled the Members Username"



Const strTxtOutbox = "Outbox"
Const strTxtMessageTo = "Message To"
Const strTxtMessagesInOutBox = "Messages remain in your Outbox until the recipient deletes the message."

'New from version 7.02
'---------------------------------------------------------------------------------
Const strTxtYourInboxIs = "Your inbox is"
Const strTxtFull = "full"
Const strTxtEmailThisPMToMe = "Email this Private Message to myself"
Const strTxtEmailBelowPrivateEmailThatYouRequested = "Below is the copy of the Private Message you requested emailed to yourself"
Const strTxtAnEmailWithPM = "An Email containing the Private Message has"
Const strTxtBeenSent = "been sent to your email address"
Const strTxtNotBeenSent = "not been sent to you as an error has occurred"
Const strTxtSelected = "Selected"

%>