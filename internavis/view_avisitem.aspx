<%@ Page Language="vb" AutoEventWireup="false" Inherits="intranett.view_avisitem" CodeFile="view_avisitem.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>intern.ntb.no</title>
		<link rel="stylesheet" href="../css/styles.css">
	</HEAD>
	<body>
		<table width="485" class="left_border" cellpadding="0" cellspacing="5" border="0">
			<tr>
				<td colspan="2" class="artikkel_header"><%= subject %></td>
			</tr>
			<tr>
				<td colspan="2" class="artikkel_tekst">( <i>
						<%= Format(dt, "dd.MM.yyyy HH:mm") %>
						-
						<%= name %>
					</i>)<p></p>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="forum_content"><%= content %></td>
			</tr>
			<tr>
				<td colspan="2"><hr size="1" noshade>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="artikkel_tekst"><%= comments %></td>
			</tr>
			<tr>
				<td colspan="2"><hr size="1" noshade>
				</td>
			</tr>
			<tr>
				<td class="artikkel_tekst" width="250"><a href="/forum/forum_posts.asp?TID=<%= topic_id %>&amp;PN=1#reply" target="_top">Skriv 
						kommentar til denne saken</a></td>
				<td class="artikkel_tekst" width="240" align="right"><a href="/forum/search_form.asp?FID=6&amp;SM=3" target="_top">S�k 
						i Internavisa</a></td>
			</tr>
		</table>
	</body>
</HTML>
