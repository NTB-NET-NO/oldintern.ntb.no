<%
'pm_welcome.asp
'---------------------------------------------------------------------------------
Const strTxtToYourPrivateMessenger = "til din postkasse"
Const strTxtPmIntroduction = "Fra postkassen kan du sende og motta private meldinger mellom deg selv og andre medlemmer. Disse meldingene kan ikke leses av andre."
Const strTxtInboxStatus = "Innboks status"
Const strTxtGoToYourInbox = "G� til innboksen din"
Const strTxtNoNewMsgsInYourInbox = "Du har ingen meldinger i innboksen."
Const strTxtYourLatestPrivateMessageIsFrom = "Din siste private melding er fra"
Const strTxtSentOn = "sendt"
Const strTxtPrivateMessengerOverview = "Oversikt over postkassen"
Const strTxtInboxOverview = "Det er her alle innkommende private meldinger blir lagret. Her kan du ogs� vise og slette meldinger du har f�tt. Den fungerer som innboksen i epostprogrammet ditt."
Const strTxtOutboxOverview = "Det er her alle utg�ende private meldinger blir lagret. Private meldinger forblir i utboksen til mottakeren sletter dem."
Const strTxtBuddyListOverview = "Dette er som en adresseliste. Du kan legge til og slette medlemmer i din venneliste. Du kan ogs� blokkere medlemmer du ikke vil motta private meldinger fra."
Const strTxtNewMsgOverview = "Her kan du lage nye private meldinger og sende dem til andre medlemmer."

'pm_inbox.asp
'---------------------------------------------------------------------------------

Const strTxtInbox = "Innboks"
Const strTxtBuddyList = "Venneliste"
Const strTxtNewPrivateMessage = "Ny privat melding"
Const strTxtNoPrivateMessages = "Du har ingen private meldinger i"
Const strTxtRead = "Les"
Const strTxtMessageTitle = "Meldingstittel"
Const strTxtMessageFrom = "Melding fra"
Const strTxtDate = "Dato"
Const strTxtBlock = "blokker"
Const strTxtSentBy = "Sendt av"
Const strTxtDeletePrivateMessageAlert = "Vil du slette de merkede meldingene?"
Const strTxtPrivateMessagesYouCanReceiveAnother = "private meldinger. Du kan i tillegg motta"
Const strTxtOutOf = "av totalt"
Const strTxtPreviousPrivateMessage = "Forrige private melding"
Const strTxtMeassageDeleted = "Meldingen er slettet fra innboksen."

'pm_show_message.asp
'---------------------------------------------------------------------------------
Const strTxtSorryYouDontHavePermissionsPM = "Beklager. Du har ikke rettigheter til � vise denne private meldingen."
Const strTxtYouDoNotHavePermissionViewPM = "Du har ikke rettigheter til � vise denne private meldingen."
Const strTxtNotificationReadPM = "Les privat melding"
Const strTxtReplyToPrivateMessage = "Svar p� privat melding"
Const strTxtAddToBuddy = "Legg til i vennelisten"
Const strTxtThisIsToNotifyYouThat = "Dette er for �  gj�re deg oppmerksom p� at"
Const strTxtHasReadPM = "har lest den private meldingen"
Const strTxtYouSentToThemOn = "du sendte"


'pm_new_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtSendNewMessage = "Send ny melding"
Const strTxtPostMessage = "Send melding"
Const strTxtEmailNotifyWhenPMIsRead = "Send meg en epost n�r meldingen er lest"
Const strTxtToUsername = "Til&nbsp;Brukernavn"
Const strSelectFormBuddyList = "eller velg fra vennelisten"
Const strTxtNoPMSubjectErrorMsg = "Tittel \t\t- Tast en tittel p� din nye private melding"
Const strTxtNoToUsernameErrorMsg = "Til Brukernavn \t- Tast eller velg brukernavn for mottaker"
Const strTxtNoPMErrorMsg = "Melding \t\t- Tast innholdet i meldingen"
Const strTxtSent = "Sendt"

'pm_post_message.asp
'---------------------------------------------------------------------------------
Const strTxtAPrivateMessageHasBeenPosted = "Du har f�tt en privat melding"
Const strTxtClickOnLinkBelowForPM = "Klikk p� denne linken for � g� til forum for private meldinger"
Const strTxtNotificationPM = "Informasjon om personlig melding"
Const strTxtTheUsernameCannotBeFound = "Kan ikke finne det brukernavnet du har brukt."
Const strTxtYourPrivateMessage = "Din private melding"
Const strTxtHasNotBeenSent = "er ikke sendt!"
Const strTxtAmendYourPrivateMessage = "Legg til i din private melding"
Const strTxtReturnToYourPrivateMessenger = "Tilbake til postkassen"
Const strTxtYouAreBlockedFromSendingPMsTo = "Du er blokkert fra � sende private meldinger til"
Const strTxtHasExceededMaxNumPPMs = "har oversteget maksimum antall private beskjeder det er mulig � motta"
Const strTxtHasSentYouPM = "har sendt deg en privat melding med f�lgende tittel"
Const strTxtToViewThePrivateMessage = "For � vise din private melding"

'pm_buddy_list.asp
'---------------------------------------------------------------------------------
Const strTxtNoBuddysInList = "Du har ingen venner i vennelisten"
Const strTxtDeleteBuddyAlert = "Vil du slette denne vennen fra vennelisten?"
Const strTxtNoBuddyErrorMsg = "Medlemsnavn \t- Tast et medlem du vil legge til i vennelisten"
Const strTxtBuddy = "Venn"
Const strTxtDescription = "Beskrivelse"
Const strTxtContactStatus = "Kontaktstatus"
Const strTxtThisPersonCanNotMessageYou = "Denne personen kan ikke sende deg meldinger"
Const strTxtThisPersonCanMessageYou = "Denne personen kan sende deg meldinger"
Const strTxtAddNewBuddyToList = "Legg ny venn til vennelisten"
Const strTxtMemberName = "Medlemsnavn"
Const strTxtAllowThisMemberTo = "Tillat dette medlemmet"
Const strTxtMessageMe = "� sende meldinger til meg"
Const strTxtNotMessageMe = "Ikke sende meldinger til meg"
Const strTxtHasNowBeenAddedToYourBuddyList = "er n� lagt til i vennelisten"
Const strTxtIsAlreadyInYourBuddyList = "finnes allerede i vennelisten"
Const strTxtUserCanNotBeFoundInDatabase = "finnes ikke i databasen.\n\nKontroller at du har stavet medlemsnavnet riktig."

Const strTxtOutbox = "Utboks"
Const strTxtMessageTo = "Melding til"
Const strTxtMessagesInOutBox = "Meldinger blir liggende i utboksen til mottakeren sletter dem."

'New from version 7.02
'---------------------------------------------------------------------------------
Const strTxtYourInboxIs = "Din innboks er"
Const strTxtFull = "full"
Const strTxtEmailThisPMToMe = "Send denne private meldingen p� epost til meg selv"
Const strTxtEmailBelowPrivateEmailThatYouRequested = "Under finner du den private meldingen du ba om � f� tilsendt p� epost."
Const strTxtAnEmailWithPM = "En epost med den private medlingen er"
Const strTxtBeenSent = "blitt sendt til din epostadresse."
Const strTxtNotBeenSent = "ikke blitt sendt til deg fordi det oppstod en feil."
Const strTxtSelected = "Valgt"

%>