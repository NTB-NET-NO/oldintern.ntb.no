<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_hash1way.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True

'Dimension variables
Dim strPassword			'Holds the forum password
Dim blnAutoLogin		'Holds whether the user wnats to be automactically logged in
Dim intForumID			'Holds the forum ID
Dim strForumCode		'Holds the users ID code
Dim strReturnPage		'Holds the page to return to
Dim strReturnPageProperties	'Holds the properties of the return page


'Get the forum page to return to
Select Case Request.QueryString("RP")
	'Read in the thread and forum to return to
	Case "PT"
		strReturnPage = "forum_posts.asp"
		strReturnPageProperties = "?RP=PT&TID=" & CLng(Request.QueryString("TID")) & "&FID=" & CInt(Request.QueryString("FID"))

	'Else return to the forum main page
	Case Else
		'Read in the forum and topic to return to
		strReturnPage = "forum_topics.asp"
		strReturnPageProperties = "?RP=TC&FID=" & CInt(Request.QueryString("FID"))
End Select


'Read in the forum id number
intForumID = CInt(Request.QueryString("FID"))

'Read in the users details from the form
strPassword = LCase(Trim(Mid(Request.Form("password"), 1, 15)))
blnAutoLogin = CBool(Request.Form("AutoLogin"))

'If user has eneterd a password make sure it is correct
If NOT strPassword = "" Then

	'Read in the forum name from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Forum.Password, " & strDbTable & "Forum.Forum_code FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID

	'Query the database
	rsCommon.Open strSQL, adoCon


	'If the query has returned a value to the recordset then check the password is correct
	If NOT rsCommon.EOF Then

		'Encrypt the entered password
		strPassword = HashEncode(strPassword)

		'Check the password is correct, if it is get the user ID and set a cookie
		If strPassword = rsCommon("Password") Then

			'Read in the users ID number and whether they want to be automactically logged in when they return to the forum
			strForumCode = rsCommon("Forum_code")

			'Write a cookie with the Forum ID number so the user logged in throughout the forum
			'Write the cookie with the name Forum containing the value Forum Code number
			Response.Cookies(strCookieName)("Forum" & intForumID) = strForumCode

			'If the user has selected to be remebered when they next login then set the expiry date for the cookie for 1 year
			If blnAutoLogin = True Then

				'Set the expiry date for 1 year (365 days)
				'If no expiry date is set the cookie is deleted from the users system 20 minutes after they leave the forum
				Response.Cookies(strCookieName).Expires = Now() + 365
			End If

			'Reset Server Objects
			rsCommon.Close
			Set rsCommon = Nothing
			adoCon.Close
			Set adoCon = Nothing


			'Redirect the user back to the forum page they have just come from
			Response.Redirect strReturnPage & strReturnPageProperties
		End If
	End If

	'Clean up
	rsCommon.Close
End If
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Forum Login</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a Password
	if (document.frmLogin.password.value==""){
		errorMsg += "\n\t<% = strTxtErrorEnterPassword %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}

// -->
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtLoginUser %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtForumLogin %><br /></td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" class="bold"><br /><% = strTxtPasswordRequiredForForum %></td>
  </tr>
</table>
<br /><%


'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'If the user has unsuccesfully tried logging in before then display a password incorrect error
If strPassword <> "" Then
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" class="error"><% = strTxtForumPasswordIncorrect %><br />
      <% = strTxtPleaseTryAgain %></td>
  </tr>
</table><br /><%

End If
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr><form method="post" name="frmLogin" action="forum_password_form.asp<% = strReturnPageProperties %>" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>">
      <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<% = strTableBgColour %>">
    <tr bgcolor="<% = strTableTitleColour %>">
      <td colspan="2" background="<% = strTableTitleBgImage %>" class="tHeading"><% = strTxtLoginUser %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td colspan="2" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text">*<% = strTxtRequiredFields %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%"  bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtPassword %>*</td>
         <td width="50%" valign="top" background="<% = strTableBgImage %>"><input type="password" name="password" id="password" size="15" maxlength="15" value="<% = strPassword %>" />
     </td>
     </tr>   
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td width="50%" class="text" background="<% = strTableBgImage %>"><% = strTxtAutoLogin %></td>
         <td width="50%" valign="top" class="text" background="<% = strTableBgImage %>"><% = strTxtYes %><input type="radio" name="AutoLogin" value="true" checked />&nbsp;&nbsp;<% = strTxtNo %><input type="radio" name="AutoLogin" value="false" /></td>
     </tr>
     <tr bgcolor="<% = strTableBottomRowColour %>" background="<% = strTableBgImage %>">
       <td valign="top" height="2" colspan="2" align="center" background="<% = strTableBgImage %>">
        <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
        <input type="submit" name="Submit" value="<% = strTxtLoginToForum %>" />
        <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
      </td>
     </tr>
    </table>
      </td>
    </tr>
  </table>
  </td>
 </form></tr>
</table>
<br /><br />
<script>document.frmLogin.password.focus()</script>
<div align="center">
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->