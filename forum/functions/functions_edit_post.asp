<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide ASP Discussion Forum
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************




'******************************************
'***   HTML to Forum Codes Function   *****
'******************************************

'Edit Post Function to convert posts back to forum codes
Private Function EditPostConvertion(ByVal strMessage)

	Dim strTempMessage	'Temporary word hold for e-mail and url words
	Dim strMessageLink	'Holds the new mesage link that needs converting back into code
	Dim lngStartPos		'Holds the start position for a link
	Dim lngEndPos		'Holds the end position for a word
	Dim intLoop		'Loop counter
	
	
	strMessage = Replace(strMessage, " target=""_blank""", "", 1, -1, 1)
	strMessage = Replace(strMessage, " border=""0""", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<img src= """, "<img src=""", 1, -1, 1)
	strMessage = Replace(strMessage, "<a href= """, "<a href=""", 1, -1, 1)
	
	
	
	'Change the path to the emotion symbols back into the emotion codes
	For intLoop = 1 to UBound(saryEmoticons)
		strMessage = Replace(strMessage, "<img src=""" & saryEmoticons(intLoop,3) & """>", saryEmoticons(intLoop,2), 1, -1, 1)
	Next
	
	
	
	'If the message has been edited remove who edited the post
	If InStr(1, strMessage, "<edited>", 1) Then strMessage = removeEditorAuthor(strMessage)
	
	
	'Change the HTML codes back into my own codes for bold and italic
	strMessage = Replace(strMessage, "<b>", "[B]", 1, -1, 1)
	strMessage = Replace(strMessage, "</b>", "[/B]", 1, -1, 1)
	strMessage = Replace(strMessage, "<i>", "[I]", 1, -1, 1)
	strMessage = Replace(strMessage, "</i>", "[/I]", 1, -1, 1)
	strMessage = Replace(strMessage, "<u>", "[U]", 1, -1, 1)
	strMessage = Replace(strMessage, "</u>", "[/U]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<hr />", "[HR]", 1, -1, 1)
	strMessage = Replace(strMessage, "<hr>", "[HR]", 1, -1, 1)
	strMessage = Replace(strMessage, "<hr>", "[HR]", 1, -1, 1)
	strMessage = Replace(strMessage, "<ol>", "[LIST=1]", 1, -1, 1)
	strMessage = Replace(strMessage, "</ol>", "[/LIST=1]", 1, -1, 1)
	strMessage = Replace(strMessage, "<ul>", "[LIST]", 1, -1, 1)
	strMessage = Replace(strMessage, "</ul>", "[/LIST]", 1, -1, 1)
	strMessage = Replace(strMessage, "<li>", "[LI]", 1, -1, 1)
	strMessage = Replace(strMessage, "</li>", "[/LI]", 1, -1, 1)
	strMessage = Replace(strMessage, "<center>", "[CENTER]", 1, -1, 1)
	strMessage = Replace(strMessage, "</center>", "[/CENTER]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<strong>", "[B]", 1, -1, 1)
	strMessage = Replace(strMessage, "</strong>", "[/B]", 1, -1, 1)
	strMessage = Replace(strMessage, "<em>", "[I]", 1, -1, 1)
	strMessage = Replace(strMessage, "</em>", "[/I]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<br />", "", 1, -1, 1)
	strMessage = Replace(strMessage, "<br>", "", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<P>", "[P]", 1, -1, 1)
	strMessage = Replace(strMessage, "</P>", "[/P]", 1, -1, 1)
	strMessage = Replace(strMessage, "<P align=center>", "[P ALIGN=CENTER]", 1, -1, 1)
	strMessage = Replace(strMessage, "<P align=left>", "[P ALIGN=LEFT]", 1, -1, 1)
	strMessage = Replace(strMessage, "<P align=right>", "[P ALIGN=RIGHT]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<div>", "[DIV]", 1, -1, 1)
	strMessage = Replace(strMessage, "</div>", "[/DIV]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=""center"">", "[DIV ALIGN=CENTER]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=""left"">", "[DIV ALIGN=LEFT]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=""right"">", "[DIV ALIGN=RIGHT]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=center>", "[DIV ALIGN=CENTER]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=left>", "[DIV ALIGN=LEFT]", 1, -1, 1)
	strMessage = Replace(strMessage, "<div align=right>", "[DIV ALIGN=RIGHT]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<blockquote>", "[BLOCKQUOTE]", 1, -1, 1)
	strMessage = Replace(strMessage, "</blockquote>", "[/BLOCKQUOTE]", 1, -1, 1)
	
	strMessage = Replace(strMessage, "<font size=""1"">", "[SIZE=1]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=""2"">", "[SIZE=2]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=""3"">", "[SIZE=3]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=""4"">", "[SIZE=4]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=""5"">", "[SIZE=5]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=""6"">", "[SIZE=6]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=6>", "[SIZE=6]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=1>", "[SIZE=1]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=2>", "[SIZE=2]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=3>", "[SIZE=3]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=4>", "[SIZE=4]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=5>", "[SIZE=5]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font size=6>", "[SIZE=6]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font face=""Arial, Helvetica, sans-serif"">", "[FONT=Arial]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font face=""Courier New, Courier, mono"">", "[FONT=Courier]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font face=""Times New Roman, Times, serif"">", "[FONT=Times]", 1, -1, 1)
	strMessage = Replace(strMessage, "<font face=""Verdana, Arial, Helvetica, sans-serif"">", "[FONT=Verdana]", 1, -1, 1)
	
	
	
	'Loop through the message till all or any IMAGE links are converted back into codes
	Do While InStr(1, strMessage, "<img ", 1) > 0
						    	
		'Find the start position in the image tag
		lngStartPos = InStr(1, strMessage, "<img ", 1)
															
		'Find the position in the message for the image closing tag
		lngEndPos = InStr(lngStartPos, strMessage, """>", 1) + 3
		
		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 10 Then lngEndPos = lngStartPos + 10
						
		'Read in the code to be converted back into the forum codes
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))	
		
		'Place the image tag into the tempoary message variable
		strTempMessage = strMessageLink
		
		'Format the HTML image tag back into forum codes
		strTempMessage = Replace(strTempMessage, "src=""", "", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, "<img ", "[IMG]", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, """>", "[/IMG]", 1, -1, 1)
		
		'Place the new fromatted codes into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)		
	Loop
	
	
	
	
	'Loop through the message till all or any HTML email links are converted back into codes
	Do While InStr(1, strMessage, "<a href=""mailto:", 1) > 0 AND InStr(1, strMessage, "</a>", 1) > 0
						    	
		'Find the start position in the message of the HTML e-mail mailto tag
		lngStartPos = InStr(1, strMessage, "<a href=""mailto:", 1)
									
									
		'Find the position in the message for the </a> closing code
		lngEndPos = InStr(lngStartPos, strMessage, "</a>", 1) + 4
		
		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 16 Then lngEndPos = lngStartPos + 16
						
		'Read in the code to be converted back into the forum codes
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))	
		
		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink
		
		'Format the HTML mailto link back into forum codes
		strTempMessage = Replace(strTempMessage, "<a href=""mailto:", "[EMAIL=", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, "</a>", "[/EMAIL]", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, """>", "]", 1, -1, 1)
		
		'Place the new fromatted codes into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)		
	Loop
	
	
	
	
	'Loop through the message till all or any hyperlinks are turned into back into froum codes
	Do While InStr(1, strMessage, "<a href=""", 1) > 0 AND InStr(1, strMessage, "</a>", 1) > 0
						    	
		'Find the start position in the message of the HTML hyperlink
		lngStartPos = InStr(1, strMessage, "<a href=""", 1)
																	
		'Find the position in the message for the </a> closing code
		lngEndPos = InStr(lngStartPos, strMessage, "</a>", 1) + 4
		
		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 9 Then lngEndPos = lngStartPos + 9
						
		'Read in the code to be converted back into forum codes from the message
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))	
		
		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink
		
		'Format the HTML hyperlink back into forum codes
		strTempMessage = Replace(strTempMessage, "<a href=""", "[URL=", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, "</a>", "[/URL]", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, """>", "]", 1, -1, 1)
		
		'Place the new fromatted codes into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)		
	Loop
	
	
	
	'Loop through the message till all font colour tags are converted back to forum codes
	Do While InStr(1, strMessage, "<font color=", 1) > 0 AND InStr(1, strMessage, "</font>", 1) > 0
						    	
		'Find the start position in the message of the HTML colour tag
		lngStartPos = InStr(1, strMessage, "<font color=", 1)
									
									
		'Find the position in the message for the </font> closing code
		lngEndPos = InStr(lngStartPos, strMessage, "</font>", 1) + 8
		
		'Make sure the end position is not in error
		If lngEndPos - lngStartPos =< 12 Then lngEndPos = lngStartPos + 12
						
		'Read in the code to be converted back into the forum codes
		strMessageLink = Trim(Mid(strMessage, lngStartPos, (lngEndPos - lngStartPos)))	
		
		'Place the message link into the tempoary message variable
		strTempMessage = strMessageLink
		
		'Format the HTML colour tag back into forum codes
		strTempMessage = Replace(strTempMessage, "<font color=", "[COLOR=", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, "</font>", "[/COLOR]", 1, -1, 1)
		strTempMessage = Replace(strTempMessage, ">", "]", 1, -1, 1)
		
		'Place the new fromatted codes into the message string body
		strMessage = Replace(strMessage, strMessageLink, strTempMessage, 1, -1, 1)
	
	Loop
	
	'Turn any left over font tages to forum codes
	strMessage = Replace(strMessage, "</font>", "[/FONT]", 1, -1, 1)
	
	
	'Turn the HTML back into the charcaters entred by the user
	strMessage = Replace(strMessage, "<", "&lt;", 1, -1, 1)
	strMessage = Replace(strMessage, ">", "&gt;", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "       ", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "      ", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "     ", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;&nbsp;", "    ", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;", "   ", 1, -1, 1)
	strMessage = Replace(strMessage, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", vbTab, 1, -1, 1)
	strMessage = Replace(strMessage, Chr(10), "", 1, -1, 1)
	
	
	
	'Return function
	EditPostConvertion = strMessage
	
End Function






'******************************************
'*** Remove Post Editor Text Function *****
'******************************************

'Format Post Function to covert forum codes to HTML
Private Function removeEditorAuthor(ByVal strMessage)

	Dim lngStartPos
	Dim lngEndPos
		
	'Get the start and end position in the string of the XML to remove
	lngStartPos = InStr(1, strMessage, "<edited>", 1)
	lngEndPos = InStr(1, strMessage, "</edited>", 1) + 9
	If lngEndPos - lngStartPos =< 8 Then lngEndPos = lngStartPos + 9

	'If there is something returned strip the XML from the message
	removeEditorAuthor = Replace(strMessage, Trim(Mid(strMessage, lngStartPos, lngEndPos-lngStartPos)), "", 1, -1, 1)
		
End Function
%>