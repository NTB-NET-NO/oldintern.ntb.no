<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/admin_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim rsSelectForum	'Holds the db recordset
Dim lngTopicID 		'Holds the topic ID number to return to
Dim intForumID		'Holds the forum ID number
Dim intNewForumID	'Holds the new forum ID if the topic is to be moved
Dim strSubject		'Holds the topic subject
Dim intTopicPriority	'Holds the priority of the topic
Dim blnLockedStatus	'Holds the lock status of the topic
Dim strCatName		'Holds the name of the category
Dim intCatID		'Holds the ID number of the category
Dim strForumName	'Holds the name of the forum to jump to
Dim lngFID		'Holds the forum id to jump to
Dim lngPollID		'Holds the poll ID
Dim lngMovedNum		'Holds the moved number if topic has been moved
Dim blnMoved		'Set to true if moved icon is to be shown



'Read in the topic ID number
lngTopicID = CLng(Request("TID"))


'If the person is not an admin or a moderator then send them away
If lngTopicID = "" Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If



'Initliase the SQL query to get the topic details from the database
strSQL = "SELECT " & strDbTable & "Topic.* "
strSQL = strSQL & "FROM " & strDbTable & "Topic "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"


'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
rsCommon.CursorType = 2

'Set the Lock Type for the records so that the record set is only locked when it is updated
rsCommon.LockType = 3

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
End If


'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)




'If this is a post back then  update the database
If (Request.Form("postBack")) AND (blnAdmin OR blnModerator) Then

	strSubject = Trim(Mid(Request.Form("subject"), 1, 41))
	intTopicPriority = CInt(Request.Form("priority"))
	blnLockedStatus = CBool(Request.Form("locked"))
	intNewForumID = CInt(Request.Form("forum"))
	blnMoved = CBool(Request.Form("moveIco"))

	'Get rid of scripting tags in the subject
	strSubject = removeAllTags(strSubject)
	strSubject = formatInput(strSubject)

	'Update the recordset
	With rsCommon

		'Update	the recorset
		If intNewForumID <> 0 Then .Fields("Forum_ID") = intNewForumID
		If blnMoved = false Then 
			.Fields("Moved_ID") = 0
		ElseIf intNewForumID <> 0 AND intNewForumID <> intForumID AND blnMoved Then 
			.Fields("Moved_ID") = intForumID
		End If
		.Fields("Subject") = strSubject
		.Fields("Priority") = intTopicPriority
		.Fields("Locked") = blnLockedStatus

		'Update db
		.Update

		'Re-run the query as access needs time to catch up
		.ReQuery
		
		'Update topic and post count
		updateTopicPostCount(intForumID)

	End With

End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Topic Admin</title>

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001-2004
    	 If you want your forum then goto http://www.webwizforums.com -->

<script language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a subject
	if (document.frmTopicAdmin.subject.value==""){
		errorMsg += "\n\t<% = strTxtErrorTopicSubject %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
</script>

<!--#include file="includes/skin_file.asp" -->

</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<div align="center" class="heading"><% = strTxtTopicAdmin %></div>
    <br /><%

'If there is no topic info returned by the rs then display an error message
If rsCommon.EOF OR (blnAdmin = False AND blnModerator = False) OR bannedIP() OR blnActiveMember = False Then

	'Close the rs
	rsCommon.Close

	Response.Write("<div align=""center"">")
	Response.Write("<span class=""lgText"">" & strTxtTopicNotFoundOrAccessDenied & "</span><br /><br /><br />")
	Response.Write("</div>")

'Else display a form to allow updating of the topic
Else

	'Read in the topic details
	intForumID = CInt(rsCommon("Forum_ID"))
	strSubject = rsCommon("Subject")
	intTopicPriority = CInt(rsCommon("Priority"))
	blnLockedStatus = CBool(rsCommon("Locked"))
	lngPollID = CLng(rsCommon("Poll_ID"))
	lngMovedNum = CLng(rsCommon("Moved_ID"))


	'Close the rs
	rsCommon.Close
%>
<form name="frmTopicAdmin" method="post" action="pop_up_topic_admin.asp" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="450" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td>
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="450">
     <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>">
      <td colspan="2" bgcolor="<% = strTableTitleColour %>" background="<% = strTableTitleBgImage %>" class="text">*<% = strTxtRequiredFields %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtSubjectFolder %>*:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="text" name="subject" size="30" maxlength="41" value="<% = strSubject %>"/></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtPriority %>:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><select name="priority">
           <option value="0"<% If intTopicPriority = 0 Then Response.Write(" selected") %>><% = strTxtNormal %></option>
           <option value="1"<% If intTopicPriority = 1 Then Response.Write(" selected") %>><% = strTxtPinnedTopic %></option>
           <option value="2"<% If intTopicPriority = 2 Then Response.Write(" selected") %>><% = strTopThisForum %></option><%

         	'If this is the forum admin let them post a priority post to all forums
         	If blnAdmin = True Then %>

           <option value="3"<% If intTopicPriority = 3 Then Response.Write(" selected") %>><% = strTxtTopAllForums %></option><%

		End If
        	%>
          </select></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtLockedTopic %>:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="checkbox" name="locked" value="true" <% If blnLockedStatus = True Then Response.Write(" checked") %> /></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text" valign="top"><% = strTxtMoveTopic %>:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><select name="forum"><%

'Create a recordset to hold the forum name and id number
Set rsSelectForum = Server.CreateObject("ADODB.Recordset")


'Read in the category name from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Category.Cat_name, " & strDbTable & "Category.Cat_ID FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"

'Query the database
rsCommon.Open strSQL, adoCon

'Loop through all the categories in the database
Do while NOT rsCommon.EOF

	'Read in the deatils for the category
	strCatName = rsCommon("Cat_name")
	intCatID = Cint(rsCommon("Cat_ID"))

	'Display a link in the link list to the forum
	Response.Write vbCrLf & "		<option value=""0"">" & strCatName & "</option>"

	'Read in the forum name from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum.Forum_ID FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = " & intCatID & " ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"

	'Query the database
	rsSelectForum.Open strSQL, adoCon

	'Loop through all the froum in the database
	Do while NOT rsSelectForum.EOF

		'Read in the forum details from the recordset
		strForumName = rsSelectForum("Forum_name")
		lngFID = CLng(rsSelectForum("Forum_ID"))


		'Display a link in the link list to the forum
		Response.Write vbCrLf & "		<option value=""" & lngFID & """"
		If CInt(Request.QueryString("FID")) = lngFID OR intForumID = lngFID Then Response.Write " selected"
		Response.Write ">&nbsp;&nbsp;-&nbsp;" & strForumName & "</option>"


		'Move to the next record in the recordset
		rsSelectForum.MoveNext
	Loop

	'Close the forum recordset so another can be opened
	rsSelectForum.Close

	'Move to the next record in the recordset
	rsCommon.MoveNext
Loop

'Reset Server Objects
rsCommon.Close
Set rsSelectForum = Nothing
%>
          </select><br />
          <input type="checkbox" name="moveIco" value="true" checked /><% = strTxtShowMovedIconInLastForum %></td>
     </tr><%

'If there is a poll then let the admin moderator edit or delete the poll
If lngPollID <> 0 Then

	%>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtPoll %></td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><a href="delete_poll.asp?TID=<% = lngTopicID %>" onClick="return confirm('<% = strTxtAreYouSureYouWantToDeleteThisPoll %>');"><% = strTxtDeletePoll %></a></td>
     </tr><%
End If

%>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" align="center" />
      <td valign="top" colspan="2" background="<% = strTableBgImage %>" />
        <input type="hidden" name="TID" value="<% = lngTopicID %>" />
        <input type="hidden" name="postBack" value="true" />
        <input type="submit" name="Submit" value="<% = strTxtUpdateTopic %>" />
        <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
</form>
<form name="frmDeleteTopic" method="post" action="delete_topic.asp" OnSubmit="return confirm('<% = strTxtDeleteTopicAlert %>')">
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr>
      <td align="center">
        <input type="hidden" name="TID" value="<% = lngTopicID %>" />
        <input type="submit" name="Submit" value="<% = strTxtDeleteTopic %>" />
      </td>
    </tr>
  </table>
   </form><br /><%

End If

%>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr>
      <td align="center">
        <a href="JavaScript:onClick=window.opener.location.href = window.opener.location.href; window.close();"><% = strTxtCloseWindow %></a>
      </td>
    </tr>
  </table>
<br /><br />
<div align="center">
<%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</div>
</body>
</html>