<%
'member_control_panel.asp
'---------------------------------------------------------------------------------

Const strTxtChangeProfile = "Endre din brukerprofil og dine foretrukne innstillinger for dette forumet"
Const strTxtAlterEmailSubscriptions = "Organiser dine forum- og emnevarslinger (abonement)"
Const strTxtReadandSendPMs = "Les og send private meldinger"
Const strTxtListOfYourForumBuddies = "Vis en liste over de du har p� din venneliste"
Const strTxtBuddyList = "Venneliste"
Const strTxtForumHelp = "Forum hjelp"
Const strTxtForumHelpFilesandFAQsToHelpYou = "Hjelp og F.A.Q for dette forumet (P� engelsk)"
Const strTxtAdminAndModFunc = "Funksjoner for administrator og moderatorer"
Const strTxtAdminFunctionsTo = "Herfra kan du slette, blokkere, endre innstillinger, etc for brukeren"
Const strTxtChangePassAndEmail = "Endre passord for p�logging"
Const strTxtProfileInfo = "Rediger profil"
Const strTxtChangeProfileInfo = "Endre din brukerprofil for dette forumet"
Const strTxtChangeForumPreferences = "Rediger dine foretrukne innstillinger p� dette forumet"

%>