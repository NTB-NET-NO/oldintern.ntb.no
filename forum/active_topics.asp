<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'If you are having problem showing posts witin the last few hours then it could be that you have changed the LCID from what
'the server is set to, if this is the case then you must forced it back to the servers own locale for the filter to run
'Session.LCID = 1033


'Dimension variables
Dim rsTopic			'Holds the Recordset for the Topic details
Dim rsForum			'Holds the forum topic details
Dim intForumID			'Holds the forum ID number
Dim strForumName		'Holds the forum name
Dim lngNumberOfReplies		'Holds the number of replies for a topic
Dim lngTopicID			'Holds the topic ID
Dim strSubject			'Holds the topic subject
Dim strTopicStartUsername 	'Holds the username of the user who started the topic
Dim lngTopicStartUserID		'Holds the users Id number for the user who started the topic
Dim lngNumberOfViews		'Holds the number of views a topic has had
Dim lngLastEntryMessageID	'Holds the message ID of the last entry
Dim strLastEntryUsername	'Holds the username of the last person to post a message in a topic
Dim lngLastEntryUserID		'Holds the user's ID number of the last person to post a meassge in a topic
Dim dtmLastEntryDate		'Holds the date the last person made a post in the topic
Dim intRecordPositionPageNum	'Holds the recorset page number to show the topics for
Dim intTotalNumOfPages		'Holds the total number of pages in the recordset
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intTopicPageLoopCounter	'Holds the number of pages there are in the forum
Dim intLinkPageNum		'Holss the page number to link to
Dim intShowTopicsFrom		'Holds when to show the topics from
Dim strShowTopicsFrom		'Holds the display text from when the topics are shown from
Dim blnForumLocked		'Set to true if the forum is locked
Dim blnTopicLocked		'set to true if the topic is locked
Dim intPriority			'Holds the priority level of the topic
Dim dblActiveFrom		'Holds the time to get active topics from
Dim intNumberOfTopicPages	'Holds the number of topic pages
Dim intTopicPagesLoopCounter	'Holds the number of loops
Dim blnNewPost			'Set to true if the post is a new post since the users last visit
Dim intForumReadRights		'Holds the read rights of the forum
Dim strForumPassword		'Holds the password for the forum
Dim strForumPaswordCode		'Holds the code for the password for the forum
Dim blnForumPasswordOK		'Set to true if the password for the forum is OK
Dim lngPollID			'Holds the topic poll id number
Dim dtmFirstEntryDate		'Holds the date of the first message
Dim intForumGroupPermission	'Holds the group permisison level for forums


'If this is the first time the page is displayed then the Forum Topic record position is set to page 1
If Request.QueryString("PN") = "" OR Request.QueryString("PN") = 0 Then
	intRecordPositionPageNum = 1

'Else the page has been displayed before so the Forum Topic record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("PN"))
End If



'Initliase the forum groip permisions
'If guest group
If intGroupID = 2 Then
	intForumGroupPermission = 1 
'If admin group
ElseIf intGroupID = 1 Then
	intForumGroupPermission = 4
'All other groups
Else
	intForumGroupPermission = 2
End If





'Get what date to show active topics till from cookie
If Request.Cookies("AT") <> "" Then
	intShowTopicsFrom = CInt(Request.Cookies("AT"))

'If this is not the first time the user has visted then use this date to show active topics from
ElseIf Session("dtmLastVisit") < CDate(Request.Cookies(strCookieName)("LTVST")) Then
	intShowTopicsFrom = 1 '1 = last visit
Else
	intShowTopicsFrom = 7 '7 = yesterday
End If

'Initialse the string to display when active topics are shown since
Select Case intShowTopicsFrom
	Case 1
		strShowTopicsFrom = strTxtLastVisitOn & " " & DateFormat(Session("dtmLastVisit"), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(Session("dtmLastVisit"), saryDateTimeData)
		dblActiveFrom = DateDiff("d", Session("dtmLastVisit"), Now()) + 1
	case 2
		strShowTopicsFrom = strTxtLastFifteenMinutes
		dblActiveFrom = 1
	case 3
		strShowTopicsFrom = strTxtLastThirtyMinutes
		dblActiveFrom = 1
	Case 4
		strShowTopicsFrom = strTxtLastFortyFiveMinutes
		dblActiveFrom = 1
	Case 5
		strShowTopicsFrom = strTxtLastHour
		dblActiveFrom = 1
	Case 6
		strShowTopicsFrom = strTxtLastTwoHours
		dblActiveFrom = 1
	Case 7
		strShowTopicsFrom = strTxtLastFourHours
		dblActiveFrom = 1
	Case 8
		strShowTopicsFrom = strTxtLastSixHours
		dblActiveFrom = 1
	Case 9
		strShowTopicsFrom = strTxtLastEightHours
		dblActiveFrom = 1
	Case 10
		strShowTopicsFrom = strTxtLastTwelveHours
		dblActiveFrom = 1
	Case 11
		strShowTopicsFrom = strTxtLastSixteenHours
		dblActiveFrom = 1
	Case 12
		strShowTopicsFrom = strTxtYesterday
		dblActiveFrom = 1
	Case 13
		strShowTopicsFrom = strTxtLastWeek
		dblActiveFrom = 7
	Case 14
		strShowTopicsFrom = strTxtLastMonth
		dblActiveFrom = 28
End Select

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title><% = strMainForumName %>: Active Topics</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to choose how many topics are show
function ShowTopics(Show){

   	strShow = escape(Show.options[Show.selectedIndex].value);
   	document.cookie = "AT=" + strShow

   	if (Show != "") self.location.href = "active_topics.asp?PN=1";
	return true;
}
// -->
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtActiveTopics %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><a href="active_topics.asp" class="boldLink"><% = strTxtActiveTopics %></a><br /></td>
 </tr>
</table>
      <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
        <tr>
          <form>
            <td><span class="text"><% = strTxtShowActiveTopicsSince %></span>
              <select name="show" onChange=ShowTopics(this)>
                <option value="1" <% If intShowTopicsFrom = 1 Then Response.Write "selected" %>><% = DateFormat(Session("dtmLastVisit"), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(Session("dtmLastVisit"), saryDateTimeData) %></option>
                <option value="2" <% If intShowTopicsFrom = 2 Then Response.Write "selected" %>><% = strTxtLastFifteenMinutes %></option>
                <option value="3" <% If intShowTopicsFrom = 3 Then Response.Write "selected" %>><% = strTxtLastThirtyMinutes %></option>
                <option value="4" <% If intShowTopicsFrom = 4 Then Response.Write "selected" %>><% = strTxtLastFortyFiveMinutes %></option>
                <option value="5" <% If intShowTopicsFrom = 5 Then Response.Write "selected" %>><% = strTxtLastHour %></option>
                <option value="6" <% If intShowTopicsFrom = 6 Then Response.Write "selected" %>><% = strTxtLastTwoHours %></option>
                <option value="7" <% If intShowTopicsFrom = 7 Then Response.Write "selected" %>><% = strTxtLastFourHours %></option>
                <option value="8" <% If intShowTopicsFrom = 8 Then Response.Write "selected" %>><% = strTxtLastSixHours %></option>
                <option value="9" <% If intShowTopicsFrom = 9 Then Response.Write "selected" %>><% = strTxtLastEightHours %></option>
                <option value="10" <% If intShowTopicsFrom = 10 Then Response.Write "selected" %>><% = strTxtLastTwelveHours %></option>
                <option value="11" <% If intShowTopicsFrom = 11 Then Response.Write "selected" %>><% = strTxtLastSixteenHours %></option>
                <option value="12" <% If intShowTopicsFrom = 12 Then Response.Write "selected" %>><% = strTxtYesterday %></option>
                <option value="13" <% If intShowTopicsFrom = 13 Then Response.Write "selected" %>><% = strTxtLastWeek %></option>
                <option value="14" <% If intShowTopicsFrom = 14 Then Response.Write "selected" %>><% = strTxtLastMonth %></option>
              </select>
            </td>
          </form>
        </tr>
      </table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" width="3%" class="tHeading" background="<% = strTableTitleBgImage %>">&nbsp;</td>
     <td bgcolor="<% = strTableTitleColour %>" width="41%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtTopics %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="15%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtThreadStarter %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtReplies %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtViews %></td>
     <td bgcolor="<% = strTableTitleColour %>" width="29%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLastPost %></td>
    </tr>
    <%

'Create a record set object to the Topics held in the database
Set rsForum = Server.CreateObject("ADODB.Recordset")

'Set the cursor type property of the record set to dynamic so we can naviagate through the record set
rsForum.CursorType = 1


'Initalise the strSQL variable with an SQL statement to query the database to get the Author and subject from the database for the topic
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ActiveTopics @dblActiveFrom = " & dblActiveFrom & ", @AuthorID = " & lngLoggedInUserID & ", @GroupID = " & intGroupID & ", @GroupPerm = " & intForumGroupPermission
Else
	
	strSQL = "SELECT " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum.Password, " & strDbTable & "Forum.Forum_code, " & strDbTable & "Topic.* "
	strSQL = strSQL & "FROM " & strDbTable & "Category, " & strDbTable & "Forum, " & strDbTable & "Topic "
	strSQL = strSQL & "WHERE ((" & strDbTable & "Category.Cat_ID = " & strDbTable & "Forum.Cat_ID AND " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID) AND (" & strDbTable & "Topic.Last_entry_date > " & strDatabaseDateFunction & " - " & dblActiveFrom & "))"
	strSQL = strSQL & " AND (" & strDbTable & "Forum.[Read] <= " & intForumGroupPermission & " OR (" & strDbTable & "Topic.Forum_ID IN ("
	strSQL = strSQL & "	SELECT " & strDbTable & "Permissions.Forum_ID "
	strSQL = strSQL & "	FROM " & strDbTable & "Permissions "
	strSQL = strSQL & "	WHERE " & strDbTable & "Permissions.Author_ID=" & lngLoggedInUserID & " OR " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND " & strDbTable & "Permissions.[Read]=TRUE))"
	strSQL = strSQL & "	)" 
	strSQL = strSQL & "ORDER BY " & strDbTable & "Category.Cat_order ASC, " & strDbTable & "Forum.Forum_Order ASC, " & strDbTable & "Topic.Last_entry_date DESC;"
End If



'Query the database
rsForum.Open strSQL, adoCon



'Initialse the string to display when active topics are shown since
Select Case intShowTopicsFrom
	Case 1
		'Filter the recorset to leave only active topics since last vists (Filter used for overcome incompatibilty problems between application and database)
		rsForum.Filter = "Last_entry_date > #" & CDate(Session("dtmLastVisit")) & "#"
	case 2
		rsForum.Filter = "Last_entry_date > #" & DateAdd("n", -15, Now()) & "#"
	case 3
		rsForum.Filter = "Last_entry_date > #" & DateAdd("n", -30, Now()) & "#"
	Case 4
		rsForum.Filter = "Last_entry_date > #" & DateAdd("n", -45, Now()) & "#"
	Case 5
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -1, Now()) & "#"
	Case 6
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -2, Now()) & "#"
	Case 7
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -4, Now()) & "#"
	Case 8
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -6, Now()) & "#"
	Case 9
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -8, Now()) & "#"
	Case 10
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -12, Now()) & "#"
	Case 11
		rsForum.Filter = "Last_entry_date > #" & DateAdd("h", -16, Now()) & "#"
End Select

'If there are no active topics display an error msg
If rsForum.EOF Then
	'If there are no Active Topic's to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""6"" class=""text"">" & strTxtNoActiveTopicsSince & " " & strShowTopicsFrom & " " & strTxtToDisplay & "</td>"
End If


	'Disply any active topics in the forum
	If NOT rsForum.EOF Then

	'Read in the forum ID
	intForumID = CInt(rsForum("Forum_ID"))

	'Set the number of records to display on each page
	rsForum.PageSize = intTopicPerPage

	'Get the record poistion to display from
	rsForum.AbsolutePage = intRecordPositionPageNum

	'Count the number of pages there are in the recordset calculated by the PageSize attribute set above
	intTotalNumOfPages = rsForum.PageCount

	'Craete a Recodset object for the topic details
	Set rsTopic = Server.CreateObject("ADODB.Recordset")


	'Loop round to read in all the Topics in the database
	For intRecordLoopCounter = 1 to intTopicPerPage

		'If there are no records left in the recordset to display then exit the for loop
		If rsForum.EOF Then Exit For



		'Read in Topic details from the database
		intForumID = CInt(rsForum("Forum_ID"))
		lngTopicID = CLng(rsForum("Topic_ID"))
		lngPollID = CLng(rsForum("Poll_ID"))
		lngNumberOfViews = CLng(rsForum("No_of_views"))
		strSubject = rsForum("Subject")
		blnTopicLocked = CBool(rsForum("Locked"))
		intPriority = CInt(rsForum("Priority"))
		strForumPassword = rsForum("Password")
		strForumPaswordCode = rsForum("Forum_code")
		



		'If the forum name is different to the one from the last forum display the forum name
		If rsForum("Forum_name") <> strForumName Then

			'Give the forum name the new forum name
			strForumName = rsForum("Forum_name")

			'Display the new forum name
			Response.Write vbCrLf & "<td bgcolor=""" & strTableTitleColour2 & """ background=""" & strTableTitleBgImage2 & """ colspan=""6""><a href=""forum_topics.asp?FID=" & intForumID & """ target=""_self"" class=""cat"">" & strForumName & "</a></td>"
		End If





		'Initalise the strSQL variable with an SQL statement to query the database to get the Author and subject from the database for the topic
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "LastAndFirstThreadAuthor @lngTopicID = " & lngTopicID
		Else
			strSQL = "SELECT " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Author_ID, " & strDbTable & "Thread.Message_date, " & strDbTable & "Author.Username "
			strSQL = strSQL & "FROM " & strDbTable & "Author INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID "
			strSQL = strSQL & "WHERE " & strDbTable & "Thread.Topic_ID = " & lngTopicID & " "
			strSQL = strSQL & "ORDER BY " & strDbTable & "Thread.Message_date ASC;"
		End If

		'Set the cursor type property of the record set to forward only so we can navigate through the record set
		rsTopic.CursorType = 1

		'Query the database
		rsTopic.Open strSQL, adoCon

		'If there is info in the database relating to the topic then get them from the record set
		If NOT rsTopic.EOF Then

			'Read in the subject and author and number of replies from the record set
			strTopicStartUsername = rsTopic("Username")
			lngTopicStartUserID = CLng(rsTopic("Author_ID"))
			lngNumberOfReplies = CLng((rsTopic.RecordCount) - 1)
			dtmFirstEntryDate = CDate(rsTopic("Message_date"))

			'Move to the last record in the record set to get the date and username of the last entry
			rsTopic.MoveLast

			'Read in the username and date of the last entry from the record set
			lngLastEntryMessageID = CLng(rsTopic("Thread_ID"))
			strLastEntryUsername = rsTopic("Username")
			lngLastEntryUserID = CLng(rsTopic("Author_ID"))
			dtmLastEntryDate = CDate(rsTopic("Message_date"))
		End If




		'Set the booleon varible if this is a new post since the users last visit and has not been read
		If (CDate(Session("dtmLastVisit")) < dtmLastEntryDate) AND (Request.Cookies("RT")("TID" & lngTopicID) = "") Then

			blnNewPost = True

		'Else this is not a new post so don't set the booleon to true
		Else
			blnNewPost = False
		End If



		'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
		Response.Write(vbCrLf & "	<tr>")
		Response.Write(vbCrLf & "	<td bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) 
		Response.Write(""" background=""" & strTableBgImage& """ width=""1%"" align=""center"">")


	     	'If the topic is pinned then display the pinned icon
	     	If intPriority = 1 Then
	     		Response.Write("<img src=""" & strImagePath & "pinned_topic_icon.gif"" border=""0"" alt=""" & strTxtPinnedTopic & """>")

	     	'If the topic is top priorty and locked then display top priporty locked icon
	     	ElseIf blnTopicLocked = True AND intPriority > 0 Then
	     	 	Response.Write("<img src=""" & strImagePath & "priority_post_locked_icon.gif"" border=""0"" alt=""" & strTxtHighPriorityPostLocked & """>")

	     	'If the topic is top priorty then display top priporty icon
	     	ElseIf intPriority > 0 Then
	     		Response.Write("<img src=""" & strImagePath & "priority_post_icon.gif"" border=""0"" alt=""" & strTxtHighPriorityPost & """>")

	     	'If the topic is closed display a closed topic icon
	      	ElseIf blnTopicLocked = True Then
	     		Response.Write("<img src=""" & strImagePath & "closed_topic_icon.gif"" border=""0"" alt=""" & strTxtLockedTopic & """>")

	     	'If the topic is a hot topic and with new replies then display hot to new replies icon
	      	ElseIf (lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews) AND (blnNewPost = True) Then
	     		Response.Write("<img src=""" & strImagePath & "hot_topic_new_posts_icon.gif"" border=""0"" alt=""" & strTxtHotTopicNewReplies & """>")

		'If this is a hot topic that contains a poll then display the hot topic poll icon
		ElseIf (lngPollID > 0) AND (lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews) Then
     			Response.Write("<img src=""" & strImagePath & "hot_topic_poll_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")

	     	'If the topic is a hot topic display hot topic icon
	      	ElseIf lngNumberOfReplies >= intNumHotReplies OR lngNumberOfViews >= intNumHotViews Then
	     		Response.Write("<img src=""" & strImagePath & "hot_topic_no_new_posts_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")

	     	'If the topic is has new replies display new replies icon
	      	ElseIf blnNewPost = True Then
	     		Response.Write("<img src=""" & strImagePath & "new_posts_icon.gif"" border=""0"" alt=""" & strTxtOpenTopicNewReplies & """>")

		'If there is a poll in the post display the poll post icon
		ElseIf lngPollID > 0 Then
     			Response.Write("<img src=""" & strImagePath & "poll_icon.gif"" border=""0"" alt=""" & strTxtHotTopic & """>")
     		
	     	'Display topic icon
	     	Else
	     		Response.Write("<img src=""" & strImagePath & "no_new_posts_icon.gif"" border=""0"" alt=""" & strTxtOpenTopic & """>")
	     	End If


     		Response.Write(vbCrLf & "	</td>")
     		Response.Write(vbCrLf & "	<td bgcolor=""")
		If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) 
		Response.Write(""" background=""" & strTableBgImage & """ width=""41%"" class=""text"">")
     
     
     		'If the user is a forum admin then give let them delete the topic
		If blnAdmin Then Response.Write("      <a href=""javascript:openWin('pop_up_topic_admin.asp?TID=" & lngTopicID & "','admin','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')""><img src=""" & strImagePath & "small_admin_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtTopicAdmin & """></a>")
     

		'If there is a poll display a poll text
		If lngPollID <> 0 Then Response.Write(strTxtPoll)


		'Display the subject of the topic
		Response.Write(vbCrLf & "	<a href=""forum_posts.asp?TID=" & lngTopicID)
		If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
		Response.Write(""" target=""_self"" title=""" & strTxtThisTopicWasStarted & DateFormat(dtmFirstEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmFirstEntryDate, saryDateTimeData) & """>" & strSubject & "</a>")
		
		

		 'Calculate the number of pages for the topic and display links if there are more than 1 page
		 intNumberOfTopicPages = ((lngNumberOfReplies + 1)\intThreadsPerPage)

		 'If there is a remainder from calculating the num of pages add 1 to the number of pages
		 If ((lngNumberOfReplies + 1) Mod intThreadsPerPage) > 0 Then intNumberOfTopicPages = intNumberOfTopicPages + 1

		 'If there is more than 1 page for the topic display links to the other pages
		 If intNumberOfTopicPages > 1 Then
		 	Response.Write("<br /><img src=""" & strImagePath & "pages_icon.gif"" align=""middle"" alt=""" & strTxtPages & """>")

		 	'Loop round to display the links to the other pages
		 	For intTopicPagesLoopCounter = 1 To intNumberOfTopicPages

		 		'If there is more than 7 pages display ... last page and exit the loop
		 		If intTopicPagesLoopCounter > 7 Then
		 			
		 			'If this is position 8 then display just the 8th page
		 			If intNumberOfTopicPages = 8 Then
		 				Response.Write(vbCrLf & " <a href=""forum_posts.asp?TID=" & lngTopicID & "&TPN=8")
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">8</a>")
					
					'Else display the last 2 pages
					Else
					
						Response.Write(" ...")
					
						Response.Write(vbCrLf & " <a href=""forum_posts.asp?TID=" & lngTopicID & "&TPN=" & intNumberOfTopicPages - 1)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">" & intNumberOfTopicPages - 1 & "</a>")
			 			
			 			Response.Write(vbCrLf & " <a href=""forum_posts.asp?TID=" & lngTopicID & "&TPN=" & intNumberOfTopicPages)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
			 			Response.Write(""" target=""_self"" class=""smLink"">" & intNumberOfTopicPages & "</a>")
					
					End If

		 			Exit For
		 		End If

		 		'Display the links to the other pages
		 		Response.Write(vbCrLf & " <a href=""forum_posts.asp?TID=" & lngTopicID & "&TPN=" & intTopicPagesLoopCounter)
		 		'If a priority topic need to make sure we don't change forum
		 		If intPriority = 3 Then Response.Write("&FID=" & intForumID & "&PR=3")
		 		Response.Write(""" target=""_self"" class=""smLink"">" & intTopicPagesLoopCounter & "</a>")
		 	Next
		 End If
		  %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="15%" class="text"><a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngTopicStartUserID %>&FID=<% = intForumID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')" title="<% Response.Write(strTxtThisTopicWasStarted & DateFormat(dtmFirstEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmFirstEntryDate, saryDateTimeData)) %>"><% = strTopicStartUsername %></a></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="7%" align="center" class="text"><% = lngNumberOfReplies %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="7%" align="center" class="text"><% = lngNumberOfViews %></td>
     <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="29%" class="smText" align="right" nowrap="nowrap"><% Response.Write(DateFormat(dtmLastEntryDate, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmLastEntryDate, saryDateTimeData)) %>
      <br /><% = strTxtBy %> <a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngLastEntryUserID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')" class="smLink"><% = strLastEntryUsername %></a> <a href="forum_posts.asp?TID=<% = lngTopicID %>&get=last#<% = lngLastEntryMessageID %>" target="_self" class="smLink"><img src="<% = strImagePath %>right_arrow.gif" align="absmiddle" border="0" alt="<% = strTxtViewLastPost %>"></a></td>
    </tr><%

		'Close the topic recordset
              	rsTopic.Close

		'Move to the next database record
		rsForum.MoveNext
	Next
End If


'Clean up
rsForum.Close
Set rsForum = Nothing
Set rsTopic = Nothing


        %>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<br />
<form>
        <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
          <tr>
            <td>
              <!-- #include file="includes/forum_jump_inc.asp" -->
            </td><%
'Release server objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'If there is more than 1 page of topics then dispaly drop down list to the other topics
If intTotalNumOfPages > 1 Then

	'Display an image link to the last topic
	Response.Write (vbCrLf & "		<td align=""right"" class=""text"" nowrap=""nowrap"">")
	
	'Display a prev link if previous pages are available
	If intRecordPositionPageNum > 1 Then Response.Write("<a href=""active_topics.asp?PN=" & intRecordPositionPageNum - 1 & """>&lt;&lt&nbsp;" & strTxtPrevious & "</a>&nbsp;")
		
	Response.Write (strTxtPage & "&nbsp;<select onChange=""ForumJump(this)"" name=""SelectTopicPage"">")

	'Loop round to display links to all the other pages
	For intTopicPageLoopCounter = 1 to intTotalNumOfPages

		'Display a link in the link list to the another topic page
		Response.Write (vbCrLf & "		  <option value=""active_topics.asp?PN=" & intTopicPageLoopCounter & """")

		'If this page number to display is the same as the page being displayed then make sure it's selected
		If intTopicPageLoopCounter = intRecordPositionPageNum Then
			Response.Write (" selected")
		End If

		'Display the link page number
		Response.Write (">" & intTopicPageLoopCounter & "</option>")

	Next

	'End the drop down list
	Response.Write (vbCrLf & "		</select>&nbsp;" & strTxtOf & "&nbsp;" & intTotalNumOfPages)
	
	'Display a next link if needed
	If intRecordPositionPageNum <> intTotalNumOfPages Then Response.Write("&nbsp;<a href=""active_topics.asp?PN=" & intRecordPositionPageNum + 1 & """>" & strTxtNext & "&nbsp;&gt;&gt;</a>")
		
	Response.Write("</td>")
End If
%>	  </tr>
        </table>
</form>
<div align="center">
 <table width="617" border="0" cellspacing="0" cellpadding="2">
  <tr>
   <td width="173" class="text"><img src="<% = strImagePath %>no_new_posts_icon.gif" alt="<% = strTxtOpenTopic %>"> <span class="smText"><% = strTxtOpenTopic %></span></td>
   <td width="174" class="text"><img src="<% = strImagePath %>hot_topic_no_new_posts_icon.gif" alt="<% = strTxtHotTopic %>"> <span class="smText"><% = strTxtHotTopic %></span></td>
   <td width="171" class="text"><img src="<% = strImagePath %>priority_post_icon.gif" alt="<% = strTxtHighPriorityPost %>"> <span class="smText"><% = strTxtHighPriorityPost %></span></td>
   <td width="94" class="text"><img src="<% = strImagePath %>pinned_topic_icon.gif" alt="<% = strTxtPinnedTopic %>"> <span class="smText"><% = strTxtPinnedTopic %></span></td>
  </tr>
  <tr>
   <td width="173" class="text"><img src="<% = strImagePath %>new_posts_icon.gif" alt="<% = strTxtOpenTopicNewReplies %>"> <span class="smText"><% = strTxtOpenTopicNewReplies %></span></td>
   <td width="174" class="text"><img src="<% = strImagePath %>hot_topic_new_posts_icon.gif" alt="<% = strTxtHotTopicNewReplies %>"> <span class="smText"><% = strTxtHotTopicNewReplies %></span></td>
   <td width="171" class="text"><img src="<% = strImagePath %>priority_post_locked_icon.gif" alt="<% = strTxtHighPriorityPostLocked %>"> <span class="smText"><% = strTxtHighPriorityPostLocked %></span></td>
   <td width="94" class="text"><img src="<% = strImagePath %>closed_topic_icon.gif" alt="<% = strTxtLockedTopic %>"> <span class="smText"><% = strTxtLockedTopic %></span></td>
  </tr>
 </table>
 <br />
</div>
<div align="center">
 <%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->