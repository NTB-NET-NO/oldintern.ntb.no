<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 





'If active users is off redirect back to the homepage
If blnActiveUsers = False Then
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If




'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"



'Dimension variables
Dim lngUserID			'Holds the active users ID
Dim strUsername			'Holds the active users username
Dim strForumName 		'Holds the forum name
Dim intGuestNumber		'Holds the Guest Number
Dim dtmLoggedIn			'Holds the date/time the user logged in
Dim dtmLastActive		'Holds the date/time the user was last active
Dim intActiveUsers		'Holds the number of active users
Dim intActiveGuests		'Holds the number of active guests
Dim intActiveMembers		'Holds the number of logged in active members
Dim intForumColourNumber	'Holds the number to calculate the table row colour	
Dim intForumID			'Holds the forum ID number if there is one

'Initilise variables
intActiveMembers = 0
intActiveGuests = 0
intActiveUsers = 0
intGuestNumber = 0
intForumColourNumber = 0


'Sort the active users array
Call SortActiveUsersList(saryActiveUsers)

%>
<html> 
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Discussion Forum Active Users</title>

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001-2004
    	 If you want your forum then goto http://www.webwizforums.com --> 
    	 
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtActiveForumUsers %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtActiveForumUsers %><br /></td>
  </tr>
</table>
   <div align="center"> <br /><%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'Get the number of active users
'Get the active users online
For intArrayPass = 1 To UBound(saryActiveUsers, 2)
	
	'If this is a guest user then increment the number of active guests veriable
	If saryActiveUsers(1, intArrayPass) = 2 Then 	
			
		intActiveGuests = intActiveGuests + 1
	End If
		
Next 

'Calculate the number of members online and total people online
intActiveUsers = UBound(saryActiveUsers, 2)
intActiveMembers = intActiveUsers - intActiveGuests

Response.Write("    	<span class=""text"">" & strTxtThereAreCurrently & " " & intActiveUsers & " " & strTxtActiveUsers & " " & strTxtOnLine & ", "  & intActiveGuests & " " & strTxtGuestsAnd & " " & intActiveMembers & " " & strTxtMembers & "</span><br />")
%>    
    <br />
    <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
         <td bgcolor="<% = strTableTitleColour %>" width="93" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtUsername %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="120" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLoggedIn %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="117" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLastActive %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="66" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtActive %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="95" align="left" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtOS %></td>
         <td bgcolor="<% = strTableTitleColour %>" width="96" align="left" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtBrowser %></td>
        </tr>
        <%  


        		
'display the active users
For intArrayPass = 1 To UBound(saryActiveUsers, 2)

	intForumColourNumber = intForumColourNumber + 1

	'Read in the details from the rs
	lngUserID = saryActiveUsers(1, intArrayPass)
	strUsername = saryActiveUsers(2, intArrayPass)
	dtmLoggedIn = saryActiveUsers(3, intArrayPass)
	dtmLastActive = saryActiveUsers(4, intArrayPass)
	strOS = saryActiveUsers(5, intArrayPass)
	strBrowserUserType = saryActiveUsers(6, intArrayPass)
	blnHideActiveUser = CBool(saryActiveUsers(7, intArrayPass))
	
			
			'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
			%>
        <tr> 
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" width="93" height="24" class="text"><% 
          
         'If the user is a Guest then display them as a Guest
         If lngUserID = 2 Then
         
         	'Add 1 to the Guest number
         	intGuestNumber = intGuestNumber + 1
         	
         	'Display the User as Guest
         	Response.Write(strTxtGuest & " "& intGuestNumber)
         
         'If the user wants to hide there ID then do so 
         ElseIf blnHideActiveUser Then
         	
         	'Display the user as an annoy
         	Response.Write(strTxtAnnoymous)
         
         'Else display the users name
         Else %>
          <a href="JavaScript:openWin('pop_up_profile.asp?PF=<% = lngUserID %>','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"><% = strUsername %></a> 
          <% 
        End If
        %>
         </td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" class="smText"><% Response.Write(DateFormat(dtmLoggedIn, saryDateTimeData) & " " & strTxtAt & "&nbsp;" & TimeFormat(dtmLoggedIn, saryDateTimeData))  %></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" class="smText"><% Response.Write(DateFormat(dtmLastActive, saryDateTimeData) & " " & strTxtAt & "&nbsp;" & TimeFormat(dtmLastActive, saryDateTimeData)) %></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" class="text"><% = DateDiff("n", dtmLoggedIn, dtmLastActive) %>&nbsp;<% = strTxtMinutes %></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" class="text" nowrap="nowrap"><% = strOS %></td>
         <td bgcolor="<% If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour) %>" background="<% = strTableBgImage %>" class="text" nowrap="nowrap"><% = strBrowserUserType %></td>
        </tr>
        <%
		
	   		
Next
	


%>
       </table>
     </tr>
    </table>
    </td>
 </tr>
</table>
    <br /><span class="text"><% = strTxtDataBasedOnActiveUsersInTheLastXMinutes %></span><br /><br /><% 
    
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
   </div>
<!-- #include file="includes/footer.asp" -->