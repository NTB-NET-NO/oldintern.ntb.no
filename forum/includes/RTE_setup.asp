<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Rich Text Editor
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

Dim blnAbout

'Initiliase variables
blnAbout = blnLCode

'The following enables and disables functions of the Rich Text Editor

'To disable or enable functions change the following to true of false

Const blnBold = true
Const blnUnderline = true
Const blnItalic = true
Const blnFontStyle = false
Const blnFontType = true
Const blnFontSize = true
Const blnTextColour = true
Const blnTextBackgroundColour = false
Const blnCut = true
Const blnCopy = true
Const blnPaste = true
Const blnUndo = true
Const blnRedo = true
Const blnLeftJustify = true
Const blnCentre = true
Const blnRightJustify = true
Const blnFullJustify = false
Const blnOrderList = true
Const blnUnOrderList = true
Const blnOutdent = true
Const blnIndent = true
Const blnAddHyperlink = true
Const blnAddImage = true
Const blnInsertTable = false
Const blnHTMLView = false	'Best to leave the HTML view disabled to stop users entering malicious HTML code
Const blnSpellCheck = true
Const blnEmoticonPopUp = false


%>