﻿Imports System
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Globalization

Partial Class rating_rating
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetStats()
    End Sub
    Sub GetStats()
        Try

            Dim myCI As New CultureInfo("nb-NO", True)
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("PortalConString"))

            ' The convert format 105 means 2010-01-01
            ' Using Having to get matches that are above 10
            Dim strSQLQuery As String = "SELECT TOP 10 COUNT(*) as Treff, ARTICLES.ArticleTitle as Tittel FROM dbo.STATS " & _
"LEFT JOIN ARTICLES ON STATS.ArticleID=ARTICLES.RefID " & _
"WHERE ArticleID != 0 " & _
"AND CONVERT(VARCHAR(10), RecordDate, 105) = CONVERT(VARCHAR(10), GETDATE()-1, 105) " & _
"GROUP BY STATS.ArticleID, ARTICLES.ArticleTitle " & _
"HAVING COUNT(*) > 10 " & _
"ORDER BY COUNT(*) DESC;"

            Dim cmd As New SqlCommand(strSQLQuery, dbNTB)

            Dim adapter As SqlDataAdapter = New SqlDataAdapter(cmd)


            ' Opens the connection to the database
            dbNTB.Open()

            ' Now get the article names from the result

            Dim ds As DataSet = New DataSet()
            ds.Locale = myCI

            adapter.Fill(ds, "ArticleHits")

            
            Dim gwStats As GridView = New GridView()

            gwStats.RowStyle.BackColor = Color.AliceBlue
            gwStats.AlternatingRowStyle.BackColor = Color.LightBlue
            gwStats.RowStyle.ForeColor = Color.Black

            gwStats.HeaderStyle.BackColor = Color.DarkBlue
            gwStats.HeaderStyle.ForeColor = Color.White


            

            gwStats.DataSource = ds

            gwStats.DataBind()

            dbNTB.Close()

            form1.Controls.Add(gwStats)


        Catch sex As SqlException
            Response.Write(sex.Message.ToString())
            Response.Write(sex.StackTrace.ToString())

            'If sex.InnerException.Message.ToString() <> "" Then
            '    Response.Write(sex.InnerException.Message.ToString())
            'End If
        Catch ex As Exception
            Response.Write(ex.Message.ToString())
            Response.Write(ex.StackTrace.ToString())
        End Try








    End Sub
    


End Class
