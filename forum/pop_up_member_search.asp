<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True 


Dim strUsername	'Holds the users username


'If this is a post back then search the member list
If Request.Form("name") <> "" Then

	'Read in the username
	strUsername = Request.Form("name")
	
	'Take out parts of the username that are not permitted
	strUsername = disallowedMemberNames(strUsername)
	
	'Get rid of milisous code
	strUsername = formatSQLInput(strUsername)
	
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Username Like '" & strUsername & "%' "
	strSQL = strSQL & "ORDER BY " & strDbTable & "Author.Username ASC;"
		
	'Query the database
	rsCommon.Open strSQL, adoCon
End If

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Member Search</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";
	
	//Check for a Username
	if (document.frmMemSearch.name.value==""){
	
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
	
		alert(msg + "\n\t<% = strTxtErrorUsername %>");
		document.frmMemSearch.name.focus();
		return false;
	}
	
	return true;
}


//Function to place the username in the text box of the opening frame
function getUserName(selectedName)
{
	
	window.opener.document.<% If Request.QueryString("RP") = "BUD" Then Response.Write("frmBuddy.username") Else Response.Write("frmAddMessage.member") %>.focus();
	window.opener.document.<% If Request.QueryString("RP") = "BUD" Then Response.Write("frmBuddy.username") Else Response.Write("frmAddMessage.member") %>.value = selectedName;
	window.close();
}
</script>
<!--#include file="includes/skin_file.asp" -->
</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0">
<div align="center" class="heading">
 <% = strTxtMemberSearch %>
</div>
<br />
<form method="post" name="frmMemSearch" action="pop_up_member_search.asp<% If Request.QueryString("RP") = "BUD" Then Response.Write("?RP=BUD") %>" onSubmit="return CheckForm();">
 <br />
 <table width="390" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% = strTableBorderColour %>" height="30">
  <tr> 
   <td height="2" width="483" align="center"> <table width="100%" border="0" cellspacing="1" cellpadding="2">
     <tr> 
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" height="26"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr> 
         <td width="32%" align="right"><span class="text"><% = strTxtMemberSearch %>:</span>&nbsp;&nbsp;</td>
         <td width="68%"><input type="text" name="name" size="15" maxlength="15" value="<% = strUsername %>"> <input type="submit" name="Submit" value="<% = strTxtSearch %>"></td>
        </tr><%
'If this is a post back then display the results
If Request.Form("name") <> "" Then

%>        
        <tr> 
         <td align="right">&nbsp;</td>
         <td>&nbsp;</td>
        </tr>
        <tr> 
        <td width="32%" align="right"><span class="text"><% = strTxtSelectMember %>:</span>&nbsp;&nbsp;</td>
         <td width="68%"><select name="userName"><%

	'If there are no records found then display an error message
	If rsCommon.EOF then
		
		Response.Write("<option value="""" selected>" & strTxtNoMatchesFound & "</option>") 
	
	'Else there are matches found so display the result
	Else   
	
		'Loop through the recordset
		Do while NOT rsCommon.EOF  
		
			'Disply the usernames found
			Response.Write("<option value=""" & rsCommon("Username") & """>" & rsCommon("Username") & "</option>")       
           		
           		'Jump to the next record in recordset
           		rsCommon.MoveNext
           
           	Loop
           
        End If        
        %>
          </select> <input type="button" name="Button" value="<% = strTxtSelect %>" onClick="getUserName(frmMemSearch.userName.options[frmMemSearch.userName.selectedIndex].value);"></td>
        </tr><% 
        
        'Clean up
        rsCommon.Close
        
End If        

%>       
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
</form>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
 <tr> 
  <td align="center"><a href="JavaScript:onClick=window.close()">
   <% = strTxtCloseWindow %>
   </a> <br />
   <br />
   <br /> 
   <% 
'Reset Server Objects
Set rsCommon = Nothing 
adoCon.Close
Set adoCon = Nothing  

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
  </td>
 </tr>
</table>
</body>
</html>
