<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************




'Set the response buffer to true
Response.Buffer = True

Dim strGroupName
Dim intSelGroupID


%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Group Permissions</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">User Group Permissions</span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br />
  When you created a forum you created Generic Permissions for that forum to allow users to do things like, View, Post, Reply, etc. in that forum.<br>
  <br />
  From here you can overwrite these Generic Forum Permissions for different User Groups, allowing different User Groups to have different permissions on forums, you can also select User Groups to be able 
  to have moderator privileges on forums.<br>
  <br />
  Please note that User Group Permissions can be overridden by setting permissions for individual users on forums.<br>
  <br />
  Select the User Group that you would like to Create, Edit, or Remove Permissions for.<br />
 </p>
</div>
<form method="post" name="frmSelectForum" action="forum_group_permissions.asp">
 <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000" height="8">
  <tr>
   <td height="24"> <table border="0" align="center"  height="8" cellpadding="4" cellspacing="1" width="100%">
     <tr bgcolor="#CCCEE6" >
      <td width="51%" height="2" align="left" valign="top" class="tHeading"><b>Select the User Group you would like to create or Edit permissions for</b></td>
     </tr>
     <tr bgcolor="#FFFFFF">
      <td width="51%"  height="12" align="left" bgcolor="#F5F5FA"> 
       <select name="GID"><%

'Read in the group name from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Group.Group_ID, " & strDbTable & "Group.Name FROM " & strDbTable & "Group ORDER BY " & strDbTable & "Group.Group_ID ASC;"

'Query the database
rsCommon.Open strSQL, adoCon


'Loop through all the categories in the database
Do while NOT rsCommon.EOF

	'Read in the deatils for the category
	strGroupName = rsCommon("Name")
	intSelGroupID = CInt(rsCommon("Group_ID"))

	'Display a link in the link list to the cat
	Response.Write (vbCrLf & "		<option value=""" & intSelGroupID & """")
	Response.Write(">" & strGroupName & "</option>")


	'Move to the next record in the recordset
	rsCommon.MoveNext
Loop

'Reset server objects
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%></select>
      </td>
     </tr>
    </table></td>
  </tr>
 </table>
 <div align="center"><br />
  <input type="submit" name="Submit" value="Edit or Create User Group Permissions">
 </div>
</form>
<br />
</body>
</html>
