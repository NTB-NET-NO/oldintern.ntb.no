/*
 overlibmws_shadow.js plug-in module - Copyright Foteos Macrides 2003-2004
   For support of the SHADOW feature.
   Initial: July 14, 2003 - Last Revised: Narch 22, 2004
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License agreement for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=0;
registerCommands('shadow,shadowx,shadowy,shadowcolor,shadowimage,shadowopacity');

/////////
// DEFAULT CONFIGURATION
if(typeof ol_shadow=='undefined')var ol_shadow=0;
if(typeof ol_shadowx=='undefined')var ol_shadowx=5;
if(typeof ol_shadowy=='undefined')var ol_shadowy=5;
if(typeof ol_shadowcolor=='undefined')var ol_shadowcolor="666666";
if(typeof ol_shadowimage=='undefined')var ol_shadowimage="";
if(typeof ol_shadowopacity=='undefined')var ol_shadowopacity=60;
// END CONFIGURATION
////////

// INIT
var o3_shadow=0;
var o3_shadowx=5;
var o3_shadowy=5;
var o3_shadowcolor="#CCCCCC";
var o3_shadowimage="";
var o3_shadowopacity=60;
var bkdrop=null;

// For setting runtime variables to default values.
function setShadowVar(){
o3_shadow=ol_shadow;
o3_shadowx=ol_shadowx;
o3_shadowy=ol_shadowy;
o3_shadowcolor=ol_shadowcolor;
o3_shadowimage=ol_shadowimage;
o3_shadowopacity=ol_shadowopacity;
}

// For commandline parser.
function parseShadowExtras(pf,i,ar){
var k=i;
if(k<ar.length){
if(ar[k]==SHADOW){eval(pf+'shadow=('+pf+'shadow==0)?1:0');return k;}
if(ar[k]==-SHADOW){eval(pf+'shadow=0');return k;}
if(ar[k]==SHADOWX){eval(pf+'shadowx='+ar[++k]);return k;}
if(ar[k]==SHADOWY){eval(pf+'shadowy='+ar[++k]);return k;}
if(ar[k]==SHADOWCOLOR){eval(pf+"shadowcolor='"+escSglQuote(ar[++k])+"'");return k;}
if(ar[k]==SHADOWIMAGE){eval(pf+"shadowimage='"+escSglQuote(ar[++k])+"'");return k;}
if(ar[k]==SHADOWOPACITY){eval(pf+'shadowopacity='+ar[++k]);return k;}}
return -1;
}

////////
// DROPSHADOW SUPPORT FUNCTIONS
////////
function dispShadow(){
if(o3_shadow){
getShadowLyrRef();
if(bkdrop)generateShadowLyr();}
}

function initShadowLyrRef(){
if(OLie55&&OLfilterPI&&o3_filtershadow){o3_shadow=0;return}
var theObj=(OLns4?o3_frame.document.layers['backdrop']:(OLie4?o3_frame.document.all['backdrop']:
(OLns6?o3_frame.document.getElementById('backdrop'):null)));
if(typeof theObj=='undefined'||!theObj){
if(OLns4&&document.classes){theObj=o3_frame.document.layers['backdrop']=new Layer(1024,o3_frame);
}else if(OLie4||OLns6){var body=(OLie4?o3_frame.document.all.tags('body')[0]:
o3_frame.document.getElementsByTagName('body')[0]);
theObj=o3_frame.document.createElement('div');theObj.id='backdrop';
with(theObj.style){position='absolute';visibility='hidden';}
body.appendChild(theObj);}}
if(typeof theObj=='undefined'||!theObj||bkdrop!=theObj){
bkdrop=null;getShadowLyrRef();}
}

function getShadowLyrRef(){
if(bkdrop||!o3_shadow)return;
bkdrop=(OLns4?o3_frame.document.layers['backdrop']:(OLie4?o3_frame.document.all['backdrop']:
(OLns6?o3_frame.document.getElementById('backdrop'):null)));
if(typeof bkdrop=='undefined'||!bkdrop){o3_shadow=0;bkdrop=null;}
}

function generateShadowLyr(){
var wd=(OLns4?over.clip.width:over.offsetWidth),hgt=(OLns4?over.clip.height:over.offsetHeight);
if(OLns4){
with(bkdrop.clip){width=wd;height=hgt;}
if(o3_shadowimage)bkdrop.background.src=o3_shadowimage;
else with(bkdrop){bgColor=o3_shadowcolor;zIndex=over.zIndex -1;}
}else{
with(bkdrop.style){width=wd+'px';height=hgt+'px';}
if(o3_shadowimage)bkdrop.style.backgroundImage="url("+o3_shadowimage+")";
else bkdrop.style.backgroundColor=o3_shadowcolor;
with(bkdrop.style){clip='rect(0px '+wd+'px '+hgt+'px 0px)';zIndex=over.style.zIndex -1;}
if(o3_shadowopacity){
var op=o3_shadowopacity;op=(op<=100&&op>0?op:100);
if(OLie4&&!OLieM&&typeof bkdrop.style.filter=='string'){
bkdrop.style.filter='Alpha(opacity='+op+')';
bkdrop.filters.alpha.enabled=1;
}else if(typeof bkdrop.style.MozOpacity=='string')bkdrop.style.MozOpacity=op/100;}}
}

function cleanUpShadowEffects(){
if(OLns4){
with(bkdrop){bgColor=null;background.src=null;}
}else{
with(bkdrop.style){backgroundColor='transparent';backgroundImage='none';}
if(OLie4&&!OLieM&&typeof bkdrop.style.filter=='string'){
bkdrop.style.filter='Alpha(opacity=100)';bkdrop.filters.alpha.enabled=0;}
else if(typeof bkdrop.style.MozOpacity=='string')bkdrop.style.MozOpacity=1.0;
if(OLns6){
with(bkdrop.style){width=1+'px';height=1+'px';}
repositionTo(bkdrop,o3_frame.pageXOffset,o3_frame.pageYOffset);}}
}

function showShadow(){
if(bkdrop&&o3_shadow){
var theObj=(OLns4?bkdrop:bkdrop.style);
theObj.visibility="visible";}
}

function hideShadow(){
if(bkdrop&&o3_shadow){
var theObj=(OLns4?bkdrop:bkdrop.style);
theObj.visibility="hidden";
cleanUpShadowEffects();}
}

function repositionShadow(X,Y){
if(bkdrop&&o3_shadow)repositionTo(bkdrop,X+o3_shadowx,Y+o3_shadowy);
}

////////
// PLUGIN REGISTRATIONS
////////
registerRunTimeFunction(setShadowVar);
registerCmdLineFunction(parseShadowExtras);

OLshadowPI=1;
OLloaded=1;
