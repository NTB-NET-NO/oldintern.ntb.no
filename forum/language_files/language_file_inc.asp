<%
'Global
'---------------------------------------------------------------------------------
Const strTxtWelcome = "Velkommen"
Const strTxtAllForums = "Alle forum"
Const strTxtTopics = "Emner"
Const strTxtPosts = "Innlegg"
Const strTxtLastPost = "Siste innlegg"
Const strTxtPostPreview = "Forh�ndsvisning"
Const strTxtAt = "kl."
Const strTxtBy = "av"
Const strTxtOn = "p�"
Const strTxtProfile = "Profil"
Const strTxtSearch = "S�ke"
Const strTxtQuote = "Sitat"
Const strTxtVisit = "Bes�k"
Const strTxtView = "Vis"
Const strTxtHome = "Hjem"
Const strTxtHomepage = "Hjemmeside"
Const strTxtEdit = "Endre"
Const strTxtDelete = "Slette"
Const strTxtEditProfile = "Endre profil"
Const strTxtLogOff = "Logge ut"
Const strTxtRegister = "Registrere"
Const strTxtLogin = "Logge inn"
Const strTxtMembersList = "Vis medlemsliste for forum"
Const strTxtForumLocked = "Forum l�st"
Const strTxtSearchTheForum = "S�k i forum"
Const strTxtPostReply = "Svar p� innlegg"
Const strTxtNewTopic = "Send innlegg"
Const strTxtCloseWindow = "Lukk vindu"
Const strTxtNoForums = "Ingen forum � vise"
Const strTxtReturnToDiscussionForum = "Tilbake til diskusjonsforum"
Const strTxtMustBeRegistered = "Du m� registrere deg for � bruke dette forum."
Const strClickHereIfNotRegistered = "Klikk her hvis du ikke er registrert"
Const strTxtResetForm = "Angre inntasting"
Const strTxtClearForm = "Nullstill skjema"
Const strTxtYes = "Ja"
Const strTxtNo = "Nei"
Const strTxtForumLockedByAdmim = "Beklager, denne funksjonen er deaktivert.<br />Forumet er l�st av Administrator."
Const strTxtRequiredFields = "Betyr n�dvendig opplysning"
Const strTxtForumJump = "Bytt forum"
Const strTxtSelectForum = "Velg forum"

'Global Error
'---------------------------------------------------------------------------------
Const strTxtErrorDisplayLine = "_______________________________________________________________"
Const strTxtErrorDisplayLine1 = "Skjemaet er ikke lagret fordi det er et problem med innholdet."
Const strTxtErrorDisplayLine2 = "Korriger problemene og lagre skjemaet p� nytt."
Const strTxtErrorDisplayLine3 = "F�lgende felt m� korrigeres: -"
Const strResetFormConfirm = "Er du sikker p� at du vil nullstille?"

'default.asp
'---------------------------------------------------------------------------------
Const strTxtCookies = "Cookies og JavaScript m� v�re aktivert i nettleseren for � kunne bruke dette forum."
Const strTxtForum = "Forum"
Const strTxtLatestForumPosts = "Siste innlegg"
Const strTxtForumStatistics = "Statistikk"
Const strTxtNoForumPostMade = "Ingen innlegg"
Const strTxtThereAre = "Det fins"
Const strTxtPostsIn = "innlegg i"
Const strTxtTopicsIn = "emner i"
Const strTxtLastPostOn = "Siste innlegg"
Const strTxtLastPostBy = "Siste innlegg av"
Const strTxtForumMembers = "medlemmer"
Const strTxtTheNewestForumMember = "Nyeste medlem er"

'forum_topics.asp
'---------------------------------------------------------------------------------
Const strTxtThreadStarter = "Opprettet av"
Const strTxtReplies = "Svar"
Const strTxtViews = "Visninger"
Const strTxtDeleteTopicAlert = "Vil du slette dette emnet?"
Const strTxtDeleteTopic = "Slette emne"
Const strTxtNextTopic = "Neste emne"
Const strTxtLastTopic = "Siste emne"
Const strTxtShowTopics = "Vis emner"
Const strTxtNoTopicsToDisplay = "Ingen innlegg for den valgte tidsperioden."
Const strTxtAll = "Alle"
Const strTxtLastWeek = "for siste uke"
Const strTxtLastTwoWeeks = "for 2 uker"
Const strTxtLastMonth = "for siste m�ned"
Const strTxtLastTwoMonths = "for siste 2 m�neder"
Const strTxtLastSixMonths = "for siste 6 m�neder"
Const strTxtLastYear = "for siste �r"

'forum_posts.asp
'---------------------------------------------------------------------------------
Const strTxtLocation = "Hjemsted"
Const strTxtJoined = "Tilsluttet"
Const strTxtForumAdministrator = "Administrator"
Const strTxtForumModerator = "Moderator"
Const strTxtDeletePostAlert = "Vil du slette dette innlegget?"
Const strTxtEditPost = "Endre innlegg"
Const strTxtDeletePost = "Slette innlegg"
Const strTxtSearchForPosts = "S�k etter andre innlegg av"
Const strTxtSubjectFolder = "Emne" 'Tittel
Const strTxtPrintVersion = "Utskriftsvennlig versjon"
Const strTxtEmailTopic = "Send emne med epost"
Const strTxtSorryNoReply = "Beklager, du kan ikke svare p� dette innlegget."
Const strTxtThisForumIsLocked = "Dette forum er l�st av Administrator."
Const strTxtPostAReplyRegister = "Hvis du vil svare p� dette emnet m� du f�rst"
Const strTxtNeedToRegister = "Hvis du ikke er registrert m� du f�rst"
Const strTxtSmRegister = "registrere deg"
Const strTxtNoThreads = "Finner ingen innlegg under dette emnet i databasen"
Const strTxtNotGiven = "Ugyldig"
Const strTxtNoMessageError = "Innlegg \t\t- Tast inn et innlegg"

'search_form.asp
'---------------------------------------------------------------------------------
Const strTxtSearchFormError = "S�k etter\t- Du m� anngi noe � s�ke etter."

'search.asp
'---------------------------------------------------------------------------------
Const strTxtSearchResults = "S�keresultater"
Const strTxtYourSearchFor = "Du s�kte etter"
Const strTxtHasFound = "og har funnet"
Const strTxtResults = "post(er)"
Const strTxtNoSearchResults = "Beklager, ditt s�k ga ingen resultater"
Const strTxtClickHereToRefineSearch = "Klikk her for � begrense s�keresultatet"
Const strTxtSearchFor = "S�k etter"
Const strTxtSearchIn = "S�k i"
Const strTxtSearchOn = "S�k p�"
Const strTxtAllWords = "Alle ord"
Const strTxtAnyWords = "Hvilke ord som helst"
Const strTxtPhrase = "Del av ord"
Const strTxtTopicSubject = "Bare innleggstittel"
Const strTxtMessageBody = "Bare innleggstekst"
Const strTxtAllOfMessage = "B�de tekst og tittel"
Const strTxtAuthor = "Forfatter"
Const strTxtSearchForum = "S�k i forum"
Const strTxtSortResultsBy = "Sorter resultatet etter"
Const strTxtLastPostTime = "Tid for siste innlegg"
Const strTxtTopicStartDate = "Startdato for emne"
Const strTxtSubjectAlphabetically = "Tittel alfabetisk"
Const strTxtNumberViews = "Antall visninger"
Const strTxtStartSearch = "Start s�k"

'printer_friendly_posts.asp
'---------------------------------------------------------------------------------
Const strTxtPrintPage = "Skriv ut side"
Const strTxtPrintedFrom = "Utskrift fra"
Const strTxtForumName = "Forumnavn"
Const strTxtForumDiscription = "Forumbeskrivelse"
Const strTxtURL = "URL"
Const strTxtPrintedDate = "Utskrift dato"
Const strTxtTopic = "Emne"
Const strTxtPostedBy = "Opprettet av"
Const strTxtDatePosted = "Opprettet dato"

'emoticons.asp
'---------------------------------------------------------------------------------
Const strTxtEmoticonSmilies = "Uttrykksikoner"
Const strTxtClickOnEmoticonToAdd = "Klikk p� ikonet du vil bruke i innlegget."

'login.asp
'---------------------------------------------------------------------------------
Const strTxtSorryUsernamePasswordIncorrect = "Beklager, brukernavnet eller passordet er feil."
Const strTxtPleaseTryAgain = "Pr�v igjen."
Const strTxtUsername = "Brukernavn"
Const strTxtPassword = "Passord"
Const strTxtLoginUser = "Innlogging"
Const strTxtClickHereForgottenPass = "Glemt passordet ditt?"
Const strTxtErrorUsername = "Brukernavn \t- Oppgi brukernavn"
Const strTxtErrorPassword = "Passord \t- Oppgi passord"

'forgotten_password.asp
'---------------------------------------------------------------------------------
Const strTxtForgottenPassword = "Glemt passord"
Const strTxtNoRecordOfUsername = "Beklager. Epost adressen stemmer ikke med den som er lagt inn for brukernavnet."
Const strTxtNoEmailAddressInProfile = "Beklager. Din profil inneholder ingen epost adresse.<br />Ditt nye passord kan ikke sendes til den med epost."
Const strTxtReregisterForForum = "Du m� omregistrere deg for � kunne bruke forumet."
Const strTxtPasswordEmailToYou = "Ditt nye passord er sedt til deg med epost."
Const strTxtPleaseEnterYourUsername = "Tast inn brukernavn og epostadresse i feltene under.<br />Ditt nye passord vil bli sendt til epostadressen i din profil."
Const strTxtValidEmailRequired = "Hvis din profil ikke inneholder en gyldin epostadresse, m� du omregistrere deg for � kunne bruke forumet."
Const strTxtEmailPassword = "Send passord med epost"
Const strTxtEmailPasswordRequest = "P.g.a. glemt passord er et nytt passord sendt med epost for forum, "
Const strTxtEmailPasswordRequest2 = "Ditt nye passord er: -"
Const strTxtEmailPasswordRequest3 = "Klikk p� linken under for � g� til forum: -"

'forum_password_form.asp
'---------------------------------------------------------------------------------
Const strTxtForumLogin = "Forum innlogging"
Const strTxtErrorEnterPassword = "Passord \t- Tast passord for � bruke dette forum"
Const strTxtPasswordRequiredForForum = "Dette er et privat forum. Du m� taste inn passord for � kunne fortsette."
Const strTxtForumPasswordIncorrect = "Beklager. Feil passord."
Const strTxtAutoLogin = "Automatisk innlogging"
Const strTxtLoginToForum = "Logg inn p� Forum"

'profile.asp
'---------------------------------------------------------------------------------
Const strTxtNoUserProfileFound = "Beklager. Ingen profil for denne brukeren."
Const strTxtRegisteredToViewProfile = "Beklager. Du m� v�re en registrert bruker for � se profiler."
Const strTxtMemberNo = "Medlem nr."
Const strTxtEmail = "Epostadresse"
Const strTxtPrivate = "Privat"

'post_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtPostNewTopic = "Nytt emne"
Const strTxtErrorTopicSubject = "Tittel \t\t- Tast inn en tittel for ditt nye emne"
Const strTxtForumMemberSuspended = "Beklager. Denne funksjonen er deaktivert fordi ditt medlemskap ikke er aktivert!"

'edit_post.asp
'---------------------------------------------------------------------------------
Const strTxtNoPermissionToEditPost = "Beklager. Du har ikke anledning til � endre dette innlegget!"
Const strTxtReturnForumTopic = "Tilbake til emne"

'email_topic.asp
'---------------------------------------------------------------------------------
Const strTxtEmailTopicToFriend = "Send emne med epost til en venn"
Const strTxtFriendSentEmail = "Epost sendt til din venn"
Const strTxtFriendsName = "Navn p� min venn"
Const strTxtFriendsEmail = "Epost til min venn"
Const strTxtYourName = "Ditt navn"
Const strTxtYourEmail = "Din epost"
Const strTxtSendEmail = "Send epost"
Const strTxtMessage = "Innlegg"
Const strTxtEmailFriendMessage = "Jeg tenkte du kunne v�re interesrert i en melding om"
Const strTxtFrom = "fra"
Const strTxtErrorFrinedsName = "Navn p� min venn \t- Tast inn din venns navn"
Const strTxtErrorFriendsEmail = "Epost til min venn \t- Tast inn en gyldig epostadresse til din venn"
Const strTxtErrorYourName = "Ditt navn \t- Tast inn ditt navn"
Const strTxtErrorYourEmail = "Din epost \t- Tast inn din gyldige epostadresse"
Const strTxtErrorEmailMessage = "Melding \t- Tast inn en melding til din venn"

'members.asp
'---------------------------------------------------------------------------------
Const strTxtForumMembersList = "Medlemsliste"
Const strTxtMemberSearch = "S�k etter medlem"
Const strTxtForumMembersOn = "medlemmer p�"
Const strTxtPageYouAerOnPage = "sider og du er p� side nr."
Const strTxtYourSearchMembersFound = "Ditt medlemss�k fant"
Const strTxtMatches = "poster"
Const strTxtUsernameAlphabetically = "Brukernavn alfabetisk"
Const strTxtNewForumMembersFirst = "Nye medlemmer f�rst"
Const strTxtOldForumMembersFirst = "Gamle medlemmer f�rst"
Const strTxtLocationAlphabetically = "Hjemsted alfabetisk"
Const strTxtRegistered = "Registrert"
Const strTxtSend = "Send"
Const strTxtNext = "Neste"
Const strTxtPrevious = "Forrige"
Const strTxtPage = "Side"
Const strTxtErrorMemberSerach = "Medlemss�k\t- Tast inn brukernavn p� den du s�ker"

'message_form.asp
'---------------------------------------------------------------------------------
Const strTxtTextFormat = "Tekstformat"
Const strTxtPreviewPost = "Forh�ndsvis"
Const strTxtMode = "Modus"
Const strTxtPrompt = "Be om"
Const strTxtBasic = "Grunnleggende"
Const strTxtAddEmailLink = "Legg til epostlink"
Const strTxtList = "Liste"
Const strTxtCentre = "Sentrert"
Const strTxtEnterBoldText = "Skriv inn teksten du vil ha uthevet"
Const strTxtEnterItalicText = "Skriv inn teksten du vil ha kursivert"
Const strTxtEnterUnderlineText = "Skriv inn teksten du vil ha underlinjert"
Const strTxtEnterCentredText = "Skriv inn teksten du vil ha sentrert"
Const strTxtEnterHyperlinkText = "Skriv inn skjermteksten for hyperlinken"
Const strTxtEnterHeperlinkURL = "Skriv inn addressen p� dokumentet du vil lage link til"
Const strTxtEnterEmailText = "Skriv inn visningsteksten for epostadressen"
Const strTxtEnterEmailMailto = "Skriv inn epostadressen du vil linke til"
Const strTxtEnterImageURL = "Skriv inn nett-adressen for bildet"
Const strTxtEnterTypeOfList = "Listetype"
Const strTxtEnterEnter = "Enter"
Const strTxtEnterNumOrBlankList = "for nummerert eller blank for punktmarkert"
Const strTxtEnterListError = "FEIL! Skriv inn"
Const strEnterLeaveBlankForEndList = "Blank linje for � slutte liste"

'IE_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtCut = "Klipp ut"
Const strTxtCopy = "Kopier"
Const strTxtPaste = "Lim inn"
Const strTxtBold = "Uthev"
Const strTxtItalic = "Kursiv"
Const strTxtUnderline = "Understrek"
Const strTxtLeftJustify = "Venstrejustert"
Const strTxtCentrejustify = "Midtstilt"
Const strTxtRightJustify = "H�yrejustert"
Const strTxtUnorderedList = "Usortert liste"
Const strTxtOutdent = "Utrykk"
Const strTxtIndent = "Innrykk"
Const strTxtAddHyperlink = "Legg til hyperlink"
Const strTxtAddImage = "Legg til bilde"
Const strTxtJavaScriptEnabled = "JavaScript m� v�re aktivert i din nettleser for � kunne bruke dette forum!"
Const strTxtShowSignature = "Vis signatur"
Const strTxtEmailNotify = "Varsle meg (via epost) n�r noen svarer"
Const strTxtUpdatePost = "Oppdater innlegg"
Const strTxtFontColour = "Farge"

'register.asp
'---------------------------------------------------------------------------------
Const strTxtRegisterNewUser = "Registrer ny bruker"
Const strTxtProfileUsernameLong = "Dette er navnet som vises n�r du bruker dette forum"
Const strTxtRetypePassword = "Gjenta passord"
Const strTxtProfileEmailLong = "Ikke n�dvendig, men nyttig hvis du mister ditt passord eller vil bli informert om svar p� dine innlegg"
Const strTxtShowHideEmail = "Vis min epostadresse"
Const strTxtShowHideEmailLong = "Skjul din epostadresse hvis du ikke vil vise den for andre brukere"
Const strTxtSelectCountry = "Velg land"
Const strTxtProfileAutoLogin = "Logg meg inn automatisk n�r jeg kommer tilbake til dette forum"
Const strTxtSignature = "Signatur"
Const strTxtSignatureLong = "Tast en signatur du vil vise p� slutten av dine innlegg"
Const strTxtErrorUsernameChar = "Brukernavn \t- Ditt brukernavn m� v�re minst 4 tegn"
Const strTxtErrorPasswordChar = "Passord \t- Ditt passord m� v�re minst 4 tegn"
Const strTxtErrorPasswordNoMatch = "Feil passord\t- Passord stemmer ikke"
Const strTxtErrorValidEmail = "Epost\t\t- Tast din epostadresse"
Const strTxtErrorValidEmailLong = "Hvis du ikke vil bruke din epostadresse kan du la feltet v�re tomt."
Const strTxtErrorNoEmailToShow = "Du kan ikke vise epostadressen din hvis du ikke har tastet den inn."
Const strTxtErrorSignatureToLong = "Signatur \t- Din signatur har for mange tegn"
Const strTxtUpdateProfile = "Oppdater profil"
Const strTxtUsrenameGone = "Beklager. Brukernavnet du har valgt er allerede opptatt.\n\nVelg et annet brukernavn."
Const strTxtEmailThankYouForRegistering = "Takk for at du registrete deg i"
Const strTxtEmailYouCanNowUseTheForumAt = "Dine innloggingsopplysninger finnes under. Du kan n� legge inn nye innlegg og svare p� andre i"
Const strTxtEmailForumAt = "forum p�"
Const strTxtEmailToThe = "til"

'register_new_user.inc
'---------------------------------------------------------------------------------
Const strTxtEmailAMeesageHasBeenPosted = "Et innlegg er lagt inn i p�"
Const strTxtEmailClickOnLinkBelowToView = "Klikk p� linken under for � lese og/eller svare"
Const strTxtEmailAMeesageHasBeenPostedOnForumNum = "Et innlegg er lagt inn i forum nr."

'registration_rules.asp
'---------------------------------------------------------------------------------
Const strTxtForumRulesAndPolicies = "Regler for forum"
Const srtTxtAccept = "Godta"
Const strTxtCancel = "Avbryte"

'New from version 6
'---------------------------------------------------------------------------------
Const strTxtHi = "Hei"
Const strTxtInterestingForumPostOn = "Interessant innlegg p�"
Const strTxtForumLostPasswordRequest = "Henvendelse om glemt passord"
Const strTxtLockForum = "L�s forum"
Const strTxtLockedTopic = "Avsluttet emne"
Const strTxtUnLockTopic = "L�s opp emne"
Const strTxtTopicLocked = "Emne avsluttet"
Const strTxtUnForumLocked = "L�s opp forum"
Const strTxtThisTopicIsLocked = "Dette emnet er avsluttet."
Const strTxtThatYouAskedKeepAnEyeOn = "som du ba oss holde �ye med."
Const strTxtTheTopicIsNowDeleted = "Emnet er slettet."
Const strTxtOf = "av"
Const strTxtTheTimeNowIs = "Idag er det"
Const strTxtYouLastVisitedOn = "Ditt siste bes�k var"
Const strTxtSendMsg = "Send PM"
Const strTxtSendPrivateMessage = "Send privat melding"
Const strTxtActiveUsers = "Aktive brukere"
Const strTxtGuestsAnd = "Gjest(er) og"
Const strTxtMembers = "Medlem(mer)"
Const strTxtPreview = "Forh�ndsvisning"
Const strTxtThereIsNothingToPreview = "Det er ingenting � forh�ndsvise"
Const strTxtEnterTextYouWouldLikeIn = "Tast inn den teksten du vil ha i"
Const strTxtEmailAddressAlreadyUsed = "Beklager. Den epostadressen du har brukt tilh�rer en annen bruker."
Const strTxtIP = "IP"
Const strTxtIPLogged = "IP Logget"
Const strTxtPages = "Sider"
Const strTxtCharacterCount = "Antall tegn"
Const strTxtAdmin = "Admin"
Const strTxtType = "Gruppe"
Const strTxtActive = "Aktiv"
Const strTxtGuest = "Gjest"
Const strTxtAccountStatus = "Kontostatus"
Const strTxtNotActive = "Ikke aktiv"
Const strTxtEmailRequiredForActvation = "N�dvendig for � kunne motta epost for � aktivere ditt medlemsskap"
Const strTxtToActivateYourMembershipFor = "For � aktivere medlemsskap for"
Const strTxtForumClickOnTheLinkBelow = "Klikk p� linken under"
Const strTxtForumAdmin = "Forum admin"
Const strTxtViewLastPost = "Vis siste innlegg"
Const strTxtSelectAvatar = "Velg avatar"
Const strTxtAvatar = "Avatar"
Const strTxtSelectAvatarDetails = "Dette er det lille ikonet som vises ved siden av innlegget ditt. Velg en fra listen eller adressen til din egen avatar. (M� v�re "
Const strTxtPixels = " pixels)."
Const strTxtForumCodesInSignature = "kan brukes i signaturen din"
Const strTxtHighPriorityPost = "Admin innlegg"
Const strTxtHighPriorityPostLocked = "L�st admin innlegg"
Const strTxtHotTopicNewReplies = "Hett emne [nye innlegg]"
Const strTxtHotTopic = "Hett emne [ingen nye innlegg]"
Const strTxtOpenTopic = "Emne [ingen nye innlegg]"
Const strTxtOpenTopicNewReplies = "Emne [nye innlegg]"
Const strTxtPinnedTopic = "Klebe-emne"
Const strTxtOpenForum = "�pent forum [ingen nye innlegg]"
Const strTxtOpenForumNewReplies = "�pent forum [nye innlegg]"
Const strTxtReadOnly = "Kun lese [ingen nye svar]"
Const strTxtReadOnlyNewReplies = "Kun lese [nye svar]"
Const strTxtPasswordRequired = "Passord n�dvendig"
Const strTxtNoAccess = "Ingen tilgang"
Const strTxtFont = "Skrifttype"
Const strTxtSize = "St�rrelse"
Const strTxtForumCodes = "Forumkoder"
Const strTxtPriority = "Klebe-emne"
Const strTxtNormal = "Vanlig emne"
Const strTxtTopAllForums = "Admin innlegg (alle forum)"
Const strTopThisForum = "Admin innlegg (dette forum)"
Const strTxtMarkAllPostsAsRead = "Merk alle innlegg som lest"
Const strTxtDeleteCookiesSetByThisForum = "Slett cookies som er satt av dette forum"

'forum_codes
'---------------------------------------------------------------------------------
Const strTxtYouCanUseForumCodesToFormatText = "Du kan bruke f�lgende forumkoder for � formatere teksten din"
Const strTxtTypedForumCode = "Tastet forumkode"
Const strTxtConvetedCode = "Konvertert kode"
Const strTxtTextFormating = "Tekstformatering"
Const strTxtImagesAndLinks = "Bilder og linker"
Const strTxtFontTypes = "Skrifttyper"
Const strTxtFontSizes ="Skriftst�rrelser"
Const strTxtFontColours ="farger"
Const strTxtEmoticons = "Emoticoner"
Const strTxtFontSize = "Skriftst�rrelse"
Const strTxtMyLink = "Min link"
Const strTxtMyEmail = "Min epost"

'insufficient_permission.asp
'---------------------------------------------------------------------------------
Const strTxtAccessDenied = "Ingen adgang"
Const strTxtInsufficientPermison = "Beklager. Kun medlemmer med tilstrekkelige tillatelser kan �pne denne siden."

'activate.asp
'---------------------------------------------------------------------------------
Const strTxtYourForumMemIsNowActive = "Takk for at du registrerte deg.<br />Ditt medlemsskap er n� aktivt."
Const strTxtErrorWithActvation = "Det har oppst�tt et problem med � aktivisere ditt medlemsskap.<br />Kontakt administrator"

'register_mail_confirm.asp
'---------------------------------------------------------------------------------
Const strTxtYouShouldReceiveAnEmail = "Du vil motta en epost innen de neste 15 minuttene.<br />Klikk p� linken i denne eposten for � aktivisere ditt medlemsskap."
Const strTxtThankYouForRegistering = "Takk for at du registrerte deg for � bruke"
Const strTxtIfErrorActvatingMembership = "Hvis du har problemer med � aktivere ditt medlemsskap, kontakt"

'active_users.asp
'---------------------------------------------------------------------------------
Const strTxtActiveForumUsers = "<br>Aktive brukere</br>"
Const strTxtAddMToActiveUsersList = "Legg meg inn i listen over aktive brukere"
Const strTxtLoggedIn = "Logget inn"
Const strTxtLastActive = "Siste aktive"
Const strTxtBrowser = "Nettleser"
Const strTxtOS = "OS"
Const strTxtMinutes = "minutter"
Const strTxtAnnoymous = "Anonym"

'not_posted.asp
'---------------------------------------------------------------------------------
Const strTxtMessageNotPosted = "Innlegg ikke registrert"
Const strTxtDoublePostingIsNotPermitted = "Dobbeltsending er ikke tillatt. Ditt innlegg er allerede registrert."
Const strTxtSpammingIsNotPermitted = "Spamming er ikke tillatt!"
Const strTxtYouHaveExceededNumOfPostAllowed = "Du har overskredet antall tillatte innlegg innen et tidsintervall.<br /><br />Pr�v igjen senere."
Const strTxtYourMessageNoValidSubjectHeading = "Ditt innlegg inneholdt ikke en gyldig tittel eller meldingstekst."

'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtActiveTopics = "Aktive emner"
Const strTxtLastVisitOn = "siden siste bes�k"
Const strTxtLastFifteenMinutes = "for siste 15 minutter"
Const strTxtLastThirtyMinutes = "for siste 30 minutter"
Const strTxtLastFortyFiveMinutes = "for siste 45 minutter"
Const strTxtLastHour = "for siste time"
Const strTxtLastTwoHours = "for siste 2 timer"
Const strTxtYesterday = "siden i g�r"
Const strTxtShowActiveTopicsSince = "Vis aktive emner"
Const strTxtNoActiveTopicsSince = "Det er ingen aktive emner"
Const strTxtToDisplay = ""
Const strTxtThereAreCurrently = "Det er for tiden"

'pm_check.inc
'---------------------------------------------------------------------------------
Const strTxtNewPMsClickToGoNowToPM = "ny privat melding.\n\nKlikk OK for � g� til postkassen."

'display_forum_topics.inc
'---------------------------------------------------------------------------------
Const strTxtFewYears = "" '"�rene"
Const strTxtWeek = "" '"uke"
Const strTxtTwoWeeks = "" '"to uker"
Const strTxtMonth = "" '"m�ned"
Const strTxtTwoMonths = "" '"to m�neder"
Const strTxtSixMonths = "" '"6 m�neder"
Const strTxtYear = "" '"�r"

'Colours
'---------------------------------------------------------------------------------
Const strTxtBlack = "Svart"
Const strTxtWhite = "Hvit"
Const strTxtBlue = "Bl�"
Const strTxtRed = "R�d"
Const strTxtGreen = "Gr�nn"
Const strTxtYellow = "Gul"
Const strTxtOrange = "Oransj"
Const strTxtBrown = "Brun"
Const strTxtMagenta = "Magenta"
Const strTxtCyan = "Cyan"
Const strTxtLimeGreen = "Lys gr�nn"

Const strTxtHasBeenSentTo = "er sendt til"
Const strTxtCharactersInYourSignatureToLong = "tegn i din signatur. Du m� forkorte til 200."
Const strTxtSorryYourSearchFoundNoMembers = "Beklager. Ditt s�k fant ingen medlemmer."
Const strTxtCahngeOfEmailReactivateAccount = "Hvis du forandrer epostadressen din vil du f� en ny epost med ny link for � aktivisere profilen."
Const strTxtAddToBuddyList = "Legg til i venneliste"

'register_mail_confirm.asp
'---------------------------------------------------------------------------------
Const strTxtYourEmailAddressHasBeenChanged = "Din epostadresse er endret, <br />Du m� re-aktivere ditt medlemsskap f�r du kan bruke forumet igjen."
Const strTxtYouShouldReceiveAReactivateEmail = "Du vil f� en epost innen 15 minutter.<br />Klikk p� linken i denne eposten for � re-aktivere ditt medlemsskap."

'Preview signature windows
'---------------------------------------------------------------------------------
Const strTxtSignaturePreview = "Forh�ndsvisning av signatur"
Const strTxtPostedMessage = "Registrert innlegg"

'New from version 7
'---------------------------------------------------------------------------------
Const strTxtMemberlist = "Medlemsliste"
Const strTxtForums = "Forum"
Const strTxtOurUserHavePosted = "V�re brukere har postet"
Const strTxtInTotalThereAre = "Det er i alt"
Const strTxtOnLine = "online"
Const strTxtWeHave = "Det finnes"
Const strTxtActivateAccount = "Aktiver konto"
Const strTxtSorryYouDoNotHavePermissionToPostInTisForum = "Beklager, men du har ikke tillatelse til � poste nye innlegg i dette forumet."
Const strTxtSorryYouDoNotHavePerimssionToReplyToPostsInThisForum = "Beklager, men du har ikke tillatelse til � svare p� innlegg i dette forumet."
Const strTxtSorryYouDoNotHavePerimssionToReplyIPBanned = "Beklager, men du kan ikke svare p� innlegg fordi din IP-adresse eller ditt omr�de ikke er tillatt av admin.<br />Kontakt Forumadministrator hvis du mener dette er feil."
Const strTxtLoginSm = "logg inn"
Const strTxtYourProfileHasBeenUpdated = "Din profil er oppdatert."
Const strTxtPosted = "Sendt:"
Const strTxtBackToTop = "Tilbake til toppen"
Const strTxtNewPassword = "Nytt passord"
Const strTxtRetypeNewPassword = "Gjenta passord"
Const strTxtRegards = "Hilsen"
Const strTxtClickTheLinkBelowToUnsubscribe = "For � fjernes fra listen som mottar beskjed over svar p� dette emnet, klikk p� linken under. "
Const strTxtPostsPerDay = "innlegg pr. dag"
Const strTxtGroup = "Gruppe"
Const strTxtLastVisit = "Siste bes�k"
Const strTxtPrivateMessage = "Privat melding"
Const strTxtSorryFunctionNotPermiitedIPBanned = "Beklager. Denne funksjonen er ikke tilgjengelig fordi du bruker en IP-adresse eller omr�de som ikke er tillatt.<br />r. Kontakt Forumadministrator hvis du mener dette er feil."
Const strTxtEmailAddressBlocked = "Beklager. Epostadressen eller domenet er blokkert av Administrator."
Const strTxtTopicAdmin = "Emneadministrasjon"
Const strTxtMovePost = "Flytt innlegg"
Const strTxtPrevTopic = "Forrige emne"
Const strTxtTheMemberHasBeenDleted = "Bruker slettet."
Const strTxtThisPageWasGeneratedIn = "Denne siden ble opprettet p�"
Const strTxtSeconds = "sekunder."
Const strTxtEditBy = "Endret av"
Const strTxtWrote = "skrev"
Const strTxtEnable = "Aktiver"
Const strTxtToFormatPosts = "for � formatere innlegg"
Const strTxtFlashFilesImages = "Flash filer/bilder"
Const strTxtSessionIDErrorCheckCookiesAreEnabled = "En sikkerhetsfeil har oppst�tt ved autorisering.<br>S�rg for at Cookies er aktivert i nettleseren og at du ikke bruker en gammel eller mellomlagret versjon av nettsiden."
Const strTxtName = "Navn"
Const strTxtModerators = "Emneledere"
Const strTxtMore = "mer..."
Const strTxtNewRegSuspendedCheckBackLater = "Beklager. Nyregistrering er sl�tt av. Pr�v igjen senere."
Const strTxtMoved = "Flyttet: "
Const strTxtNoNameError = "Navn \t\t- Tast ditt navn"
Const strTxtHelp = "Hjelp"

'PM system
'---------------------------------------------------------------------------------
Const strTxtPrivateMessenger = "Postkasse"
Const strTxtUnreadMessage = "Ulest melding"
Const strTxtReadMessage = "Lest melding"
Const strTxtNew = "ny"
Const strTxtYouHave = "Du har"
Const strTxtNewMsgsInYourInbox = "ny(e) melding(er) i innboksen!"
Const strTxtNoneSelected = "Ingen valg"
Const strTxtAddBuddy = "Legg til venn"

'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtSelectMember = "Velg medlem"
Const strTxtSelect = "Velg"
Const strTxtNoMatchesFound = "Ingen funnet"

'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtLastFourHours = "for siste 4 timer"
Const strTxtLastSixHours = "for siste 6 timer"
Const strTxtLastEightHours = "for siste 8 timer"
Const strTxtLastTwelveHours = "for siste 12 timer"
Const strTxtLastSixteenHours = "for siste 16 timer"

'permissions
'---------------------------------------------------------------------------------
Const strTxtYou = "Du"
Const strTxtCan = "kan"
Const strTxtCannot = "kan ikke"
Const strTxtpostNewTopicsInThisForum = "lage nye emner"
Const strTxtReplyToTopicsInThisForum = "svare p� innlegg"
Const strTxtEditYourPostsInThisForum = "endre dine innlegg"
Const strTxtDeleteYourPostsInThisForum = "slette dine innlegg"
Const strTxtCreatePollsInThisForum = "lage sp�rreunders�kelser"
Const strTxtVoteInPOllsInThisForum = "stemme i sp�rreunders�kelser"

'register.asp
'---------------------------------------------------------------------------------
Const strTxtRegistrationDetails = "Registreringsdetaljer"
Const strTxtProfileInformation = "Profilinformasjon"
Const strTxtForumPreferences = "Foretrukne innstillinger"
Const strTxtICQNumber = "ICQ-nummer"
Const strTxtAIMAddress = "AIM-addresse"
Const strTxtMSNMessenger = "MSN Messenger"
Const strTxtYahooMessenger = "Yahoo Messenger"
Const strTxtOccupation = "Yrke"
Const strTxtInterests = "Interesser"
Const strTxtDateOfBirth = "F�dselsdato"
Const strTxtNotifyMeOfReplies = "Gi meg beskjed om svar p� innlegg"
Const strTxtSendsAnEmailWhenSomeoneRepliesToATopicYouHavePostedIn = "Sender en epost n�r noen svarer p� et emne du har sendt meldinger til. Dette kan forandres hver gang du sender et innlegg."
Const strTxtNotifyMeOfPrivateMessages = "Gi meg beskjed via epost n�r jeg mottar en privat melding"
Const strTxtAlwaysAttachMySignature = "Legg alltid min signatur til innlegg."
Const strTxtEnableTheWindowsIEWYSIWYGPostEditor = "Aktiver Windows IE 5 + WYSIWYG meldingseditor."
Const strTxtTimezone = "Avstand fra forumets tidsone"
Const strTxtPresentServerTimeIs = "Server dato og tid er: "
Const strTxtDateFormat = "Datoformat"
Const strTxtDayMonthYear = "Dag/M�ned/�r"
Const strTxtMonthDayYear = "M�ned/Dag/�r"
Const strTxtYearMonthDay = "�r/M�ned/Dag"
Const strTxtYearDayMonth = "�r/Dag/M�ned"
Const strTxtHours = "timer"
Const strTxtDay = "Dag"
Const strTxtCMonth = "M�ned"
Const strTxtCYear = "�r"
Const strTxtRealName = "Virkelig navn"
Const strTxtMemberTitle = "Medlemstittel"

'Polls
'---------------------------------------------------------------------------------
Const strTxtCreateNewPoll = "Opprette ny sp�rreunders�kelse"
Const strTxtPollQuestion = "Sp�rsm�l"
Const strTxtPollChoice = "Valg"
Const strTxtErrorPollQuestion = "Sp�rsm�l \t- tast inn et sp�rsm�l for denne sp�rreunders�kelsen."
Const strTxtErrorPollChoice = "Valg \t- Tast inn minst 2 valg for dette sp�rsm�let."
Const strTxtSorryYouDoNotHavePermissionToCreatePollsForum = "Beklager. Du har ikke tillatelse til � opprette sp�rreunders�kelser i dette forum."
Const strTxtAllowMultipleVotes = "Tillat flere stemmer i denne sp�rreunders�kelsen."
Const strTxtMakePollOnlyNoReplies = "Kun opprette sp�rreunders�kelse (ingen svar tillatt)."
Const strTxtYourNoValidPoll = "Din sp�rreunders�kelse inneholdt ingen gyldige sp�rsm�l eller valg."
Const strTxtPoll = "Sp�rreunders�kelse:"
Const strTxtVote = "Stem"
Const strTxtVotes = "Stemmer"
Const strTxtCastMyVote = " Jeg stemmer"
Const strTxtPollStatistics = "Statistikk"
Const strTxtThisTopicIsClosedNoNewVotesAccepted = "Dette emnet er avsluttet. Ingen flere stemmer tillatt."
Const strTxtYouHaveAlreadyVotedInThisPoll = "Du har allerede stemt i denne sp�rreunders�kelsen."
Const strTxtThankYouForCastingYourVote = "Takk for at du ga din stemme."
Const strsTxYouCanNotNotVoteInThisPoll = "Du kan ikke stemme i denne sp�rreunders�kelsen."
Const strTxtYouDidNotSelectAChoiceForYourVote = "Beklager. Din stemme ble ikke registrert.\n\nDu gjorde ikke et gyldig valg."
Const strTxtThisIsAPollOnlyYouCanNotReply = "Dette er kun en sp�rreunders�kelse. Du kan ikke svare."

'Email Notify
'---------------------------------------------------------------------------------
Const strTxtWatchThisTopic = "Varsle om svar p� dette emnet"
Const strTxtUn = "Ikke "
Const strTxtWatchThisForum = "Varsle om ny innlegg i dette forum"
Const strTxtYouAreNowBeNotifiedOfPostsInThisForum = "Du vil f� epost om alle nye innlegg i dette forum.\n\nFor � oppheve, klikk \'Ikke varsle om nye innlegg i dette forumet\' nederst p� siden."
Const strTxtYouAreNowNOTBeNotifiedOfPostsInThisForum = "Du vil ikke f� epost om nye innlegg i dette forum.\n\nFor � aktivere, klikk \'Varsle om nye innlegg i dette forum\' nederst p� siden."
Const strTxtYouWillNowBeNotifiedOfAllReplies = "Du vil f� epost om alle svar p� meldinger i dette emnet.\n\nFor � oppheve, klikk \'Ikke varsle om svar p� dette emnet\' nederst p� siden."
Const strTxtYouWillNowNOTBeNotifiedOfAllReplies = "Du vil ikke f� epost om alle svar p� meldinger i dette emnet.\n\nFor � aktivere, klikk \'Varsle om svar p� dette emnet\' nederst p� siden."

'email_messenger.asp
'---------------------------------------------------------------------------------
Const strTxtEmailMessenger = "Epostmeldinger"
Const strTxtRecipient = "Mottaker"
Const strTxtNoHTMLorForumCodeInEmailBody = "OBS: Epost m� sendes som uformatert tekst (ingen HTML eller forumkoder).<br /><br />Din egen epostadresse blir brukt som returadresse."
Const strTxtYourEmailHasBeenSentTo = "Din epost er sendt til"
Const strTxtYouCanNotEmail = "Du kan ikke sende epost"
Const strTxtYouDontHaveAValidEmailAddr = "du har ingen gyldig epostadresse i din profil."
Const strTxtTheyHaveChoosenToHideThierEmailAddr = "de har valgt � skjule epostadresser."
Const strTxtTheyDontHaveAValidEmailAddr = "de har ikke gyldige epostadresser i sin profil."
Const strTxtSendACopyOfThisEmailToMyself = "Send en kopi av denne epost til meg selv."
Const strTxtTheFollowingEmailHasBeenSentToYouBy = "F�lgende epost er sendt til deg av"
Const strTxtFromYourAccountOnThe = "fra din konto p�"
Const strTxtIfThisMessageIsAbusive = "Hvis dette innlegget er s�ppelpost eller hvis innholdet er st�tende, kontakt Forumadministrator p� f�lgende adresse"
Const strTxtIncludeThisEmailAndTheFollowing = "Vedlegg denne eposten og"
Const strTxtReplyToEmailSetTo = "V�r klar over at svaradresse p� denne epost er satt til"
Const strTxtMessageSent = "Melding sendt"

'Uploads
'---------------------------------------------------------------------------------
Const strTxtImageUpload = "Last opp bilde"
Const strTxtFileUpload = "Last opp fil"
Const strTxtAvatarUpload = "Last opp avatar"
Const strTxtUpload = "Last opp"
Const strTxtImage = "Bilde"
Const strTxtImagesMustBeOfTheType = "Bilder m� v�re av typen"
Const strTxtAndHaveMaximumFileSizeOf = "og ha max filst�rrelse"
Const strTxtImageOfTheWrongFileType = "Bildet er av feil type"
Const strTxtImageFileSizeToLarge = "Sendt bilde har for stor filst�rrelse: "
Const strTxtMaximumFileSizeMustBe = "Max filst�rrelse kan v�re"
Const strTxtFile = "Fil"
Const strTxtFilesMustBeOfTheType = "Filer m� v�re av type"
Const strTxtFileOfTheWrongFileType = "Filen er av feil type"
Const strTxtFileSizeToLarge = "Max. filst�rrelse m� v�re"
Const strTxtPleaseWaitWhileFileIsUploaded = "V�r t�lmodig. Filen blir lastet opp til nettserveren."
Const strTxtPleaseWaitWhileImageIsUploaded = "V�r t�lmodig. Bildet blir lastet opp til nettserveren."

'forum_closed.asp
'---------------------------------------------------------------------------------
Const strTxtForumClosed = "Forumet er stengt"
Const strTxtSorryTheForumsAreClosedForMaintenance = "Beklager. Forum er stengt for vedlikehold.<br />Pr�v igjen senere."

'report_post.asp
'---------------------------------------------------------------------------------
Const strTxtReportPost = "Rapport�r innlegg"
Const strTxtSendReport = "Send rapport"
Const strTxtProblemWithPost = "Problem med innlegg"
Const strTxtPleaseStateProblemWithPost = "Oppgi hva du mener er problemet med det valgte innlegget. En kopi av innlegget blir sendt til Emneadministrator og/eller Forumadministrator."
Const strTxtTheFollowingReportSubmittedBy = "F�lgende rapport er sendt av"
Const strTxtWhoHasTheFollowingIssue = "som har f�lgende problem med innlegget"
Const strTxtToViewThePostClickTheLink = "Klikk p� linken under for � vise innlegget"
Const strTxtIssueWithPostOn = "Rapport om innlegg p�"
Const strTxtYourReportEmailHasBeenSent = "Ditt innlegg er sendt til Emneadministrator og/eller Forumadministrator."

'New from version 7.5
'---------------------------------------------------------------------------------
Const strTxtImportantTopics = "Viktige emner"
Const strTxtQuickLogin = "Hurtig innlogging"
Const strTxtThisTopicWasStarted = "Dette temaet er started: "
Const strTxtResendActivationEmail = "Send aktiverings e-post p� nytt"
Const strTxtNoOfStars = "Antall stjerner"
Const strTxtOnLine2 = "P�logget"
Const strTxtIeSpellNotDetected = "ieSpell ble ikke funnet. Klikk Ok for � g� til nedlastingssiden."
Const strTxtstrTxtOrderedList = "Ordnet liste"
Const strTxtTextColour = "Tekstfarge"
Const strTxtstrSpellCheck = "Stavekontroll"
Const strTxtCode = "Kode"
Const strTxtCodeandFixedWidthData = "Kode med bestemte bredde"
Const strTxtQuoting = "Sitere"
Const strTxtMyCodeData = "Min kode/innhold med bestemt bredde"
Const strTxtQuotedMessage = "Sitat"
Const strTxtWithUsername = "med brukernavn"
Const strTxtOK = "Ok"
Const strTxtGo = "G�"
Const strTxtDataBasedOnActiveUsersInTheLastXMinutes = "Denne informasjonen er basert p� aktive brukere de siste 10 minutter"
Const strTxtSoftwareVersion = "Programvare versjon"
Const strTxtForumMembershipNotAct = "Ditt medlemskap er ikke aktivert"
Const strTxtMustBeRegisteredToPost = "Beklager, men du m� v�re registrert for � legge inn meldinger i dette forumet."
Const strTxtSettings = "Innstillinger"
Const strTxtMemberCPMenu = "Kontrollpanel for ditt medlemskap"
Const strTxtYouCanAccessCP = "Du har tilgang til forumets tjenester og kan endre dine innstillinger/preferanser gjennom forumets "
Const strTxtEditMembersSettings = "Rediger dette medlemmets innstillinger"
Const strTxtSecurityCodeConfirmation = "Bekreftelse av sikkerhetskode (obligatorisk)"
Const strTxtUniqueSecurityCode = "Unik sikkerhetskode"
Const strTxtConfirmSecurityCode = "Bekreft sikkerhetskode* "
Const strTxtEnter6DigitCode = "Oppgi sikkerhetskoden som er vist ovenfor.<br />Kun siffer er tillatt, '0' er tallet null..."
Const strTxtErrorSecurityCode = "Sikkerhetskode \t- Du m� oppgi den seks-siffrede sikkerhetskoden"
Const strTxtSecurityCodeDidNotMatch = "Beklager, men sikkerhetskoden du oppga stemmer ikke med den som vist.\n\nEn ny kode har blitt generert\n\nInnformasjonskapsler m� v�re aktivert for � bruke denne tjenesten."
Const strTxtCookiesMustBeEnabled = "Innformasjonskapsler m� v�re aktivert for � se bilder"

'login_user_test.asp
'---------------------------------------------------------------------------------
Const strTxtSuccessfulLogin = "Innloggingen var vellykket"
Const strTxtSuccessfulLoginReturnToForum = "Innloggingen var vellykket, du vil bli videref�rt til forumet om et lite �yeblikk"
Const strTxtUnSuccessfulLoginText = "Innloggingen mislyktes p� grunn av problemer med innformasjonskapsler.<br /><br />Dette nettstedet m� gis tillatelse til � bruke innformasjonskapsler (i din nettleser)."
Const strTxtUnSuccessfulLoginReTry = "Klikk her for � fors�ke � logge inn p� nytt"
Const strTxtToActivateYourForumMem = "For � aktivere ditt medlemskap klikker du p� lenken som ble sendt deg i en epost etter at du registrerte deg f�rste gang"

'email_notify_subscriptions.asp
'---------------------------------------------------------------------------------
Const strTxtEmailNotificationSubscriptions = "Oppsett for epost-varsler"
Const strTxtSelectForumErrorMsg = "Velg forum\t- Velg et forum � abonnere p�"
Const strTxtYouHaveNoSubToEmailNotify = "Du har ingen abonnement for varslinger via epost"
Const strTxtThatYouHaveSubscribedTo = "du abonnerer p�"
Const strTxtUnsusbribe = "Stopp abonnement"
Const strTxtAreYouWantToUnsubscribe = "Er du sikker p� at du vil stoppe dine abonnement p� disse"

'RTE editor
'---------------------------------------------------------------------------------
Const strTxtFontStyle = "Font Style"
Const strTxtBackgroundColour = "Background Colour"
Const strTxtUndo = "Angre"
Const strTxtRedo = "Gj�r om"
Const strTxtJustify = "Justify"
Const strTxtToggleHTMLView = "Bytt visningsm�te"
Const strTxtAboutRichTextEditor = "Om denne redigeringskomponenten"
Const strTxtImageURL = "Kilde (url)"
Const strTxtAlternativeText = "Alternativ&nbsp;tekst"
Const strTxtLayout = "Oppsett"
Const strTxtAlignment = "Justering"
Const strTxtBorder = "Kantlinje"
Const strTxtSpacing = "Luft rundt bildet (px)"
Const strTxtHorizontal = "Horisontalt"
Const strTxtVertical = "Vertikalt"
Const strTxtRows = "Rader"
Const strTxtColumns = "Kolonner"
Const strTxtWidth = "Bredde"
Const strTxtCellPad = "Cell Pad"
Const strTxtCellSpace = "Cell Space"
Const strTxtInsertTable = "Sett in tabell"

'New from version 7.51
'---------------------------------------------------------------------------------
Const strTxtSubscribeToForum = "Abonnere p� epostvarsel ved nye innlegg"
Const strTxtSelectForumToSubscribeTo = "Velg forum � abonnere p�"

'New from version 7.7
'---------------------------------------------------------------------------------
Const strTxtOnlineStatus = "Status"
Const strTxtOffLine = "Ikke p�logget"

'New from version 7.8
'---------------------------------------------------------------------------------
Const strTxtConfirmOldPass = "Bekreft gammelt passord"
Const strTxtConformOldPassNotMatching = "Det gamle passordet stemmer ikke det registrerte passordet for din konto.\n\nFor � endre passordet m� du oppgi korrekt passord."

'New from version 8.0
'---------------------------------------------------------------------------------
Const strTxtSub = "Sub"
Const strTxtHidden = "Skjult"
Const strTxtShowPost = "Vis innlegg"
Const strTxtHidePost = "Skjul innlegg"
Const strTxtAreYouSureYouWantToHidePost = "Er du sikker p� at du vil skjule dette innlegg"
Const strTxtModeratedPost = "Kontrollert innlegg"
Const strTxtYouArePostingModeratedForum = "Du skriver innlegg i et kontrollert forum."
Const strTxtBeforePostDisplayedAuthorised = "F�r innlegget ditt kan vises m� det godkjennes av en administrator."
Const strTxtHiddenTopics = "Skjulte emner"
Const strTxtVerifiedBy = "Godkjent av"

%>