<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill	All Rights Reserved.
'**
'**  This program is free software; you	can modify (at your own	risk) any part of it
'**  under the terms of	the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must	remain in tacked in the	scripts	and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole	or any part of this program even
'**  if	it is modified or reverse engineered in	whole or in part without express
'**  permission	from the author.
'**
'**  You may not pass the whole	or any part of this application	off as your own	work.
'**
'**  All links to Web Wiz Guide	and powered by logo's must remain unchanged and	in place
'**  and must remain visible when the pages are	viewed unless permission is first granted
'**  by	the copyright holder.
'**
'**  This program is distributed in the	hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the	implied	warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES	WHETHER	EXPRESSED OR IMPLIED.
'**
'**  You should	have received a	copy of	the License along with this program;
'**  if	not, write to:-	Web Wiz	Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No	official support is available for this program but you may post	support	questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered	by e-mail ever!
'**
'**  For correspondence	or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or	at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'If a private message go to pm post message page otherwise goto	post message page
If strMode = "PM" Then
	strPostPage = "pm_post_message.asp"
Else
	strPostPage = "post_message.asp?PN=" & Trim(Mid(Request.Form("PN"), 1, 8))
End If
%>
<script	language="JavaScript" src="message_form_js.asp" type="text/javascript"></script>
<form method="post" name="frmAddMessage" action="<% = strPostPage %>" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table	width="610" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" height="230" align="center">
  <tr>
   <td>
    <table width="100%"	border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% =	strTableColour %>" background="<% = strTableBgImage %>"	height="201">
     <tr>
      <td>
       <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
	<tr align="left">
	 <td colspan="2" height="31" class="text">*<% =	strTxtRequiredFields %></td>
	</tr><%
	
'If the poster is in a guest then get them to enter a name
If lngLoggedInUserID = 2 AND (strMode <> "edit" AND strMode <> "editTopic") Then
	%>
	<tr>
	 <td align="right" width="15%" class="text"><%	= strTxtName %>*:</td>
	 <td width="70%"><input type="text" name="Gname" size="20" maxlength="20" /></td>
	</tr><%
End If


'If this is a private message display the username box
If strMode = "PM" Then
%>
	<tr>
	 <td align="right" width="15%" class="text"><% = strTxtToUsername %>:</td>
	 <td width="70%"><%

	 'Get the users	buddy list if they have	one
	 
	'Initlise the sql statement
	strSQL = "SELECT " & strDbTable & "Author.Username "
	strSQL = strSQL	& "FROM	" & strDbTable & "Author INNER	JOIN " & strDbTable & "BuddyList ON " & strDbTable & "Author.Author_ID = " & strDbTable & "BuddyList.Buddy_ID "
	strSQL = strSQL	& "WHERE " & strDbTable & "BuddyList.Author_ID=" & lngLoggedInUserID &	" AND " & strDbTable & "BuddyList.Buddy_ID <> 2 "
	strSQL = strSQL	& "ORDER By " & strDbTable & "Author.Username ASC;"

	'Query the database
	rsCommon.Open strSQL, adoCon

	  %><input type="text" name="member" size="15" maxlength="15" value="<%	= Server.HTMLEncode(strBuddyName) %>"<% If	NOT rsCommon.EOF Then Response.Write(" onChange=""document.frmAddMessage.selectMember.options[0].selected = true;""") %> />
	  <a href="JavaScript:openWin('pop_up_member_search.asp','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=255')"><img src="<% = strImagePath %>search.gif" alt="<% = strTxtMemberSearch %>"	border="0" align="absmiddle"></a><%

	'If there are records returned then display the	users buddy list
	If NOT rsCommon.EOF Then %>
	  <span	class="text"> <% = strSelectFormBuddyList %>:</span>
	  <select name="selectMember" onChange="member.value=''" />
	    <option value="">--	<% = strTxtNoneSelected	%> --</option><%
		'Loop throuhgn and display the buddy list
		Do While NOT rsCommon.EOF

			Response.Write("<option	value=""" & rsCommon("Username") & """>" & rsCommon("Username") &	"</option>")

			'Move to next record in	rs
			rsCommon.MoveNext
		Loop
		%>
	  </select><%


	Else
		Response.Write("<input type=""hidden"" name=""selectMember"" value="""" />")
	End If

	'Reset server variables
	rsCommon.Close
	
       %></td>
	</tr><%
End If

'If this is a new post or editing the first thread then	display	the subject text box
If strMode = "new" or strMode="editTopic" or strMode = "PM" or strMode = "poll"	Then
%>
	<tr>
	 <td align="right" width="15%" class="text"><%	= strTxtSubjectFolder %>*:</td>
	 <td width="70%">
	  <input type="text" name="subject" size="30" maxlength="41"<% If strMode="editTopic" or strMode="PM" Then Response.Write(" value=""" &	strTopicSubject	& """")	%> /><%

	'If this is the	forums moderator or forum admim	then let them select the	priority level of the post
	If (blnAdmin = True OR blnPriority = True) AND (strMode	= "new"	or strMode="editTopic" or strMode = "poll") Then
%>
	  <span	class="text">&nbsp;<% =	strTxtPriority %>: <select name="priority">
	   <option value="0"<% If intTopicPriority = 0 Then Response.Write(" selected")	%>><% =	strTxtNormal %></option>
	   <option value="1"<% If intTopicPriority = 1 Then Response.Write(" selected")	%>><% =	strTxtPinnedTopic %></option><%
		'If this is the	forum admin or moderator let them post an annoucment to	this forum
		If blnAdmin = True OR blnModerator Then	%>
	   <option value="2"<% If intTopicPriority = 2 Then Response.Write(" selected")	%>><% =	strTopThisForum	%></option><%
		End If

		'If this is the	forum admin let	them post an announcement post to all forums
		If blnAdmin = True Then	%>
	   <option value="3"<% If intTopicPriority = 3 Then Response.Write(" selected")	%>><% =	strTxtTopAllForums %></option>
	   <%
		End If
		%>
	  </select>
	  </span><%

	'Else the priority of the post is normal
	Else
		Response.Write("<input type=""hidden"" name=""priority"" value=""0"" />")
	End If
	%>
	 </td>
	</tr><%

End If


'If this is a new poll then display space to enter the poll
If strMode = "poll" Then

	%><!--#include file="poll_form_inc.asp"	--><%

End If

 %>	<tr>
	 <td align="right" width="15%"><span class="text"> </span></td>
	 <td width="70%" valign="bottom">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	   <tr>
	    <td>
	     <table width="477"	border="0" cellspacing="0" cellpadding="1">
	      <tr>
	       <td width="346">
		<select	name="selectFont" onChange="FontCode(selectFont.options[selectFont.selectedIndex].value, 'FONT');document.frmAddMessage.selectFont.options[0].selected = true;">
		 <option selected>-- <%	= strTxtFont %>	--</option>
		 <option value="FONT=Arial">Arial</option>
		 <option value="FONT=Courier">Courier New</option>
		 <option value="FONT=Times">Times New Roman</option>
		 <option value="FONT=Verdana">Verdana</option>
		</select>
		<select	name="selectSize" onChange="FontCode(selectSize.options[selectSize.selectedIndex].value, 'SIZE');document.frmAddMessage.selectSize.options[0].selected = true;">
		 <option selected>-- <%	= strTxtSize %>	--</option>
		 <option value="SIZE=1">1</option>
		 <option value="SIZE=2">2</option>
		 <option value="SIZE=3">3</option>
		 <option value="SIZE=4">4</option>
		 <option value="SIZE=5">5</option>
		 <option value="SIZE=6">6</option>
		</select>
		<select	name="selectColour" onChange="FontCode(selectColour.options[selectColour.selectedIndex].value, 'COLOR');document.frmAddMessage.selectColour.options[0].selected = true;">
		 <option value="0" selected>-- <% = strTxtFontColour %>	--</option>
		 <option value="COLOR=BLACK"><% = strTxtBlack	%></option>
		 <option value="COLOR=WHITE"><% = strTxtWhite	%></option>
		 <option value="COLOR=BLUE"><% = strTxtBlue %></option>
		 <option value="COLOR=RED"><%	= strTxtRed %></option>
		 <option value="COLOR=GREEN"><% = strTxtGreen	%></option>
		 <option value="COLOR=YELLOW"><% = strTxtYellow %></option>
		 <option value="COLOR=ORANGE"><% = strTxtOrange %></option>
		 <option value="COLOR=BROWN"><% = strTxtBrown	%></option>
		 <option value="COLOR=MAGENTA"><% = strTxtMagenta %></option>
		 <option value="COLOR=CYAN"><% = strTxtCyan %></option>
		 <option value="COLOR=LIME GREEN"><% = strTxtLimeGreen %></option>
		</select>
	     </table>
	     <table width="477"	border="0" cellspacing="0" cellpadding="1">
	      <tr>
	       <td width="357" nowrap="nowrap"><a href="JavaScript:AddMessageCode('B','<% = strTxtEnterBoldText	%>', '')"><img src="<% = strImagePath %>post_button_bold.gif" align="absmiddle"	border="0" alt="<% = strTxtBold	%>"></a>
		<a href="JavaScript:AddMessageCode('I','<% = strTxtEnterItalicText %>',	'')"><img src="<% = strImagePath %>post_button_italic.gif" align="absmiddle" border="0"	alt="<%	= strTxtItalic %>"></a>
		<a href="JavaScript:AddMessageCode('U','<% = strTxtEnterUnderlineText %>', '')"><img src="<% = strImagePath %>post_button_underline.gif" align="absmiddle" border="0" alt="<% =	strTxtUnderline	%>"></a>
		<a href="JavaScript:AddCode('URL')"><img src="<% = strImagePath	%>post_button_hyperlink.gif" align="absmiddle" border="0" alt="<% = strTxtAddHyperlink %>"></a>
		<a href="JavaScript:AddCode('EMAIL')"><img src="<% = strImagePath %>post_button_email.gif" align="absmiddle" border="0"	alt="<%	= strTxtAddEmailLink %>"></a>
		<a href="JavaScript:AddMessageCode('CENTER','<%	= strTxtEnterCentredText %>', '')"><img	src="<%	= strImagePath %>post_button_centre.gif" align="absmiddle" border="0" alt="<% =	strTxtCentre %>"></a>
		<a href="JavaScript:AddCode('LIST', '')"><img src="<% =	strImagePath %>post_button_list.gif" align="absmiddle" border="0" alt="<% = strTxtList %>"></a>
		<a href="JavaScript:AddCode('INDENT', '')"><img	src="<%	= strImagePath %>post_button_indent.gif" align="absmiddle" border="0" alt="<% =	strTxtIndent %>"></a>
		<a href="JavaScript:AddCode('IMG')"><img src="<% = strImagePath	%>post_button_image.gif" align="absmiddle" border="0" alt="<% =	strTxtAddImage %>"></a><%

'If image uploading is allowed have an image upload button
If blnImageUpload Then
	%>
		<a href="javascript:openWin('upload_images.asp?MSG=BSC&FID=<% =	intForumID %>','images','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=400,height=150')"><img src="<% = strImagePath %>post_button_image_upload.gif" align="absmiddle"	alt="<%	= strTxtImageUpload %>"	border="0"></a><%
End If

'If file uploading is allowed have an file upload button
If blnAttachments Then
	%>
		<a href="javascript:openWin('upload_files.asp?MSG=BSC&FID=<% = intForumID %>','files','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=400,height=150')"><img src="<% = strImagePath %>post_button_file_upload.gif" align="absmiddle" alt="<% = strTxtFileUpload	%>" border="0"></a><%
End If

%>
		</td>
	       <td width="126" align="right" class="text" nowrap="nowrap"><% = strTxtMode %>:
		<select	name="selectMode" onChange=PromptMode(this)>
		 <option value="1" selected><% = strTxtPrompt %></option>
		 <option value="0"><% =	strTxtBasic %></option>
		</select>
	       </td>
	      </tr>
	     </table>
	    </td>
	   </tr>
	  </table>
	 </td>
	</tr>
	<tr>
	 <td valign="top" align="right"	width="15%" class="text"><% = strTxtMessage %>*:<%

'If emoticons are enabled show them next to the post window
If blnEmoticons Then
	
%>	
         <table border="0" cellspacing="0" cellpadding="4" align="center">
          <tr><td class="smText" colspan="3" align="center"><br /><% = strTxtEmoticons %></td></tr><%

	
	'Intilise the index position (we are starting at 1 instead of position 0 in the array for simpler calculations)
	intIndexPosition = 1
	
	'Calcultae the number of outer loops to do
	intNumberOfOuterLoops = 4
	
	'If there is a remainder add 1 to the number of loops
	If UBound(saryEmoticons) MOD 2 > 0 Then intNumberOfOuterLoops = intNumberOfOuterLoops + 1
	
	'Loop throgh th list of emoticons
	For intLoop = 1 to intNumberOfOuterLoops
	      
	
	        Response.Write("<tr>")
	
		'Loop throgh th list of emoticons
		For intInnerLoop = 1 to 3  
		
			'If there is nothing to display show an empty box
			If intIndexPosition > UBound(saryEmoticons) Then 
				Response.Write(vbCrLf & "	<td class=""text"">&nbsp;</td>")
	
			'Else show the emoticon
			Else 
				Response.Write(vbCrLf & "	<td class=""text""><a href=""JavaScript:AddSmileyIcon('" & saryEmoticons(intIndexPosition,2) & "')""><img src=""" & saryEmoticons(intIndexPosition,3) & """ border=""0"" alt=""" & saryEmoticons(intIndexPosition,2) & """></a></td>")
	              	End If
	              
	              'Minus one form the index position
	              intIndexPosition = intIndexPosition + 1 
		Next            
	
		Response.Write("</tr>")
		
	Next             
	%>
         <tr><td colspan="3" align="center"><a href="javascript:openWin('emoticons_smilies.asp','emot','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=400,height=400')" class="smLink"><% = strTxtMore %></a></td></tr>
         </table></td><%
End If

%></td>
	 <td width="70%" valign="top">
	  <textarea name="message" cols="57" rows="12" wrap="PHYSICAL" onSelect="storeCaret(this);" onClick="storeCaret(this);" onKeyup="storeCaret(this);"><% If strMode="edit" OR strMode = "quote" or strMode="editTopic"	or strMode="PM"	Then Response.Write strMessage %></textarea>
	 </td>
	</tr>
	<tr>
	 <td align="right" width="92">&nbsp;</td>
	 <td width="508" valign="bottom" class="text">&nbsp;<input type="checkbox" name="forumCodes" value="True" checked /><% Response.Write(strTxtEnable & " <a href=""JavaScript:openWin('forum_codes.asp','codes','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=550,height=400')"">" & strTxtForumCodes & "</a> " & strTxtToFormatPosts) %>
	 </td></tr><%

'If not	PM then	display	another	row
If strMode <> "PM" Then
	'If signature of e-mail	notify then display row	to show
	If (blnLoggedInUserEmail = True	AND blnEmail = True) OR	blnLoggedInUserSignature = True	Then

%>
	<tr>
	 <td align="right" width="92">&nbsp;</td>
	 <td width="508" valign="bottom" class="text"><%

		'If the	user has a signature offer them	the chance to show it
		If blnLoggedInUserSignature = True Then

	%>		&nbsp;<input type="checkbox" name="signature" value="True"<% If	blnAttachSignature = True Then Response.Write("	checked") %> /><%	= strTxtShowSignature %>&nbsp;<%
		End If

		'Display e-mail	notify of replies option
		If blnEmail = True AND blnLoggedInUserEmail = True Then

	%>		&nbsp;<input type="checkbox" name="email" value="True"<% If blnReplyNotify = True Then Response.Write("	checked") %> /><%	= strTxtEmailNotify %>&nbsp;<%

		End If
		  %>
	 </td>
	</tr>
	</tr><%

	End If

'If this is a private e-mail and e-mail	is on and the user gave	an e-mail address let them choose to be	notified when pm msg is	read
ElseIf strMode = "PM" AND blnEmail = True AND blnLoggedInUserEmail Then
	%>
	<tr>
	 <td align="right" width="92">&nbsp;</td>
	 <td width="508" valign="bottom" class="text">&nbsp;<input type="checkbox" name="email"	value="True" /><% = strTxtEmailNotifyWhenPMIsRead %>
	 </td>
	</tr><%
End If
%>
	<td><%
If NOT strMode = "PM" Then  %>
	 <input	type="hidden" name="mode" value="<% = strMode %>" />
	 <input	type="hidden" name="FID" value="<% = intForumID	%>" />
	 <input	type="hidden" name="TID" value="<% = lngTopicID	%>" />
	 <input	type="hidden" name="PID" value="<% = lngMessageID %>" />
	 <input	type="hidden" name="TPN" value="<% = intRecordPositionPageNum %>" /><%
	 'If reply get the thread position number in the topic
	 If strMode = "reply" Then	%>
	 <input	type="hidden" name="ThreadPos" value="<% = (intTotalNumOfThreads + 1) %>" /><%
	End If
End If %>
	 <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
	 &nbsp;</td>
	<td width="70%"	align="left">
	 <p><%
'Select	the button for this page
If strMode="edit" OR strMode = "editTopic" Then

	Response.Write("	  <input type=""submit"" name=""Submit"" value=""" & strTxtUpdatePost & """ onClick=""return CheckForm();"" />")

ElseIf strMode = "new" OR strMode = "poll" Then

	Response.Write("	  <input type=""submit"" name=""Submit"" value=""" & strTxtNewTopic & """ onClick=""return CheckForm();"" />")

ElseIf strMode = "PM" Then

	Response.Write("	  <input type=""submit"" name=""Submit"" value=""" & strTxtPostMessage & """ onClick=""return CheckForm();"" />")

Else

	Response.Write("	  <input type=""submit"" name=""Submit"" value=""" & strTxtPostReply & """ onClick=""return CheckForm();"" />")

End If
			    %>
	  <input type="button" name="Preview" value="<%	= strTxtPreviewPost %>"	onClick="OpenPreviewWindow('post_preview.asp', document.frmAddMessage);" />
	  <input type="reset" name="Reset" value="<% = strTxtResetForm %>" />
	 </p>
	</td>
	</tr>
       </table>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</form>