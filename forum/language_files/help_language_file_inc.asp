<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by email ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Global
'---------------------------------------------------------------------------------
Const strTxtForumHelp = "Forum Help"
Const strTxtChooseAHelpTopic = "Choose a help topic"
Const strTxtLoginAndRegistration = "Registration and Logging into the Forum"
Const strTxtUserPreferencesAndForumSettings = "User Preferences and Forum Settings"
Const strTxtPostingIssues = "Posting Issues"
Const strTxtMessageFormatting = "Message Formatting"
Const strTxtUsergroups = "Usergroups"
Const strTxtPrivateMessaging = "Private Messaging"

Const strTxtWhyCantILogin = "Why can't I login?"
Const strTxtDoINeedToRegister = "Do I need to register?"
Const strTxtLostPasswords = "Lost Passwords"
Const strTxtIRegisteredInThePastButCantLogin = "I Registered in the past but can't login"

Const strTxtHowDoIChangeMyForumSettings = "How do I change my forum settings?"
Const strTxtForumTimesAndDates = "Forum Times and Dates are not set to my local time"
Const strTxtWhatDoesMyRankIndicate = "What does my rank indicate?"
Const strTxtCanIChangeMyRank = "Can I change my rank?"

Const strTxtHowPostMessageInTheForum = "How do I post a message in the forum?"
Const strTxtHowDeletePosts = "How do I delete posts?"
Const strTxtHowEditPosts = "How do I  edit posts?"
Const strTxtHowSignaturToMyPost = "How do I add a signature to my post?"
Const strTxtHowCreatePoll = "How do I create a poll?"
Const strTxtWhyNotViewForum = "Why can I not view a forum?"
Const strTxtInternetExplorerWYSIWYGPosting = "Rich Text Editor (WYSIWYG) posting issues"

Const strTxtWhatForumCodes = "What are Forum Codes?"
Const strTxtCanIUseHTML = "Can I use HTML?"
Const strTxtWhatEmoticons = "What are Emoticons (Smileys)"
Const strTxtCanPostImages = "Can I post images?"
Const strTxtWhatClosedTopics = "What are closed topics?"

Const strTxtWhatForumAdministrators = "What are Forum Administrators?"
Const strTxtWhatForumModerators = "What are Forum Moderators?"
Const strTxtWhatUsergroups = "What are Usergroups?"

Const strTxtIPrivateMessages = "I cannot send Private Messages"
Const strTxtIPrivateMessagesToSomeUsers = "I cannot send Private Messages to some users"
Const strTxtHowCanPreventSendingPrivateMessages = "How can I prevent someone from sending me Private Messages"


Const strTxtFAQ1 = "To login into the Forum you must use the Username and Password that you entered when registering for the Forum. If you have not yet registered then you must first do so in order to login. If you have registered and still are unable to login then first check that you have all cookies enabled on your web browser, you may need to add this web site to your browsers list of trusted web sites. If you are banned from the Forums then this may prevent you form logging in, in which case check with the Forums administrator."
Const strTxtFAQ2 = "You may not need to register to post in the Forums, it is up to the Forums Administrator as to whether they allow you to post in the Forums as an unregistered user. However, by registering you will be able to use additional features. It only takes a few minutes to register, so it is recommended that you do so."
Const strTxtFAQ3 = "If you have lost your password then don't panic. Although passwords can not be retrieved they can be reset. To reset your password click on the Login button and at the bottom of the login page you will have a link to the lost password page to request that a new password be emailed to you. If this option is not available or you do not have a valid email address in your profile then you need to contact the Forum Administrator or a Moderator and ask them to change your password for you."
Const strTxtFAQ4 = "It could be that you haven't posted anything for a while, or never posted anything. It is common for the Forum Administrator to periodically delete users from the database to free up usernames and reduce the size of the database."
Const strTxtFAQ5 = "You can change your forum settings, profile information, registration details, etc. from your <a href=""member_control_panel.asp"" target=""_self"">Member Control Panel Menu</a>, once you have logged into the Forum. You will be able to control many aspects and access member features from this central menu."
Const strTxtFAQ6 = "The time used in the Forums is that of the server time and date, if the server is located in another country then the times and dates will be the local times of that country. To change the Times and Dates to your own local settings then simply edit your 'Forum Preferences' through your <a href=""member_control_panel.asp"" target=""_self"">Member Control Panel Menu</a>, and set by how many hours the time is offset from your own local time. The Forums are not designed to adjust between standard and daylight saving times, so you may need to adjust the time in your Forum Preferences by another hour during these months."
Const strTxtFAQ7 = "Ranks in the forums indicate which user group you are a member of and to identify users, for example, moderators and administrators may have a special rank. Depending on the setup of the forum you may be able to use different features of the forum depending on which rank you belong to."
Const strTxtFAQ8 = "Normally you can not, but if the forum administrator has setup ranks using the ladder system you may be able to move up groups in the forum by the number of posts you have made."
Const strTxtFAQ9 = "To post a message in the Forums click on the relevant button on the forum or topic screens. Depending on how the forum administrator has setup the forum depends if you need to login first before you can post a message. The facilities available to you in each of the forums are listed at the bottom of the topic screen."
Const strTxtFAQ10 = "Unless you are a Forum Moderator or an Administrator you can only delete your own posts and only if the forum has been setup with the relevant rights for you to be able to delete your post. If someone has replied to your post then you will no-longer be able to delete it."
Const strTxtFAQ11 = "Unless you are a Forum Moderator or an Administrator you can only edit your own posts and only if the forum administrator has created the relevant rights for you to do so. When you edit your posts depending on the forum setup, it will display the username, time, and date of when the post was edited at the bottom of the post."
Const strTxtFAQ12 = "If the forum administrator has allowed the use of signatures in the forums you can add a signature to the bottom of your posts. To do so you need to first create a signature in your 'Profile Information' through your <a href=""member_control_panel.asp"" target=""_self"">Member Control Panel Menu</a>, once you have done this you can add your signature to the bottom of your posts by checking the 'Show Signature' checkbox at the bottom of the posting form."
Const strTxtFAQ13 = "If you have sufficient rights to create a poll in a forum you will see a 'New Poll' button at the top of the screen on the forum and topic screens. When creating a poll you need to enter a poll question and at least two options for the poll. You may also select whether people can vote multiple times or just once in the poll."
Const strTxtFAQ14 = "Some forums are setup to allow only certain users or groups f users to access them. To view, read, post, etc. in a forum you may first need permission which only a forum moderator or a forum administrator can grant you."
Const strTxtFAQ15 = "If you are using Internet Explorer 5+ (windows only), Netscape 7.1, Mozilla 1.3+, Mozilla Firebird 0.6.1+, and if the forum administrator has enabled it you should have a Rich Text (WYSIWYG) Editor to type your messages with. If you find that you are having problems posting using the WYSIWYG editor then you can disable this WYSIWYG Editor by simply editing your profile and selecting to turn off the WYSIWYG Posting Editor."
Const strTxtFAQ16 = "Forum Codes allow you to format the messages you post in the forums. Forum Codes are very similar to HTML except tags are enclosed in square brackets, [ and ], rather than, &lt; and &gt;. You can also disable Forum Codes when posting a message. <a href=""JavaScript:openWin('forum_codes.asp','codes','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=550,height=400')"">Click here to view Forum Codes that are available in the forums</a>."
Const strTxtFAQ17 = "HTML cannot be used in your posts, this is done for security reasons as malicious HTML code can be used to destroy the layout of the forum or even crash a web browser when a user tries to view a post."
Const strTxtFAQ18 = "Emoticons or Smileys are small graphical images that can be used to express feelings or show emotions. If the forum administrators have allowed Emoticons in the forums then you can see them next to the posting form when posting a message. To add an emoticon to your post then simply click on the emoticon you would like to add to your post."
Const strTxtFAQ19 = "Images can be added to your posts, if the forum administrators have allowed uploading of images you can use this to upload an image from your own computer into your message. However, if image uploading is not available then you will need to link to the image that needs to be stored on a publicly accessible web server, eg. http://www.mysite.com/my-picture.jpg."
Const strTxtFAQ20 = "Closed Topics are set this way by forum administrators or moderators. Once a Topic is closed you will no-longer be able to post a reply in that topic or vote in a poll."
Const strTxtFAQ21 = "Forums Administrators are people who have the highest level of control over the forums, they have the ability to turn on and off features on the forums, ban users, remove users, edit and delete posts, create users groups, etc."
Const strTxtFAQ22 = "Moderators are individuals or groups of users who look after the day to day running of the forums. They have the power to edit, delete, move, close, unclose, topics and posts, in the forum they moderate. Moderators generally are there to prevent people from posting offensive or abusive material."
Const strTxtFAQ23 = "Usergroups are a way to group users. Each user is a member of a usergroup and each group can be assigned individual rights in forums, to read, view, post, create polls, etc."
Const strTxtFAQ24 = "There maybe several reasons for this, you are not logged in, you are not registered, or the forum administrators have disabled the Private Messaging system."
Const strTxtFAQ25 = "This maybe because the person you are trying to send a Private Message to has blocked you from being able to send them Private Messages, if this is the case you should receive a message informing you of this if you try to send them a Private Message."
Const strTxtFAQ26 = "If you do find that you are getting unwanted Private Messages from a user you can block them from sending you Private Messages. To do this go into the Private Messaging system and go to your buddy list. Go to add the user as a buddy, but instead choose the option from the drop down list 'Not to message you', this will prevent this user from sending you anymore Private Messages."
%>