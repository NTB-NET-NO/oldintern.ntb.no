<%
'Topic admin
'---------------------------------------------------------------------------------
Const strTxtUpdateTopic = "Oppdatere emne"
Const strTxtTopicNotFoundOrAccessDenied = "Kan ikke finne emne (eller du har ikke adgang til denne siden)."
Const strTxtMoveTopic = "Flytt emne"
Const strTxtDeletePoll = "Slett sp�rreunders�kelse"
Const strTxtAreYouSureYouWantToDeleteThisPoll = "Er du sikker p� at du vil slette denne sp�rreunders�kelsen?"

'move_post_form.asp
'---------------------------------------------------------------------------------
Const strTxtSelectTheForumYouWouldLikePostIn = "Velg hvilket forum du vil bruke for dette innlegget"
Const strTxtMovePostErrorMsg = "Forum\t- Velg hvilket forum du vil flytte innlegget til"
Const strTxtSelectForumClickNext = "Steg 1: - Velg hvilket forum du vil flytte innlegget til.<br />Klikk deretter p� Neste-knappen."
Const strTxtSelectTopicToMovePostTo = "Steg 2: - Velg hvilket emne du vil flytte innlegget til fra listen over de 400 siste emnene.<br />Du kan ogs� lage et nytt emne i feltet nederst p� siden."
Const strTxtSelectTheTopicYouWouldLikeThisPostToBeIn = "Velg hvilket emne du vil bruke for dette innlegget."
Const strTxtOrTypeTheSubjectOfANewTopic = "Eller lag en tittel p� et nytt emne."

'pop_up_IP_blocking.asp
'---------------------------------------------------------------------------------
Const strTxtIPBlocking = "IP-blokkering"
Const strTxtBlockedIPList = "Blokkert IP-addresseliste"
Const strTxtYouHaveNoBlockedIpAddesses = "Du har ingen blokkerte IP-adresser"
Const strTxtRemoveIP = "Fjern IP"
Const strTxtBlockIPAddressOrRange = "Blokker IP-adresse eller omr�de"
Const strTxtIpAddressRange = "IP-addresse/omr�de"
Const strTxtBlockIPAddressRange = "Blokker IP-addresse/omr�de"
Const strTxtBlockIPRangeWhildcardDescription = "Jokertegnet * kan brukes f�r � blokkere IP-omr�der. <br />F.eks. For � blokkere omr�det '200.200.200.0 - 255' bruk '200.200.200.*'"
Const strTxtErrorIPEmpty = "IP-adresse/omr�de \t- Tast inn IP-adresse eller omr�de du vil blokkere"

'register.asp - in admin mode
'---------------------------------------------------------------------------------
Const strTxtAdminModeratorFunctions = "Adminfunksjoner"
Const strTxtUserIsActive = "Bruker er aktiv"
Const strTxtDeleteThisUser = "Slette dette medlem?"
Const strTxtCheckThisBoxToDleteMember = "Kryss av i dette feltet for � slette medlemmet. Kan ikke angres."
Const strTxtNumberOfPosts = "Antall innlegg"
Const strTxtNonRankGroup = "Ikke stigegruppe"
Const strTxtRankGroupMinPosts = "Stigegruppe - Min. innlegg"

'Topic admin
'---------------------------------------------------------------------------------
Const strTxtUpdateForum = "Oppdatere forum"
Const strTxtForumNotFoundOrAccessDenied = "Kan ikke finne forum, eller ingen adgang til denne siden."
Const strTxtErrorForumName = "Forumnavn \t- Tast inn navn p� dette forum"
Const strTxtErrorForumDescription  = "Forumsbeskrivelse \t- Tast inn en beskrivelse p� dette forum"
Const strTxtResyncTopicPostCount = "Oppdatere forum og innleggstellere"

'New from version 7.02
'---------------------------------------------------------------------------------
Const strTxtShowMovedIconInLastForum = "Vis 'flyttet' ikon i siste forum"

'New from version 8.0
'---------------------------------------------------------------------------------
Const strTxtHideTopic = "Skjul emne"
Const strTxtIfYouAreShowingTopic = "For � vise et emne, s�rg for at minst ett innlegg ogs� er synlig."
Const strTxtShowHiddenTopicsSince = "Vis skjulte emner/innlegg siden"
Const strTxtHiddenTopicsPosts = "Skjulte emner/innlegg"
Const strTxtNoHiddenTopicsPostsSince = "Det finnes ingen skjulte emner/innlegg siden"

%>