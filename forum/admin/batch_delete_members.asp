<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the timeout higher so that it doesn't timeout half way through
Session.Timeout =  1000

'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Dimension variables
Dim lngDelAuthorID		'Holds the authors ID to be deleted
Dim intNoOfDays			'Holds the number of days to delete posts from
Dim lngNumberOfMembers		'Holds the number of members that are deleted
Dim rsThread			'Holds the threads recordset
Dim blnUnActive			'Set to true if deleting non active accounts only

'Initilise variables
lngNumberOfMembers = 0

'get teh number of days to delte from
intNoOfDays = CInt(Request.Form("days"))
blnUnActive = CBool(Request.Form("unactive"))



'Get all the Topics from the database to be deleted

'Initalise the strSQL variable with an SQL statement to get the topic from the database
strSQL = "SELECT " & strDbTable & "Author.Author_ID FROM " & strDbTable & "Author "
strSQL = strSQL & "WHERE (" & strDbTable & "Author.Join_date < " & strDatabaseDateFunction & " - " & intNoOfDays  & " AND " & strDbTable & "Author.No_of_posts=0) AND " & strDbTable & "Author.Author_ID > 2"
If blnUnActive = True Then 
	If strDatabaseType = "SQLServer" Then
		strSQL = strSQL & " AND " & strDbTable & "Author.Active = 0"
	Else
		strSQL = strSQL & " AND " & strDbTable & "Author.Active = False"
	End If
End If
strSQL = strSQL & ";"


'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
rsCommon.CursorType = 2

'Set set the lock type of the recordset to optomistic while the record is deleted
rsCommon.LockType = 3


'Query the database
rsCommon.Open strSQL, adoCon


'Create a record set object to the Threads held in the database
Set rsThread = Server.CreateObject("ADODB.Recordset")


'Loop through all all the members to delete
Do While NOT rsCommon.EOF

	'Get the author ID
	lngDelAuthorID = CLng(rsCommon("Author_ID"))


	'Check to make sure that there isn't any posts by the member
	strSQL = "SELECT TOP 1 " & strDbTable & "Thread.Thread_ID FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Author_ID=" & lngDelAuthorID & ";"

	'Query the database
	rsThread.Open strSQL, adoCon
	
	'If there are no posts start deleting
	If rsThread.EOF Then
		
		'Delete the members buddy list
		'Initalise the strSQL variable with an SQL statement
		strSQL = "DELETE FROM " & strDbTable & "BuddyList WHERE (Author_ID ="  & lngDelAuthorID & ") OR (Buddy_ID ="  & lngDelAuthorID & ")"
		
		'Write to database
		adoCon.Execute(strSQL)	
		
		
		'Delete the members private msg's
		strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (Author_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)	
		
		
		'Delete the members private msg's
		strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (From_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Set all the users private messages to Guest account
		strSQL = "UPDATE " & strDbTable & "PMMessage SET From_ID=2 WHERE (From_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Set all the users posts to the Guest account
		strSQL = "UPDATE " & strDbTable & "Thread SET Author_ID=2 WHERE (Author_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
				
		
		'Delete the user from the email notify table
		strSQL = "DELETE FROM " & strDbTable & "EmailNotify WHERE (Author_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Delete the user from forum permissions table
		strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE (Author_ID ="  & lngDelAuthorID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Delete the record set
		rsCommon.Delete
		
		
		'Total up the number of members deleted
		lngNumberOfMembers = lngNumberOfMembers + 1
	End If
	
	'Close the recordset
	rsThread.Close

	'Move to the next record
	rsCommon.MoveNext
Loop



'Reset Server Objects
Set rsThread = Nothing
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Batch Delete Members</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">Batch Delete Members </span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br>
  <br>
  <br>
  <br>
  <br />
  <% = lngNumberOfMembers %> Members have been Deleted.<br />
 </p>
</div>
</body>
</html>
