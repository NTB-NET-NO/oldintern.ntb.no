Imports System.Data.SqlClient
Imports System.Web.Configuration


Namespace intranett

    Partial Class leftmenu2

        Inherits System.Web.UI.UserControl


#Region " Web Form Designer Generated Code "
        Protected WithEvents dlSubMenu As System.Web.UI.WebControls.DataList

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Protected lmdr As SqlDataReader
        Protected subitems As String = ""
        Protected submenus As New ArrayList



        Private Sub BindMenu(ByVal menuid As Integer, ByVal menu As DataList)

            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))

            Dim cd As New SqlCommand
            ' cd.Parameters.Item("@newtext").Value = ConfigurationSettings.AppSettings("newtext")
            ' cd.Parameters.Item("@newtext").Value = WebConfigurationManager.AppSettings("newtext")


            cd.Connection = dbNTB
            dbNTB.Open()
            cd.CommandType = CommandType.StoredProcedure
            cd.CommandText = "_GetMenu"
            cd.Parameters.Clear()
            cd.Parameters.Add(New SqlParameter("@menuid", SqlDbType.Int))
            cd.Parameters.Item("@menuid").Value = menuid
            cd.Parameters.Add(New SqlParameter("@newtext", SqlDbType.VarChar, 255))
            ' cd.Parameters.Item("@newtext").Value = ConfigurationSettings.AppSettings("newtext")
            cd.Parameters.Item("@newtext").Value = WebConfigurationManager.AppSettings("newtext")

            cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
            ' cd.Parameters.Item("@sitecode").Value = ConfigurationSettings.AppSettings("SiteCode")
            cd.Parameters.Item("@sitecode").Value = WebConfigurationManager.AppSettings("SiteCode")
            lmdr = cd.ExecuteReader
            menu.DataSource = lmdr
            menu.DataBind()
            lmdr.Close()
            dbNTB.Close()
        End Sub

        Private Function BindSubMenu(ByVal menuid As Integer) As String
            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
            Dim cd As New SqlCommand
            Dim dr As SqlDataReader
            Dim m As String

            cd.Connection = dbNTB
            dbNTB.Open()
            cd.CommandType = CommandType.StoredProcedure
            cd.CommandText = "_GetMenu"
            cd.Parameters.Clear()
            cd.Parameters.Add(New SqlParameter("@menuid", SqlDbType.Int))
            cd.Parameters.Item("@menuid").Value = menuid
            cd.Parameters.Add(New SqlParameter("@newtext", SqlDbType.VarChar, 255))
            ' cd.Parameters.Item("@newtext").Value = ConfigurationSettings.AppSettings("newtext")
            cd.Parameters.Item("@newtext").Value = WebConfigurationManager.AppSettings("newtext")
            cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
            ' cd.Parameters.Item("@sitecode").Value = ConfigurationSettings.AppSettings("SiteCode")
            cd.Parameters.Item("@sitecode").Value = WebConfigurationManager.AppSettings("SiteCode")

            m = "<div id='sub" & menuid & "' style='Z-INDEX:1; VISIBILITY:hidden; WIDTH:80px; POSITION:absolute'>"
            m += "<table class='artikkel_tekst' cellpadding='2' cellspacing='1' width='80' bgcolor='#000000'>"
            m += "<tr><td bgcolor='#EDFFCF'>"
            dr = cd.ExecuteReader
            While dr.Read
                m += "<a href='" & dr("URL") & "' target='" & dr("Target") & "' class='artikkel_tekst'>" & dr("LinkName") & "</a>&nbsp;<font color='red'><b>" & dr("IsNewText") & "</b><br>"
            End While
            m += "</td></tr></table></div>"
            BindSubMenu = m
        End Function

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim i As Integer
            If Not Page.IsPostBack Then
                BindMenu(9, dlMenu)
                For i = 0 To submenus.Count - 1
                    subitems += BindSubMenu(submenus(i))
                Next
            End If
        End Sub

        Private Sub dlMenu_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlMenu.ItemCreated
            Try
                If CType(sender, DataList).DataSource("SubMenu") > 0 Then submenus.Add(CType(sender, DataList).DataSource("SubMenu"))
            Catch
            End Try
        End Sub

        Private Sub dlMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlMenu.ItemDataBound

            If e.Item.DataItem("URL").ToString() = "" Then
                e.Item.FindControl("label").Visible = True
            Else
                e.Item.FindControl("link").Visible = True
            End If


        End Sub
    End Class

End Namespace
