<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting and setting a cookie
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Dimension variables
Dim rsCategory			'Holds the categories for the forums
Dim rsForum 			'Holds the Recordset for the forum details
Dim intForumID			'Holds the forum ID number
Dim strCategory			'Holds the category name
Dim intCatID			'Holds the id for the category
Dim strForumName		'Holds the forum name
Dim strForumDiscription		'Holds the forum description
Dim strForumPassword		'Holds the forum password if there is one
Dim strModeratorsList		'Holds a list of moderators for the forum
Dim dtmForumStartDate		'Holds the forum start date
Dim lngNumberOfTopics		'Holds the number of topics in a forum
Dim lngNumberOfPosts		'Holds the number of Posts in the forum
Dim lngTotalNumberOfTopics	'Holds the total number of topics in a forum
Dim lngTotalNumberOfPosts	'Holds the total number of Posts in the forum
Dim intNumberofForums		'Holds the number of forums
Dim lngLastEntryMeassgeID	'Holds the message ID of the last entry
Dim lngLastEntryTopicID		'Holds the topic ID of the last entry
Dim dtmLastEntryDate		'Holds the date of the last entry to the forum
Dim strLastEntryUser		'Holds the the username of the user who made the last entry
Dim lngLastEntryUserID		'Holds the ID number of the last user to make and entry
Dim dtmLastEntryDateAllForums	'Holds the date of the last entry to all fourms
Dim strLastEntryUserAllForums	'Holds the the username of the user who made the last entry to all forums
Dim lngLastEntryUserIDAllForums	'Holds the ID number of the last user to make and entry to all forums
Dim blnForumLocked		'Set to true if the forum is locked
Dim intForumColourNumber	'Holds the number to calculate the table row colour
Dim intForumReadRights		'Holds the interger number to calculate if the user has read rights on the forum
Dim intForumPostRights		'Holds the interger valuse to calculate if the suer can poist in the forum
Dim intForumReplyRights		'Holds the interger value to calculate if the user can reply to a post
Dim blnHideForum		'Set to true if this is a hidden forum
Dim intCatShow			'Holds the ID number of the category to show if only showing one category
Dim intActiveUsers		'Holds the number of active users
Dim intActiveGuests		'Holds the number of active guests
Dim intActiveMembers		'Holds the number of logged in active members
Dim strMembersOnline		'Holds the names of the members online

'Initialise variables
lngTotalNumberOfTopics = 0
lngTotalNumberOfPosts = 0
intNumberofForums = 0
intForumColourNumber = 0
intActiveMembers = 0
intActiveGuests = 0
intActiveUsers = 0


'Read in the category to show
If Request.QueryString("C") Then
	intCatShow = CInt(Request.QueryString("C"))
Else
	intCatShow = 0
End If


'Craete a recordset to get the forum details
Set rsCategory = Server.CreateObject("ADODB.Recordset")


'Read the various categories from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "CategoryAll"
Else
	strSQL = "SELECT " & strDbTable & "Category.Cat_name, " & strDbTable & "Category.Cat_ID FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"
End If

'Query the database
rsCategory.Open strSQL, adoCon

%>
<html>
<head>
<title><% = strMainForumName %></title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" --><%

Response.Write(vbCrLf & "   <table width=""" & strTableVariableWidth & """ border=""0"" cellspacing=""4"" cellpadding=""3"" align=""center"">")
Response.Write(vbCrLf & "    <tr>")


Response.Write(vbCrLf & "	<td class=""smText""> " & strTxtTheTimeNowIs & " " & DateFormat(now(), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(now(), saryDateTimeData) & ".<br />")

'If this is not the first time the user has visted the site display the last visit time and date
If Session("dtmLastVisit") < CDate(Request.Cookies(strCookieName)("LTVST")) Then
   	Response.Write(strTxtYouLastVisitedOn & " " & DateFormat(Session("dtmLastVisit"), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(Session("dtmLastVisit"), saryDateTimeData) & ".")
End If


'Display main page link if in a category view
If intCatShow <> 0 Then Response.Write("<br /><span class=""bold""><img src=""" & strImagePath & "open_folder_icon.gif"" border=""0"" align=""absmiddle"">&nbsp;<a href=""default.asp"" target=""_self"" class=""boldLink"">" & strMainForumName & "</a></span>")


Response.Write(vbCrLf & "  <br /><br /></td>")


'If the user has not logged in (guest user ID = 2) then show them a quick login form
If lngLoggedInUserID = 2 Then

	Response.Write(vbCrLf & "	<td align=""right"" class=""smText""><form method=""post"" name=""frmLogin"" action=""login_user.asp"">")
	If blnLongSecurityCode Then Response.Write(strTxtLogin) Else Response.Write(strTxtQuickLogin)
	Response.Write(" " & _
	vbCrLf & "	    <input type=""text"" size=""10"" name=""name"" class=""smText"" />" & _
	vbCrLf & "  	    <input type=""password"" size=""10"" name=""password"" class=""smText"" />" & _
	vbCrLf & "	    <input type=""hidden"" name=""QUIK"" value=""true"" />" & _
	vbCrLf & "	    <input type=""hidden"" name=""NS"" value=""1"" />")
	If blnLongSecurityCode = false Then Response.Write(vbCrLf & "	    <input type=""hidden"" name=""sessionID"" value=""" & Session.SessionID & """ />")
	Response.Write(vbCrLf & "	    <input type=""submit"" value=""" & strTxtGo & """ class=""smText"" />" & _
	vbCrLf & "	</form></td>")

End If

Response.Write(vbCrLf & "    </tr>")

 %>
   </table>
   <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="3" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
        <td bgcolor="<% = strTableTitleColour %>" width="1%" class="tHeading" background="<% = strTableTitleBgImage %>">&nbsp;</td>
        <td bgcolor="<% = strTableTitleColour %>" width="56%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtForum %></td>
        <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtTopics %></td>
        <td bgcolor="<% = strTableTitleColour %>" width="7%" align="center" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtPosts %></td>
        <td bgcolor="<% = strTableTitleColour %>" width="29%" class="tHeading" align="center" background="<% = strTableTitleBgImage %>"><% = strTxtLastPost %></td>
       </tr><%

'Check there are categories to display
If rsCategory.EOF Then

	'If there are no categories to display then display the appropriate error message
	Response.Write (vbCrLf & "<tr><td bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtNoForums & "</td></tr>")

'Else there the are categories so write the HTML to display categories and the forum names and a discription
Else

	'Create a recordset to get the forum details
	Set rsForum = Server.CreateObject("ADODB.Recordset")


	'Loop round to read in all the categories in the database
	Do While NOT rsCategory.EOF

		'Get the category name from the database
		strCategory = rsCategory("Cat_name")
		intCatID = CInt(rsCategory("Cat_ID"))

		'Display the category name
		Response.Write vbCrLf & "<tr><td bgcolor=""" & strTableTitleColour2 & """ background=""" & strTableTitleBgImage2 & """ colspan=""5""><a href=""default.asp?C=" & intCatID & """ target=""_self"" class=""cat"">" & strCategory & "</a></td></tr>"

		'If there this is the cat to show then show it
		If intCatShow = intCatID OR intCatShow = 0 Then

			'Read the various forums from the database
			'Initalise the strSQL variable with an SQL statement to query the database
			If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereCatIs @intCatID = " & intCatID
			Else
				strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = " & intCatID & " ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"
			End If

			'Query the database
			rsForum.Open strSQL, adoCon

			'Check there are forum's to display
			If rsForum.EOF Then

				'If there are no forum's to display then display the appropriate error message
				Response.Write vbCrLf & "<tr><td bgcolor=""" & strTableColour & """  background=""" & strTableBgImage & """ colspan=""5"" class=""text"">" & strTxtNoForums & "</td></tr>"

			'Else there the are forum's to write the HTML to display it the forum names and a discription
			Else


				'Loop round to read in all the forums in the database
				Do While NOT rsForum.EOF

					'Initialise variables
					lngLastEntryTopicID = 0
					strModeratorsList = ""


					'Read in forum details from the database
					intForumID = CInt(rsForum("Forum_ID"))
					strForumName = rsForum("Forum_name")
					strForumDiscription = rsForum("Forum_description")
					dtmForumStartDate = CDate(rsForum("Date_Started"))
					strForumPassword = rsForum("Password")
					lngNumberOfPosts = CLng(rsForum("No_of_posts"))
					lngNumberOfTopics = CLng(rsForum("No_of_topics"))
					blnForumLocked = CBool(rsForum("Locked"))
					intForumReadRights = CInt(rsForum("Read"))
					intForumPostRights = CInt(rsForum("Post"))
					intForumReplyRights = CInt(rsForum("Reply_posts"))
					blnHideForum = CBool(rsForum("Hide"))



					'Call the function to check the forum permissions
					Call forumPermisisons(intForumID, intGroupID, intForumReadRights, intForumPostRights, intForumReplyRights, 0, 0, 0, 0, 0, 0, 0)



					'Add all the posts and topics together to get the total number for the stats at the bottom of the page
					lngTotalNumberOfPosts = lngTotalNumberOfPosts + lngNumberOfPosts
					lngTotalNumberOfTopics = lngTotalNumberOfTopics + lngNumberOfTopics



					'If this forum is to be hidden and but the user is allowed access to it set the hidden boolen back to false
					If blnHideForum = True AND blnRead = True Then blnHideForum = False



					'If the forum is to be hidden then don't show it
					If blnHideForum = False Then

						'Get the row number
						intForumColourNumber = intForumColourNumber + 1


						'Initilaise variables for the information required for each forum
						dtmLastEntryDate = dtmForumStartDate
						strLastEntryUser = strTxtForumAdministrator
						lngLastEntryUserID = 1


						'Get the List of Group Moderators for the Forum
						If blnShowMod Then

							'Initalise the strSQL variable with an SQL statement to query the database to get the moderators for this forum
							If strDatabaseType = "SQLServer" Then
								strSQL = "EXECUTE " & strDbProc & "ModeratorGroup @intForumID = " & intForumID
							Else
								strSQL = "SELECT " & strDbTable & "Group.Group_ID, " & strDbTable & "Group.Name "
								strSQL = strSQL & "FROM " & strDbTable & "Group, " & strDbTable & "Permissions "
								strSQL = strSQL & "WHERE " & strDbTable & "Group.Group_ID = " & strDbTable & "Permissions.Group_ID AND " & strDbTable & "Permissions.Moderate = True AND " & strDbTable & "Permissions.Forum_ID = " & intForumID & ";"

							End If

							'Query the database
							rsCommon.Open strSQL, adoCon

							'Initlaise the Moderators List varible if there are records returned for the forum
							If NOT rsCommon.EOF Then strModeratorsList = "<br /><span class=""smText"">" & strTxtModerators & ":</span>"

							'Loop round to build a list of moderators, if there are any
							Do While NOT rsCommon.EOF

								'Place the moderators username into the string
								strModeratorsList = strModeratorsList & " <a href=""members.asp?GID=" & rsCommon("Group_ID") & """ class=""smLink"">" & rsCommon("Name") & "</a>"

								'Move to the next record
								rsCommon.MoveNext
							Loop

							'Close the recordset
							rsCommon.Close



							'Initalise the strSQL variable with an SQL statement to query the database to get the moderators for this forum
							If strDatabaseType = "SQLServer" Then
								strSQL = "EXECUTE " & strDbProc & "Moderators @intForumID = " & intForumID
							Else
								strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username "
								strSQL = strSQL & "FROM " & strDbTable & "Permissions, " & strDbTable & "Author "
								strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Permissions.Author_ID AND " & strDbTable & "Permissions.Moderate = True AND " & strDbTable & "Permissions.Forum_ID = " & intForumID & ";"

							End If

							'Query the database
							rsCommon.Open strSQL, adoCon

							'Initlaise the Moderators List varible if there are records returned for the forum
							If NOT rsCommon.EOF AND strModeratorsList = "" Then strModeratorsList = "<br /><span class=""smText"">" & strTxtModerators & ":</span>"

							'Loop round to build a list of moderators, if there are any
							Do While NOT rsCommon.EOF

								'Place the moderators username into the string
								strModeratorsList = strModeratorsList & " <a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & rsCommon("Author_ID") & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"" class=""smLink"">" & rsCommon("Username") & "</a>"

								'Move to the next record
								rsCommon.MoveNext
							Loop

							'Close the recordset
							rsCommon.Close
						End If




						'Initalise the strSQL variable with an SQL statement to query the database for the date of the last entry and the author for the thread
						If strDatabaseType = "SQLServer" Then
							strSQL = "EXECUTE " & strDbProc & "LastForumPostEntry @intForumID = " & intForumID
						Else
							strSQL = "SELECT Top 1 " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID, " & strDbTable & "Thread.Topic_ID, " & strDbTable & "Thread.Thread_ID, " & strDbTable & "Thread.Message_date "
							strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Thread  "
							strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID AND " & strDbTable & "Thread.Topic_ID IN "
							strSQL = strSQL & "	(SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
							strSQL = strSQL & "	FROM " & strDbTable & "Topic "
							strSQL = strSQL & "	WHERE " & strDbTable & "Topic.Forum_ID = " & intForumID & " "
							strSQL = strSQL & "	ORDER BY " & strDbTable & "Topic.Last_entry_date DESC) "
							strSQL = strSQL & "ORDER BY " & strDbTable & "Thread.Message_date DESC;"
						End If

						'Query the database
						rsCommon.Open strSQL, adoCon

						'If there are threads for topic then read in the date and author of the last entry
						If NOT rsCommon.EOF Then

							'Read in the deatils from the recorset of the last post details
							lngLastEntryMeassgeID = CLng(rsCommon("Thread_ID"))
							lngLastEntryTopicID = CLng(rsCommon("Topic_ID"))
							dtmLastEntryDate = CDate(rsCommon("Message_date"))
							strLastEntryUser = rsCommon("Username")
							lngLastEntryUserID = CLng(rsCommon("Author_ID"))
						End If

						'Reset variables
						rsCommon.Close




						'Calculate the last forum entry across all forums for the statistics at the bottom of the forum
						If dtmLastEntryDateAllForums < dtmLastEntryDate Then
							dtmLastEntryDateAllForums = dtmLastEntryDate
							strLastEntryUserAllForums = strLastEntryUser
							lngLastEntryUserIDAllForums = lngLastEntryUserID
						End If



						'Write the HTML of the forum descriptions and hyperlinks to the forums
						Response.Write(vbCrLf & "       <tr>" & _
						vbCrLf & "        <td bgcolor=""")
						If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour)
						Response.Write(""" background=""" & strTableBgImage & """ width=""1%"" class=""text"">")


		     				'If the user has no access to a forum diplay a no access icon
		     				If blnRead = False AND blnModerator = False AND blnAdmin = False Then
							Response.Write ("  <img src=""" & strImagePath & "forum_no_entry_icon.gif"" alt=""" & strTxtNoAccess & """>")

						'If the forum requires a password diplay the password icon
						ElseIf strForumPassword <> "" Then
							Response.Write ("  <img src=""" & strImagePath & "password_required_icon.gif"" alt=""" & strTxtPasswordRequired & """>")

						'If the forum is read only and has new posts show the locked new posts icon
						ElseIf CDate(Session("dtmLastVisit")) < dtmLastEntryDate AND (blnForumLocked = True) AND blnAdmin = False AND blnModerator = False Then
							Response.Write ("  <img src=""" & strImagePath & "locked_new_posts_icon.gif"" alt=""" & strTxtReadOnlyNewReplies & """>")

						'If the forum is read only show the locked new posts icon
						ElseIf blnForumLocked Then
							Response.Write ("  <img src=""" & strImagePath & "closed_topic_icon.gif"" alt=""" & strTxtReadOnly & """>")

						'If the forum has new posts show the new posts icon
						ElseIf CDate(Session("dtmLastVisit")) < dtmLastEntryDate Then
							Response.Write ("  <img src=""" & strImagePath & "new_posts_icon.gif"" alt=""" & strTxtOpenForumNewReplies & """>")

						'If the forum is open but with no new replies
						Else
							Response.Write ("  <img src=""" & strImagePath & "no_new_posts_icon.gif"" alt=""" & strTxtOpenForum & """>")
						End If

						Response.Write(vbCrLf & "        </td>" & _
						vbCrLf & "        <td bgcolor=""")
						If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour)
						Response.Write(""" background=""" & strTableBgImage & """ width=""1%"" class=""text"">")


						'If this is the forum admin then let them have access to the forum admin pop up window
						If blnAdmin Then Response.Write("      <a href=""javascript:openWin('pop_up_forum_admin.asp?FID=" & intForumID & "','admin','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=325')""><img src=""" & strImagePath & "small_admin_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtForumAdmin & """></a>")


						Response.Write(vbCrLf & "        <a href=""forum_topics.asp?FID=" & intForumID & """ target=""_self"">" & strForumName & "</a><br />" & strForumDiscription & strModeratorsList & "</td>" & _
						vbCrLf & "        <td bgcolor=""")
						If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour)
						Response.Write(""" background=""" & strTableBgImage & """ width=""7%"" align=""center"" class=""text"">" & lngNumberOfTopics & "</td>" & _
						vbCrLf & "        <td bgcolor=""")
						If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour)
						Response.Write(""" background=""" & strTableBgImage & """ width=""7%"" align=""center"" class=""text"">" & lngNumberOfPosts & "</td>" & _
						vbCrLf & "        <td bgcolor=""")
						If (intForumColourNumber MOD 2 = 0 ) Then Response.Write(strTableEvenRowColour) Else Response.Write(strTableOddRowColour)
						Response.Write(""" background=""" & strTableBgImage & """ width=""29%"" class=""smText"" align=""right""  nowrap=""nowrap"">" & DateFormat(dtmLastEntryDate, saryDateTimeData) & "&nbsp;" &  strTxtAt & "&nbsp;" & TimeFormat(dtmLastEntryDate, saryDateTimeData) & "" & _
						vbCrLf & "        <br />" & strTxtBy & "&nbsp;<a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & lngLastEntryUserID & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"" class=""smLink"">" & strLastEntryUser & "</a> <a href=""forum_posts.asp?TID=" & lngLastEntryTopicID & "&get=last#" & lngLastEntryMeassgeID & """ target=""_self""><img src=""" & strImagePath & "right_arrow.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtViewLastPost & """></a></td>" & _
						vbCrLf & "       </tr>")


					End If

					'Count the number of forums
					intNumberofForums = intNumberofForums + 1

					'Move to the next database record
					rsForum.MoveNext

				'Loop back round for next forum
				Loop
			End If


			'Close recordsets
			rsForum.Close

		End If

		'Move to the next database record
		rsCategory.MoveNext
	'Loop back round for next category
	Loop
End If

'Release server variables
rsCategory.Close
Set rsCategory = Nothing
Set rsForum = Nothing
%>
      </table>
     </td>
    </tr>
   </table>
   </td>
 </tr>
</table>
   <br />
   <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
    <td>
         <table width="100%" border="0" cellspacing="1" cellpadding="4">
          <tr>
           <td bgcolor="<% = strTableTitleColour %>" width="44%" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtForumStatistics %></td>
          </tr>
          <tr>
           <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="smText" valign="top"><%


Response.Write(vbCrLf & "		" & strTxtOurUserHavePosted & "&nbsp;" & lngTotalNumberOfPosts & "&nbsp;" & strTxtPostsIn & "&nbsp;" & lngTotalNumberOfTopics & "&nbsp;" & strTxtTopicsIn & "&nbsp;" & intNumberofForums & "&nbsp;" & strTxtForums)
Response.Write(vbCrLf & "		<br />" & strTxtLastPostOn & "&nbsp;" & DateFormat(dtmLastEntryDateAllForums, saryDateTimeData) & "&nbsp;" & strTxtAt & "&nbsp;" & TimeFormat(dtmLastEntryDateAllForums, saryDateTimeData) & " " & strTxtBy & "&nbsp;<a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & lngLastEntryUserIDAllForums & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"" class=""smLink"">" & strLastEntryUserAllForums & "</a>")

'Get the latest forum posts

'Cursor type to one to count
rsCommon.CursorType = 1

'Get the last signed up user
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "AuthorDesc"
Else
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "ORDER BY " & strDbTable & "Author.Author_ID DESC;"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'Display some statistics for the members
If NOT rsCommon.EOF Then

Response.Write(vbCrLf & "		<br />" & strTxtWeHave & "&nbsp;" & rsCommon.RecordCount & "&nbsp;" & strTxtForumMembers & _
vbCrLf & "		<br />" & strTxtTheNewestForumMember & "&nbsp;<a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & rsCommon("Author_ID") & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"" class=""smLink"">" & rsCommon("Username") & "</a>")


End If

'Close the recordset
rsCommon.Close


'Get the number of active users if enabled
If blnActiveUsers Then

	'Get the active users online
	For intArrayPass = 1 To UBound(saryActiveUsers, 2)

		'If this is a guest user then increment the number of active guests veriable
		If saryActiveUsers(1, intArrayPass) = 2 Then

			intActiveGuests = intActiveGuests + 1

		'Else add the name of the members name of the active users to the members online string
		ElseIf CBool(saryActiveUsers(7, intArrayPass)) = false Then
			If strMembersOnline <> "" Then strMembersOnline = strMembersOnline & ", "
			strMembersOnline = strMembersOnline & "<a href=""JavaScript:openWin('pop_up_profile.asp?PF=" & saryActiveUsers(1, intArrayPass) & "','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=590,height=425')"" class=""smLink"">" & saryActiveUsers(2, intArrayPass) & "</a>"
		End If

	Next

	'Calculate the number of members online and total people online
	intActiveUsers = UBound(saryActiveUsers, 2)
	intActiveMembers = intActiveUsers - intActiveGuests

	Response.Write(vbCrLf & "		<br />" & strTxtInTotalThereAre & "&nbsp;" & intActiveUsers & "&nbsp;<a href=""active_users.asp"" target=""_self"" class=""smLink"">" & strTxtActiveUsers & "</a> " & strTxtOnLine & ",&nbsp;" & intActiveGuests & "&nbsp;" & strTxtGuestsAnd & "&nbsp;" & intActiveMembers & "&nbsp;" & strTxtMembers)
	If strMembersOnline <> "" Then Response.Write(vbCrLf & "		<br />" & strTxtMembers & "&nbsp;" & strTxtOnLine & ":&nbsp;" & strMembersOnline)
End If
           %>
           </td>
          </tr>
         </table>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
   </td>
 </tr>
</table><%

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>   <div align="center"><br />
    <table width="533" border="0" cellspacing="0" cellpadding="2">
     <tr>
      <td width="202" class="smText"><img src="<% = strImagePath %>no_new_posts_icon.gif" alt="<% = strTxtOpenForum %>"> <% = strTxtOpenForum %></td>
      <td width="186" class="smText"><img src="<% = strImagePath %>closed_topic_icon.gif" alt="<% = strTxtReadOnly %>"> <% = strTxtReadOnly %></td>
      <td width="133" class="smText"><img src="<% = strImagePath %>password_required_icon.gif" alt="<% = strTxtPasswordRequired %>"> <% = strTxtPasswordRequired %></td>
     </tr>
     <tr>
      <td width="202" class="smText"><img src="<% = strImagePath %>new_posts_icon.gif" alt="<% = strTxtOpenForumNewReplies %>"> <% = strTxtOpenForumNewReplies %></td>
      <td width="186" class="smText"><img src="<% = strImagePath %>locked_new_posts_icon.gif" alt="<% = strTxtReadOnlyNewReplies %>"> <% = strTxtReadOnlyNewReplies %></td>
      <td width="133" class="smText"><img src="<% = strImagePath %>forum_no_entry_icon.gif" alt="<% = strTxtNoAccess %>"> <% = strTxtNoAccess %></td>
     </tr>
    </table>
    <br />
    <span class="text"><a href="mark_posts_as_read.asp" target="_self" class="smLink"><% = strTxtMarkAllPostsAsRead %></a> :: <a href="remove_cookies.asp" target="_self" class="smLink"><% = strTxtDeleteCookiesSetByThisForum %></a></span><br />
    <br />
    <span class="text"><% = strTxtCookies %></span><br />
    <br /><%

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"

%>
</div>
   <!-- #include file="includes/footer.asp" -->