<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Declare variables
Dim lngTopicID		'Holds the topic ID
Dim intForumID		'Holds the forum ID
Dim strSubject		'Holds the new subject
Dim lngOldTopicID	'Holds the old topic ID number
Dim lngPostID		'Holds the post ID
Dim strPostDateTime	'Holds the date of the post


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")
End If



'Read in the post ID
lngPostID = CLng(Request.Form("PID"))


'Query the datbase to get the forum ID for this post
strSQL = "SELECT " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Thread.Message_date "
strSQL = strSQL & "FROM " & strDbTable & "Topic, " & strDbTable & "Thread "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID AND " & strDbTable & "Thread.Thread_ID = " & lngPostID & ";"

'Query the database
rsCommon.Open strSQL, adoCon


'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
	lngOldTopicID = CLng(rsCommon("Topic_ID"))
	strPostDateTime = CDate(rsCommon("Message_date"))
End If

'Clean up
rsCommon.Close


'Call the moderator function and see if the user is a moderator
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)


'If the person is not a moderator or admin then send them away
If blnAdmin = False AND blnModerator = False Then
	'Reset Server Objects
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")


'The person is an admin of modertor so move the post
Else

	'Read in the forum details
	lngTopicID = CLng(Request.Form("topicSelect"))
	intForumID = CInt(Request.Form("toFID"))
	strSubject = Request.Form("subject")




	'If a new subject has been entered then place it into the database
	If strSubject <> "" Then

		'Get rid of scripting tags in the subject
		strSubject = removeAllTags(strSubject)
		strSubject = formatInput(strSubject)

		'Initalise the SQL string with a query to get the Topic details
		strSQL = "SELECT TOP 1 " & strDbTable & "Topic.* FROM " & strDbTable & "Topic "
		strSQL = strSQL & "WHERE Forum_ID =" & intForumID & " "
		strSQL = strSQL & "ORDER By " & strDbTable & "Topic.Topic_ID DESC;"

		With rsCommon
			'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
			.CursorType = 2

			'Set the Lock Type for the records so that the record set is only locked when it is updated
			.LockType = 3

			'Open the topic table
			.Open strSQL, adoCon

			'Insert the new topic details in the recordset
			.AddNew

			.Fields("Forum_ID") = intForumID
			.Fields("Subject") = strSubject
			.Fields("Start_date") =	strPostDateTime
			.Fields("Last_entry_date") = strPostDateTime
			.Fields("Poll_ID") = 0
			.Fields("Priority") = 0

			'Update the database with the new topic details
			.Update

			'Re-run the Query once the database has been updated
			.Requery

			'Move to the last record in the recordset to get the new topic's ID number
			.MoveLast

			'Read in the new topic's ID number
			lngTopicID = CLng(rsCommon("Topic_ID"))

			'Clean up
			.Close
		End With
	End If



		
	'Move the post to another topic use ADO otherwise access is to slow
	strSQL = "SELECT " & strDbTable & "Thread.Topic_ID FROM " & strDbTable & "Thread WHERE Thread_ID =" & lngPostID & ";"

	With rsCommon
	
		'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
		.CursorType = 2
		
		'Set the Lock Type for the records so that the record set is only locked when it is updated
		.LockType = 3
		
		'Open the thread table
		.Open strSQL, adoCon
		
		'If the record is returned update the topic ID
		If NOT .EOF Then
			
			.Fields("Topic_ID") = lngTopicID
		End If
		
		'Update the database
		.Update
		
		'Requery the db for slow old access
		.Requery
		
		'Clean up
		.Close
	
	
	
	
		'Check there are still topics in the old topic
		strSQL = "SELECT TOP 1 " & strDbTable & "Thread.Thread_ID FROM " & strDbTable & "Thread WHERE Topic_ID =" & lngOldTopicID & ";"
	
		'Open the thread table
		.Open strSQL, adoCon
		
	
		'See if there is a topic left in the old topic
		If .EOF Then
			'If there are no topics left then delete the old topic
			strSQL = "DELETE FROM " & strDbTable & "Topic WHERE Topic_ID=" & lngOldTopicID & ";"
	
			'Write to database
			adoCon.Execute(strSQL)
		End If
	
		'Close the recordset
		.Close
	End With

End If

'Reset main server variables
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<script language="JavaScript">
	window.opener.location.href = "forum_posts.asp?TID=<% = lngTopicID %>"
	window.close();
</script>
</head>
</html>