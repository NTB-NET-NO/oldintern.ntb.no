<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

Dim lngProfileNum		'Holds the profile number of the user we are getting the profile for
Dim strUsername			'Holds the users username
Dim intUsersGroupID		'Holds the users group ID
Dim strEmail			'Holds the new users e-mail address
Dim blnShowEmail		'Boolean set to true if the user wishes there e-mail address to be shown
Dim strLocation			'Holds the new users location
Dim strHomepage			'Holds the new users homepage if they have one
Dim strAvatar			'Holds the avatar image
Dim intForumID			'Holds the forum ID if within a forum
Dim strICQNum			'Holds the users ICQ Number
Dim strAIMAddress		'Holds the users AIM address
Dim strMSNAddress		'Holds the users MSN address
Dim strYahooAddress		'Holds the users Yahoo Address
Dim strOccupation		'Holds the users Occupation
Dim strInterests		'Holds the users Interests
Dim dtmJoined			'Holds the joined date
Dim lngNumOfPosts		'Holds the number of posts the user has made
Dim dtmDateOfBirth		'Holds the users Date Of Birth
Dim dtmLastVisit		'Holds the date the user last came to the forum
Dim strGroupName		'Holds the group name
Dim intRankStars 		'Holds the rank stars
Dim strRankCustomStars		'Holds the custom stars image if there is one
Dim blnProfileReturned		'Boolean set to false if the user's profile is not found in the database
Dim blnGuestUser		'Set to True if the user is a guest or not logged in
Dim blnActive			'Set to true of the users account is active
Dim strRealName			'Holds the persons real name
Dim strMemberTitle		'Holds the members title
Dim blnIsUserOnline		'Set to true if the user is online


'Initalise variables
blnProfileReturned = True
blnGuestUser = False
blnShowEmail = False
blnModerator = False
blnIsUserOnline = False
lngNumOfPosts = 0

'If the user is using a banned IP address then don't let the view a profile
If bannedIP() Then blnActiveMember = False

'Read in the profile number to get the details on
lngProfileNum = CLng(Request.QueryString("PF"))

'If the user has logged in then the Logged In User ID number will be more than 0
If intGroupID <> 2 Then


	'First see if the user is a in a moderator group for any forum
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Permissions.Moderate "
	strSQL = strSQL & "FROM " & strDbTable & "Permissions "
	If strDatabaseType = "SQLServer" Then
		strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = 1;"
	Else
		strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = True;"
	End If
	

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If a record is returned then the user is a moderator in one of the forums
	If NOT rsCommon.EOF Then blnModerator = True

	'Clean up
	rsCommon.Close


	'Read the various forums from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.*, " & strDbTable & "Group.Name, " & strDbTable & "Group.Stars, " & strDbTable & "Group.Custom_stars "
	strSQL = strSQL & "FROM " & strDbTable & "Author, " & strDbTable & "Group "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Group_ID = " & strDbTable & "Group.Group_ID AND " & strDbTable & "Author.Author_ID = " & lngProfileNum

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Read in the details if a profile is returned
	If NOT rsCommon.EOF Then

		'Read in the new user's profile from the recordset
		strUsername = rsCommon("Username")
		strRealName = rsCommon("Real_name")
		intUsersGroupID = CInt(rsCommon("Group_ID"))
		strEmail = rsCommon("Author_email")
		blnShowEmail = CBool(rsCommon("Show_email"))
		strHomepage = rsCommon("Homepage")
		strLocation = rsCommon("Location")
		strAvatar = rsCommon("Avatar")
		strMemberTitle = rsCommon("Avatar_title")
		strICQNum = rsCommon("ICQ")
		strAIMAddress = rsCommon("AIM")
		strMSNAddress = rsCommon("MSN")
		strYahooAddress = rsCommon("Yahoo")
		strOccupation = rsCommon("Occupation")
		strInterests = rsCommon("Interests")
		If isDate(rsCommon("DOB")) Then dtmDateOfBirth = CDate(rsCommon("DOB"))
		dtmJoined = CDate(rsCommon("Join_date"))
		lngNumOfPosts = CLng(rsCommon("No_of_posts"))
		dtmLastVisit = rsCommon("Last_visit")
		strGroupName = rsCommon("Name")
		intRankStars = CInt(rsCommon("Stars"))
		strRankCustomStars = rsCommon("Custom_stars")
		blnActive = CBool(rsCommon("Active"))

	'Else no profile is returned so set an error variable
	Else
		blnProfileReturned = False

	End If

	'Reset Server Objects
	rsCommon.Close
	
	'Clean up email link
	If strEmail <> "" Then
		strEmail = formatLink(strEmail)
		strEmail = formatInput(strEmail)
	End If
	
	
	If blnActiveUsers Then
		'Get the users online status
		For intArrayPass = 1 To UBound(saryActiveUsers, 2)
			If saryActiveUsers(1, intArrayPass) = lngProfileNum Then blnIsUserOnline = True
		Next
	End If


'Else the user is not logged in
Else
	'Set the Guest User boolean to true as the user must be a guest
	blnGuestUser = True

End If


%>
<html>
<head>
<title>Users Profile</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<script language="JavaScript">

//Function to open link in main window
function openInMainWin(winLocation){
	window.opener.location.href = winLocation
	window.opener.focus();
	window.close();
}
</script>

<!--#include file="includes/skin_file.asp" -->

</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<div align="center" class="heading"><% = strTxtProfile %></div><br />
<div align="center"><%

'If no profile can be found then display the appropriate message
If blnProfileReturned = False Then

	Response.Write (vbCrLf & "<span class=""text"">" & strTxtNoUserProfileFound & "</span>")

'If the user is a guest then tell them they must register or login before they can view other users profiles
ElseIf blnGuestUser = True OR blnActiveMember = False Then

	Response.Write (vbCrLf & "<span class=""text"">" & strTxtRegisteredToViewProfile)
	
	'If mem suspended display message
	If blnActiveMember = false and InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) Then
		Response.Write("<span class=""bold""><br /><br />" & strTxtForumMemberSuspended & "</span>")
	'Else account not yet active
	ElseIf blnActiveMember = false Then
		Response.Write("<span class=""bold""><br /><br />" & strTxtForumMembershipNotAct & "</span><br /><br />" & strTxtToActivateYourForumMem)
	End If
	
	Response.Write("</span><br /><br />")
Else

%><table width="550" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableProfileBorderColour %>" align="center">
  <tr>
    <td>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableProfileBgColour %>">
      <tr>
       <td bgcolor="<% = strTableProfileBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="<% = strTableProfileBgColour %>">
        <tr>
         <td colspan="2" background="<% = strTableProfileTitleBgImage %>" bgcolor="<% = strTableProfileTitleColour %>" class="tHeading"><% Response.Write(strTxtProfile & ": " & strUsername) %></td>
        </tr>
        <tr>
         <td width="25%" background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>"  align="right" class="text"><% = strTxtUsername %>:</td>
         <td width="75%" background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% = strUsername %></td>
        </tr><%

        'If the user has an avatar then display it
        If blnAvatar = True AND NOT strAvatar = "" Then
        	%>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text" valign="top"><% = strTxtAvatar %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% Response.Write("<img src=""" & strAvatar & """ width=""" & intAvatarWidth & """ height=""" & intAvatarHeight & """ alt=""" & strTxtAvatar & """ OnError=""this.src='avatars/blank.gif', height='0';"">") %>&nbsp;</td>
        </tr><%
	End If
	
	'If there is a member title display it
	If strMemberTitle <> "" Then
	%>
	<tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtMemberTitle %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% = strMemberTitle %></td>
        </tr><%
        
	End If
	%>
	<tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtGroup %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% = strGroupName %> <img src="<% 
         	If strRankCustomStars <> "" Then Response.Write(strRankCustomStars) Else Response.Write(strImagePath & intRankStars & "_star_rating.gif") 
		Response.Write(""" alt=""" & strGroupName & """ align=""absmiddle"">") %></td>
        </tr>
         <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtAccountStatus %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If blnActive = True Then Response.Write(strTxtActive) Else Response.Write(strTxtNotActive)%></td>
        </tr>
        </tr><%
        
        'If active users are enabled display if they are online or not
        If blnActiveUsers Then
        	%>
         <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtOnlineStatus %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If blnIsUserOnline = True Then Response.Write(strTxtOnLine2) Else Response.Write(strTxtOffLine)%></td>
        </tr><%
	End If
%>
        <tr>
         <td width="25%" background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>"  align="right" class="text"><% = strTxtRealName %>:</td>
         <td width="75%" background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strRealName <> "" Then Response.Write(strRealName) Else Response.Write(strTxtNotGiven) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtJoined %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% = DateFormat(dtmJoined, saryDateTimeData) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtLastVisit %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If isDate(dtmLastVisit) Then Response.Write(DateFormat(dtmLastVisit, saryDateTimeData)) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtPosts %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% = lngNumOfPosts %> <span class="smText"><% If lngNumOfPosts > 0 AND DateDiff("d", dtmJoined, Now()) > 0 Then Response.Write("[" & FormatNumber(lngNumOfPosts / DateDiff("d", dtmJoined, Now()), 2) & " " & strTxtPostsPerDay) & "]" %></span></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtLocation %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strLocation = "" Or isNull(strLocation) Then Response.Write(strTxtNotGiven) Else Response.Write(strLocation) %></td>
        </tr>
         <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtDateOfBirth %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% 
         
         'If there is a Date of Birth display it
         If isDate(dtmDateOfBirth) Then 
         	
        	 'As formatting the date also will add a time off set, make sure the date is correct
		If strTimeOffSet = "-" Then
			dtmDateOfBirth = DateAdd("h", + intTimeOffSet, dtmDateOfBirth)
		ElseIf strTimeOffSet = "+" Then
			dtmDateOfBirth = DateAdd("h", - intTimeOffSet, dtmDateOfBirth)
		End If
         	
         	'Display the persons Date of Birth
         	Response.Write(DateFormat(dtmDateOfBirth, saryDateTimeData)) 
         	
         Else 	
         	'Display that a Date of Birth was not given
         	Response.Write(strTxtNotGiven) 
         	
        End If%></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtHomepage %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strHomepage = "" OR IsNull(strHomepage) Then Response.Write(strTxtNotGiven) Else Response.Write("<a href=""" & strHomepage & """ target=""_blank"">" & strHomepage & "</a>") %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtOccupation %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strOccupation = "" OR IsNull(strOccupation) Then Response.Write(strTxtNotGiven) Else Response.Write(strOccupation) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtInterests %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strInterests = "" OR IsNull(strInterests) Then Response.Write(strTxtNotGiven) Else Response.Write(strInterests) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtEmail %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><%

        'If the user has choosen not to display there e-mail then this field will show private
	If blnShowEmail = False AND blnAdmin = False AND strEmail <> "" Then
        	Response.Write(strTxtPrivate)

        'If no password then display not given
        ElseIf strEmail = "" OR isNull(strEmail) Then
            	Response.Write(strTxtNotGiven)

        'If email address is shown and the email messenger of the forum is enabled show link button
        ElseIf blnEmailMessenger AND blnEmail Then

        	Response.Write("<a href=""javascript:openInMainWin('email_messenger.asp?SEID=" & lngProfileNum & "')""><img src=""" & strImagePath & "email_button.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtSendEmail & """></a>")

        'Else the user allows there e-mail address to be shown so show there e-mail address
        Else
            	Response.Write("<a href=""mailto:" & strEmail & """>" & strEmail & "</a>")
        End If


    %></td><%

    	'If the private messager is on show PM link
    	If blnPrivateMessages Then

    		%>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtPrivateMessage %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% Response.Write("<a href=""javascript:openInMainWin('pm_new_message_form.asp?name=" & Server.URLEncode(Replace(strUsername, "'", "\'",  1, -1, 1)) & "')""><img src=""" & strImagePath & "pm_icon.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtSendPrivateMessage & """></a>") %></td>
        </tr><%

	End If

%>
    	<tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtMSNMessenger %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strMSNAddress <> "" Then Response.Write(strMSNAddress) Else Response.Write(strTxtNotGiven) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtAIMAddress %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strAIMAddress <> "" Then Response.Write("<a href=""javascript:openInMainWin('aim:goim?screenname=" & Server.URLEncode(strAIMAddress) & "&message=Hello+Are+you+there?')""><img src=""" & strImagePath & "aol.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtAIMAddress & """></a>") Else Response.Write(strTxtNotGiven) %></td>
        </tr>
        <tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtYahooMessenger %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strYahooAddress <> "" Then Response.Write("<a href=""javascript:openInMainWin('http://edit.yahoo.com/config/send_webmesg?.target=" & Server.URLEncode(strYahooAddress) & "&amp;.src=pg')""><img src=""" & strImagePath & "yim.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtYahooMessenger & """></a>") Else Response.Write(strTxtNotGiven) %></td>
        </tr>
    	<tr>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" align="right" class="text"><% = strTxtICQNumber %>:</td>
         <td background="<% = strTableProfileBgImage %>" bgcolor="<% =(strTableRowProfileColour) %>" class="text"><% If strICQNum <> "" Then Response.Write("<a href=""javascript:openInMainWin('http://wwp.icq.com/scripts/search.dll?to=" & strICQNum & "')""><img src=""" & strImagePath & "icq.gif""  border=""0"" align=""absmiddle"" alt=""" & strTxtICQNumber & """></a>") Else Response.Write(strTxtNotGiven) %></td>
        </tr>
       </table></td>
      </tr>
     </table></td>
  </tr>
 </table><%

	'If the user is an admin or a moderator give them the chance to edit the profile unless it's the main admin account of the guest account
	If blnAdmin OR blnModerator Then

%><br />
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
   <form>
    <tr>
      <td align="center"><input type="submit" name="Submit" value="<% = strTxtEditMembersSettings %>" onClick="openInMainWin('member_control_panel.asp?PF=<% = lngProfileNum %>&M=A');" /></td>
    </tr>
    </form>
  </table><%
	End If

End If


'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
  <br />
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
  <td align="center" height="34">
   <a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a>
   <br /><br /><%
   
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</td>
 </tr>
</table>
</div>
</body>
</html>