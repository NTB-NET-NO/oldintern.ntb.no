<!--#include file="language_files/language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the timeout of the forum
Server.ScriptTimeout = 90
Session.Timeout = 20

'Set the date time format to your own if you are getting a CDATE error
Session.LCID = 1044

Dim adoCon 			'Database Connection Variable Object
Dim strCon			'Holds the string to connect to the db
Dim rsCommon			'Holds the configuartion recordset
Dim strSQL			'Holds the SQL query for the database
Dim lngLoggedInUserID		'Holds a logged in users ID number
Dim strLoggedInUsername		'Holds a logged in users username
Dim intGroupID			'Holds the group ID number the member is a group of
Dim strWebsiteName		'Holds the website name
Dim strMainForumName		'Holds the forum name
Dim strForumPath		'Holds the virtual path to the forum
Dim strForumEmailAddress	'Holds the forum e-mail address
Dim blnTextLinks		'Set to true if you want text links instead of the powered by logo
Dim blnRTEEditor		'Set to true if the Rich Text Editor(RTE) is enabled
Dim blnEmail			'Boolean set to true if e-mail is on
Dim strMailComponent		'Email coponent the forum useses
Dim strOutgoingMailServer	'Forums incomming mail server
Dim strLoggedInUserCode		'Holds the user code of the user
Dim blnLCode			'set to true
Dim blnAdmin			'set to true if the user is a forum admininstrator (Group ID 1)
Dim blnModerator		'Set to true if the user is a forum moderator
Dim blnGuest			'set to true for the Guest account (Group ID 2)
Dim blnActiveMember		'Set to false if the member is no longer allowed to post messages on the forum
Dim blnLoggedInUserEmail	'Set to true if the user has entered there e-mail
Dim blnLoggedInUserSignature	'set to true if the user has enetered a signature
Dim intTopicPerPage		'Holds the number of topics to show on each page
Dim strTitleImage		'Holds the path and name for the title image for the forum
Dim blnEmoticons		'Set to true if emoticons are turned on
Dim strDatabaseDateFunction	'Holds a different date function for Access or SQL server
Dim strDatabaseType		'Holds the type of database used
Dim blnGuestPost		'Set to true if guests can post
Dim blnAvatar			'Set to true if the forum can use avatars
Dim blnEmailActivation		'Set to true if the e-mail activation is turned on
Dim blnSendPost			'Set to true if post is sent with e-mail notification
Dim intNumHotViews		'Holds the number of how many views a topic has before it becomes a hot topic
Dim intNumHotReplies		'Holds the number of replies before a topic becomes a hot topic
Dim blnPrivateMessages		'Set to true if private messages are allowed
Dim intNumPrivateMessages	'Holds the number of private messages allowed by each user
Dim intThreadsPerPage		'Holds the number of threads shown per page
Dim strDbPathAndName		'Holds the path and name of the database
Dim intSpamTimeLimitSeconds	'Holds the number of secounds between posts
Dim intSpamTimeLimitMinutes	'Holds the number of minutes the user can post five posts in
Dim strDateFormat		'Holds the users date format
Dim strTimeOffSet		'Holds the users time offset in + or -
Dim intTimeOffSet		'Holds the users time offset
Dim blnReplyNotify		'Set to true if the user wants to be notified of replies to posts
Dim blnAttachSignature		'Set to true if the user always wants there signature attached
Dim blnWYSIWYGEditor		'Set to true if the user wants to use the IE WYSIWYG post editor
Dim intMaxPollChoices		'Holds the maximum allowed number of poll choices
Dim blnEmailMessenger		'Set to true if the email system is on
Dim blnActiveUsers		'Set to true if active users is enabled
Dim blnForumClosed		'Set to true of the forum is cloded for maintence
Dim blnShowEditUser		'Set to true if we are to show the username and time a post is edited
Dim blnShowProcessTime		'Set to true if we are to show how long the page took to be processed on the server
Dim dblStartTime		'Holds the start time for the page process
Dim blnClosedForumPage		'Set to true if we are looking at the closed forum page
Dim blnFlashFiles		'Set to true if Flash support is enabled
Dim strWebsiteURL 		'Holds the URL to the sites homepage
Dim blnShowMod			'Set to true if mod groups are shown on the main forum page
Dim blnAvatarUploadEnabled	'Set to true if avatars are enabled
Dim blnRegistrationSuspeneded	'Set to true if new registrations are suspended
Dim strLoggedInUserEmail	'Holds the logged in users email address
Dim strImageTypes		'Holds the types of images allowed in the forum
Dim blnLongRegForm		'Set to true if the reg form is to be the long version
Dim blnLongSecurityCode		'Set to true if the security code feature is required when logging in
Dim blnUseApplicationVariables	'Set to true if application varibles are used

'These are used for forum permissions
Dim blnRead
Dim blnPost
Dim blnReply
Dim blnEdit
Dim blnDelete
Dim blnPriority
Dim blnPollCreate
Dim blnVote
Dim blnAttachments
Dim blnImageUpload


'Initialise variables
Const strVersion = "7.8"
Const strRTEversion = "1.2c"
lngLoggedInUserID = 0
strLoggedInUsername = strTxtGuest
blnActiveMember = true
blnLoggedInUserEmail = false
blnLoggedInUserSignature = false
intGroupID = 2
lngLoggedInUserID = 2
blnAdmin = false
blnModerator = false
blnGuest = true
intTimeOffSet = 0
strTimeOffSet = "+"
blnWYSIWYGEditor = true
blnLongRegForm = true
blnLongSecurityCode = false




'Database Type
'strDatabaseType = "Access"
strDatabaseType = "SQLServer"

'Set up the database table name prefix and stored procedure prefix
'(This is useful if you are running multiple forums from one database)
' - make sure you also change this in the msSQL_server_setup.asp file if setting up an ms SQL server database)
Const strDbTable = "tbl"
Const strDbProc = "wwfSp"


'Set up the forum cookie name
'(This is useful if you run multiple copies of Web Wiz Forums on the same site so that cookies don't interfer with each other)
Const strCookieName = "WWF"


'Set if application variables are used for forum configuration
'This will make your forum run faster as there are less hits on the database, but if you are using free web hosting or
'are on a server where you share your application oject with others then you will need to set this to false
blnUseApplicationVariables = true



'Set Encrypted passwords (ignore unless you don't wish to use Encrypted passwords in your forum)
'This will make your forum unsecure from hackers if you disable this!!!!!
'This can NOT be changed once your forum is in use!!!
'If you do disable Encrypted Passwords - You will also need to directly edit the database to type in the admin password to the Password field in the tblAuthor table at record position 1 also edit both common.asp files to change this variable
Const blnEncryptedPasswords = true 'True = Encrypted Passwords Enabled  -  Flase = Encrypted Passwords Disabled




'Create database connection
'Create a connection odject
Set adoCon = Server.CreateObject("ADODB.Connection")

'If this is access set the access driver
If strDatabaseType = "Access" Then



	'--------------------- Set the path and name of the database --------------------------------------------------------------------------------

	'Virtual path to database
	strDbPathAndName = Server.MapPath("admin/database/wwForum.mdb")  'This is the path of the database from this files location on the server

	'Physical path to database
	'strDbPathAndName = "" 'Use this if you use the physical server path, eg:- C:\Inetpub\private\wwForum.mdb



	'BRINKSTER USERS (Web Wiz Forums only works with free Brinkster accounts, not for the paid accounts)
	'Brinkster users remove the ' single quote mark from infront of the line below and replace USERNAME with your Brinkster uersname

	'strDbPathAndName =  Server.MapPath("/USERNAME/db/wwForum.mdb")

	'PLEASE NOTE: - For extra security it is highly recommended you change the name of the database, wwForum.mdb, to another name and then
	'replace the wwForum.mdb found above with the name you changed the forum database to.

	'---------------------------------------------------------------------------------------------------------------------------------------------





	'------------- If you are having problems with the script then try using a diffrent driver or DSN by editing the lines below --------------

	'Generic MS Access Database connection info and driver (if this driver does not work then comment it out and use one of the alternative faster JET OLE DB drivers)
	'strCon = "DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=" & strDbPathAndName

	'Alternative drivers faster than the generic one above
	'strCon = "Provider=Microsoft.Jet.OLEDB.3.51; Data Source=" & strDbPathAndName 'This one is if you convert the database to Access 97
	strCon = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strDbPathAndName  'This one is for Access 2000/2002

	'If you wish to use DSN then comment out the driver above and uncomment the line below (DSN is slower than the above drivers)
	'strCon = "DSN=DSN_NAME" 'Place the DSN where you see DSN_NAME

	'---------------------------------------------------------------------------------------------------------------------------------------------





	'The now() function is used in Access for dates
	strDatabaseDateFunction = "Now()"


'Else set the MS SQL server stuff
Else

	%><!--#include file="admin/SQL_server_connection.asp" --><%

	'The GetDate() function is used in SQL Server
	strDatabaseDateFunction = "GetDate()"
End If


'Set the connection string to the database
adoCon.connectionstring = strCon

'Set an active connection to the Connection object
adoCon.Open






'Intialise the main ADO recordset object
Set rsCommon = Server.CreateObject("ADODB.Recordset")



'Read in the Forum configuration
If isEmpty(Application("blnConfigurationSet")) OR isNull(Application("blnConfigurationSet")) OR Application("blnConfigurationSet") = false OR blnUseApplicationVariables = false Then

	'Initialise the SQL variable with an SQL statement to get the configuration details from the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "SelectConfiguration"
	Else
		strSQL = "SELECT TOP 1 " & strDbTable & "Configuration.* From " & strDbTable & "Configuration;"
	End If

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there is config deatils in the recordset then read them in
	If NOT rsCommon.EOF Then

		'read in the configuration details from the recordset
		strWebsiteName = rsCommon("website_name")
		strMainForumName = rsCommon("forum_name")
		strWebsiteURL = rsCommon("website_path")
		strForumPath = rsCommon("forum_path")
		strMailComponent = rsCommon("mail_component")
		strOutgoingMailServer = rsCommon("mail_server")
		strForumEmailAddress = rsCommon("forum_email_address")
		blnLCode = CBool(rsCommon("L_Code"))
		blnEmail = CBool(rsCommon("email_notify"))
		blnTextLinks = rsCommon("Text_link")
		blnRTEEditor = CBool(rsCommon("IE_editor"))
		intTopicPerPage = CInt(rsCommon("Topics_per_page"))
		strTitleImage = rsCommon("Title_image")
		blnEmoticons = CBool(rsCommon("Emoticons"))
		blnAvatar = CBool(rsCommon("Avatar"))
		blnEmailActivation = CBool(rsCommon("Email_activate"))
	 	intNumHotViews = CInt(rsCommon("Hot_views"))
		intNumHotReplies = CInt(rsCommon("Hot_replies"))
		blnSendPost = CBool(rsCommon("Email_post"))
		blnPrivateMessages = CBool(rsCommon("Private_msg"))
		intNumPrivateMessages = CInt(rsCommon("No_of_priavte_msg"))
		intThreadsPerPage = CInt(rsCommon("Threads_per_page"))
		intSpamTimeLimitSeconds = CInt(rsCommon("Spam_seconds"))
		intSpamTimeLimitMinutes = CInt(rsCommon("Spam_minutes"))
		intMaxPollChoices = CInt(rsCommon("Vote_choices"))
		blnEmailMessenger = CBool(rsCommon("Email_sys"))
		blnActiveUsers = CBool(rsCommon("Active_users"))
		blnForumClosed = CBool(rsCommon("Forums_closed"))
		blnShowEditUser = CBool(rsCommon("Show_edit"))
		blnShowProcessTime = CBool(rsCommon("Process_time"))
		blnFlashFiles = CBool(rsCommon("Flash"))
		blnShowMod = CBool(rsCommon("Show_mod"))
		blnAvatarUploadEnabled = CBool(rsCommon("Upload_avatar"))
		blnRegistrationSuspeneded = CBool(rsCommon("Reg_closed"))
		strImageTypes = rsCommon("Upload_img_types")


		'If we are using application level variables the configuration into the application level variables
		If blnUseApplicationVariables Then

			'Lock the application so only one user updates it at a time
			Application.Lock

			'read in the configuration details from the recordset
			Application("strWebsiteName") = strWebsiteName
			Application("strMainForumName") = strMainForumName
			Application("strWebsiteURL") = strWebsiteURL
			Application("strForumPath") = strForumPath
			Application("strMailComponent") = strMailComponent
			Application("strOutgoingMailServer") = strOutgoingMailServer
			Application("strForumEmailAddress") = strForumEmailAddress
			Application("blnLCode") = CBool(blnLCode)
			Application("blnEmail") = CBool(blnEmail)
			Application("blnTextLinks") = blnTextLinks
			Application("blnRTEEditor") = CBool(blnRTEEditor)
			Application("intTopicPerPage") = CInt(intTopicPerPage)
			Application("strTitleImage") = strTitleImage
			Application("blnEmoticons") = CBool(blnEmoticons)
			Application("blnAvatar") = CBool(blnAvatar)
			Application("blnEmailActivation") = CBool(blnEmailActivation)
		 	Application("intNumHotViews") = CInt(intNumHotViews)
			Application("intNumHotReplies") = CInt(intNumHotReplies)
			Application("blnSendPost") = CBool(blnSendPost)
			Application("blnPrivateMessages") = CBool(blnPrivateMessages)
			Application("intNumPrivateMessages") = CInt(intNumPrivateMessages)
			Application("intThreadsPerPage") = CInt(intThreadsPerPage)
			Application("intSpamTimeLimitSeconds") = CInt(intSpamTimeLimitSeconds)
			Application("intSpamTimeLimitMinutes") = CInt(intSpamTimeLimitMinutes)
			Application("intMaxPollChoices") = CInt(intMaxPollChoices)
			Application("blnEmailMessenger") = CBool(blnEmailMessenger)
			Application("blnActiveUsers") = CBool(blnActiveUsers)
			Application("blnForumClosed") = CBool(blnForumClosed)
			Application("blnShowEditUser") = CBool(blnShowEditUser)
			Application("blnShowProcessTime") = CBool(blnShowProcessTime)
			Application("blnFlashFiles") = CBool(blnFlashFiles)
			Application("blnShowMod") = CBool(blnShowMod)
			Application("blnAvatarUploadEnabled") = CBool(blnAvatarUploadEnabled)
			Application("blnRegistrationSuspeneded") = CBool(blnRegistrationSuspeneded)
			Application("strImageTypes") = strImageTypes

			'Set the configuartion set application variable to true
			Application("blnConfigurationSet") = True

			'Unlock the application
			Application.UnLock

		End If


	End If

	'Close the recordset
	rsCommon.Close


'If we are using application level variables for the forum configuration then load in the variables from the application variables.
ElseIf blnUseApplicationVariables Then

		'read in the configuration details from the application varaibales
		strWebsiteName = Application("strWebsiteName")
		strMainForumName = Application("strMainForumName")
		strWebsiteURL = Application("strWebsiteURL")
		strForumPath = Application("strForumPath")
		strMailComponent = Application("strMailComponent")
		strOutgoingMailServer = Application("strOutgoingMailServer")
		strForumEmailAddress = Application("strForumEmailAddress")
		blnLCode = CBool(Application("blnLCode"))
		blnEmail = CBool(Application("blnEmail"))
		blnTextLinks = Application("blnTextLinks")
		blnRTEEditor = CBool(Application("blnRTEEditor"))
		intTopicPerPage = CInt(Application("intTopicPerPage"))
		strTitleImage = Application("strTitleImage")
		blnEmoticons = CBool(Application("blnEmoticons"))
		blnAvatar = CBool(Application("blnAvatar"))
		blnEmailActivation = CBool(Application("blnEmailActivation"))
	 	intNumHotViews = CInt(Application("intNumHotViews"))
		intNumHotReplies = CInt(Application("intNumHotReplies"))
		blnSendPost = CBool(Application("blnSendPost"))
		blnPrivateMessages = CBool(Application("blnPrivateMessages"))
		intNumPrivateMessages = CInt(Application("intNumPrivateMessages"))
		intThreadsPerPage = CInt(Application("intThreadsPerPage"))
		intSpamTimeLimitSeconds = CInt(Application("intSpamTimeLimitSeconds"))
		intSpamTimeLimitMinutes = CInt(Application("intSpamTimeLimitMinutes"))
		intMaxPollChoices = CInt(Application("intMaxPollChoices"))
		blnEmailMessenger = CBool(Application("blnEmailMessenger"))
		blnActiveUsers = CBool(Application("blnActiveUsers"))
		blnForumClosed = CBool(Application("blnForumClosed"))
		blnShowEditUser = CBool(Application("blnShowEditUser"))
		blnShowProcessTime = CBool(Application("blnShowProcessTime"))
		blnFlashFiles = CBool(Application("blnFlashFiles"))
		blnShowMod = CBool(Application("blnShowMod"))
		blnAvatarUploadEnabled = CBool(Application("blnAvatarUploadEnabled"))
		blnRegistrationSuspeneded = CBool(Application("blnRegistrationSuspeneded"))
		strImageTypes = Application("strImageTypes")
End If



'If the forums are closed redirect to the forums closed page
If blnForumClosed AND blnClosedForumPage = False Then

	'Reset server objects
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect to the forum closed page
	Response.Redirect("forum_closed.asp")
End If


'Initalise the process start time
If blnShowProcessTime Then dblStartTime = Timer()


'Set a cookie with the last date/time the user used the forum to calculate if there any new posts
'If the date/time the user was last here is 20 minutes since the last visit then set the session variable to the users last date they were here
If Session("dtmLastVisit") = "" AND Request.Cookies(strCookieName)("LTVST") <> "" Then

	Session("dtmLastVisit") = CDate(Request.Cookies(strCookieName)("LTVST"))
	Response.Cookies(strCookieName)("LTVST") = CDbl(Now())
	Response.Cookies(strCookieName).Expires = DateAdd("yyyy", 1, Now())

'If the last entry date is not alreay set set it to now
ElseIf Session("dtmLastVisit") = "" Then
	Session("dtmLastVisit") = Now()
End If


'If the cookie is older than 5 mintues set a new one
If isNumeric(Request.Cookies(strCookieName)("LTVST")) Then

	If CDate(Request.Cookies(strCookieName)("LTVST")) < DateAdd("n", -5, Now()) Then
		Response.Cookies(strCookieName)("LTVST") = CDbl(Now())
		Response.Cookies(strCookieName).Expires = DateAdd("yyyy", 1, Now())
	End If

'If there is no date in the cookie or it is empty then set the date to now()
Else
	Response.Cookies(strCookieName)("LTVST") = CDbl(Now())
	Response.Cookies(strCookieName).Expires = DateAdd("yyyy", 1, Now())
End If




'If someone has placed the default.asp in the path to the forum then remove it as it's not needed
strForumPath = Replace(strForumPath, "default.asp", "")



'Read in users ID number from the cookie
strLoggedInUserCode = Trim(Mid(Request.Cookies(strCookieName)("UID"), 1, 44))

'If a cookie exsists on the users system then read in there username from the database
If strLoggedInUserCode <> "" Then

	'Make the usercode SQL safe
	strLoggedInUserCode = formatSQLInput(strLoggedInUserCode)

	'Initalise the strSQL variable with an SQL statement to query the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "ChkUserID @strUserID = '" & strLoggedInUserCode & "'"
	Else
		strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Group_ID, " & strDbTable & "Author.Active, " & strDbTable & "Author.Signature, " & strDbTable & "Author.Author_email, " & strDbTable & "Author.Date_format, " & strDbTable & "Author.Time_offset, " & strDbTable & "Author.Time_offset_hours, " & strDbTable & "Author.Reply_notify, " & strDbTable & "Author.Attach_signature, " & strDbTable & "Author.Rich_editor, " & strDbTable & "Author.Last_visit "
		strSQL = strSQL & "FROM " & strDbTable & "Author "
		strSQL = strSQL & "WHERE " & strDbTable & "Author.User_code = '" & strLoggedInUserCode & "';"
	End If

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If the database has returned a record then run next bit
	If NOT rsCommon.EOF Then

		'Read in the users details from the recordset
		strLoggedInUsername = rsCommon("Username")
		intGroupID  = rsCommon("Group_ID")
		lngLoggedInUserID = CLng(rsCommon("Author_ID"))
		blnActiveMember = CBool(rsCommon("Active"))
		strDateFormat = rsCommon("Date_format")
		strTimeOffSet = rsCommon("Time_offset")
		intTimeOffSet = CInt(rsCommon("Time_offset_hours"))
		blnReplyNotify = CBool(rsCommon("Reply_notify"))
		blnAttachSignature = CBool(rsCommon("Attach_signature"))
		blnWYSIWYGEditor = CBool(rsCommon("Rich_editor"))
		strLoggedInUserEmail = rsCommon("Author_Email")
		If rsCommon("Signature") <> Trim("") Then blnLoggedInUserSignature = True

		'See if the user has entered an email address
		If strLoggedInUserEmail <> Trim("") Then blnLoggedInUserEmail = True



		'Read in the Last Visit Date for the user from the db if we haven't already
		If Session("ViRead") = "" Then
			If isDate(rsCommon("Last_visit")) Then Session("dtmLastVisit") = CDate(rsCommon("Last_visit"))
			Session("ViRead") = True
		End If

		'Check that there is a last visit date in the db or we will get an error
		If isDate(rsCommon("Last_visit")) Then

			'If the Last Visit date in the db is older than 5 minutes for the user then update it
			If CDate(rsCommon("Last_visit")) < DateAdd("n", -5, Now()) Then

				'Initilse sql statement
			 	If strDatabaseType = "SQLServer" Then
					strSQL = "EXECUTE " & strDbProc & "UpdateLasVisit @lngUserID = " & lngLoggedInUserID
				Else
					strSQL = "UPDATE " & strDbTable & "Author SET " & strDbTable & "Author.Last_visit = Now() WHERE " & strDbTable & "Author.Author_ID=" & lngLoggedInUserID & ";"
				End If

				'Write to database
				adoCon.Execute(strSQL)

			End If

		'Else there is no date already in db for the last time this visitor came to the site so update db
		Else

			'Initilse sql statement
			 If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "UpdateLasVisit @lngUserID = " & lngLoggedInUserID
			Else
				strSQL = "UPDATE " & strDbTable & "Author SET " & strDbTable & "Author.Last_visit=Now() WHERE " & strDbTable & "Author.Author_ID=" & lngLoggedInUserID & ";"
			End If

			'Write to database
			adoCon.Execute(strSQL)

		End If

		'If the members account is not active then set there group to 2 (Guest Group)
		If blnActiveMember = False Then intGroupID = 2

		'Set the Guest boolean to false
		blnGuest = False
	End If

	'Clean up
	rsCommon.Close
End If

'Make sure the main admin account remains active and full access rights and in the admin group
If lngLoggedInUserID = 1 Then
	intGroupID = 1
	blnActiveMember = True
End If

'If in the admin group set the admin boolean to true
If intGroupID = 1 Then blnAdmin = True


'If active users is on update the table
If blnActiveUsers Then

	%><!--#include file="includes/active_users_inc.asp" --><%
End If

%>
<!--#include file="functions/functions_common.asp" -->
<!--#include file="functions/functions_filters.asp" -->