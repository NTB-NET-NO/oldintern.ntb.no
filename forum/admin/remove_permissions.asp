<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim intUserGroupID	'Holds the ID number of the group
Dim intForumID		'Holds the forum ID number
Dim lngMemberID		'Holds the member ID number
Dim strMode		'Hols the page mode


'Read in the details
intForumID = CInt(Request("FID"))
intUserGroupID = CInt(Request("GID"))
lngMemberID = CLng(Request("UID"))
strMode = Request("M")


'If UP - User Permission only dfelete the user permimissons
If strMode = "UP" Then
	'Delete the user permissions for forums form the database
	strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE " & strDbTable & "Permissions.Forum_ID=" & intForumID & " AND " & strDbTable & "Permissions.Author_ID = " & lngMemberID & ";"

'Else User Group permisions
Else
	'Delete the group permissions for forums form the database
	strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE " & strDbTable & "Permissions.Forum_ID=" & intForumID & " AND " & strDbTable & "Permissions.Group_ID = " & intUserGroupID & ";"
End If

'Write to database
adoCon.Execute(strSQL)
	



'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

If strMode = "UP" Then
	'Return to the forum user permisisons
	Response.Redirect("forum_user_permissions.asp?UID=" & lngMemberID)
Else
	'Return to the forum group permisisons
	Response.Redirect("forum_group_permissions.asp?GID=" & intUserGroupID)
End If
%>