<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Declare variables
Dim intNumOfNewPM	'Holds the number of new pm's
Dim lngLastPMID		'Holds the ID of the last PM
Dim lngLastNotifiedPMID	'Holds the ID stored in the cookie of the last PM ID the user was notified of

'If the user is logged in and there account is active display if they have private messages
If intGroupID <> 2 AND blnActiveMember AND blnPrivateMessages Then

	'Initlise the sql statement
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "CountOfPMs @lngLoggedInUserID = " & lngLoggedInUserID
	Else
		strSQL = "SELECT Count(" & strDbTable & "PMMessage.PM_ID) AS CountOfPM FROM " & strDbTable & "PMMessage "
		strSQL = strSQL & "WHERE " & strDbTable & "PMMessage.Read_Post = 0 AND " & strDbTable & "PMMessage.Author_ID = " & lngLoggedInUserID & ";"
	End If

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there are no new messages then display so
	If CLng(rsCommon("CountOfPM")) = 0 Then

	 	Response.Write("<a href=""pm_welcome.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "read_private_message_icon.gif"" align=""absmiddle"" alt=" & strTxtReadMessage & " border=""0"">" & strTxtPrivateMessenger & "</a>")

	'Else there are new pm's so display how many
	Else
		'Get the number of new pm's this user has
		intNumOfNewPM = CInt(rsCommon("CountOfPM"))

		'Display the number of new pm's
		Response.Write("<a href=""pm_inbox.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "unread_private_message_icon.gif"" align=""absmiddle"" alt=" & strTxtUnreadMessage & "  border=""0"">" & strTxtPrivateMessenger & "</a><span class=""smText""> (" & intNumOfNewPM & " " & strTxtNew & ")</span>")

		'Close the recodset
		rsCommon.Close

		'Now get the date of the last PM
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "DateOfLastUnReadPM @lngLoggedInUserID = " & lngLoggedInUserID
		Else
			strSQL = "SELECT TOP 1 " & strDbTable & "PMMessage.PM_ID FROM " & strDbTable & "PMMessage "
			strSQL = strSQL & "WHERE " & strDbTable & "PMMessage.Read_Post = 0 AND " & strDbTable & "PMMessage.Author_ID = " & lngLoggedInUserID & " "
			strSQL = strSQL & "ORDER BY PM_ID DESC;"
		End If

		'Query the database
		rsCommon.Open strSQL, adoCon

		'Get the ID of the last PM
		lngLastPMID = CLng(rsCommon("PM_ID"))

		'Get the ID from the cookie of the last PM the user was notified of
		If isNumeric(Request.Cookies("LPM")) Then lngLastNotifiedPMID = CLng(Request.Cookies("LPM"))

		'Display alert box if there are new pm's unless they aready have been told
		If lngLastNotifiedPMID = Null OR lngLastPMID > lngLastNotifiedPMID Then

			'Display the alert
			Response.Write("<script language=""JavaScript""><!-- ")
			Response.Write(vbCrLf & vbCrLf & "//Save cookie with last PM date")
			Response.Write(vbCrLf & "document.cookie = 'LPM=" & lngLastPMID & "'")
			Response.Write(vbCrLf & vbCrLf & "//Display pop for new private message")
			Response.Write(vbCrLf & "checkPrivateMsg = confirm('" & strTxtYouHave & " " & intNumOfNewPM & " " & strTxtNewPMsClickToGoNowToPM & "')")
			Response.Write(vbCrLf & "if (checkPrivateMsg == true) {")
			Response.Write(vbCrLf & "	window.location='pm_inbox.asp'")
			Response.Write(vbCrLf & "}")
			Response.Write(vbCrLf & "// --></script>")
		End If
	End If

	'Relese server objects
	rsCommon.Close
End If
%>