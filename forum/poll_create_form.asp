<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="functions/functions_edit_post.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Dimension variables
Dim strMode			'Holds the mode of the page
Dim intForumID			'Holds the forum ID number
Dim lngTopicID			'Holds the Topic ID number
Dim lngMessageID		'Holds the Thread ID of the post
Dim strForumName		'Holds the name of the forum
Dim blnForumLocked		'Set to true if the forum is locked
Dim intTopicPriority		'Holds the priority of the topic
Dim strPostPage 		'Holds the page the form is posted to
Dim intRecordPositionPageNum	'Holds the recorset page number to show the Threads for
Dim strMessage			'Holds the post message
Dim intPollLoopCounter		'Holds the poll loop counter
Dim intIndexPosition		'Holds the index poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If

'Intialise variables
lngTopicID = 0
lngMessageID = 0
intTopicPriority = 0
intRecordPositionPageNum = 1
strMode = "poll"


'Read in the forum and topic ID number and mode
intForumID = CInt(Request.QueryString("FID"))


'Read in the forum name and forum permissions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon


'If there is a record returned by the recordset then check to see if you need a password to enter it
If NOT rsCommon.EOF Then
	
	'Read in forum details from the database
	strForumName = rsCommon("Forum_name")
	
	'Read in wether the forum is locked or not
	blnForumLocked = CBool(rsCommon("Locked"))
	
	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsCommon("Read")), CInt(rsCommon("Post")), CInt(rsCommon("Reply_posts")), CInt(rsCommon("Edit_posts")), CInt(rsCommon("Delete_posts")), CInt(rsCommon("Priority_posts")), CInt(rsCommon("Poll_create")), CInt(rsCommon("Vote")), CInt(rsCommon("Attachments")), CInt(rsCommon("Image_upload")))
	
	'If the forum requires a password and a logged in forum code is not found on the users machine then send them to a login page
	If NOT rsCommon("Password") = "" and NOT Request.Cookies(strCookieName)("Forum" & intForumID) = rsCommon("Forum_code") Then
		
		'Reset Server Objects
		rsCommon.Close
		Set rsCommon = Nothing 
		Set rsCommon = Nothing
		adoCon.Close
		Set adoCon = Nothing		
		
		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "forum_password_form.asp?FID=" & intForumID
	End If
End If

'Reset server object
rsCommon.Close

'If the forum level for the user on this forum is read only set the forum to be locked
If (blnRead = False AND blnModerator = False AND blnAdmin = False) Then blnForumLocked = True

%>
<html> 
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Create New Poll</title>

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001
    	 If you want your forum then goto http://www.webwizforums.com --> 

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {
	
	var errorMsg = "";

<%
'If Gecko Madis API (RTE) need to strip default input from the API
If RTEenabled = "Gecko" Then Response.Write("	//For Gecko Madis API (RTE)" & vbCrLf & "	if (document.frmAddMessage.message.value.indexOf('<br>') > -1 && document.frmAddMessage.message.value.length == 8) document.frmAddMessage.message.value = '';" & vbCrLf)


'If this is a guest posting check that they have entered their name
If blnPost And lngLoggedInUserID = 2 Then
%>	
	//Check for a name
	if (document.frmAddMessage.Gname.value==""){
		errorMsg += "\n\t<% = strTxtNoNameError %>";
	}
<%
End If	

%>
	//Check for a subject
	if (document.frmAddMessage.subject.value==""){
		errorMsg += "\n\t<% = strTxtErrorTopicSubject %>";
	}
	
	//Check for poll question
	if (document.frmAddMessage.pollQuestion.value==""){
		errorMsg += "\n\t<% = strTxtErrorPollQuestion %>";
	}
	
	//Check for poll at least two poll choices
	if ((document.frmAddMessage.choice1.value=="") || (document.frmAddMessage.choice2.value=="")){
		errorMsg += "\n\t<% = strTxtErrorPollChoice %>";
	}
	
	//Check for message
	if (document.frmAddMessage.message.value==""){
		errorMsg += "\n\t<% = strTxtNoMessageError %>";
	}
	
	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
		
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
	
	//Reset the submition page back to it's original place
	document.frmAddMessage.action = "post_message.asp?PN=<% = CInt(Request.QueryString("PN")) %>"
	document.frmAddMessage.target = "_self";
<% 
If RTEenabled() <> "false" AND blnRTEEditor AND blnWYSIWYGEditor Then Response.Write(vbCrLf & "	document.frmAddMessage.Submit.disabled=true;")
%> 
	
	return true;
}
// -->
</script>

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr> 
  <td align="left" class="heading"><% = strTxtCreateNewPoll %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% Response.Write ("<a href=""forum_topics.asp?FID=" & intForumID & """ target=""_self"" class=""boldLink"">" & strForumName & "</a>" & strNavSpacer) %><% = strTxtCreateNewPoll %><br /></td>
  </tr>
</table><br /><%


'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

 
 
'If the user has logged in and allowed to create polls then display the from to allow the user to post a new message
If blnPollCreate = True AND blnActiveMember = True AND (blnForumLocked = False OR blnAdmin = True) Then

	'See if the users browser is RTE enabled
	If RTEenabled() <> "false" AND blnRTEEditor = True AND blnWYSIWYGEditor = True Then
					
		'Open the message form for RTE enabled browsers
		%><!--#include file="includes/RTE_message_form_inc.asp" --><%
	Else
		'Open up the mesage form for non RTE enabled browsers
		%><!--#include file="includes/message_form_inc.asp" --><%
	End If

'If the users account is suspended then let them know
ElseIf blnActiveMember = False Then
		
	Response.Write (vbCrLf & "<div align=""center""><br /><br /><span class=""text"">")
	
	'If mem suspended display message
	If  InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) Then
		Response.Write(strTxtForumMemberSuspended)
	'Else account not yet active
	Else
		Response.Write("<span class=""lgText"">" & strTxtForumMembershipNotAct & "</span><br /><br />" & strTxtToActivateYourForumMem)
	End If
	'If email is on then place a re-send activation email link
	If InStr(1, strLoggedInUserCode, "N0act", vbTextCompare) = False AND blnEmailActivation AND blnLoggedInUserEmail Then Response.Write("<br /><br /><a href=""JavaScript:openWin('resend_email_activation.asp','actMail','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=475,height=200')"">" & strTxtResendActivationEmail & "</a>")
	
	Response.Write("</span><br /><br /><br /><br /></div>")

'Else if the forum is locked display a message telling the user so
ElseIf blnForumLocked = True Then
	
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtForumLockedByAdmim & "</span><br /><br /><br /><br /><br /></div>"

'Else if the user does not have permision to create polls
ElseIf blnPollCreate = False AND strMode <> "poll" Then
	
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtSorryYouDoNotHavePermissionToPostInTisForum & "</span><br /><br /><br /><br /><br /></div>"

'Else the user is not logged in so let them know to login before they can post a message
Else
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtMustBeRegisteredToPost & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "register.gif""  alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle""></a>&nbsp;&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "login.gif""  alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle""></a><br /><br /><br /><br /></div>"
End If
%>
<br />
<div align="center">
<% 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
<!-- #include file="includes/footer.asp" -->