<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True

'Declare variables
Dim intIndexPosition		'Holds the idex poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Forum Codes</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!--#include file="includes/skin_file.asp" -->
</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
  <td align="center"><span class="heading"><% = strTxtForumCodes %></span><br />
   <span class="text"><% = strTxtYouCanUseForumCodesToFormatText %></span></td>
  </tr>
</table>
<br />
<table width="510" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td>
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="510">
       <tr>
        <td width="62%" class="bold" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>"><% = strTxtTypedForumCode %></td>
        <td width="38%" class="bold" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>"><% = strTxtConvetedCode %></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
     <td colspan="2" class="text" align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
       <tr>
        <td width="63%" class="bold"><% = strTxtTextFormating %></td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="text">[B]<% = strTxtBold %>[/B]</td>
        <td width="37%"><b class="text"><% = strTxtBold %></b></td>
       </tr>
       <tr>
        <td width="63%" class="text">[I]<% = strTxtItalic %>[/I]</td>
        <td width="37%"><i class="text"><% = strTxtItalic %></i></td>
       </tr>
       <tr>
        <td width="63%" class="text">[U]<% = strTxtUnderline %>[/U]</td>
        <td width="37%"><u class="text"><% = strTxtUnderline %></u></td>
       </tr>
       <tr>
        <td width="63%" class="text">[CENTER]<% = strTxtCentre %>[/CENTER]</td>
        <td width="37%" align="center" class="text"><% = strTxtCentre %></td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtImagesAndLinks %></td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="text">[IMG]http://myWeb.com/smiley.gif[/IMG]</td>
        <td width="37%"><img src="smileys/smiley4.gif" width="17" height="17"></td>
       </tr>
       <tr>
        <td width="63%" class="text">[URL=http://www.myWeb.com]<% = strTxtMyLink %>[/URL]</td>
        <td width="37%"><a href="#"><% = strTxtMyLink %></a></td>
       </tr>
       <tr>
        <td width="63%" class="text">[URL]http://www.myWeb.com[/URL]</td>
        <td width="37%"><a href="#">http://www.myWeb.com</a></td>
       </tr>
       <tr>
        <td width="63%" class="text">[EMAIL=me@myWeb.com]<% = strTxtMyEmail %>[/EMAIL]</td>
        <td width="37%"><a href="#"><% = strTxtMyEmail %></a></td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtFontTypes %></td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="text">[FONT=Arial]Arial[/FONT]</td>
        <td width="37%"><font face="Arial, Helvetica, sans-serif" size="2">Arial</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[FONT=Courier]Courier[/FONT]</td>
        <td width="37%"><font face="Courier New, Courier, mono">Courier</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[FONT=Times]Times[/FONT]</td>
        <td width="37%"><font face="Times New Roman, Times, serif">Times</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[FONT=Verdana]Verdana[/FONT]</td>
        <td width="37%"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Verdana</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtFontSizes %></td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=1]<% = strTxtFontSize %> 1[/SIZE]</td>
        <td width="37%"><font size="1"><% = strTxtFontSize %> 1</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=2]<% = strTxtFontSize %> 2[/SIZE]</td>
        <td width="37%"><font size="2"><% = strTxtFontSize %> 2</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=3]<% = strTxtFontSize %> 3[/SIZE]</td>
        <td width="37%"><font size="3"><% = strTxtFontSize %> 3</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=4]<% = strTxtFontSize %> 4[/SIZE]</td>
        <td width="37%"><font size="4"><% = strTxtFontSize %> 4</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=5]<% = strTxtFontSize %> 5[/SIZE]</td>
        <td width="37%"><font size="5"><% = strTxtFontSize %> 5</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[SIZE=6]<% = strTxtFontSize %> 6[/SIZE]</td>
        <td width="37%"><font size="6"><% = strTxtFontSize %> 6</font></td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtFontColours %></td>
        <td width="37%">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=BLACK]<% = strTxtBlack & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="black"><% = strTxtBlack & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=WHITE]<% = strTxtWhite & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="white"><% = strTxtWhite & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=BLUE]<% = strTxtBlue & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="blue"><% = strTxtBlue & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=RED]<% = strTxtRed  & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="red"><% = strTxtRed & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=GREEN]<% = strTxtGreen & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="green"><% = strTxtGreen & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=YELLOW]<% = strTxtYellow & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="yellow"><% = strTxtYellow & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=ORANGE]<% = strTxtOrange & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="orange"><% = strTxtOrange & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=BROWN]<% = strTxtBrown & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="brown"><% = strTxtBrown & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=MAGENTA]<% = strTxtMagenta & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="magenta"><% = strTxtMagenta & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=CYAN]<% = strTxtCyan & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="cyan"><% = strTxtCyan & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">[COLOR=LIME GREEN]<% = strTxtLimeGreen & " " & strTxtFont %>[/COLOR]</td>
        <td width="37%" class="text"><font color="limegreen"><% = strTxtLimeGreen & " " & strTxtFont %></font></td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtQuoting %></td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" colspan="2" class="text">[Quote=Username]<% = strTxtQuotedMessage & " " & strTxtWithUsername %>[/QUOTE]</td>
       </tr>
       <tr>
        <td width="63%" colspan="2" class="text">[Quote]<% = strTxtQuotedMessage %>[/QUOTE]</td>
       </tr>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtCodeandFixedWidthData %></td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" colspan="2" class="text">[CODE]<% = strTxtMyCodeData %>[/CODE]</td>
       </tr><%

If blnFlashFiles Then %>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtFlashFilesImages %></td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" colspan="2" class="text">[FLASH WIDTH=50 HEIGHT=50]http://www.myWeb.com/flash.swf[/FLASH]</td>
       </tr><%
End If


If blnEmoticons Then %>
       <tr>
        <td width="63%" class="text">&nbsp;</td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td width="63%" class="bold"><% = strTxtEmoticons %></td>
        <td width="37%" class="text">&nbsp;</td>
       </tr>
       <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="4"><%

'Intilise the index position (we are starting at 1 instead of position 0 in the array for simpler calculations)
intIndexPosition = 1

'Calcultae the number of outer loops to do
intNumberOfOuterLoops = UBound(saryEmoticons) / 2

'If there is a remainder add 1 to the number of loops
If UBound(saryEmoticons) MOD 2 > 0 Then intNumberOfOuterLoops = intNumberOfOuterLoops + 1

'Loop throgh th list of emoticons
For intLoop = 1 to intNumberOfOuterLoops

        Response.Write("<tr>")

	'Loop throgh th list of emoticons
	For intInnerLoop = 1 to 2

		'If there is nothing to display show an empty box
		If intIndexPosition > UBound(saryEmoticons) Then
			Response.Write("<td width=""5"" class=""text"">&nbsp;</td>")
			Response.Write("<td width=""92"" class=""text"">&nbsp;</td>")
			Response.Write("<td width=""44"" class=""text"">&nbsp;</td>")
		'Else show the emoticon
		Else
			Response.Write("<td width=""5"" class=""text""><img src=""" & saryEmoticons(intIndexPosition,3) & """ border=""0"" alt=""" & saryEmoticons(intIndexPosition,2) & """></td>")
                	Response.Write("<td width=""92"" class=""text"" nowrap=""nowrap"">" & saryEmoticons(intIndexPosition,1) & "</td>")
                	Response.Write("<td width=""44"" class=""text"">" & saryEmoticons(intIndexPosition,2) & "</td>")
              	End If

              'Minus one form the index position
              intIndexPosition = intIndexPosition + 1
	Next

	Response.Write("</tr>")
Next
%></table>
        </td>
       </tr><%
End If
      %>
      </table>
     </td>
    </tr>
   </table>
   </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center" height="34"><a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a></td>
  </tr>
</table>
<div align="center"><%

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</div>
</body>
</html>