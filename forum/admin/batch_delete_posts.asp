<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Session.Timeout =  1000

'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Dimension variables
Dim intNoOfDays			'Holds the number of days to delete posts from
Dim intForumID			'Holds the forum ID number
Dim lngNumberOfTopics		'Holds the number of topics that are deleted
Dim lngPollID			'Holds the poll ID if there is one to delete
Dim rsThread			'Holds the threads recordset
Dim intPriority			'Holds the topic priority to delete

'Initilise variables
lngNumberOfTopics = 0

'get teh number of days to delte from
intNoOfDays = CInt(Request.Form("days"))
intForumID = CInt(Request.Form("FID"))
intPriority = CInt(Request.Form("priority"))




'Get all the Topics from the database to be deleted

'Initalise the strSQL variable with an SQL statement to get the topic from the database
strSQL = "SELECT " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Poll_ID FROM " & strDbTable & "Topic "
If intForumID = 0 Then
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays
Else
	strSQL = strSQL & "WHERE (" & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays  & ") AND (" & strDbTable & "Topic.Forum_ID=" & intForumID & ")"
End If
If intPriority <> 4 Then strSQL = strSQL & " AND (" & strDbTable & "Topic.Priority=" & intPriority & ")"
strSQL = strSQL & ";"

'Query the database
rsCommon.Open strSQL, adoCon


'Create a record set object to the Threads held in the database
Set rsThread = Server.CreateObject("ADODB.Recordset")


'Loop through all the topics to get all the thread in the topics to be deleted
Do While NOT rsCommon.EOF

	'Update the number of topics deletd
	lngNumberOfTopics = lngNumberOfTopics + 1
	
	'Get the Poll ID
	lngPollID = CLng(rsCommon("Poll_ID"))
	
	
	
	'See if there are any guest posters and delete thier names form the guest name table
	
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "SELECT " & strDbTable & "Thread.Thread_ID FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID=" & rsCommon("Topic_ID") & ";"
	
	'Query the database
	rsThread.Open strSQL, adoCon
	
	'Loop through and delete al names in the guest name table
	Do While NOT rsThread.EOF
		
		'First we need to delete any entry in the GuestName table incase this was a guest poster posting the message
		strSQL = "DELETE FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID=" & CLng(rsThread("Thread_ID")) & ";"
		
		'Excute SQL
		adoCon.Execute(strSQL)
		
		'Move next
		rsThread.MoveNext
	Loop
	
	'Close the rs
	rsThread.Close
	
	

	'Delete the thread
	strSQL = "DELETE FROM " & strDbTable & "Thread WHERE " & strDbTable & "Thread.Topic_ID =" & rsCommon("Topic_ID") & ";"

	'Delete the threads
	adoCon.Execute(strSQL)
	
	

	'If there is a poll delete that as well
	If lngPollID > 0 Then

		'Delete the poll choice
		strSQL = "DELETE FROM " & strDbTable & "PollChoice WHERE " & strDbTable & "PollChoice.Poll_ID =" & lngPollID & ";"

		'Delete the threads
		adoCon.Execute(strSQL)

		'Delete the poll choice
		strSQL = "DELETE FROM " & strDbTable & "Poll WHERE " & strDbTable & "Poll.Poll_ID =" & lngPollID & ";"

		'Delete the threads
		adoCon.Execute(strSQL)
	End If


	'Move to the next record
	rsCommon.MoveNext
Loop



'Initalise the strSQL variable with an SQL statement to get the topic from the database
strSQL = "DELETE FROM " & strDbTable & "Topic "
If intForumID = 0 Then
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays
Else
	strSQL = strSQL & "WHERE (" & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays  & ") AND (" & strDbTable & "Topic.Forum_ID=" & intForumID & ")"
End If
If intPriority <> 4 Then strSQL = strSQL & " AND (" & strDbTable & "Topic.Priority=" & intPriority & ")"
strSQL = strSQL & ";"

'Delete the topics
adoCon.Execute(strSQL)



'Update post count
updateTopicPostCount(intForumID)


'Reset Server Objects
Set rsThread = Nothing
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Batch Delete Forum Topics</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">Batch Delete Forum Topics </span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br>
  <br>
  <br>
  <br>
  <br />
  <span class="bold">
  <% = lngNumberOfTopics %>
  Topics have been Deleted.</span><br />
  <br>
  <br>
  <br>
  <br>
  <a href="resync_forum_post_count.asp">Click here to re-sync Post and Topic Counts for the Forums</a> </p>
</div>
</body>
</html>
