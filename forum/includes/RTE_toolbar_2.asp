<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Rich Text Editor
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'RTE Toolbox 2

Response.Write(vbCrLf & "             <span id=""ToolBar2"">")

If blnCut AND RTEenabled <> "Gecko" Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_cut.gif"" align=""absmiddle"" onClick=""FormatText('cut')"" style=""cursor: pointer;"" title=""" & strTxtCut & """> ")
If blnCopy AND RTEenabled <> "Gecko" Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_copy.gif"" align=""absmiddle"" onClick=""FormatText('copy')"" style=""cursor: pointer;"" title=""" & strTxtCopy & """> ")
If blnPaste AND RTEenabled <> "Gecko" Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_paste.gif"" align=""absmiddle"" onClick=""FormatText('paste')"" style=""cursor: pointer;"" title=""" & strTxtPaste & """>&nbsp;")
If blnUndo Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_undo.gif"" align=""absmiddle"" onClick=""FormatText('undo')"" style=""cursor: pointer;"" title=""" & strTxtUndo & """>")
If blnRedo Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_redo.gif"" align=""absmiddle"" onClick=""FormatText('redo')"" style=""cursor: pointer;"" title=""" & strTxtRedo & """>&nbsp;")
If blnLeftJustify Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_left_just.gif"" align=""absmiddle"" onClick=""FormatText('JustifyLeft', '')"" style=""cursor: pointer;"" title=""" & strTxtLeftJustify & """>")
If blnCentre Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_centre.gif"" align=""absmiddle"" border=""0"" onClick=""FormatText('JustifyCenter', '')"" style=""cursor: pointer;"" title=""" & strTxtCentrejustify & """>")
If blnRightJustify Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_right_just.gif"" align=""absmiddle"" onClick=""FormatText('JustifyRight', '')"" style=""cursor: pointer;"" title=""" & strTxtRightJustify & """>")
If blnFullJustify Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_justify.gif"" align=""absmiddle"" onClick=""FormatText('JustifyFull', '')"" style=""cursor: pointer;"" title=""" & strTxtJustify & """>&nbsp;")
If blnOrderList Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_or_list.gif"" align=""absmiddle"" border=""0"" onClick=""FormatText('InsertOrderedList', '')"" style=""cursor: pointer;"" title=""" & strTxtstrTxtOrderedList & """>")
If blnUnOrderList Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_list.gif"" align=""absmiddle"" border=""0"" onClick=""FormatText('InsertUnorderedList', '')"" style=""cursor: pointer;"" title=""" & strTxtUnorderedList & """>&nbsp;")
If blnOutdent Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_outdent.gif"" align=""absmiddle"" onClick=""FormatText('Outdent', '')"" style=""cursor: pointer;"" title=""" & strTxtOutdent & """>")
If blnIndent Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_indent.gif"" align=""absmiddle"" border=""0"" onClick=""FormatText('indent', '')"" style=""cursor: pointer;"" title=""" & strTxtIndent & """>&nbsp;")
If blnAddHyperlink Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_hyperlink.gif"" align=""absmiddle"" border=""0"" onClick=""FormatText('createLink','/dokument/')"" style=""cursor: pointer;"" title=""" & strTxtAddHyperlink & """>")


'If this is IE then open pop up image insert window
If (RTEenabled = "winIE" OR RTEenabled = "winIE5") AND blnAddImage Then

	Response.Write(vbCrLf & "             <a href=""javascript:openWin('RTE_image_window.asp','insertImg','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=200')""><img src=""" & strImagePath & "post_button_image.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtAddImage & """ style=""cursor: pointer;""></a>")

'If this is Gecko have a pop up JS prompt for link to image URL
ElseIf blnAddImage Then
	Response.Write(vbCrLf & "	     <img src=""" & strImagePath & "post_button_image.gif"" align=""absmiddle"" border=""0"" title=""Add Image"" onClick=""AddImage()"" style=""cursor: pointer;"">")
End If

'If this is IE then open pop up table insert window
If (RTEenabled = "winIE" OR RTEenabled = "winIE5") AND blnInsertTable Then Response.Write(vbCrLf & "             <a href=""javascript:openWin('RTE_table_window.asp','insertTable','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=190')""><img src=""" & strImagePath & "post_insert_table.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtInsertTable & """ style=""cursor: pointer;""></a>")

'Button Pop up for emoticons
If blnEmoticonPopUp Then Response.Write(vbCrLf & "             <a href=""javascript:openWin('RTE_emoticons_smilies.asp','emot','toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width=400,height=400')""><img src=""" & strImagePath & "post_button_smiley.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtEmoticons & """ style=""cursor: pointer;""></a>&nbsp;")

'If this is IE then show the spell check button
If (RTEenabled = "winIE" OR RTEenabled = "winIE5") AND blnSpellCheck Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_spell_check.gif"" align=""absmiddle"" border=""0"" onClick=""checkspell()"" style=""cursor: pointer;"" title=""" & strTxtstrSpellCheck & """>")


Response.Write(vbCrLf & "	     </span>")


If blnHTMLView Then Response.Write(vbCrLf & "	     <img src=""" & strImagePath & "post_button_html.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtToggleHTMLView & """ onClick=""HTMLview()"" style=""cursor: pointer;"">")
If blnAbout Then Response.Write(vbCrLf & "             &nbsp;<a href=""javascript:openWin('RTE_about.asp','about','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=200')""><img src=""" & strImagePath & "post_button_about.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtAboutRichTextEditor & """ style=""cursor: pointer;""></a>")

%>