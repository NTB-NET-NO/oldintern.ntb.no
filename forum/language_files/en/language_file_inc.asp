<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by email ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Global
'---------------------------------------------------------------------------------
Const strTxtWelcome = "Welcome"
Const strTxtAllForums = "All Forums"
Const strTxtTopics = "Topics"
Const strTxtPosts = "Posts"
Const strTxtLastPost = "Last Post"
Const strTxtPostPreview = "Post Preview"
Const strTxtAt = "at"
Const strTxtBy = "By"
Const strTxtOn = "on"
Const strTxtProfile = "Profile"
Const strTxtSearch = "Search"
Const strTxtQuote = "Quote"
Const strTxtVisit = "Visit"
Const strTxtView = "View"
Const strTxtHome = "Home"
Const strTxtHomepage = "Homepage"
Const strTxtEdit = "Edit"
Const strTxtDelete = "Delete"
Const strTxtEditProfile = "Edit Profile"
Const strTxtLogOff = "Logout"
Const strTxtRegister = "Register"
Const strTxtLogin = "Login"
Const strTxtMembersList = "Display List of Forum Members"
Const strTxtForumLocked = "Forum Locked"
Const strTxtSearchTheForum = "Search The Forum"
Const strTxtPostReply = "Post Reply"
Const strTxtNewTopic = "Post New Topic"
Const strTxtCloseWindow = "Close Window"
Const strTxtNoForums = "There are no Forums to display"
Const strTxtReturnToDiscussionForum = "Return to the Discussion Forum"
Const strTxtMustBeRegistered = "Sorry, you must be a registered forum member in order to use this feature."
Const strClickHereIfNotRegistered = "Click here if you are not a registered forum member"
Const strTxtResetForm = "Reset Form"
Const strTxtClearForm = "Clear Form"
Const strTxtYes = "Yes"
Const strTxtNo = "No"
Const strTxtForumLockedByAdmim = "Sorry, this function has been disabled.<br />This Forum has been locked by the Forum Administrator."
Const strTxtRequiredFields = "Indicates required fields"

Const strTxtForumJump = "Forum Jump"
Const strTxtSelectForum = "Select Forum"


'Global Error
'---------------------------------------------------------------------------------
Const strTxtErrorDisplayLine = "_______________________________________________________________"
Const strTxtErrorDisplayLine1 = "The form has not been submitted because there are problem(s) with the form."
Const strTxtErrorDisplayLine2 = "Please correct the problem(s) and re-submit the form."
Const strTxtErrorDisplayLine3 = "The following field(s) need to be corrected: -"
Const strResetFormConfirm = "Are you sure you want to reset the form?"


'default.asp
'---------------------------------------------------------------------------------
Const strTxtCookies = "Cookies and JavaScript must be enabled on your web browser in order to use this forum"
Const strTxtForum = "Forum"
Const strTxtLatestForumPosts = "Latest Forum Posts"
Const strTxtForumStatistics = "Forum Statistics"
Const strTxtNoForumPostMade = "There have been no Forum Posts"
Const strTxtThereAre = "There are"
Const strTxtPostsIn = "Posts in"
Const strTxtTopicsIn = "Topics in"
Const strTxtLastPostOn = "Last Post on"
Const strTxtLastPostBy = "Last Post by"
Const strTxtForumMembers = "Forum Members"
Const strTxtTheNewestForumMember = "The Newest Forum Member is"


'forum_topics.asp
'---------------------------------------------------------------------------------
Const strTxtThreadStarter = "Topic Starter"
Const strTxtReplies = "Replies"
Const strTxtViews = "Views"
Const strTxtDeleteTopicAlert = "Are you sure you want to delete this topic?"
Const strTxtDeleteTopic = "Delete Topic"
Const strTxtNextTopic = "Next Topic"
Const strTxtLastTopic = "Last Topic"
Const strTxtShowTopics = "Show Topics"
Const strTxtNoTopicsToDisplay = "There are no messages posted in this forum in the last"

Const strTxtAll = "All"
Const strTxtLastWeek = "from the Last Week"
Const strTxtLastTwoWeeks = "from the Last Two Weeks"
Const strTxtLastMonth = "from the Last Month"
Const strTxtLastTwoMonths = "from the Last Two Months"
Const strTxtLastSixMonths = "from the Last Six Months"
Const strTxtLastYear = "from the Last Year"


'forum_posts.asp
'---------------------------------------------------------------------------------
Const strTxtLocation = "Location"
Const strTxtJoined = "Joined"
Const strTxtForumAdministrator = "Forum Administrator"
Const strTxtForumModerator = "Forum Moderator"
Const strTxtDeletePostAlert = "Are you sure you want to delete this post?"
Const strTxtEditPost = "Edit Post"
Const strTxtDeletePost = "Delete Post"
Const strTxtSearchForPosts = "Search for other posts by"
Const strTxtSubjectFolder = "Subject"
Const strTxtPrintVersion = "Printable version"
Const strTxtEmailTopic = "Email this topic"
Const strTxtSorryNoReply = "Sorry, you can NOT post a reply."
Const strTxtThisForumIsLocked = "This forum has been locked by a forum administrator."
Const strTxtPostAReplyRegister = "If you wish to post a reply to this topic you must first"
Const strTxtNeedToRegister = "If you are not already registered you must first"
Const strTxtSmRegister = "register"
Const strTxtNoThreads = "There are no posts in the database relating to this topic"
Const strTxtNotGiven = "Not Given"
Const strTxtNoMessageError = "Message \t\t- Enter a Message to post"


'search_form.asp
'---------------------------------------------------------------------------------
Const strTxtSearchFormError = "Search For\t- Enter something to search for"


'search.asp
'---------------------------------------------------------------------------------
Const strTxtSearchResults = "Search Results"
Const strTxtYourSearchFor = "Your search for"
Const strTxtHasFound = "has found"
Const strTxtResults = "results"
Const strTxtNoSearchResults = "Sorry, your search found no results"
Const strTxtClickHereToRefineSearch = "Click here to refine your search"
Const strTxtSearchFor = "Search For"
Const strTxtSearchIn = "Search In"
Const strTxtSearchOn = "Search On"
Const strTxtAllWords = "All Words"
Const strTxtAnyWords = "Any Words"
Const strTxtPhrase = "Phrase"
Const strTxtTopicSubject = "Topic Subject"
Const strTxtMessageBody = "Message Body"
Const strTxtAuthor = "Author"
Const strTxtSearchForum = "Search Forum"
Const strTxtSortResultsBy = "Sort Results By"
Const strTxtLastPostTime = "Last Post Time"
Const strTxtTopicStartDate = "Topic Start Date"
Const strTxtSubjectAlphabetically = "Subject Alphabetically"
Const strTxtNumberViews = "Number of Views"
Const strTxtStartSearch = "Start Search"


'printer_friendly_posts.asp
'---------------------------------------------------------------------------------
Const strTxtPrintPage = "Print Page"
Const strTxtPrintedFrom = "Printed From"
Const strTxtForumName = "Forum Name"
Const strTxtForumDiscription = "Forum Discription"
Const strTxtURL = "URL"
Const strTxtPrintedDate = "Printed Date"
Const strTxtTopic = "Topic"
Const strTxtPostedBy = "Posted By"
Const strTxtDatePosted = "Date Posted"


'emoticons.asp
'---------------------------------------------------------------------------------
Const strTxtEmoticonSmilies = "Emoticons"
Const strTxtClickOnEmoticonToAdd = "Click on the emoticon you would like to add to your message."


'login.asp
'---------------------------------------------------------------------------------
Const strTxtSorryUsernamePasswordIncorrect = "Sorry, the Username or Password entered is incorrect."
Const strTxtPleaseTryAgain = "Please try again."
Const strTxtUsername = "Username"
Const strTxtPassword = "Password"
Const strTxtLoginUser = "Forum Login"
Const strTxtClickHereForgottenPass = "Forgotten your password?"
Const strTxtErrorUsername = "Username \t- Enter your Forum Username"
Const strTxtErrorPassword = "Password \t- Enter your Forum Password"


'forgotten_password.asp
'---------------------------------------------------------------------------------
Const strTxtForgottenPassword = "Forgotten Password"
Const strTxtNoRecordOfUsername = "Sorry, the email address entered does not match the one listed for that username."
Const strTxtNoEmailAddressInProfile = "Sorry, your profile does not contain an email address.<br />Your new password can not be emailed to you."
Const strTxtReregisterForForum = "You will need to re-register to use the forum."
Const strTxtPasswordEmailToYou = "Your new password has been emailed to you."
Const strTxtPleaseEnterYourUsername = "Please enter your username and the email address in the boxes below.<br />Your new password will then be sent to the email address in your profile."
Const strTxtValidEmailRequired = "If your forum profile does not contain a valid email address for you then you will have to re-register to use the forum."
Const strTxtEmailPassword = "Request New Password"

Const strTxtEmailPasswordRequest = "A Forgotten Password request has been made for a new password to be emailed to you for the Forum, "
Const strTxtEmailPasswordRequest2 = "Your new password is: -"
Const strTxtEmailPasswordRequest3 = "To go to the forum now click on the link below: -"


'forum_password_form.asp
'---------------------------------------------------------------------------------
Const strTxtForumLogin = "Forum Login"
Const strTxtErrorEnterPassword = "Password \t- Enter a Password to use this Forum"
Const strTxtPasswordRequiredForForum = "This is a private forum and requires that you enter a forum password to proceed."
Const strTxtForumPasswordIncorrect = "Sorry the Password entered is incorrect."
Const strTxtAutoLogin = "Auto Login"
Const strTxtLoginToForum = "Login To Forum"


'profile.asp
'---------------------------------------------------------------------------------
Const strTxtNoUserProfileFound = "Sorry no profile can be found for this user"
Const strTxtRegisteredToViewProfile = "Sorry, you must be a registered forum member to view profiles."
Const strTxtMemberNo = "Member No."
Const strTxtEmail = "Email Address"
Const strTxtPrivate = "Private"


'post_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtPostNewTopic = "Post New Topic"
Const strTxtErrorTopicSubject = "Subject \t\t- Enter a Subject for your new Topic"
Const strTxtForumMemberSuspended = "Sorry, this function is disabled as your Forum Membership has been suspended!"

'edit_post.asp
'---------------------------------------------------------------------------------
Const strTxtNoPermissionToEditPost = "Sorry, you do not have permission to edit this post!"
Const strTxtReturnForumTopic = "Return to Forum Topic"


'email_topic.asp
'---------------------------------------------------------------------------------
Const strTxtEmailTopicToFriend = "Email Topic To a Friend"
Const strTxtFriendSentEmail = "Your Friend has been sent the email"
Const strTxtFriendsName = "Friends Name"
Const strTxtFriendsEmail = "Friends Email"
Const strTxtYourName = "Your Name"
Const strTxtYourEmail = "Your Email"
Const strTxtSendEmail = "Send Email"
Const strTxtMessage = "Message"

Const strTxtEmailFriendMessage = "I thought you might be interested in a post on"
Const strTxtFrom = "from"

Const strTxtErrorFrinedsName = "Friends Name \t- Enter your Friends Name"
Const strTxtErrorFriendsEmail = "Friends Email \t- Enter a valid email address for your friend"
Const strTxtErrorYourName = "Your Name \t- Enter your Name"
Const strTxtErrorYourEmail = "Your Email \t- Enter your valid email address"
Const strTxtErrorEmailMessage = "Message \t- Enter a message for your friend"


'members.asp
'---------------------------------------------------------------------------------
Const strTxtForumMembersList = "Forum Members List"
Const strTxtMemberSearch = "Member Search"
Const strTxtForumMembersOn = "forum members on"
Const strTxtPageYouAerOnPage = "pages and you are on page number"
Const strTxtYourSearchMembersFound = "Your search of the forum members found"
Const strTxtMatches = "matches"

Const strTxtUsernameAlphabetically = "Username Alphabetically"
Const strTxtNewForumMembersFirst = "New Forum Members First"
Const strTxtOldForumMembersFirst = "Old Forum Members First"
Const strTxtLocationAlphabetically = "Location Alphabetically"

Const strTxtRegistered = "Registered"
Const strTxtSend = "Send"
Const strTxtNext = "Next"
Const strTxtPrevious = "Prev"
Const strTxtPage = "Page"

Const strTxtErrorMemberSerach = "Member Search\t- Enter a Members Username to search for"


'message_form.asp
'---------------------------------------------------------------------------------
Const strTxtTextFormat = "Text Format"
Const strTxtPreviewPost = "Preview Post"
Const strTxtMode = "Mode"
Const strTxtPrompt = "Prompt"
Const strTxtBasic = "Basic"
Const strTxtAddEmailLink = "Add Email Link"
Const strTxtList = "List"
Const strTxtCentre = "Centre"

Const strTxtEnterBoldText = "Enter text you want formatted in Bold"
Const strTxtEnterItalicText = "Enter text you want formatted in Italic"
Const strTxtEnterUnderlineText = "Enter text you want Underlined"
Const strTxtEnterCentredText = "Enter text you want Centred"
Const strTxtEnterHyperlinkText = "Enter the on screen display text for the Hyperlink"
Const strTxtEnterHeperlinkURL = "Enter the URL address to create Hyperlink to"
Const strTxtEnterEmailText = "Enter the on screen display text for the email address"
Const strTxtEnterEmailMailto = "Enter the email address to link to"
Const strTxtEnterImageURL = "Enter the web address of the image"
Const strTxtEnterTypeOfList = "Type of list"
Const strTxtEnterEnter = "Enter"
Const strTxtEnterNumOrBlankList = "for numbered or leave blank for bulleted"
Const strTxtEnterListError = "ERROR! Please enter"
Const strEnterLeaveBlankForEndList = "List item Leave blank to end list"


'IE_message_form.asp
'---------------------------------------------------------------------------------
Const strTxtCut = "Cut"
Const strTxtCopy = "Copy"
Const strTxtPaste = "Paste"
Const strTxtBold = "Bold"
Const strTxtItalic = "Italic"
Const strTxtUnderline = "Underline"
Const strTxtLeftJustify = "Left Justify"
Const strTxtCentrejustify = "Centre Justify"
Const strTxtRightJustify = "Right Justify"
Const strTxtUnorderedList = "Unordered List"
Const strTxtOutdent = "Outdent"
Const strTxtIndent = "Indent"
Const strTxtAddHyperlink = "Add Hyperlink"
Const strTxtAddImage = "Add Image"
Const strTxtJavaScriptEnabled = "JavaScript must be enabled on your web browser for you to post a message in the forum!"
Const strTxtShowSignature = "Show Signature"
Const strTxtEmailNotify = "Email Notify me of Replies"
Const strTxtUpdatePost = "Update Post"
Const strTxtFontColour = "Colour"


'register.asp
'---------------------------------------------------------------------------------
Const strTxtRegisterNewUser = "Register New User"

Const strTxtProfileUsernameLong = "This is the name displayed when you use the forum"
Const strTxtRetypePassword = "Retype Password"
Const strTxtProfileEmailLong = "Not required, but useful if you wish to be notified when someone answers one of your posts or if you lose your password."
Const strTxtShowHideEmail = "Show my Email Address"
Const strTxtShowHideEmailLong = "Hide your email address if you want it kept private from other users."
Const strTxtSelectCountry = "Select Country"
Const strTxtProfileAutoLogin = "Automatically log me in when I return to the Forum"
Const strTxtSignature = "Signature"
Const strTxtSignatureLong = "Enter a signature that you would like shown at the bottom of your Forum Posts"

Const strTxtErrorUsernameChar = "Username \t- Your Username must be at least 4 characters"
Const strTxtErrorPasswordChar = "Password \t- Your Password must be at least 4 characters"
Const strTxtErrorPasswordNoMatch = "Password Error\t- The passwords entered do not match"
Const strTxtErrorValidEmail = "Email\t\t- Enter your valid email address"
Const strTxtErrorValidEmailLong = "If you don't want to enter your email address then leave the email field blank"
Const strTxtErrorNoEmailToShow = "You can not show your email address if you haven\'t entered one!"
Const strTxtErrorSignatureToLong = "Signature \t- Your signature has to many characters"
Const strTxtUpdateProfile = "Update Profile"


Const strTxtUsrenameGone = "Sorry, the Username you requested is already taken.\n\nPlease choose another Username."
Const strTxtEmailThankYouForRegistering = "Thank-you for taking the time to register to use "
Const strTxtEmailYouCanNowUseTheForumAt = "Your login details can be found below and now you have registered for a new account you can post new messages and reply to existing ones on the"
Const strTxtEmailForumAt = "Forum at"
Const strTxtEmailToThe = "to "


'register_new_user.inc
'---------------------------------------------------------------------------------
Const strTxtEmailAMeesageHasBeenPosted = "A message has been posted in the forum on"
Const strTxtEmailClickOnLinkBelowToView = "To view and/or reply to the post then click on the link below"
Const strTxtEmailAMeesageHasBeenPostedOnForumNum = "A message has been posted in the forum number"


'registration_rules.asp
'---------------------------------------------------------------------------------
Const strTxtForumRulesAndPolicies = "Forum Rules and Policies"
Const srtTxtAccept = "Accept"
Const strTxtCancel = "Cancel"




'New from version 6
'---------------------------------------------------------------------------------
Const strTxtHi = "Hi"
Const strTxtInterestingForumPostOn = "Interesting Forum post on"
Const strTxtForumLostPasswordRequest = "Forum Lost Password Request"
Const strTxtLockForum = "Lock Forum"
Const strTxtLockedTopic = "Closed&nbsp;Topic"
Const strTxtUnLockTopic = "Un-Lock Topic"
Const strTxtTopicLocked = "Topic Closed"
Const strTxtUnForumLocked = "Un-Lock Forum"
Const strTxtThisTopicIsLocked = "This topic is closed."
Const strTxtThatYouAskedKeepAnEyeOn = "that you asked us to keep an eye on."
Const strTxtTheTopicIsNowDeleted = "The Topic has now been Deleted."
Const strTxtOf = "of"
Const strTxtTheTimeNowIs = "The time now is"
Const strTxtYouLastVisitedOn = "You last visited on"
Const strTxtSendMsg = "Send PM"
Const strTxtSendPrivateMessage = "Send Private Message"
Const strTxtActiveUsers = "Active Users"
Const strTxtGuestsAnd = "Guest(s) and"
Const strTxtMembers = "Member(s)"
Const strTxtPreview = "Preview"
Const strTxtThereIsNothingToPreview = "There is nothing to preview"
Const strTxtEnterTextYouWouldLikeIn = "Enter the text that you would like in"
Const strTxtEmailAddressAlreadyUsed = "Sorry, the email address entered has already been used to register another member."
Const strTxtIP = "IP"
Const strTxtIPLogged = "IP Logged"
Const strTxtPages = "Pages"
Const strTxtCharacterCount = "Character Count"
Const strTxtAdmin = "Admin"


Const strTxtType = "Group"
Const strTxtActive = "Active"
Const strTxtGuest = "Guest"
Const strTxtAccountStatus = "Account Status"
Const strTxtNotActive = "Not Active"



Const strTxtEmailRequiredForActvation = "Required to be able to receive an email to activate your membership"
Const strTxtToActivateYourMembershipFor = "To activate your membership for"
Const strTxtForumClickOnTheLinkBelow = "click on the link below"
Const strTxtForumAdmin = "Forum Admin"
Const strTxtViewLastPost = "View Last Post"
Const strTxtSelectAvatar = "Select Avatar"
Const strTxtAvatar = "Avatar"
Const strTxtSelectAvatarDetails = "This is the small icon shown next to your posts. Either select one from the list or type the path in to your own Avatar (must be "
Const strTxtPixels = " pixels)."
Const strTxtForumCodesInSignature = "can be used in your signature"

Const strTxtHighPriorityPost = "Announcement"
Const strTxtHighPriorityPostLocked = "Locked Announcement"
Const strTxtHotTopicNewReplies = "Hot Topic [new posts]"
Const strTxtHotTopic = "Hot Topic [no new posts]"
Const strTxtOpenTopic = "Topic [no new posts]"
Const strTxtOpenTopicNewReplies = "Topic [new post]"
Const strTxtPinnedTopic = "Sticky Topic"

Const strTxtOpenForum = "Open Forum [no new posts]"
Const strTxtOpenForumNewReplies = "Open Forum [new posts]"
Const strTxtReadOnly = "Read Only [no new replies]"
Const strTxtReadOnlyNewReplies = "Read Only [new posts]"
Const strTxtPasswordRequired = "Password Required"
Const strTxtNoAccess = "No Access"

Const strTxtFont = "Font"
Const strTxtSize = "Size"
Const strTxtForumCodes = "Forum Codes"

Const strTxtPriority = "Sticky Topic"
Const strTxtNormal = "Normal Topic"
Const strTxtTopAllForums = "Announcement (all forums)"
Const strTopThisForum = "Announcement (this forum)"


Const strTxtMarkAllPostsAsRead = "Mark all posts as read"
Const strTxtDeleteCookiesSetByThisForum = "Delete cookies set by this forum"


'forum_codes
'---------------------------------------------------------------------------------
Const strTxtYouCanUseForumCodesToFormatText = "You can use the following Forum Codes to Format your text"
Const strTxtTypedForumCode = "Typed Forum Code"
Const strTxtConvetedCode = "Converted Code"
Const strTxtTextFormating = "Text Formatting"
Const strTxtImagesAndLinks = "Images and Links"
Const strTxtFontTypes = "Font Type"
Const strTxtFontSizes ="Font Size"
Const strTxtFontColours ="Font Colours"
Const strTxtEmoticons = "Emoticons"
Const strTxtFontSize = "Font Size"
Const strTxtMyLink = "My Link"
Const strTxtMyEmail = "My Email"



'insufficient_permission.asp
'---------------------------------------------------------------------------------
Const strTxtAccessDenied = "Access Denied"
Const strTxtInsufficientPermison = "Sorry, only members with sufficient permission can access this page."


'activate.asp
'---------------------------------------------------------------------------------
Const strTxtYourForumMemIsNowActive = "Thank-you for registering.<br /><br /><span class=""lgText"">Your Forum membership is now active.</span>"
Const strTxtErrorWithActvation = "There is a problem activating your membership.<br /><br />Please contact the "


'register_mail_confirm.asp
'---------------------------------------------------------------------------------
Const strTxtYouShouldReceiveAnEmail = "Your Forum Membership needs to be activated! <br /><br />An activation email will be sent in a few moments to the email address you gave when registering.<br />Click on the link in this email to activate your Forum Membership."
Const strTxtThankYouForRegistering = "Thank-you for registering to use"
Const strTxtIfErrorActvatingMembership = "If you have a problem activating your membership please contact the"


'active_users.asp
'---------------------------------------------------------------------------------
Const strTxtActiveForumUsers = "Active Forum Users"
Const strTxtAddMToActiveUsersList = "Add me to Active Users list"
Const strTxtLoggedIn = "Logged In"
Const strTxtLastActive = "Last Active"
Const strTxtBrowser = "Browser"
Const strTxtOS = "OS"
Const strTxtMinutes = "minutes"
Const strTxtAnnoymous = "Anonymous"



'not_posted.asp
'---------------------------------------------------------------------------------
Const strTxtMessageNotPosted = "Message Not Posted"
Const strTxtDoublePostingIsNotPermitted = "Double posting is not permitted; your message has been posted already."
Const strTxtSpammingIsNotPermitted = "Spamming is not permitted!"
Const strTxtYouHaveExceededNumOfPostAllowed = "You have exceeded the number of posts permitted in the time span.<br /><br />Please try again later."
Const strTxtYourMessageNoValidSubjectHeading = "Your message did not contain a valid subject heading and/or message body."


'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtActiveTopics = "Active Topics"
Const strTxtLastVisitOn = "Last visit on"
Const strTxtLastFifteenMinutes = "Last 15 minutes"
Const strTxtLastThirtyMinutes = "Last 30 minutes"
Const strTxtLastFortyFiveMinutes = "Last 45 minutes"
Const strTxtLastHour = "Last hour"
Const strTxtLastTwoHours = "Last 2 hours"
Const strTxtYesterday = "Yesterday"
Const strTxtShowActiveTopicsSince = "Show Active Topics since"
Const strTxtNoActiveTopicsSince = "There are no Active Topics since"
Const strTxtToDisplay = "to display"
Const strTxtThereAreCurrently = "There are currently"



'pm_check.inc
'---------------------------------------------------------------------------------
Const strTxtNewPMsClickToGoNowToPM = "new Private Message(s).\n\nClick OK to go to your Private Messenger."


'display_forum_topics.inc
'---------------------------------------------------------------------------------
Const strTxtFewYears = "few years"
Const strTxtWeek = "week"
Const strTxtTwoWeeks = "two weeks"
Const strTxtMonth = "month"
Const strTxtTwoMonths = "two months"
Const strTxtSixMonths = "6 months"
Const strTxtYear = "year"

'Colours
'---------------------------------------------------------------------------------
Const strTxtBlack = "Black"
Const strTxtWhite = "White"
Const strTxtBlue = "Blue"
Const strTxtRed = "Red"
Const strTxtGreen = "Green"
Const strTxtYellow = "Yellow"
Const strTxtOrange = "Orange"
Const strTxtBrown = "Brown"
Const strTxtMagenta = "Magenta"
Const strTxtCyan = "Cyan"
Const strTxtLimeGreen = "Lime Green"


Const strTxtHasBeenSentTo = "has been sent to"
Const strTxtCharactersInYourSignatureToLong = "characters in your signature, you must shorten it to below 200"
Const strTxtSorryYourSearchFoundNoMembers = "Sorry, your search found no forum members that match your criteria"
Const strTxtCahngeOfEmailReactivateAccount = "If you change your email address you will be sent an email to re-activate your account"
Const strTxtAddToBuddyList = "Add to Buddy List"


'register_mail_confirm.asp
'---------------------------------------------------------------------------------
Const strTxtYourEmailAddressHasBeenChanged = "Your email address has been changed, <br />you will have to re-activate your forum membership before you can use the forum again."
Const strTxtYouShouldReceiveAReactivateEmail = "An activation email will be sent in a few moments to the email address in your profile.<br />Click on the link in this email to re-activate your Forum Membership."


'Preview signature window
'---------------------------------------------------------------------------------
Const strTxtSignaturePreview = "Signature Preview"
Const strTxtPostedMessage = "Posted Message"



'New from version 7
'---------------------------------------------------------------------------------

Const strTxtMemberlist = "Memberlist"
Const strTxtForums = "Forum(s)"
Const strTxtOurUserHavePosted = "Our users have posted"
Const strTxtInTotalThereAre = "In total there are"
Const strTxtOnLine = "online"
Const strTxtWeHave = "We have"
Const strTxtActivateAccount = "Activate Account"
Const strTxtSorryYouDoNotHavePermissionToPostInTisForum = "Sorry, you do not have permission to post new topics in this forum"
Const strTxtSorryYouDoNotHavePerimssionToReplyToPostsInThisForum = "Sorry, you do not have permission to reply to posts in this forum"
Const strTxtSorryYouDoNotHavePerimssionToReplyIPBanned = "Sorry, you can not reply to posts, your IP address or range is not permitted.<br />Please contact the forum administrator if you feel this is in error."
Const strTxtLoginSm = "login"
Const strTxtYourProfileHasBeenUpdated = "Your profile has been updated."
Const strTxtPosted = "Posted:"
Const strTxtBackToTop = "Back to Top"
Const strTxtNewPassword = "New Password"
Const strTxtRetypeNewPassword = "Retype New Password"
Const strTxtRegards = "Regards"
Const strTxtClickTheLinkBelowToUnsubscribe = "If you no-longer wish to recieve email notification for this Topic or Forum click on the link below "
Const strTxtPostsPerDay = "posts per day"
Const strTxtGroup = "Group"
Const strTxtLastVisit = "Last Visit"
Const strTxtPrivateMessage = "Private Message"
Const strTxtSorryFunctionNotPermiitedIPBanned = "Sorry, this function is not available as you are using an IP address or range that is not permitted.<br />Please contact the forum administrator if you feel this is in error."
Const strTxtEmailAddressBlocked = "Sorry, the email address or domain entered has been blocked by the forum administrator"
Const strTxtTopicAdmin = "Topic Admin"
Const strTxtMovePost = "Move Post"
Const strTxtPrevTopic = "Prev Topic"
Const strTxtTheMemberHasBeenDleted = "The Member has been Deleted."
Const strTxtThisPageWasGeneratedIn = "This page was generated in"
Const strTxtSeconds = "seconds."
Const strTxtEditBy = "Edited by"
Const strTxtWrote = "wrote"
Const strTxtEnable = "Enable"
Const strTxtToFormatPosts = "to format post"
Const strTxtFlashFilesImages = "Flash Files/Images"
Const strTxtSessionIDErrorCheckCookiesAreEnabled = "A security error has occurred with authentication.<br>Please ensure that all cookies are enabled on your web browser, and you are not using a saved or cached copy of the page."
Const strTxtName = "Name"
Const strTxtModerators = "Moderators"
Const strTxtMore = "more..."
Const strTxtNewRegSuspendedCheckBackLater = "Sorry, new registrations are currently suspended, please check back again later."
Const strTxtMoved = "Moved: "
Const strTxtNoNameError = "Name \t\t- Enter your name"
Const strTxtHelp = "Help"

'PM system
'---------------------------------------------------------------------------------
Const strTxtPrivateMessenger = "Private Messenger"
Const strTxtUnreadMessage = "Unread message"
Const strTxtReadMessage = "Read message"
Const strTxtNew = "new"
Const strTxtYouHave = "You have"
Const strTxtNewMsgsInYourInbox = "new message(s) in your inbox!"
Const strTxtNoneSelected = "None Selected"
Const strTxtAddBuddy = "Add Buddy"


'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtSelectMember = "Select Member"
Const strTxtSelect = "Select"
Const strTxtNoMatchesFound = "No matches found"


'active_topics.asp
'---------------------------------------------------------------------------------
Const strTxtLastFourHours = "Last 4 hours"
Const strTxtLastSixHours = "Last 6 hours"
Const strTxtLastEightHours = "Last 8 hours"
Const strTxtLastTwelveHours = "Last 12 hours"
Const strTxtLastSixteenHours = "Last 16 hours"


'permissions
'---------------------------------------------------------------------------------
Const strTxtYou = "You"
Const strTxtCan = "can"
Const strTxtCannot = "cannot"
Const strTxtpostNewTopicsInThisForum = "post new topics in this forum"
Const strTxtReplyToTopicsInThisForum = "reply to topics in this forum"
Const strTxtEditYourPostsInThisForum = "edit your posts in this forum"
Const strTxtDeleteYourPostsInThisForum = "delete your posts in this forum"
Const strTxtCreatePollsInThisForum = "create polls in this forum"
Const strTxtVoteInPOllsInThisForum = "vote in polls in this forum"


'register.asp
'---------------------------------------------------------------------------------
Const strTxtRegistrationDetails = "Registration Details"
Const strTxtProfileInformation = "Profile Information (not required)"
Const strTxtForumPreferences = "Forum Preferences"
Const strTxtICQNumber = "ICQ Number"
Const strTxtAIMAddress = "AIM Address"
Const strTxtMSNMessenger = "MSN Messenger"
Const strTxtYahooMessenger = "Yahoo Messenger"
Const strTxtOccupation = "Occupation"
Const strTxtInterests = "Interests"
Const strTxtDateOfBirth = "Date of Birth"
Const strTxtNotifyMeOfReplies = "Notify me of replies to posts"
Const strTxtSendsAnEmailWhenSomeoneRepliesToATopicYouHavePostedIn = "Sends an email when someone replies to a topic you have posted in. This can be changed whenever you post."
Const strTxtNotifyMeOfPrivateMessages = "Notify me by email when I receive a Private Message"
Const strTxtAlwaysAttachMySignature = "Always attach my signature to posts"
Const strTxtEnableTheWindowsIEWYSIWYGPostEditor = "Enable the WYSIWYG post editor <br /><span class=""smText"">Only browsers that are detected as being Rich Text Enabled will have this feature available when posting.</span>"
Const strTxtTimezone = "Time offset from forum time"
Const strTxtPresentServerTimeIs = "Present server date and time is: "
Const strTxtDateFormat = "Date Format"
Const strTxtDayMonthYear = "Day/Month/Year"
Const strTxtMonthDayYear = "Month/Day/Year"
Const strTxtYearMonthDay = "Year/Month/Day"
Const strTxtYearDayMonth = "Year/Day/Month"
Const strTxtHours = "hours"
Const strTxtDay = "Day"
Const strTxtCMonth = "Month"
Const strTxtCYear = "Year"
Const strTxtRealName = "Real Name"
Const strTxtMemberTitle = "Member Title"


'Polls
'---------------------------------------------------------------------------------
Const strTxtCreateNewPoll = "Create New Poll"
Const strTxtPollQuestion = "Poll Question"
Const strTxtPollChoice = "Poll Choice"
Const strTxtErrorPollQuestion = "Poll Question \t- Enter a Question for this Poll"
Const strTxtErrorPollChoice = "Poll Choice \t- Enter a least two choices for this Poll"
Const strTxtSorryYouDoNotHavePermissionToCreatePollsForum = "Sorry, you do not have permission to create polls in this forum"
Const strTxtAllowMultipleVotes = "Allow Multiple Votes in this Poll"
Const strTxtMakePollOnlyNoReplies = "Make Poll only (no replies allowed)"
Const strTxtYourNoValidPoll = "Your Poll did not contain a valid Question or Choices."
Const strTxtPoll = "Poll:"
Const strTxtVote = "Vote"
Const strTxtVotes = "Votes"
Const strTxtCastMyVote = " Cast My Vote"
Const strTxtPollStatistics = "Poll Statistics"
Const strTxtThisTopicIsClosedNoNewVotesAccepted = "This topic is closed, no new votes accepted"
Const strTxtYouHaveAlreadyVotedInThisPoll = "You have already voted in this poll"
Const strTxtThankYouForCastingYourVote = "Thank-you for casting your vote."
Const strsTxYouCanNotNotVoteInThisPoll = "You can not vote in this poll"
Const strTxtYouDidNotSelectAChoiceForYourVote = "Sorry your vote was not cast.\n\nYou did not select a Poll Choice to vote for."
Const strTxtThisIsAPollOnlyYouCanNotReply = "This is a Poll only, you can not post a reply."


'Email Notify
'---------------------------------------------------------------------------------
Const strTxtWatchThisTopic = "Watch this topic for replies"
Const strTxtUn = "Un-"
Const strTxtWatchThisForum = "Watch this forum for new posts"
Const strTxtYouAreNowBeNotifiedOfPostsInThisForum = "You will now be notified by email of all Posts in this Forum.\n\nTo un-watch this forum click on the \'Un-Watch this forum for new posts\' link at the bottom of the page."
Const strTxtYouAreNowNOTBeNotifiedOfPostsInThisForum = "You will now not be notified by email of Posts in this Forum.\n\nTo re-watch this forum click on the \'Watch this forum for new posts\' link at the bottom of the page."
Const strTxtYouWillNowBeNotifiedOfAllReplies = "You will now be notified by email of all Replies in this Topic.\n\nTo un-watch this topic click on the \'Un-Watch this topic for replies\' link at the bottom of the page."
Const strTxtYouWillNowNOTBeNotifiedOfAllReplies = "You will now not be notified by email of Replies in this Topic.\n\nTo re-watch this topic click on the \'Watch this topic for replies\' link at the bottom of the page."


'email_messenger.asp
'---------------------------------------------------------------------------------
Const strTxtEmailMessenger = "Email Messenger"
Const strTxtRecipient = "Recipient"
Const strTxtNoHTMLorForumCodeInEmailBody = "Please note that the email is sent in plain text only (no HTML or forum codes).<br /><br />The return email address is set as your own."
Const strTxtYourEmailHasBeenSentTo = "Your Email has been sent to"
Const strTxtYouCanNotEmail = "You can not email"
Const strTxtYouDontHaveAValidEmailAddr = "you do not have a valid email address in your profile."
Const strTxtTheyHaveChoosenToHideThierEmailAddr = "they have chosen to hide their email address."
Const strTxtTheyDontHaveAValidEmailAddr = "they do not have a valid email address in their profile."
Const strTxtSendACopyOfThisEmailToMyself = "Send a copy of this email to myself"
Const strTxtTheFollowingEmailHasBeenSentToYouBy = "The following email has been sent to you by"
Const strTxtFromYourAccountOnThe = "from your account on the"
Const strTxtIfThisMessageIsAbusive = "If this message is spam or you find offensive please contact the webmaster of the forum at the following address"
Const strTxtIncludeThisEmailAndTheFollowing = "Include this email and the following"
Const strTxtReplyToEmailSetTo = "Please note that the reply address to this email has been set to that of"
Const strTxtMessageSent = "Message sent"


'Uploads
'---------------------------------------------------------------------------------
Const strTxtImageUpload = "Image Upload"
Const strTxtFileUpload = "File Upload"
Const strTxtAvatarUpload = "Upload Avatar"
Const strTxtUpload = "Upload"

Const strTxtImage = "Image"
Const strTxtImagesMustBeOfTheType = "Images must be of the type"
Const strTxtAndHaveMaximumFileSizeOf = "and have a maximum file size of"
Const strTxtImageOfTheWrongFileType = "The image uploaded is of the wrong file type"
Const strTxtImageFileSizeToLarge = "The image file size is to large at"
Const strTxtMaximumFileSizeMustBe = "Maximum file size must be"

Const strTxtFile = "File"
Const strTxtFilesMustBeOfTheType = "Files must be of the type"
Const strTxtFileOfTheWrongFileType = "The file uploaded is of the wrong file type"
Const strTxtFileSizeToLarge = "The file size is to large at"


Const strTxtPleaseWaitWhileFileIsUploaded = "Please be patient while the file is being uploaded to the server."
Const strTxtPleaseWaitWhileImageIsUploaded = "Please be patient while the image is being uploaded to the server."


'forum_closed.asp
'---------------------------------------------------------------------------------
Const strTxtForumClosed = "Forums Closed"
Const strTxtSorryTheForumsAreClosedForMaintenance = "Sorry, the forums are presently closed for maintenance.<br />Please check back again later."


'report_post.asp
'---------------------------------------------------------------------------------
Const strTxtReportPost = "Report Post"
Const strTxtSendReport = "Send Report"
Const strTxtProblemWithPost = "Problem With Post"
Const strTxtPleaseStateProblemWithPost = "Please state below the issue with the post, a copy of the post will be emailed to the forum moderators and/or forum administrators so they can deal with it appropriately."
Const strTxtTheFollowingReportSubmittedBy = "The following report has been submitted by"
Const strTxtWhoHasTheFollowingIssue = "who has the following issue with this post"
Const strTxtToViewThePostClickTheLink = "To view the post then click on the link below"
Const strTxtIssueWithPostOn = "Issue With Post on"
Const strTxtYourReportEmailHasBeenSent = "Your email has been sent to the forum moderators and/or forum administrators so they can deal with it appropriately."


'New from version 7.5
'---------------------------------------------------------------------------------
Const strTxtImportantTopics = "Important Topics"
Const strTxtQuickLogin = "Quick Login"
Const strTxtThisTopicWasStarted = "This topic was started: "
Const strTxtResendActivationEmail = "Resend Activation Email"
Const strTxtNoOfStars = "Number of Stars"
Const strTxtOnLine2 = "Online"
Const strTxtIeSpellNotDetected = "ieSpell not detected. Click OK to go to download page."
Const strTxtstrTxtOrderedList = "Ordered List"
Const strTxtTextColour = "Text Colour"
Const strTxtstrSpellCheck = "Spell Check"
Const strTxtCode = "Code"
Const strTxtCodeandFixedWidthData = "Code and Fixed Width Data"
Const strTxtQuoting = "Quoting"
Const strTxtMyCodeData = "My Code or Fixed Width Data"
Const strTxtQuotedMessage = "Quoted Message"
Const strTxtWithUsername = "With Username"
Const strTxtOK = "OK"
Const strTxtGo = "Go"
Const strTxtDataBasedOnActiveUsersInTheLastXMinutes = "This data is based on users active over the past ten minutes"
Const strTxtSoftwareVersion = "Software Version"
Const strTxtForumMembershipNotAct = "Your Forum Membership is not yet activated!"
Const strTxtMustBeRegisteredToPost = "Sorry, you must be a registered forum member in order to post in this forum."
Const strTxtSettings = "Settings"
Const strTxtMemberCPMenu = "Member Control Panel Menu"
Const strTxtYouCanAccessCP = "You can access forum features and change your Forum Membership preferences through your Forum "
Const strTxtEditMembersSettings = "Edit This Members Forum Settings"
Const strTxtSecurityCodeConfirmation = "Security Code Confirmation (required)"
Const strTxtUniqueSecurityCode = "Unique Security Code"
Const strTxtConfirmSecurityCode = "Confirm Security Code* "
Const strTxtEnter6DigitCode = "Please enter the 6 digit code shown above in image format.<br />Only numbers are allowed, a '0' is a numerical zero."
Const strTxtErrorSecurityCode = "Security Code \t- You must enter the 6 digit security code"
Const strTxtSecurityCodeDidNotMatch = "Sorry, the Security Code entered did not match that displayed.\n\nA new Security Code has been generated.\n\nPlease ensure that cookies are enabled on your browser."
Const strTxtCookiesMustBeEnabled = "Cookies must be enabled on your web browser to see images."

'login_user_test.asp
'---------------------------------------------------------------------------------
Const strTxtSuccessfulLogin = "Successful Login"
Const strTxtSuccessfulLoginReturnToForum = "Your login was successful, please wait while you are returned to the Forum"
Const strTxtUnSuccessfulLoginText = "Your login was un-successful due to a cookie problem. <br /><br />Please ensure that all cookies are enabled on your web browser for this web site."
Const strTxtUnSuccessfulLoginReTry = "Click here to retry logging into the Forum."
Const strTxtToActivateYourForumMem = "To activate your forum membership click on the link in the activation email you should have received after registering."

'email_notify_subscriptions.asp
'---------------------------------------------------------------------------------
Const strTxtEmailNotificationSubscriptions = "Email Notification Subscriptions"
Const strTxtSelectForumErrorMsg = "Select Forum\t- Select a Forum to Subscribe to"
Const strTxtYouHaveNoSubToEmailNotify = "You have no subscriptions to email notification"
Const strTxtThatYouHaveSubscribedTo = "that you have Subscribed to Email Notification"
Const strTxtUnsusbribe = "Unsubscribe"
Const strTxtAreYouWantToUnsubscribe = "Are you sure you want to Un-subscribe from these"



'RTE editor
'---------------------------------------------------------------------------------
Const strTxtFontStyle = "Font Style"
Const strTxtBackgroundColour = "Background Colour"
Const strTxtUndo = "Undo"
Const strTxtRedo = "Redo"
Const strTxtJustify = "Justify"
Const strTxtToggleHTMLView = "Toggle HTML View"
Const strTxtAboutRichTextEditor = "About Rich Text Editor"
Const strTxtImageURL = "Image URL"
Const strTxtAlternativeText = "Alternative Text"
Const strTxtLayout = "Layout"
Const strTxtAlignment = "Alignment"
Const strTxtBorder = "Border"
Const strTxtSpacing = "Spacing"
Const strTxtHorizontal = "Horizontal"
Const strTxtVertical = "Vertical"
Const strTxtRows = "Rows"
Const strTxtColumns = "Columns"
Const strTxtWidth = "Width"
Const strTxtCellPad = "Cell Pad"
Const strTxtCellSpace = "Cell Space"
Const strTxtInsertTable = "Insert Table"


'New from version 7.51
'---------------------------------------------------------------------------------
Const strTxtSubscribeToForum = "Subscribe to Email Notification of New Posts"
Const strTxtSelectForumToSubscribeTo = "Select Forum to Subscribe to"


'New from version 7.7
'---------------------------------------------------------------------------------
Const strTxtOnlineStatus = "Online Status"
Const strTxtOffLine = "Offline"


'New from version 7.8
'---------------------------------------------------------------------------------
Const strTxtConfirmOldPass = "Confirm Old Password"
Const strTxtConformOldPassNotMatching = "The Password Confirmation does not match that held on record for your account.\n\nIf you wish to change your password, please enter your old password correctly."



'New from version 8.0
'---------------------------------------------------------------------------------
Const strTxtSub = "Sub"
Const strTxtHidden = "Hidden"
Const strTxtShowPost = "Show Post"
Const strTxtHidePost = "Hide Post"
Const strTxtAreYouSureYouWantToHidePost = "Are you sure you want to hide this post?"
Const strTxtModeratedPost = "Moderated Post"
Const strTxtYouArePostingModeratedForum = "You are posting in a moderated forum."
Const strTxtBeforePostDisplayedAuthorised = "Before your post will be displayed in the forum it will first need to be authorised by a Forum Administrator or Moderator."
Const strTxtHiddenTopics = "Hidden Topics"
Const strTxtVerifiedBy = "Verified By"
%>