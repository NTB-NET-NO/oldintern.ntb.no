<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_upload.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the timeout of the page
Server.ScriptTimeout =  1000


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Declare variables
Dim strFileTypes	'Holds the file types allowed to be uploaded
Dim intMaxFileSize	'Holds the largest file size
Dim intForumID		'Holds the forum ID
Dim strMessageBoxType	'Holds the message box teype to return to
Dim blnFileUploaded	'Set to true if the file is uploaded
Dim strErrorMessage	'Holds the error emssage if the file is not uploaded
Dim strFileUploadPath	'Holds the path and folder the uploaded files are stored in
Dim saryFileUploadTypes	'Array holding the file types allowed to be uploaed
Dim intFileSize		'Holds the max file size
Dim strUploadComponent	'Holds the upload component used
Dim lngErrorFileSize	'Holds the file size if the file is not saved because it is to large
Dim blnExtensionOK	'Set to false if the extension of the file is not allowed
Dim strImageName	'Holds the image name
Dim strUserFolderName	'Holds the folder name safe username



'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")
End If


'Intiliase variables
blnExtensionOK = True


'read in the forum ID and message box type
intForumID = CInt(Request.QueryString("FID"))
strMessageBoxType = Request.QueryString("MSG")



'Read in the forum name and forum permissions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'Read in wether the forum is locked or not
If NOT rsCommon.EOF Then

	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsCommon("Read")), CInt(rsCommon("Post")), CInt(rsCommon("Reply_posts")), 0, 0, 0, 0, 0, CInt(rsCommon("Attachments")), CInt(rsCommon("Image_upload")))

'Else if there is no forum then they probally haven't come from the upload button so redirect home
Else

	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If


'Reset Server Objects
rsCommon.Close



'Read in the file types that can be uploaded
If blnImageUpload AND blnRead AND (blnPost OR blnReply) Then

	'Initialise the SQL variable with an SQL statement to get the configuration details from the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "SelectConfiguration"
	Else
		strSQL = "SELECT " & strDbTable & "Configuration.* From " & strDbTable & "Configuration;"
	End If

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there be records returned get em
	If NOT rsCommon.EOF Then

		'Read in the image types and size form the database
		'Read in the configuration details from the recordset
		strUploadComponent = rsCommon("Upload_component")
		strFileUploadPath = rsCommon("Upload_img_path")
		saryFileUploadTypes = Split(Trim(strImageTypes), ";")
		strFileTypes = rsCommon("Upload_img_types")
		intMaxFileSize = CInt(rsCommon("Upload_img_size"))
		
		'Replace \ with / in upload path
		strFileUploadPath = Replace(strFileUploadPath, "\", "/", 1, -1, 1)
	End If
End If



'If this is a post back then upload the image
If Request.QueryString("PB") = "Y" Then

	'Place the username in a varible to get the folder name for the user
	strUserFolderName = strLoggedInUsername
		
	'Calculate the folder name safe username for folder
	strUserFolderName = decodeString(strUserFolderName)
	strUserFolderName = characterStrip(strUserFolderName)

	'Call upoload file function
	strImageName = fileUpload(strFileUploadPath, saryFileUploadTypes, intMaxFileSize, strUploadComponent, lngErrorFileSize, blnExtensionOK, strUserFolderName)

End If



'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Image Upload</title>

<!-- Web Wiz Rich Text Editor ver. <% = strRTEversion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own Rich Text Editor then goto http://www.richtexteditor.org -->

<script  language="JavaScript">
<%
'If the image has been saved then place it in the post
If lngErrorFileSize = 0 AND blnExtensionOK = True AND strImageName <> "" Then

	'If this is Rich text editor and IE5.0 use the exeCommand to place it in the editor
	If strMessageBoxType = "RTE" AND RTEenabled = "winIE5" Then
	%>

	window.opener.frames.message.document.focus();
	window.opener.frames.message.document.execCommand('InsertImage', false, '<% = strFileUploadPath & "/" & strUserFolderName & "/" & strImageName %>');
	window.close();
<%

	'If this is Rich text editor use the exeCommand to place it in the editor
	ElseIf strMessageBoxType = "RTE" Then
		%>

	window.opener.document.getElementById("message").contentWindow.focus();
	window.opener.document.getElementById("message").contentWindow.document.execCommand('InsertImage', false, '<% = strFileUploadPath & "/" & strUserFolderName & "/" & strImageName %>');
	window.close();
<%

	'Else if they are using the basic editor place the image path into it
	ElseIf strMessageBoxType = "BSC" Then
		%>
	window.opener.document.frmAddMessage.message.focus();
	window.opener.document.frmAddMessage.message.value += "[IMG]<% = strFileUploadPath & "/" & strUserFolderName & "/" & strImageName %>[/IMG]";
	window.close();
<%
	End If
End If
%>
</script>
<style type="text/css">
<!--
html, body {
  background: ButtonFace;
  color: ButtonText;
  font: font-family: Verdana, Arial, Helvetica, sans-serif;
  font-size: 12px;
  margin: 2px;
  padding: 4px;
}
.text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
}
.error {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
}
legend {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #0000FF;
}
-->
</style>
</head>
<body OnLoad="self.focus(); document.forms.frmImageUp.Submit.disabled=true;"><%

'If the user is allowed to upload then show them the form
If blnImageUpload AND blnRead AND (blnPost OR blnReply) Then	

	%>
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0">
 <form action="upload_images.asp?MSG=<% = strMessageBoxType %>&FID=<% = intForumID %>&PB=Y" method="post" enctype="multipart/form-data" name="frmImageUp" target="_self" id="frmImageUp" onSubmit="alert('<% = strTxtPleaseWaitWhileImageIsUploaded %>')">
  <tr> 
   <td width="100%"> 
    <fieldset>
    <legend><% = strTxtImageUpload %></legend>
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
     <tr> 
      <td width="10%" align="right" class="text"><% = strTxtImage %>:</td>
      <td width="90%"><input name="file" name="id" type="file" size="<% If BrowserType() = "Netscape 4" Then Response.Write("20") Else Response.Write("35") %>" onFocus="document.forms.frmImageUp.Submit.disabled=false;" onClick="document.forms.frmImageUp.Submit.disabled=false;" />
        </td>
     </tr>
     <tr align="center"> 
      <td colspan="2" class="text"><br /><% 
      	
      	'If the file upload has failed becuase of the wrong extension display an error message
	If blnExtensionOK = False Then

		Response.Write("<span class=""error"">" & strTxtImageOfTheWrongFileType & ".<br />" & strTxtImagesMustBeOfTheType & ", "  &  Replace(strFileTypes, ";", ", ", 1, -1, 1) & "</span>")

	'Else if the file upload has failed becuase the size is to large display an error message
	ElseIf lngErrorFileSize <> 0 Then

		Response.Write("<span class=""error"">" & strTxtImageFileSizeToLarge & " " & lngErrorFileSize & "KB.<br />" & strTxtMaximumFileSizeMustBe & " " & intMaxFileSize & "KB</span>")
	
	'Else display a message of the image types that can be uploaded
	Else
      
      		Response.Write(strTxtImagesMustBeOfTheType & ", " & Replace(strFileTypes, ";", ", ", 1, -1, 1) & ", " & strTxtAndHaveMaximumFileSizeOf & " " & intMaxFileSize & "KB") 
      	
      	End If
      %></td>
     </tr>
    </table>
    </fieldset></td>
  </tr>
  <tr align="right"> 
   <td> <input type="submit" name="Submit" value="     <% = strTxtOK %>     "> &nbsp; <input type="button" name="cancel" value=" <% = strTxtCancel %> " onClick="window.close()"></td>
  </tr>
 </form>
</table><%

End If

%>
</body>
</html>