<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true
Response.Buffer = True



'Dimension variables
Dim strNewUsername                 'Holds the users username
Dim strMemberName	'Holds the member name to lookup
Dim intErrorCode	'Holds the error code if user not found
Dim lngMemberID		'Holds the ID number of the member
Dim blnMemNotFound	'Holds the error code if user not found
Dim blnUserNameOK	'Holds if the username is OK
Dim blnMemNameUpdated	'Set to true if the member name is updated

'Initiliase variables
blnMemNotFound = false
blnMemNameUpdated = false
blnUserNameOK = true


'If this is a postback check for the user exsisting in the db before redirecting
If Request.Form("postBack") Then
	
		
	
	'Read in the members name to lookup
	strMemberName = Trim(Mid(Request.Form("member"), 1, 20))
	strNewUsername = Trim(Mid(Request.Form("newMember"), 1, 15))
	
	'Take out parts of the username that are not permitted
	strMemberName = disallowedMemberNames(strMemberName)
	
	'Make sure the user has not entered disallowed usernames
        If InStr(1, strNewUsername, "admin", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "password", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "salt", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "author", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "code", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "username", vbTextCompare) Then blnUsernameOK = False
        If InStr(1, strNewUsername, "N0act", vbTextCompare) Then blnUsernameOK = False
	
	'Get rid of milisous code
	strMemberName = formatSQLInput(strMemberName)
	strNewUsername = formatSQLInput(strNewUsername)
	
	
	'Check new username isn't already taken
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Author_ID From " & strDbTable & "Author WHERE " & strDbTable & "Author.Username='" & strNewUsername & "';"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'See if a user with that name is returned by the database
	If NOT rsCommon.EOF Then blnUserNameOK = false
	
	'Reset Server Objects
	rsCommon.Close

	'If the username is OK then get the user to change
	If blnUserNameOK Then
		
		'Initalise the strSQL variable with an SQL statement to query the database
		strSQL = "SELECT " & strDbTable & "Author.Username From " & strDbTable & "Author WHERE " & strDbTable & "Author.Username='" & strMemberName & "';"
		
		'Set the Lock Type for the records so that the record set is only locked when it is updated
		rsCommon.LockType = 3

		'Set the Cursor Type to dynamic
		rsCommon.CursorType = 2
		
		'Query the database
		rsCommon.Open strSQL, adoCon
		
		'See if a user with that name is returned by the database
		If NOT rsCommon.EOF Then
			
			'Update the recordset
			rsCommon.Fields("UserName") = strNewUsername

			'Update the database with the new user's details
			rsCommon.Update
		
			'Set to true if the member name is updated
			blnMemNameUpdated = true
		
		'Else there is no user with that name returned so set an error code
		Else
		
			blnMemNotFound = true	
			
		End If
		
		'Reset Server Objects
		rsCommon.Close
	End If


End If


'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Change Username </title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Check for a group
	if (document.frmAddMessage.member.value==""){
		alert("Please enter a members username");
		return false;
	}
	
	return true
}


//Function to open pop up window
function openWin(theURL,winName,features) {
  	window.open(theURL,winName,features);
}
// -->
</script>
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center" class="text"><span class="heading">Change Username </span><br />
 <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
 <br>
 From here you can change the username of your forum members.
 <p class="text">Select the Forum Member that you would like to change the username for. </p>
 <p></p>
</div>
<form action="change_username.asp" method="post" name="frmAddMessage" target="_self" id="frmAddMessage" onSubmit="return CheckForm();">
 <tr>
  <td width="500"><br> 
   <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000">
    <tr>
   <td width="500"> <table width="100%" border="0" align="center" class="normal" cellpadding="4" cellspacing="1">
       <tr bgcolor="#CCCEE6"> 
        <td class="tHeading"><b> Select a Member</b></td>
       </tr>
       <tr bgcolor="#FFFFFF">
        <td bgcolor="#F5F5FA" class="text">Old Username&nbsp;         <input name="member" type="text" id="member" size="20" maxlength="20" value="<% If blnMemNameUpdated = false Then Response.Write(strMemberName) %>">
         <input type="button" name="Button" value="Search for Member" onClick="openWin('../pop_up_member_search.asp','profile','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=1,width=440,height=255')"></td>
       </tr>
       <tr bgcolor="#FFFFFF">
        <td bgcolor="#CCCEE6" class="tHeading">Enter new Username for Member</td>
       </tr>
       <tr bgcolor="#FFFFFF">
        <td bgcolor="#F5F5FA" class="text">New Username 
        <input name="newMember" type="text" id="newMember" size="20" maxlength="15" value="<% If blnMemNameUpdated = false Then Response.Write(strNewUsername) %>"></td>
       </tr>
      </table></td>
  </tr>
 </table>
 <div align="center"><span class="text"><br>
  <input type="hidden" name="postBack" value="true" />
  <input type="submit" name="Submit" value="Change Username">
 </span><br />
    <br />
 </div>
</form><%

'If the username can not be found display an error message pop-up
If blnMemNotFound  Then
        Response.Write("<script  language=""JavaScript"">")
        Response.Write("alert('The Username entered could not be found.\n\nPlease check your spelling and try again.');")
        Response.Write("</script>")

End If 

'If the username is already gone display an error message pop-up
If blnUserNameOK = false  Then
        Response.Write("<script  language=""JavaScript"">")
        Response.Write("alert('Sorry, the Username you requested is already taken.\n\nPlease choose another Username.');")
        Response.Write("</script>")

End If 

'If the username is already gone display an error message pop-up
If blnMemNameUpdated Then
        Response.Write("<script  language=""JavaScript"">")
        Response.Write("alert('The member \'" & strMemberName & "\' has had their username changed to \'" & strNewUsername & "\'.');")
        Response.Write("</script>")

End If 

%>
</body>
</html>