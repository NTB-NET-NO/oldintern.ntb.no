<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the response buffer to true
Response.Buffer = True


'Dimension variables
Dim rsAdminDetails	'recordset holding the admin details
Dim strMode		'holds the mode of the page, set to true if changes are to be made to the database
Dim strEncyptedPassword	'Holds the new password
Dim blnUserNameOK	'Set to ture if the Name is not already in the database
Dim strCheckUserName	'Holds the Name from the database that we are checking against
Dim blnUpdated		'Set to true if the username and password are updated


'Initialise variables
blnUserNameOK = True
blnUpdated = False

'Redirect if this is not the main forum account
If lngLoggedInUserID <> 1 Then

	'Reset Server Objects
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	Response.Redirect("admin_menu.asp")
End If




'Read in the users details from the form
strMode = Request.Form("mode")


'If the user is changing there username and password then update the database
If strMode = "postBack" Then

	'Read in the userName and password from the form
	strUserName = Trim(Mid(Request.Form("userName"), 1, 15))
	strPassword = LCase(Trim(Mid(Request.Form("password"), 1, 15)))

	'If there is no userName entered then don't save
	If strUserName = "" Then blnUserNameOK = False

	'Make sure the user has not entered disallowed userNames
	If InStr(1, strUserName, "password", vbTextCompare) Then blnUserNameOK = False
	If InStr(1, strUserName, "author", vbTextCompare) Then blnUserNameOK = False
	If InStr(1, strUserName, "code", vbTextCompare) Then blnUserNameOK = False
	If InStr(1, strUserName, "userName", vbTextCompare) Then blnUserNameOK = False
	If InStr(1, strUserName, "N0act", vbTextCompare) Then blnUserNameOK = False

	'Clean up user input
        strUserName = formatSQLInput(strUserName)

	'Intialise the ADO recordset object
	Set rsCommon = Server.CreateObject("ADODB.Recordset")

	'Read in the userNames from the database to check the userName does not alreday exsist
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.UserName FROM " & strDbTable & "Author WHERE " & strDbTable & "Author.UserName = '" & strUserName & "' AND NOT " & strDbTable & "Author.Author_ID = 1;"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'If there is a record returned then the userName is already in use
	If NOT rsCommon.EOF Then blnUserNameOK = False

	'Remove SQL safe single quote double up set in the format SQL function
        strUserName = Replace(strUserName, "''", "'", 1, -1, 1)


	'Clean up
	rsCommon.Close

	'If the UserName dose not already exsists then save the users details to the database
	If blnUserNameOK Then


		'Only encrypt password if this is enabled
		If blnEncryptedPasswords Then
			
			'Generate new salt
	                strSalt = getSalt(Len(strPassword))
	
	                'Concatenate salt value to the password
	                strEncyptedPassword = strPassword & strSalt
	
	                'Re-Genreate encypted password with new salt value
	                strEncyptedPassword = HashEncode(strEncyptedPassword)
		
		'Else the password is not set to be encrypted so place the un-encrypted password into the strEncyptedPassword variable
		Else
			strEncyptedPassword = strPassword
		End If


		'Intialise the strSQL variable with an SQL string to open a record set for the Author table
		strSQL = "SELECT " & strDbTable & "Author.UserName,  " & strDbTable & "Author.Password, " & strDbTable & "Author.Salt, " & strDbTable & "Author.User_code "
		strSQL = strSQL & "From " & strDbTable & "Author "
		strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID=1;"


		'Set the Lock Type for the records so that the record set is only locked when it is updated
		rsCommon.LockType = 3

		'Set the Cursor Type to dynamic
		rsCommon.CursorType = 2

		'Open the author table
		rsCommon.Open strSQL, adoCon

		'Randomise the system timer
		Randomize Timer

		'Calculate a code for the user
                strUserCode = userCode(strUserName)

		With rsCommon
			'Update the recordset
			.Fields("UserName") = strUserName
			.Fields("Password") = strEncyptedPassword
			.Fields("Salt") = strSalt
			.Fields("User_code") = strUserCode

			'Update the database with the new user's details
			.Update

			'Re-run the NewUser query to read in the updated recordset from the database
			.Requery

			'Write a cookie with the User ID number so the user logged in throughout the forum
			'Write the cookie with the Name Forum containing the value UserID number
			Response.Cookies(strCookieName)("UID") = strUserCode

			'Read back in the new userName
			strUserName = rsCommon("UserName")

			'Clean up
			.Close
		End With
		
		'Set the update field to true
		blnUpdated = True

	End If
End If

'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>
<meta Name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Change Admin Username &amp; Password</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Initialise variables
        var errorMsg = "";
        var errorMsgLong = "";

	//Check for a username
        if (document.frmChangePassword.userName.value.length <= 3){
                errorMsg += "\n\tUserName \t- Your Username must be at least 4 characters";
        }

        //Check for a password
        if (document.frmChangePassword.password.value.length <= 3){
                errorMsg += "\n\tPassword \t- Your Password must be at least 4 characters";
        }

        //Check both passwords are the same
        if ((document.frmChangePassword.password.value) != (document.frmChangePassword.password2.value)){
                errorMsg += "\n\tPassword Error\t- The passwords entered do not match";
                document.frmChangePassword.password.value = "";
                document.frmChangePassword.password2.value = "";
        }

        //If there is a problem with the form then display an error
	if ((errorMsg != "") || (errorMsgLong != "")){
		msg = "_________________________________________________________________\n\n";
		msg += "The form has not been submitted because there are problem(s) with the form.\n";
		msg += "Please correct the problem(s) and re-submit the form.\n";
		msg += "_________________________________________________________________\n\n";
		msg += "The following field(s) need to be corrected: -\n";

		errorMsg += alert(msg + errorMsg + "\n" + errorMsgLong);
		return false;
	}

	return true;
}
// -->
</script>
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center">
 <p class="text"><span class="heading">Change Admin Username &amp; Password</span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br />
  Make sure you <b>remember</b> the new<b> username</b> and <b>password</b> <br />
  as you <b>will not</b> be able to Login or <b>Administer the Forum without them</b>!!!<br>
  <br>
  Passwords are one way 160bit encrypted and so can NOT be retrieved.<br />
 </p>
</div><%

If blnUserNameOK = False Then
%>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
 <tr>
  <td align="center" class="lgText">Sorry the Username you requested is already taken.<br />
   Please choose another Username.</td>
 </tr>
</table><%

End If


If blnUpdated Then
%>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
 <tr>
  <td align="center" class="lgText">Your Username and/or Password have been updated.</td>
 </tr>
</table><%

End If
%>
<form method="post" Name="frmChangePassword" action="change_admin_username.asp" onSubmit="return CheckForm();">
 <table width="350" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000" height="30">
  <tr>
   <td height="2" width="483" align="center"> <table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#000000">
     <tr>
      <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr bgcolor="#F5F5FA"> 
         <td width="40%" align="right" class="text">Username:&nbsp;&nbsp;</td>
         <td width="60%"> 
          <input type="text" Name="userName" size="15" maxlength="15" value="<% = strUserName %>"> </td>
        </tr>
        <tr bgcolor="#F5F5FA"> 
         <td width="40%" align="right" class="text">Password:&nbsp; </td>
         <td width="60%"> 
          <input type="password" Name="password" size="15" maxlength="15"> </td>
        </tr>
        <tr bgcolor="#F5F5FA"> 
         <td width="40%" align="right" class="text">Confirm Password:&nbsp; </td>
         <td width="60%"> 
          <input type="password" Name="password2" size="15" maxlength="15"> </td>
        </tr>
        <tr bgcolor="#F5F5FA"> 
         <td width="40%" align="right"> 
          <input type="hidden" Name="mode" value="postBack"> </td>
         <td width="60%"> 
          <input type="submit" Name="Submit" value="Update Details"> <input type="reset" Name="Reset" value="Clear"> </td>
        </tr>
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
</form>
<br />
</body>
</html>