<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting and setting a cookie
Response.Buffer = True 


'Dimension variables
Dim strUserCode			'Holds a code for the user
Dim lngUserID			'Holds the new users ID number
Dim blnActivated		'Set to true if the account is activated
Dim intForumID			'Holds the ID number of teh forum

blnActivated = False

'Read in the users ID from the query string
strUserCode = Trim(Mid(Request.QueryString("ID"), 1, 44))

'Make the usercode SQL safe
strUserCode = formatSQLInput(strUserCode)


'If therese a usercode activate it
If strUserCode <> "" Then
	
	'Intialise the strSQL variable with an SQL string to open a record set for the Author table
	strSQL = "SELECT " & strDbTable & "Author.* From " & strDbTable & "Author WHERE " & strDbTable & "Author.User_code = '" & strUserCode & "';"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
	
	'Open the author table
	rsCommon.Open strSQL, adoCon
	
	'If these a record returned then alls well so cteate a new id code and activate the membership
	If NOT rsCommon.EOF AND InStr(1, strUserCode, "N0act", vbTextCompare) = 0 Then
		
		'Calculate a new code for the user
		strUserCode = userCode(rsCommon("Username"))

		'Update the database by actvating the users account
		rsCommon.Fields("User_code") = strUserCode
		rsCommon.Fields("Active") = 1
			
		'Update the database with the new user's details
		rsCommon.Update
			
		'Write a cookie with the User ID number so the user logged in throughout the forum	
		'Write the cookie with the name Forum containing the value UserID number
		Response.Cookies(strCookieName)("UID") = strUserCode
		
		'Set the activate boolean to true
		blnActivated = True
	End If
	
	'Release objects
	rsCommon.Close
End If
%>  
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Register New User</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
		
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtActivateAccount %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtActivateAccount %><br /></td>
  </tr>
</table>
<div align="center">
<br />
  <br />
  <br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  <td align="center" class="text"><%
'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing  


'If the account is now active display a message
If blnActivated = True Then
  	
  	Response.Write(strTxtYourForumMemIsNowActive & _
	vbCrLf & "<br /><br />" & strTxtYouCanAccessCP & "<a href=""member_control_panel.asp"" target=""_self"">" & strTxtMemberCPMenu & "</a>") 

'If their account has been suspended and they are trying to reactivate it then tell them they can't
ElseIf InStr(1, strUserCode, "N0act", vbTextCompare) Then
	
	Response.Write(strTxtForumMemberSuspended)


'Theres been a problem so display an error message
Else 
   	Response.Write(strTxtErrorWithActvation & " " & strMainForumName & " " & " <a href=""mailto:" & strForumEmailAddress & """>" & strTxtForumAdministrator & "</a>.")
End If 

Response.Write("<br /><br /><a href=""default.asp"" target=""_self"">" & strTxtReturnToDiscussionForum & "</a>")

%></td>
  </tr>
 </table>
</div> 
<br /><br /><br /><br /><br />
  <div align="center">
<% 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->