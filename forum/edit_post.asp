<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_edit_post.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Rich Text Editor
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Dimension variables
Dim strMode			'Holds the mode of the page
Dim intForumID			'Holds the forum ID number
Dim lngTopicID			'Holds the Topic ID number
Dim strTopicSubject		'Holds the title of the topic
Dim intTopicPriority		'Holds the priority of the topic
Dim lngMessageID		'Holds the message ID to be edited
Dim strQuoteUsername		'Holds the quoters username
Dim strQuoteMessage		'Holds the message to be quoted
Dim lngPostUserID		'Holds the user ID of the user to post the message
Dim blnForumLocked		'Set to true if the forum is locked
Dim blnEmailNotify		'Set to true if the users want to be notified by e-mail of a post
Dim strPostPage 		'Holds the page the form is posted to
Dim intRecordPositionPageNum	'Holds the recorset page number to show the Threads for
Dim strMessage			'Holds the post message
Dim strForumName		'Holds the name of the forum
Dim intIndexPosition		'Holds the idex poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns
Dim blnTopicLocked		'Holds if the topic is locked or not


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If





'Read in the message ID number to edit
intForumID = CInt(Request.QueryString("FID"))
lngMessageID = CLng(Request.QueryString("PID"))
intRecordPositionPageNum = CInt(Request.QueryString("TPN"))
strMode = Trim(Mid(Request.QueryString("M"), 1, 2))

'Set the page mode
If strMode = "Q" Then 
	strMode = "quote"
ElseIf strMode = "R" Then 
	strMode = "reply"
Else
	strMode = "edit"
End If

	
'Get the message from the database
If strMode <> "reply" Then	
	'Initalise the strSQL variable with an SQL statement to query the database get the message details
	strSQL = "SELECT " & strDbTable & "Topic.Locked, " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Forum_ID, " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Priority, " & strDbTable & "Topic.Start_date, " & strDbTable & "Topic.Locked, " & strDbTable & "Thread.Author_ID, " & strDbTable & "Thread.Message, " & strDbTable & "Thread.Message_date, " & strDbTable & "Author.Username "
	strSQL = strSQL & "FROM " & strDbTable & "Topic, " & strDbTable & "Thread, " & strDbTable & "Author " 
	strSQL = strSQL & "WHERE (" & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID AND " & strDbTable & "Thread.Author_ID = " & strDbTable & "Author.Author_ID) AND " & strDbTable & "Thread.Thread_ID=" & lngMessageID & ";"
		
	'Query the database
	rsCommon.Open strSQL, adoCon 
		
		
	'Read in the details from the recordset
	blnTopicLocked = CBool(rsCommon("Locked"))
	lngTopicID = CLng(rsCommon("Topic_ID"))
	intForumID = CInt(rsCommon("Forum_ID"))
	lngPostUserID = CLng(rsCommon("Author_ID"))
	strQuoteUsername = rsCommon("Username")
	strTopicSubject = rsCommon("Subject")
	intTopicPriority = CInt(rsCommon("Priority"))
	'If the forums not locked check that the topics not locked either
	If blnForumLocked = False Then blnForumLocked = CBool(rsCommon("Locked"))
		
	
	
	
	
	'If this is a post being edited format the post and check dates
	If strMode = "edit" Then
	
		'Read in message from rs
		strMessage = rsCommon("Message")
	
		'Convert message back to forum codes and text
		strMessage = EditPostConvertion(strMessage)
		
		'If the start topic date and the message date are the same then the user can edit the topic title
		If rsCommon("Start_date") = rsCommon("Message_date") Then strMode = "editTopic"
	
	
	'Else for quote messages
	ElseIf strMode = "quote" Then
	
		'Read in the message from rs
		strQuoteMessage = rsCommon("Message")
	
	End If
	
	
	
	
	'Clean up
	rsCommon.Close
	
	
	
	
	
	
	'If this is a quoted message read in any further details and format the post
	If strMode = "quote" Then
		
		'If the post being quoted is written by a guest see if they have a name
		If lngPostUserID = 2 Then
			
			'Initalise the strSQL variable with an SQL statement to query the database
			If strDatabaseType = "SQLServer" Then
				strSQL = "EXECUTE " & strDbProc & "GuestPoster @lngThreadID = " & lngMessageID
			Else
				strSQL = "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = " & lngMessageID & ";"
			End If
				
			'Query the database
			rsCommon.Open strSQL, adoCon
			
			'Read in the quoters name	
			If NOT rsCommon.EOF Then strQuoteUsername = rsCommon("Name")
				
			'Close recordset
			rsCommon.Close
		End If
		
		
		'Build up the quoted thread post
		strMessage = " [QUOTE=" & strQuoteUsername & "] " 
		
		'Read in the quoted thread from the recordset
		strMessage = strMessage & strQuoteMessage
		
		
		'Convert the signature back to original format
		strMessage = EditPostConvertion (strMessage)
		
		'Place the forum code for closing quote at the end	
		strMessage = strMessage & "[/QUOTE] "
	
	End If

'Else change the mode from reply to quote
Else

	strMode = "quote"	

End If
	







'Read in the forum permissions from the database
'Initalise the strSQL variable with an SQL statement to query the database
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
Else
	strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returned by the recordset then check to see if you need a password to enter it
If NOT rsCommon.EOF Then
	
	'Read in forum details from the database
	strForumName = rsCommon("Forum_name")
	
	'Read in wether the forum is locked or not
	blnForumLocked = CBool(rsCommon("Locked"))
	
	'Check the user is welcome in this forum
	Call forumPermisisons(intForumID, intGroupID, CInt(rsCommon("Read")), CInt(rsCommon("Post")), CInt(rsCommon("Reply_posts")), CInt(rsCommon("Edit_posts")), CInt(rsCommon("Delete_posts")), CInt(rsCommon("Priority_posts")), 0, 0, CInt(rsCommon("Attachments")), CInt(rsCommon("Image_upload")))
	
	'If the forum requires a password and a logged in forum code is not found on the users machine then send them to a login page
	If NOT rsCommon("Password") = "" and NOT Request.Cookies(strCookieName)("Forum" & intForumID) = rsCommon("Forum_code") Then
		
		'Reset Server Objects
		rsCommon.Close
		Set rsCommon = Nothing 
		Set adoCon = Nothing
		Set adoCon = Nothing		
		
		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect "forum_password_form.asp?FID=" & intForumID
	End If
End If

'Reset server object
rsCommon.Close


'If the forum level for the user on this forum is read only set the forum to be locked
If (blnRead = False AND blnModerator = False AND blnAdmin = False) Then blnForumLocked = True


%>
<html> 
<head>
<title><% If strMode = "quote" Then Response.Write(strTxtPostReply) Else Response.Write(strTxtEditPost) %></title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001-2004
    	 If you want your forum then goto http://www.webwizforums.com --> 

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {
	
	var errorMsg = "";
<%
'If Gecko Madis API (RTE) need to strip default input from the API
If RTEenabled = "Gecko" Then Response.Write("	//For Gecko Madis API (RTE)" & vbCrLf & "	if (document.frmAddMessage.message.value.indexOf('<br>') > -1 && document.frmAddMessage.message.value.length == 8) document.frmAddMessage.message.value = '';" & vbCrLf)


'If we are editing the first topic then  check for a subject
If strMode = "editTopic" Then
%>	
	//Check for a subject
	if (document.frmAddMessage.subject.value==""){
		errorMsg += "\n\t<% = strTxtErrorTopicSubject %>";
	}<%
End If

'If this is a guest posting check that they have entered their name
If strMode="quote" And lngLoggedInUserID = 2 Then
%>	
	//Check for a name
	if (document.frmAddMessage.Gname.value==""){
		errorMsg += "\n\t<% = strTxtNoNameError %>";
	}
<%
End If

%>	
	
	//Check for message
	if (document.frmAddMessage.message.value==""){
		errorMsg += "\n\t<% = strTxtNoMessageError %>";
	}
	
	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
		
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
	
	//Reset the submition action
	document.frmAddMessage.action = "post_message.asp?PN=<% = CInt(Request.QueryString("PN")) %>"
	document.frmAddMessage.target = "_self";
<% 
If RTEenabled() <> "false" AND blnRTEEditor AND blnWYSIWYGEditor Then Response.Write(vbCrLf & "	document.frmAddMessage.Submit.disabled=true;")
%>
	
	return true;
}
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr> 
  <td align="left" class="heading"><%  If strMode = "quote" Then Response.Write(strTxtPostReply) Else Response.Write(strTxtEditPost) %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% Response.Write ("<a href=""forum_topics.asp?FID=" & intForumID & """ target=""_self"" class=""boldLink"">" & strForumName & "</a>" & strNavSpacer) %><%  If strMode = "quote" Then Response.Write(strTxtPostReply) Else Response.Write(strTxtEditPost) %><br /></td>
  </tr>
</table><br /><%

 
'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
 
'If the Post is by the logged in user or the adminstrator/moderator then display a form to edit the post
If (((lngLoggedInUserID = lngPostUserID OR blnAdmin OR blnModerator) AND (blnEdit OR blnAdmin) AND (strMode="edit" OR strMode="editTopic")) OR (blnReply = True AND strMode = "quote")) AND blnActiveMember AND (blnForumLocked = False OR blnAdmin) AND (blnTopicLocked = False Or blnAdmin) Then


	'See if the users browser is RTE enabled
	If RTEenabled() <> "false" AND blnRTEEditor = True AND blnWYSIWYGEditor = True Then
					
		'Open the message form for RTE enabled browsers
		%><!--#include file="includes/RTE_message_form_inc.asp" --><%
	Else
		'Open up the mesage form for non RTE enabled browsers
		%><!--#include file="includes/message_form_inc.asp" --><%
	End If


Response.Write(vbCrLf & "  <br />")


'Else if the forum is locked display a message telling the user so
ElseIf blnForumLocked = True Then
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtForumLockedByAdmim & "</span><br /><br /><br /><br /><br /></div>"


'Display message if the topic is locked and the message is quoted
ElseIf blnTopicLocked AND strMode = "quote" Then

	Response.Write(vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" &  strTxtSorryNoReply & "<br />" & strTxtThisTopicIsLocked & "</span><br /><br /><br /><br /><br /></div>")


'Else if the user does not have permision to reply in this forum
ElseIf blnReply = False AND intGroupID <> 2 AND strMode = "quote" Then
	
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtSorryYouDoNotHavePerimssionToReplyToPostsInThisForum & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""javascript:history.back(1)"" target=""_self"">" & strTxtReturnForumTopic & "</a><br /><br /><br /><br /></div>"
	
'Else the user is not logged in so let them know to login before they can post a message
ElseIf strMode = "quote" Then
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtSorryYouDoNotHavePerimssionToReplyToPostsInThisForum & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "register.gif""  alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle""></a>&nbsp;&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "login.gif""  alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle""></a><br /><br /><br /><br /></div>"

'Else the user is not the person who posted the message so display an error message
Else
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtNoPermissionToEditPost & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""javascript:history.back(1)"" target=""_self"">" & strTxtReturnForumTopic & "</a><br /><br /><br /><br /></div>"
End If


Response.Write(vbCrLf & "<div align=""center"">")


'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
</div>
<!-- #include file="includes/footer.asp" -->