Namespace intranett

    Class evaluering_frameset
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Protected thread_ID As Integer = 0
        Protected forum_ID As Integer = 0
        Protected topSection As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Request.QueryString("TID") <> "" Then
                thread_ID = Request.QueryString("TID")
            End If

            If Request.QueryString("FID") <> "" Then
                forum_ID = Request.QueryString("FID")
            End If

            Select Case forum_ID
                Case 9
                    topSection = "EV_IRIX_TOP"
                Case 10
                    topSection = "EV_SPORT_TOP"
                Case 11
                    topSection = "EV_KULTUR_TOP"
            End Select

        End Sub

    End Class

End Namespace
