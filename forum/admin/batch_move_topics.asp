<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Session.Timeout =  1000

'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Declare veraibles
Dim intNoOfDays
Dim intFromForumID
Dim intToForumID
Dim intPriority


'get teh number of days to delte from
intNoOfDays = CInt(Request.Form("days"))
intFromForumID = CInt(Request.Form("FFID"))
intToForumID = CInt(Request.Form("TFID"))
intPriority = CInt(Request.Form("priority"))
		
'Initalise the strSQL variable with an SQL statement to get the topic from the database
strSQL = "UPDATE " & strDbTable & "Topic SET " & strDbTable & "Topic.Forum_ID=" & intToForumID & " "
If intFromForumID = 0 Then
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays
Else
	strSQL = strSQL & "WHERE (" & strDbTable & "Topic.Last_entry_date < " & strDatabaseDateFunction & " - " & intNoOfDays  & ") AND (" & strDbTable & "Topic.Forum_ID=" & intFromForumID & ")"
End If
If intPriority <> 4 Then strSQL = strSQL & " AND (" & strDbTable & "Topic.Priority=" & intPriority & ")"
strSQL = strSQL & ";"


'Delete the topics
adoCon.Execute(strSQL)	



'Update post count
updateTopicPostCount(intFromForumID)
updateTopicPostCount(intToForumID)
	
'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Batch Move Forum Topics</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"> 
 <p class="text"><span class="heading">Close Forum Topics</span><br />
  <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
  <br />
  <br>
  <br>
  <br>
  <br>
  <span class="bold">Topics have been Moved</span><br />
  <br />
  <br />
  <br>
  <br>
  <a href="resync_forum_post_count.asp">Click here to re-sync Post and Topic Counts for the Forums</a></p>
</div>
</body>
</html>
