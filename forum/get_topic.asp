<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Dimension variables
Dim intForumID		'Holds the forum ID
Dim lngTopicID		'Holds the topic ID
Dim strDirection	'Holds the direction to move in
Dim lngNewTopicID	'Holds the new Topic ID



'Read in the topic ID and directorion
intForumID = CInt(Request.QueryString("FID"))
lngTopicID = CLng(Request.QueryString("TID"))
strDirection = Request.QueryString("DIR")



'******************************************
'***  	   Get the next topic          ****
'******************************************

'If the direction is next get the next topic
If strDirection = "N" Then
	
	'Initliase the SQL query to get the topic and forumID from the database
	strSQL = "SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
	strSQL = strSQL & "FROM " & strDbTable & "Topic "
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Forum_ID = " & intForumID & " AND " & strDbTable & "Topic.Topic_ID > " & lngTopicID & " "
	strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Topic_ID ASC;"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'If there is a record returned read in the topic ID to move to
	If NOT rsCommon.EOF Then
		
		lngNewTopicID = CLng(rsCommon("Topic_ID"))
		
		'Clean up
		rsCommon.Close
		
	'Else we need to get the first topic of the forum as there isn't a next topic
	Else
			
		'Clean up
		rsCommon.Close
		
		'Create a new query to get the first topic in the forum
		strSQL = "SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
		strSQL = strSQL & "FROM " & strDbTable & "Topic "
		strSQL = strSQL & "WHERE " & strDbTable & "Topic.Forum_ID = " & intForumID & " "
		strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Topic_ID ASC;"
	
		'Query the database
		rsCommon.Open strSQL, adoCon
		
		'Get the topic ID of the first topic in the database
		If NOT rsCommon.EOF Then lngNewTopicID = CLng(rsCommon("Topic_ID"))
		
		'Clean up
		rsCommon.Close
	End If
End If



'******************************************
'***  	   Get the previous topic      ****
'******************************************

'If the direction is next get the previous topic
If strDirection = "P" Then
	
	'Initliase the SQL query to get the topic and forumID from the database
	strSQL = "SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
	strSQL = strSQL & "FROM " & strDbTable & "Topic "
	strSQL = strSQL & "WHERE " & strDbTable & "Topic.Forum_ID = " & intForumID & " AND " & strDbTable & "Topic.Topic_ID < " & lngTopicID & " "
	strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Topic_ID DESC;"
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'If there is a record returned read in the topic ID to move to
	If NOT rsCommon.EOF Then
		
		lngNewTopicID = CLng(rsCommon("Topic_ID"))
		
		'Clean up
		rsCommon.Close
		
	'Else we need to get the last topic of the forum as there isn't a previous topic
	Else
		'Clean up
		rsCommon.Close
		
		'Create a new query to get the first topic in the forum
		strSQL = "SELECT TOP 1 " & strDbTable & "Topic.Topic_ID "
		strSQL = strSQL & "FROM " & strDbTable & "Topic "
		strSQL = strSQL & "WHERE " & strDbTable & "Topic.Forum_ID = " & intForumID & " "
		strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Topic_ID DESC;"
	
		'Query the database
		rsCommon.Open strSQL, adoCon
		
		'Get the topic ID of the last topic in the database
		If NOT rsCommon.EOF Then lngNewTopicID = CLng(rsCommon("Topic_ID"))
		
		'Clean up
		rsCommon.Close
	End If
End If


'Reset main server variables
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing


'Redirect back to the posts page to display the topic
Response.Redirect("forum_posts.asp?TID=" & lngNewTopicID)
%>