Imports System.Data.SqlClient
Imports System.Web.Configuration



Namespace intranett


    Partial Class section

        Inherits System.Web.UI.Page
        Public MenuId As Integer = 1
        Protected dr As SqlDataReader

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lMenuNo As System.Web.UI.WebControls.Literal
        Protected WithEvents pMenu As System.Web.UI.WebControls.Panel
        Protected WithEvents pTicker As System.Web.UI.WebControls.Panel
        Protected WithEvents pBodyFront As System.Web.UI.WebControls.Panel

        'Protected WithEvents lblPictureHTML As System.Web.UI.HtmlControls.HtmlGenericControl




        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub DoIplogin()
            Dim iploggedinuser As String = ""
            iploggedinuser = CheckIP(Request.UserHostAddress, Session("CurrentArticleMainGroup"))
            If iploggedinuser <> "" Then
                Session("UserId") = iploggedinuser

                AddUserHit(Session("UserID"), True, False, Session("SiteCode"))

                Dim arr As Array = Split(GetUserData(Session("UserId"), "MainGroups"), ",")
                Dim i As Integer
                For i = 0 To arr.Length - 1
                    If arr(i) <> "" Then Session("MainGroup" & CStr(arr(i))) = "1"
                Next
            End If
        End Sub

        Private Sub ShowSection()

            ' Dim dbNTB As New SqlConnection(ConfigurationSettings.AppSettings("conString"))
            Dim dbNTB As New SqlConnection(WebConfigurationManager.AppSettings("conString"))
            Dim cd As New SqlCommand
            Dim forcelogin As Boolean = Request.QueryString("ForceLogin") = "1"
            Dim section As String
            Dim ShowMenu As Boolean = True
            section = Request.QueryString("section")
            If section = "" Then section = "FRONTPAGE"
            If section = "FRONTPAGE" Then CheckUpdateDailyPicture(MapPath("dbilde\"))

            If Request.QueryString("sort") <> "" Then
                Utils.phonebookSort = Request.QueryString("sort")
            End If

            ' liten hack for � logge bruker inn p� nyhetstjenesten f�rst for � vise korrekte stoffgrupper og kategorier...
            If forcelogin And Session("UserId") = "" Then
                Session("CurrentArticleMainGroup") = "-1"
                Session("RefreshSearch") = section

                DoIplogin()

                If Session("UserID") = "" Then
                    Session("TargetURL") = Request.RawUrl
                    Server.Transfer("login.aspx?Section=" & section)
                End If
            ElseIf Session("UserId") <> "" Then
                AddUserHit(Session("UserID"), False, False, Session("SiteCode"))
            End If

            cd.Connection = dbNTB
            cd.CommandType = CommandType.StoredProcedure
            cd.CommandText = "_GetSection"
            cd.Parameters.Add(New SqlParameter("@section", SqlDbType.VarChar, 100))
            cd.Parameters.Item("@section").Value = section
            cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
            cd.Parameters.Item("@sitecode").Value = Session("SiteCode")

            dbNTB.Open()
            dr = cd.ExecuteReader
            pPicture.Visible = False
            pPictureHTML.Visible = False
            imgTitleGif.Visible = False
            If dr.Read() Then
                If (dr("HTMLContent") = 1) Then
                    lblContent.Text = ReplaceTags(dr("Content"), section)

                    If Session("RefreshSearch") = section Then
                        lblContent.Text &= vbCrLf & "<script>parent.search.document.location.reload();</script>"
                        Session("RefreshSearch") = ""
                    End If

                    pHTMLContent.Visible = True
                    pTextContent.Visible = False
                    pPicture.Visible = False
                Else
                    pHTMLContent.Visible = False
                    If Not (dr("Title") Is DBNull.Value) Then lblTitle.Text = ReplaceTags(ToHTML(dr("Title")))
                    If Not (dr("Ingress") Is DBNull.Value) Then lblIngress.Text = ReplaceTags(ToHTML(dr("Ingress")))
                    If Not (dr("Text") Is DBNull.Value) Then lblText.Text = ReplaceTags(ToHTML(dr("Text")))
                    If Not (dr("Footer") Is DBNull.Value) Then lblFooter.Text = ReplaceTags(ToHTML(dr("Footer")))
                    If (dr("Footer").ToString = "") Then pFooter.Visible = False Else pFooter.Visible = True

                    If dr("HasPicture") = 1 And dr("PictureHTML").ToString = "" Then
                        If Not (dr("PictureHeading") Is DBNull.Value) Then lblPictureHeading.Text = ReplaceTags(ToHTML(dr("PictureHeading")))
                        If Not (dr("PictureText") Is DBNull.Value) Then lblPictureText.Text = ReplaceTags(ToHTML(dr("Picturetext")))
                        pPicture.Visible = True
                        imgSection.ImageUrl = "picture.aspx?Section=" & section

                        'If dr("PictureWidth") > dr("PictureHeight") Then
                        ' imgSection.Width = System.Web.UI.WebControls.Unit.Pixel(ConfigurationSettings.AppSettings("SectionPictureMaxWidth"))
                        imgSection.Width = System.Web.UI.WebControls.Unit.Pixel(WebConfigurationManager.AppSettings("SectionPictureMaxWidth"))
                        'Else
                        'imgSection.Height = System.Web.UI.WebControls.Unit.Pixel(ConfigurationSettings.AppSettings("SectionPictureMaxHeight"))
                        'End If
                    Else
                        pPicture.Visible = False
                        If dr("PictureHTML").ToString <> "" Then
                            mainTable.Width = 300
                            pPictureHTML.Visible = True
                            lblPictureHTML.Text = dr("PictureHTML")
                        End If
                    End If
                End If
                If dr("TitleGif").ToString <> "" Then
                    topTbl.BackImageUrl = "images/headere/vert-bar.gif"
                    imgTitleGif.Visible = True
                    imgTitleGif.ImageUrl = dr("TitleGif")
                End If
                ' pLastModified.Visible = (ConfigurationSettings.AppSettings("ShowLastmodified") = "1")
                pLastModified.Visible = (WebConfigurationManager.AppSettings("ShowLastmodified") = "1")
                lblLastModified.Text = CType(dr("LastModified"), String)
                MenuId = dr("MenuNo")
                ShowMenu = (dr("ShowMenu") = 1) And (Request.QueryString("ShowMenu") <> "0")
            Else
                pHTMLContent.Visible = True
                lblContent.Text = "<b>Seksjon ikke funnet!</b>"
            End If
            dr.Close()
            dbNTB.Close()

            'If Not ShowMenu Then
            'pMenu.Visible = False
            'body.Attributes.Add("background", "")
            'End If

            If Request.QueryString("MenuId") <> "" Then MenuId = CInt(Request.QueryString("MenuId"))
            If MenuId = 0 Then MenuId = 1
            'Vis kun ticker p� www.ntb.no forsiden
            If section = "FRONTPAGE" And Session("SiteCode") = "WWW" Then
                'pTicker.Visible = True
            Else
                'pTicker.Visible = False
            End If
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ShowSection()
        End Sub
    End Class


End Namespace
