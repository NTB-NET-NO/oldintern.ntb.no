<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


Dim laryMesageID 	'Holds the message id number

'Clean up
Set rsCommon = Nothing


'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then 
	'Clean up
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("default.asp")
End If



'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then 
	'Clean up
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If



'If this is delete all then delete all messages
If Request.Form("delAll") <> "" Then
	
	'Delete the PM from the database	
	'Initalise the strSQL variable
	strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (" & strDbTable & "PMMessage.Author_ID="  & lngLoggedInUserID & " );"
			
	'Delete the message from the database
	adoCon.Execute(strSQL)
	
End If


'If this is deleting only the selected ones then  do so
If Request.Form("delSel") <> "" Then
	
	'Run through till all checked messages are deleted
	For each laryMesageID in Request.Form("chkDelete")
	
		'Delete the PM from the database	
		'Initalise the strSQL variable
		strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (((" & strDbTable & "PMMessage.Author_ID)="  & lngLoggedInUserID & " ) AND ((" & strDbTable & "PMMessage.PM_ID)= " & CLng(laryMesageID) & "));"
				
		'Delete the message from the database
		adoCon.Execute(strSQL)
	Next
End If

'This delete is for the delete button on the show pm page
If Request.QueryString("pm_id") <> "" Then
	
	'Delete the topic from the database	
	'Initalise the strSQL variable with an SQL statement to get the topic from the database
	strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (((" & strDbTable & "PMMessage.Author_ID)="  & lngLoggedInUserID & " ) AND ((" & strDbTable & "PMMessage.PM_ID)= " & CLng(Request.QueryString("pm_id")) & "));"
			
	'Delete the message from the database
	adoCon.Execute(strSQL)
End If


'Reset Server Objects
adoCon.Close
Set adoCon = Nothing


'Return to the page showing the threads
Response.Redirect "pm_inbox.asp?MSG=DEL"
%>