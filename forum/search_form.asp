<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim strReturnPage		'Holds the page to return to
Dim strForumName 		'Holds the forum name
Dim intForumID 			'Holds the fourum ID
Dim intReadPermission		'holds the forums read permisisons
Dim intOrderBy			'Holds the order by cluase
Dim strSearchKeywords		'Holds the keywords to search for
Dim strSearchIn			'Holds where the serach is to be done
Dim strSearchMode		'Holds the search mode
Dim intForumID2

'Read in values passed to this form
intOrderBy = CInt(Request.QueryString("OB"))
strSearchKeywords = Trim(Mid(Request.QueryString("KW"), 1, 35))
strSearchIn = Trim(Mid(Request.QueryString("SI"), 1, 3))
intForumID2 = CInt(Request.QueryString("FM"))
strSearchMode = Trim(Mid(Request.QueryString("SM"), 1, 3))
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title><% = strMainForumName %> Search</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Check for a somthing to search for
	if (document.frmSearch.KW.value==""){

		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		alert(msg + "\n\t<% = strTxtSearchFormError %>\n\n");
		document.frmSearch.KW.focus();
		return false;
	}
	<%
If RTEenabled() <> "false" Then Response.Write(vbCrLf & "	frmSearch.Submit.disabled=true;")
%>

	return true;
}
// -->
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr>
  <td align="left" class="heading"><% = strTxtSearchTheForum %></td>
 </tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtSearchTheForum %><br /></td>
 </tr>
</table> <br />
<form method="get" name="frmSearch" action="search.asp" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="<% = strTableBorderColour %>" height="8">
  <tr>
   <td height="2" width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
     <tr>
      <td bgcolor="<% = strTableBgColour %>"><table width="100%" border="0" align="center"  height="8" cellpadding="4" cellspacing="1">
        <tr>
         <td align="left" width="57%" height="2"  bgcolor="<% = strTableTitleColour %>" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSearchFor %></td>
         <td height="2" width="43%" bgcolor="<% = strTableTitleColour %>" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSearchOn %></td>
        </tr>
        <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td align="left" width="57%"  height="2" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"> <input type="text" name="KW" maxlength="35" value="<% = Server.HTMLEncode(strSearchKeywords) %>">
          <br /> <span class="text"><% = strTxtSearchOn %>&nbsp;:
          <input type="radio" name="SM" value="1" <% If strSearchMode = "1" OR strSearchMode = "" Then Response.Write "CHECKED" %>><% = strTxtAllWords %>
          <input type="radio" name="SM" value="2" <% If strSearchMode = "2" Then Response.Write "CHECKED" %>><% = strTxtAnyWords %>
          <input type="radio" name="SM" value="3" <% If strSearchMode = "3" Then Response.Write "CHECKED" %>><% = strTxtPhrase %></span></td>
         <td height="2" width="43%" valign="top" background="<% = strTableBgImage %>"> <select name="SI">
           <option value="TXT" <% If strSearchIn = "TXT" OR strSearchIn = "" Then Response.Write "selected" %>><% = strTxtAllOfMessage %></option>
           <option value="PT" <% If strSearchIn = "PT" Then Response.Write "selected" %>><% = strTxtMessageBody %></option>
           <option value="TC" <% If strSearchIn = "TC" Then Response.Write "selected" %>><% = strTxtTopicSubject %></option>
           <option value="AR" <% If strSearchIn = "AR" Then Response.Write "selected" %>><% = strTxtUsername %></option>
          </select> </td>
        </tr>
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
 <br />
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="<% = strTableBorderColour %>" height="8">
  <tr>
   <td height="24" width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
     <tr>
      <td bgcolor="<% = strTableBgColour %>"><table width="100%" border="0" align="center"  height="8" cellpadding="4" cellspacing="1">
        <tr>
         <td align="left" width="57%" height="2"  bgcolor="<% = strTableTitleColour %>" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSearchForum %></td>
         <td height="2" width="43%" bgcolor="<% = strTableTitleColour %>" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtSortResultsBy %></td>
        </tr>
        <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
         <td align="left" width="57%"  height="12" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"> <select name="FM">
           <option value="0"><% = strTxtAllForums %></option><%


'Read in the forum name from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum ORDER BY " & strDbTable & "Forum.Cat_ID ASC, " & strDbTable & "Forum.Forum_Order ASC;"

'Query the database
rsCommon.Open strSQL, adoCon

'Loop through all the froum in the database
Do while NOT rsCommon.EOF

	'Read in the forum details from the recordset
	strForumName = rsCommon("Forum_name")
	intForumID = CLng(rsCommon("Forum_ID"))
	intReadPermission = CInt(rsCommon("Read"))

	'Call the function to check the users security level within this forum
	Call forumPermisisons(intForumID, intGroupID, intReadPermission, 0, 0, 0, 0, 0, 0, 0, 0, 0)

	'Display the fourms to search as long as they are not private or they have logged in if they are and they have permission to use forum
	If (isNull(rsCommon("Password")) = True OR Request.Cookies(strCookieName)("Forum" & intForumID) = rsCommon("Forum_code")) AND (blnRead = True OR blnAdmin = True OR blnModerator = True) Then
		'Display a link in the link list to the forum
		Response.Write vbCrLf & "<option value=" & intForumID & " "
		If CInt(Request.QueryString("FID")) = intForumID OR CInt(Request.QueryString("forum")) = intForumID Then Response.Write "selected"
		Response.Write ">" & strForumName & "</option>"
	End If

	'Move to the next record in the recordset
	rsCommon.MoveNext
Loop

'Reset server objects
rsCommon.Close
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
          </select> </td>
         <td height="12" width="43%" valign="top" background="<% = strTableBgImage %>"> <select name="OB">
           <option value="1" <% If intOrderBy = "1" OR intOrderBy = "" Then Response.Write "selected" %>><% = strTxtLastPostTime %></option>
           <option value="2" <% If intOrderBy = "2" Then Response.Write "selected" %>><% = strTxtTopicStartDate %></option>
           <option value="3" <% If intOrderBy = "3" Then Response.Write "selected" %>><% = strTxtSubjectAlphabetically %></option>
           <option value="4" <% If intOrderBy = "4" Then Response.Write "selected" %>><% = strTxtNumberViews %></option></select> </td>
        </tr>
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
 <br /><script>document.frmSearch.KW.focus()</script>
 <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
   <td align="center"> <input type="submit" name="Submit" value="<% = strTxtStartSearch %>"> <input type="reset" name="Reset" value="<% = strTxtResetForm %>"> </td>
  </tr>
 </table>
</form>
<div align="center"><%

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->
