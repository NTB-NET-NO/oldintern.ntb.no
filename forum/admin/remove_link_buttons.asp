<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the response buffer to true
Response.Buffer = True


'Dimension variables
Dim blnLinksRemoved
Dim blnWrongCode
Dim strEnteredSerial
Dim strFID
Dim blnFieldsXX
Dim strLiName
Dim strLiEM
Dim strLiType
Dim strXCode
Dim blnLCode
blnLinksRemoved = false
blnWrongCode = false
blnFieldsXX = false
strCode = "BFE33F5B3BEC351BF32F9FE55DC2F8EFF33C297D"
strCode2 = "73ADD37AB4EF0672557E1AD37886325159BA4E0C"
strFID = decodeString(strCodeField)
strCode = decodeString(strCode)
If strDatabaseType = "SQLServer" Then
strSQL = "EXECUTE " & strDbProc & "SelectConfiguration"
Else
strSQL = "SELECT TOP 1 " & strDbTable & "Configuration.* From " & strDbTable & "Configuration;"
End If
With rsCommon
.Open strSQL, adoCon, 2, 3
If NOT .EOF Then
If .Fields(strFID) = False Then blnLinksRemoved = True
blnLCode = CBool(.Fields("L_code"))
End If
If Request.Form("postBack") Then
strLiName = Request.Form("liname")
strLiEM = Request.Form("email")
strLiType = Request.Form("cType")
strXCode= Replace(Request.Form("code"), "'", "", 1, -1, 1)
If strLiName = "" OR strLiEM = "" OR strLiType = "" Then blnFieldsXX = true
If NOT .EOF AND blnFieldsXX = false Then
strEnteredSerial = HashEncode((LCase(strXCode) & strSalt))
If strEnteredSerial = strCode OR strEnteredSerial = strCode2 Then
.Fields(strFID) = False
.Update
blnLinksRemoved = True
Application("blnConfigurationSet") = false
On Error Resume Next
Call codeChecker(strLiName, strLiEM, strLiType, strXCode, strVersion)
On Error goto 0
Else
blnWrongCode = True
End If
End If
End If
End With
rsCommon.Close
Set rsCommon = Nothing
Set adoCon = Nothing
Set adoCon = Nothing
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Remove Link Buttons</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	//Initialise variables
        var errorMsg = "";
        var errorMsgLong = "";

        if (document.frmLinkCode.liname.value ==''){
                errorMsg += "\n\Name \t\t- Enter the name you used when purchasing the link removal code";
        }
        
        if (document.frmLinkCode.email.value ==''){
                errorMsg += "\n\Email \t\t- Enter the email address you used when purchasing the link removal code";
        }

        if (document.frmLinkCode.email.value.length >0 && (document.frmLinkCode.email.value.indexOf("@",0) == -1||document.frmLinkCode.email.value.indexOf(".",0) == -1)) {
                errorMsg +="\n\tEmail\t\t- Enter your valid email address";
        }
        
        if (document.frmLinkCode.code.value ==''){
                errorMsg += "\n\Code \t\t- Enter your link removal code";
        }
        
        if (document.frmLinkCode.cType.value ==''){
                errorMsg += "\n\Code Type \t- Enter your link removal code type";
        }

        //If there is aproblem with the form then display an error
        if ((errorMsg != "") || (errorMsgLong != "")){
                msg = "_______________________________________________________________\n\n";
                msg += "The form has not been submitted because there are problem(s) with the form.\n";
                msg += "Please correct the problem(s) and re-submit the form.\n";
                msg += "_______________________________________________________________\n\n";
                msg += "The following field(s) need to be corrected: -\n";

                errorMsg += alert(msg + errorMsg + "\n" + errorMsgLong);
                return false;
        }

        return true;
}
// -->
</script>

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"><span class="heading">Remove Web Wiz Guide Link Buttons </span><br>
 <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a><br />
 <br />
 <% If blnLinksRemoved = True Then %>
 <br />
 <span class="text">Thank-you for your purchase from Web Wiz Guide, the links should now be removed from the forum.<br />
 <br />
 If there are any problems please e-mail Web Wiz Guide at: -</span><br />
 <a href="mailto:purchase@webwizguide.info">purchase@webwizguide.info</a><br />
 <% Else %>
 <br />
 <span class="text">As part of the license for Web Wiz Forums you are required to leave Powered By Web Wiz Guide links in place. I believe that this is a small price to pay for the many years of ongoing 
 time that have gone into the development and support of the Forum software.<br />
 <br />
 However, many people have asked if they can remove these links for various reasons so I am asking for a small donation to help me cover some of the costs involved in the many 1,000's of hours of ongoing 
 development, support, and distribution of this and other free applications and services from Web Wiz Guide.<br />
 <br />
 For more information or to find out how you can purchase a link removal key for Web Wiz Forums from Web Wiz Guide click on the link below: -</span><br />
 <br />
 <a href="http://www.webwizguide.info/purchase/default.asp" target="_blank">Click here to purchase a link removal key for Web Wiz Forums</a> <br />
 <br />
</div>
<%
If blnWrongCode OR blnFieldsXX Then
%>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
 <tr>
  <td align="center"><b><br />
   <span class="heading">Sorry the code you have entered is incorrect<br />or you have not filled in ALL the fields correctly!<br />
   <br>
   </span></b></td>
 </tr>
</table>
<%
End If
%>
<form method="post" name="frmLinkCode" action="remove_link_buttons.asp" onSubmit="return CheckForm();">
 <table width="450" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000" height="30">
  <tr>
   <td height="2" width="483" align="center"> <table width="100%" border="0" cellspacing="1" cellpadding="2">
     <tr>
      <td bgcolor="#FFFFFF"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr bgcolor="#F5F5FA"> 
         <td colspan="2" class="text">Enter the link removal code in the box below&nbsp; </td>
        </tr>
        <tr bgcolor="#F5F5FA">
         <td align="right" class="text">Registration Name:</td>
         <td><input name="liname" type="text" id="liname" size="25" maxlength="50" value="<% = strLiName %>"></td>
        </tr>
        <tr bgcolor="#F5F5FA">
         <td align="right" class="text">Registration Email:</td>
         <td><input name="email" type="text" id="email" size="25" maxlength="50" value="<% = strLiEM %>"></td>
        </tr>
        <tr bgcolor="#F5F5FA">
         <td align="right" class="text">Link Removal Code: </td>
         <td><input type="text" name="code" size="15" maxlength="15" ></td>
        </tr>
        <tr bgcolor="#F5F5FA"> 
         <td width="34%" align="right" class="text">Link Removal Type: </td>
         <td width="66%"><select name="cType" id="cType">
          <option value="" selected>-- Please Select --</option>
          <option value="Standard Commercial Key">Standard Commercial Key</option>
          <option value="Standard Non-Commericial Key">Standard Non-Commericial Key</option>
          <option value="Standard Developer Key">Standard Developer Key</option>
          <option value="Studio Commercial Key">Studio Commercial Key</option>
          <option value="Non-Commericial Key">Studio Non-Commericial Key</option>
          <option value="Studio Developer Key">Studio Developer Key</option>
         </select></td>
        </tr>
        <tr bgcolor="#F5F5FA"> 
         <td width="34%" align="right"> 
          <input type="hidden" name="postBack" value="true"> </td>
         <td width="66%"> 
          <input type="submit" name="Submit" value="Submit Form"> <input type="reset" name="Reset" value="Reset Form"> </td>
        </tr>
       </table></td>
     </tr>
    </table></td>
  </tr>
 </table>
</form>
<% End If %>
<div align="center"> 
 <p class="text"><br />
  For more info contact: -<br />
  <a href="mailto:purchase@webwizguide.info">purchase@webwizguide.info</a><br />
  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.<font size="2"></font></p>
</div>
</body>
</html>