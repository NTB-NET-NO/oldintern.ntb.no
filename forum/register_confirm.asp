<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting and setting a cookie
Response.Buffer = True

Dim intForumID
Dim strMode

'read in the mode of the page
strMode = Request.QueryString("TP")

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Registration Confirmation</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% If strMode = "UPD" OR strMode = "DEL" Then Response.Write(strTxtEditProfile) Else Response.Write(strTxtRegisterNewUser) %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% If strMode = "UPD" OR strMode = "DEL" Then Response.Write(strTxtEditProfile) Else Response.Write(strTxtRegisterNewUser) %><br /></td>
  <%
'Release server objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%></tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="center">
  <br /><br />
  <span class="text"><%

'If this is a re-activation then tell the member
If strMode = "REACT" Then

	Response.Write(strTxtYourEmailAddressHasBeenChanged)

'Else member updating profile
ElseIf strMode = "UPD" Then

	Response.Write("<span class=""bold"">" & strTxtYourProfileHasBeenUpdated & "</span>" & _
	vbCrLf & "<br /><br />" & strTxtYouCanAccessCP & "<a href=""member_control_panel.asp"" target=""_self"">" & strTxtMemberCPMenu & "</a>") 

'Else if the admin has deleted the member disply the delete msg
ElseIf strMode = "DEL" Then

	Response.Write(strTxtTheMemberHasBeenDleted)

'Else welcome the new member
ElseIf strMode = "ACT" Then
	
	Response.Write("<span class=""bold"">" & strTxtThankYouForRegistering & " " & strMainForumName & "</span>")

'Else welcome new user
Else

	Response.Write("<span class=""bold"">" & strTxtThankYouForRegistering & " " & strMainForumName & "</span>" & _
	vbCrLf & "<br /><br />" & strTxtYouCanAccessCP & "<a href=""member_control_panel.asp"" target=""_self"">" & strTxtMemberCPMenu & "</a>") 
	

End If 


'If this is a re-activation then tell the member
If strMode = "REACT" Then

 	Response.Write(vbCrLf & "<br /><br /><span class=""lgText"">" & strTxtYouShouldReceiveAReactivateEmail & "</span>")

'Else welcome the new member
ElseIf strMode = "ACT" Then

 	Response.Write(vbCrLf & "<br /><br /><span class=""lgText"">" & strTxtYouShouldReceiveAnEmail & "</span>")
End If
%>
<br /><br /><a href="forum_topics.asp?FID=<% = intForumID %>" target="_self"><% = strTxtReturnToDiscussionForum %></a><br /><br />
<%

'If this person needs to activate the account
If strMode = "REACT" OR strMode = "ACT" Then
 %>
 <br /><br />
 <% = strTxtIfErrorActvatingMembership & " " & strMainForumName & " " & " <a href=""mailto:" & strForumEmailAddress & """>" & strTxtForumAdministrator & "</a>." %><br /><%
End If
%>
<br />
</span></div>
<br /><br />
   </td>
  </tr>
</table>
<div align="center">
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->