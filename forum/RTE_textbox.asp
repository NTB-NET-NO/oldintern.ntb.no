<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="functions/functions_edit_post.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 

'Dimension variables
Dim strMode		'Holds the mode of the page
Dim strMessage		'Holds the message to be edited
Dim lngPostID		'Holds the post ID number
Dim strQuoteUsername	'Holds the quoters username
Dim strQuoteMessage	'Holds the message to be quoted
Dim lngQuoteUserID	'Holds the quoters user ID
Dim intForumID		'Holds the forum ID


'Read in the message ID number to edit
strMode = Request.QueryString("mode")
lngPostID = CLng(Request.QueryString("POID"))




'If the message is to be edited then read in the message from the database
If strMode = "edit" or strMode="editTopic" Then
	
	'Initalise the strSQL variable with an SQL statement to query the database get the message details
	strSQL = "SELECT " & strDbTable & "Thread.Message, " & strDbTable & "Forum.Forum_ID " & _
	"FROM (" & strDbTable & "Forum INNER JOIN " & strDbTable & "Topic ON " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID) INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID " & _
	"WHERE " & strDbTable & "Thread.Thread_ID=" & lngPostID & ";"
	
	'Query the database
	rsCommon.Open strSQL, adoCon 
	
	'Read in the details from the recordset
	strMessage = rsCommon("Message")
	intForumID = CInt(rsCommon("Forum_ID"))
	
	'Clean up
	rsCommon.Close



'If the message is to have a quote from someone else then read in there message
ElseIf strMode = "quote" Then
	
	'Initialise the sql query to get the thread details to be quoted
	strSQL = "SELECT " & strDbTable & "Author.Author_ID, " & strDbTable & "Author.Username, " & strDbTable & "Thread.Message, " & strDbTable & "Forum.Forum_ID " & _
	"FROM " & strDbTable & "Author INNER JOIN ((" & strDbTable & "Forum INNER JOIN " & strDbTable & "Topic ON " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Topic.Forum_ID) INNER JOIN " & strDbTable & "Thread ON " & strDbTable & "Topic.Topic_ID = " & strDbTable & "Thread.Topic_ID) ON " & strDbTable & "Author.Author_ID = " & strDbTable & "Thread.Author_ID " & _
	"WHERE " & strDbTable & "Thread.Thread_ID = " & lngPostID
	
	'Query the database
	rsCommon.Open strSQL, adoCon 
	
	'Read in the quoters username and message
	strQuoteUsername = rsCommon("Username")
	strQuoteMessage = rsCommon("Message")
	lngQuoteUserID = CLng(rsCommon("Author_ID"))
	intForumID = CInt(rsCommon("Forum_ID"))
	
	'Close recordset
	rsCommon.Close
	
	
	
	'If the post being quoted is written by a guest see if they have a name
	If lngQuoteUserID = 2 Then
		
		'Initalise the strSQL variable with an SQL statement to query the database
		If strDatabaseType = "SQLServer" Then
			strSQL = "EXECUTE " & strDbProc & "GuestPoster @lngThreadID = " & lngPostID
		Else
			strSQL = "SELECT " & strDbTable & "GuestName.Name FROM " & strDbTable & "GuestName WHERE " & strDbTable & "GuestName.Thread_ID = " & lngPostID & ";"
		End If
			
		'Query the database
		rsCommon.Open strSQL, adoCon
		
		'Read in the quoters name	
		If NOT rsCommon.EOF Then strQuoteUsername = rsCommon("Name")
			
		'Close recordset
		rsCommon.Close
	End If
	
	
	'Build up the quoted thread post
	strMessage = "[QUOTE=" & strQuoteUsername & "]"
	
	'Read in the quoted thread from the recordset
	strMessage = strMessage & strQuoteMessage
	strMessage = strMessage & "[/QUOTE]"




'If a private message read in the message again if the user has returned to ammend after getting username wrong	
ElseIf strMode = "PM" AND Session("PmMessage") <> "" Then
	strMessage = Session("PmMessage")
	Session("PmMessage") = Null 
End If




'If we are replying to a private message then format it
If strMode = "PM" AND NOT lngPostID = 0 Then
	
	'Initlise the sql statement
	strSQL = "SELECT " & strDbTable & "PMMessage.*, " & strDbTable & "Author.Username " & _
	"FROM " & strDbTable & "PMMessage, " & strDbTable & "Author " & _
	"WHERE " & strDbTable & "Author.Author_ID = " & strDbTable & "PMMessage.From_ID AND " & strDbTable & "PMMessage.PM_ID=" & lngPostID & " AND " & strDbTable & "PMMessage.Author_ID=" & lngLoggedInUserID & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon 
	
	
	'Build up the reply pm post
	strMessage = "<br /><br /><br />-- " & strTxtPreviousPrivateMessage & " --"
	strMessage = strMessage & "<br /><b>" & strTxtSentBy & " :</b> " & rsCommon("Username") 
	strMessage = strMessage & "<br /><b>" & strTxtSent & " :</b> " & DateFormat(CDate(rsCommon("PM_Message_Date")), saryDateTimeData) & " " & strTxtAt & " " & TimeFormat(CDate(rsCommon("PM_Message_Date")), saryDateTimeData) & "<br /><br />"
	
	'Read in the quoted thread from the recordset
	strMessage = strMessage & rsCommon("PM_Message")
	
	'Clean up
	rsCommon.Close
End If




'Make the post idetical to before it was posted by removing border and target tags from the images and links
If NOT strMessage = "" Then strMessage = Replace(strMessage, """ border=""0"" target=""_blank"">", """>", 1, -1, 1)
If NOT strMessage = "" Then strMessage = Replace(strMessage, """ border=""0"">", """>", 1, -1, 1)
		





'If this is an edit or quote then stripout who edited the post and check permisisons
If strMode = "edit" OR strMode="editTopic" OR strMode = "quote" Then
	
	
	'If the message has been edited remove who edited the post
	If InStr(1, strMessage, "<edited>", 1) Then strMessage = removeEditorAuthor(strMessage)
	
	
	'Read in the forum permissions from the database
	'Initalise the strSQL variable with an SQL statement to query the database
	If strDatabaseType = "SQLServer" Then
		strSQL = "EXECUTE " & strDbProc & "ForumsAllWhereForumIs @intForumID = " & intForumID
	Else
		strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE Forum_ID = " & intForumID & ";"
	End If
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	'If there is a record returned by the recordset then check to see if you need a password to enter it
	If NOT rsCommon.EOF Then
		
		'Check the user is welcome in this forum
		Call forumPermisisons(intForumID, intGroupID, CInt(rsCommon("Read")), CInt(rsCommon("Post")), CInt(rsCommon("Reply_posts")), CInt(rsCommon("Edit_posts")), 0, 0, 0, 0, 0, 0)
		
		'If the forum requires a password and a logged in forum code is not found on the users machine then set the message to be blank
		If NOT rsCommon("Password") = "" and NOT Request.Cookies(strCookieName)("Forum" & intForumID) = rsCommon("Forum_code") Then
			
			strMessage = ""
		End If
	End If
	
	'Reset server object
	rsCommon.Close
	
	
	'If the user dosn't have permisison to view/edit/post/etc. then don't let them read the post
	If strMode = "edit" OR strMode="editTopic" AND blnAdmin = False AND blnModerator = False Then
		
		If blnRead = False OR blnPost = False OR blnEdit = False Then strMessage = ""
	
	ElseIf strMode = "quote" Then
		
		If blnRead = False OR blnPost = False OR blnReply = False Then strMessage = ""
	
	End If
		
End If


	
'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
<html>
<head>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!--#include file="includes/skin_file.asp" -->

<%
If RTEenabled = "Gecko" Then
	
	Response.Write(vbCrLf & "<script language=""javascript"">" & _
	vbCrLf & "	<!--" & _
	vbCrLf & "	function enableDesignMode() {" & _
 	vbCrLf & "	document.designMode = ""on""" & _
	vbCrLf & "	}" & _
	vbCrLf & "-->" & _
	vbCrLf & "</script>")
End If
%>
</head>
<body bgcolor="<% = strIETextBoxColour %>" class="text" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"<% If RTEenabled = "Gecko" Then Response.Write(" onLoad=""setTimeout('enableDesignMode()', 20);""") %>>
<% = strMessage %></body>
</html>