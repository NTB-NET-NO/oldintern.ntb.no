<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by email ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Topic admin
'---------------------------------------------------------------------------------
Const strTxtUpdateTopic = "Update Topic"
Const strTxtTopicNotFoundOrAccessDenied = "Either the topic can not be found or you do not have permission to access this page"
Const strTxtMoveTopic = "Move Topic"
Const strTxtDeletePoll = "Delete Poll"
Const strTxtAreYouSureYouWantToDeleteThisPoll = "Are you sure you want to delete this poll?"



'move_post_form.asp
'---------------------------------------------------------------------------------
Const strTxtSelectTheForumYouWouldLikePostIn = "Select the Forum you would like this post to be in"
Const strTxtMovePostErrorMsg = "Forum\t- Please select a Forum to move the Post to"
Const strTxtSelectForumClickNext = "Step 1: - Select the Forum you would like to move the Post to.<br />Then click on the Next button."
Const strTxtSelectTopicToMovePostTo = "Step 2: - Select the Topic to move the Post to from the last 400 Topics in this<br />forum. Alternatively type the name of a new Topic in the box at the<br />bottom to place the Post in a new Topic."
Const strTxtSelectTheTopicYouWouldLikeThisPostToBeIn = "Select the Topic you would like this post to be in"
Const strTxtOrTypeTheSubjectOfANewTopic = "Or, type the Subject of a new Topic"



'pop_up_IP_blocking.asp
'---------------------------------------------------------------------------------
Const strTxtIPBlocking = "IP Blocking"
Const strTxtBlockedIPList = "Blocked IP Address List"
Const strTxtYouHaveNoBlockedIpAddesses = "You have no blocked IP address"
Const strTxtRemoveIP = "Remove IP"
Const strTxtBlockIPAddressOrRange = "Block IP Address or Range"
Const strTxtIpAddressRange = "IP Address/Range"
Const strTxtBlockIPAddressRange = "Block IP Address/Range"
Const strTxtBlockIPRangeWhildcardDescription = "The * wildcard character can be used to block IP ranges. <br />eg. To block the range '200.200.200.0 - 255' you would use '200.200.200.*'"
Const strTxtErrorIPEmpty = "IP Address/Range \t- Enter a an IP address or range to block"


'register.asp - in admin mode
'---------------------------------------------------------------------------------
Const strTxtAdminModeratorFunctions = "Admin and Moderator Functions"
Const strTxtUserIsActive = "User is active"
Const strTxtDeleteThisUser = "Delete this member?"
Const strTxtCheckThisBoxToDleteMember = "Check this box to delete this member, this cannot be undone."
Const strTxtNumberOfPosts = "Number of posts"
Const strTxtNonRankGroup = "Non Ladder Group"
Const strTxtRankGroupMinPosts = "Ladder Group - Min. Posts"



'Topic admin
'---------------------------------------------------------------------------------
Const strTxtUpdateForum = "Update Forum"
Const strTxtForumNotFoundOrAccessDenied = "Either the forum can not be found or you do not have permission to access this page"
Const strTxtErrorForumName = "Forum Name \t- Enter a Name for the Forum"
Const strTxtErrorForumDescription  = "Forum Description \t- Enter a Description for the Forum"


Const strTxtResyncTopicPostCount = "Update the Forum Topic and Post Count"


'New from version 7.02
'---------------------------------------------------------------------------------
Const strTxtShowMovedIconInLastForum = "Show moved icon in last forum"

'New from version 8.0
'---------------------------------------------------------------------------------
Const strTxtHideTopic = "Hide Topic"
Const strTxtIfYouAreShowingTopic = "If you are displaying the topic make sure at least 1 post is showing in the topic"
Const strTxtShowHiddenTopicsSince = "Show Hidden Topics/Posts Since"
Const strTxtHiddenTopicsPosts = "Hidden Topics/Posts"
Const strTxtNoHiddenTopicsPostsSince = "There are no Hidden Topics/Posts since"
%>