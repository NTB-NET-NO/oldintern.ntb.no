 <%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

%><table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr> 
  <td class="text">
   <% 
'If there is a forum image then dsiplay it
If NOT strTitleImage = "" Then
    Response.Write("<a href=""" & strWebsiteURL & """ target=""_self""><img src=""" & strTitleImage & """ border=""0""></a>")
End If
%>
  </td>
  <td align="center" height="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td align="center"><%
'If the user is admin display a link to the admin memnu
If intGroupID = 1 Then Response.Write("<a href=""admin/frame_set.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "admin_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtAdmin & """>" & strTxtAdmin & "</a>")
     
'Make sure that the forum ID number has a number in it otherwise set it to 0
If isEmpty(intForumID) OR intForumID = "" Then intForumID = 0

	'Display the other common buttons
	Response.Write ("&nbsp;&nbsp;<a href=""active_topics.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "active_topics.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtActiveTopics & """>" & strTxtActiveTopics & "</a>")
	Response.Write ("&nbsp;&nbsp;<a href=""members.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "members_list.gif"" border=""0"" align=""absmiddle"" alt=""" & strTxtMembersList & """>" & strTxtMemberlist & "</a>")
	Response.Write ("&nbsp;&nbsp;<a href=""search_form.asp?FID=" & intForumID & """ target=""_self"" class=""nav""><img src=""" & strImagePath & "search.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtSearchTheForum & """>" & strTxtSearch & "</a>")
	Response.Write ("&nbsp;&nbsp;<a href=""help.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "help_icon.gif"" align=""absmiddle"" border=""0"" alt=""" & strTxtHelp & """>" & strTxtHelp & "</a>")
      	Response.Write ("<br />")
      	
      	%></td>
    </tr>
    <tr>
     <td align="center"><!-- #include file="pm_check_inc.asp" --><%
     
'If the user has logged in then the Logged In User ID number will not be 0 and not 2 for the guest account
If lngLoggedInUserID <> 0 AND lngLoggedInUserID <> 2 Then
	
	'Dispaly a welcome message to the user in the top bar
	Response.Write ("&nbsp;&nbsp;<a href=""member_control_panel.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "admin_icon.gif"" border=""0"" align=""absmiddle"" alt=""" & strTxtMemberCPMenu & """>" & strTxtSettings & "</a>")
	Response.Write ("&nbsp;&nbsp;<a href=""log_off_user.asp"" target=""_self"" class=""nav""><img src=""" & strImagePath & "log_off_icon.gif"" alt=""" & strTxtLogOff & """ border=""0"" align=""absmiddle"">" & strTxtLogOff & " [" & strLoggedInUsername & "]</a>")
	

'Else the user is not logged
Else
    	'Display a welcome guset message with the option to login or register
    	Response.Write ("&nbsp;&nbsp;<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self"" class=""nav""><img src=""" & strImagePath & "register_icon.gif"" alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle"">" & strTxtRegister & "</a>")
    	Response.Write ("&nbsp;&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self"" class=""nav""><img src=""" & strImagePath & "login_icon.gif"" alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle"">" & strTxtLogin & "</a>")
End If


response.write("<br><a href = 'http://www.facebook.com/pages/NTB-sprak/133030710043843' target='_new'>NTB-spr�k p� Facebook</a>")

%></td>
    </tr>
   </table>
  </td>
 </tr>
</table>