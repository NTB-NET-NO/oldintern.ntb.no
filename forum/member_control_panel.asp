<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/cp_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Declare variables
Dim intForumID
Dim lngUserProfileID            'Holds the users ID of the profile to get
Dim strMode			'Holds the mode of the page



'If the user his not activated their mem
If blnActiveMember = False Then

        'clean up before redirecting
        Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

        'redirect to insufficient permissions page
        Response.Redirect("insufficient_permission.asp?M=ACT")
End If

'If the user has not logged in then redirect them to the insufficient permissions page
If intGroupID = 2 Then

        'clean up before redirecting
        Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

        'redirect to insufficient permissions page
        Response.Redirect("insufficient_permission.asp")
End If



'Read in the mode of the page
strMode = Trim(Mid(Request.QueryString("M"), 1, 1))


'If this is not an admin but in admin mode then see if the user is a moderator
If blnAdmin = False AND strMode = "A" Then
	
	'Initalise the strSQL variable with an SQL statement to query the database
        strSQL = "SELECT " & strDbTable & "Permissions.Moderate "
        strSQL = strSQL & "FROM " & strDbTable & "Permissions "
        If strDatabaseType = "SQLServer" Then
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = 1;"
        Else
                strSQL = strSQL & "WHERE " & strDbTable & "Permissions.Group_ID = " & intGroupID & " AND  " & strDbTable & "Permissions.Moderate = True;"
        End If
               

        'Query the database
         rsCommon.Open strSQL, adoCon

        'If a record is returned then the user is a moderator in one of the forums
        If NOT rsCommon.EOF Then blnModerator = True

        'Clean up
        rsCommon.Close
End If


'Get the user ID of the member being edited if in admin mode
If (blnAdmin OR (blnModerator AND CLng(Request.QueryString("PF")) > 2)) AND strMode = "A" AND CLng(Request.QueryString("PF")) <> 2 Then
	
	lngUserProfileID = CLng(Request.QueryString("PF"))

'Get the logged in ID number
Else
	lngUserProfileID = lngLoggedInUserID
End If
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Member Control Panel Menu</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<a name="top"></a>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td class="heading"><% = strTxtMemberCPMenu %></td>
</tr>
 <tr> 
  <td width="60%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtMemberCPMenu %><br /><%

 
	Response.Write(vbCrlf & "  <td width=""40%"" align=""right"" nowrap=""nowrap""><a href=""member_control_panel.asp")
	'If this is in admin mode then add the profile to be edited
	If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("?PF=" & lngUserProfileID & "&M=A")
	Response.Write(""" target=""_self""><img src=""" & strImagePath & "cp_menu.gif"" border=""0"" alt=""" & strTxtMemberCPMenu & """></a>")

	
	Response.Write("<a href=""register.asp")
	'If this is in admin mode then add the profile to be edited
	If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("?PF=" & lngUserProfileID & "&M=A")
	Response.Write(""" target=""_self""><img src=""" & strImagePath & "profile.gif"" border=""0"" alt=""" & strTxtEditProfile & """></a>")

'email notify is on show link to email subcriptions
If blnEmail Then
		
	Response.Write("<a href=""email_notify_subscriptions.asp")	
	'If this is in admin mode then allow the admin or modertor change this users email subscriptions
	If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("?PF=" & lngUserProfileID & "&M=A")
	Response.Write(""" target""_self""><img src=""" & strImagePath & "email_notify.gif"" border=""0"" alt=""" & strTxtEmailNotificationSubscriptions & """></a>")

End If

%></td>
  </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtMemberCPMenu %></td>
    </tr>
    <tr> 
     <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr>
           <td><a href="register.asp<% If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("?PF=" & lngUserProfileID & "&M=A") %>"><% = strTxtEditProfile %></a><br /><% = strTxtChangeProfile %><br /></td>
          </tr>
          <tr>
           <td><br /><a href="register.asp?FPN=1<% If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("&PF=" & lngUserProfileID & "&M=A") %>"><% = strTxtRegistrationDetails %></a><br /><% = strTxtChangePassAndEmail %><br /></td>
          </tr>
          </tr>
          <tr>
           <td><br /><a href="register.asp?FPN=2<% If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("&PF=" & lngUserProfileID & "&M=A") %>"><% = strTxtProfileInfo %></a><br /><% = strTxtChangeProfileInfo %><br /></td>
          </tr>
          </tr>
          <tr>
           <td><br /><a href="register.asp?FPN=3<% If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("&PF=" & lngUserProfileID & "&M=A") %>"><% = strTxtForumPreferences %></a><br /><% = strTxtChangeForumPreferences %><br /></td>
          </tr><%
          
'email notify is on show link to email subcriptions
If blnEmail Then
%>	
          <tr>
           <td><br /><a href="email_notify_subscriptions.asp<% If strMode = "A" AND (blnAdmin OR blnModerator) Then Response.Write("?PF=" & lngUserProfileID & "&M=A") %>"><% = strTxtEmailNotificationSubscriptions %></a><br /><% = strTxtAlterEmailSubscriptions %><br /></td>
          </tr><%
End If
         
'If PM is on then show links to PM functions
If blnPrivateMessages AND strMode <> "A" Then
	%>
          <tr>
           <td><br /><a href="pm_welcome.asp"><% = strTxtPrivateMessenger %></a><br /><% = strTxtReadandSendPMs %><br /></td>
          </tr>
          <tr>
           <td><br /><a href="pm_buddy_list.asp"><% = strTxtBuddyList %></a><br /><% = strTxtListOfYourForumBuddies %><br /></td>
          </tr><%
End If

'If admin mod mode have link to admin functions
If strMode = "A" AND (blnAdmin OR blnModerator) Then
%>
          <tr>
           <td><br /><a href="register.asp?PF=<% = lngUserProfileID %>&M=A#admin"><% = strTxtAdminAndModFunc %></a><br /><% = strTxtAdminFunctionsTo %><br /></td>
          </tr><%
End If

%>
          <tr>
           <td><br /><a href="help.asp"><% = strTxtForumHelp %></a><br /><% = strTxtForumHelpFilesandFAQsToHelpYou %><br /></td>
          </tr>
         </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr>
  <form>
   <td>
    <!-- #include file="includes/forum_jump_inc.asp" -->
   </td>
  </form>
 </tr>
</table>
<div align="center"><br /><%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

	
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******


'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->