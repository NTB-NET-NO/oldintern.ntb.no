﻿Imports System.IO
Imports System.Web

Namespace intranett


    Partial Class trykk_Default
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            Try

                ' tHIS IS WHERE IT WENT EARLIER: /dokumenter/trykk/trykkoversikten.htm
                ' The following variable must be set in web.config
                ' Dim strDirectory As String = "C:\utvikling\websites\intern.ntb.no\dokumenter\Trykk"
                Dim strDir As String = "/dokumenter/Trykk/"
                Dim strDirectory As String = Server.MapPath(strDir)
                ' Dim fileArray As Array = Directory.GetFiles


                Dim myFile As String = ""
                Dim code As String = ""
                Dim file As String = ""
                Dim arrYear(,,) As String = Nothing

                ' Array containing the months in Norwegian
                Dim arrMonth() As String = {"Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"}

                If Directory.Exists(strDirectory) = True Then
                    ReDim arrYear(2012, 12, 31)

                    For Each myFile In Directory.GetFiles(strDirectory, "*.xls")
                        file = Path.GetFileName(myFile)

                        Dim fi As FileInfo = New FileInfo(Path.GetFullPath(myFile))
                        code = "<a href='" & file & "'>" & file & "</a><br>"
                        Dim dtCreationDate As DateTime = fi.LastWriteTime




                        arrYear(dtCreationDate.Year, dtCreationDate.Month, dtCreationDate.Day) = file.ToString()
                        ' Response.Write(dtCreationDate.Year.ToString() & "<br>")

                    Next
                Else
                    Response.Write("Does not exists!")
                End If

                Dim strCode As String = ""
                Dim boolHasData As Boolean = False

                Dim label As Label = New Label()
                Dim i, c, d As Integer


                For i = DateTime.Now.Year - 1 To DateTime.Now.Year
                    ' Response.Write(i & "<br>")
                    If i > DateTime.Now.Year - 1 Then
                        strCode &= "<br>"
                    End If
                    strCode &= "<strong style='font-size: 10pt;'>" & i & "</strong><br>"
                    For c = 0 To 11

                        For d = 1 To 31
                            If arrYear(i, c, d) <> "" Then
                                strCode &= "<a href = '" & strDir & arrYear(i, c, d) & "'>" & d.ToString() & "</a>&nbsp;"
                                ' Response.Write(strCode)
                                boolHasData = True


                            End If

                        Next
                        If boolHasData = True Then
                            ' Response.Write("<br>")
                            strCode &= "<br>"
                            boolHasData = False
                        End If
                        ' Response.Write(arrMonth(c).ToString() & "<br>")
                        strCode &= arrMonth(c).ToString() & "<br>"
                    Next

                Next

                label.Text = strCode
                label.Font.Name = "Arial"
                label.Font.Size = 8
                

                Form.Controls.Add(label)


            Catch nex As NullReferenceException
                Response.Write(nex.ToString())

            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try



        End Sub
    End Class
End Namespace