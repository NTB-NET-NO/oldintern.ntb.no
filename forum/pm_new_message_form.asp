<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/pm_language_file_inc.asp" -->
<!--#include file="functions/functions_edit_post.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<!--#include file="includes/emoticons_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"

'Declare variables
Dim strMode 			'Holds the mode of the page
Dim strPostPage 		'Holds the page the form is posted to
Dim lngMessageID		'Holds the pm id
Dim strTopicSubject		'Holds the subject
Dim strBuddyName		'Holds the to username
Dim dtmReplyPMDate		'Holds the reply pm date
Dim strMessage			'Holds the post message
Dim intForumID			'Holds the forum number
Dim intIndexPosition		'Holds the idex poistion in the emiticon array
Dim intNumberOfOuterLoops	'Holds the outer loop number for rows
Dim intLoop			'Holds the loop index position
Dim intInnerLoop		'Holds the inner loop number for columns


'Set the mode of the page
strMode = "PM"
lngMessageID = 0


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If



'If Priavte messages are not on then send them away
If blnPrivateMessages = False Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If


'If the user is not allowed then send them away
If intGroupID = 2 OR blnActiveMember = False Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("insufficient_permission.asp")
End If


'If there is a person who to send to then read in there name
'This is encoded before being displayed for security
strBuddyName = Request.QueryString("name")



'If edit read in the detials
If Request.QueryString("code") = "edit" Then
	'Read in the details of the message to be edited
	strTopicSubject = Trim(Mid(Request.Form("subject"), 1, 41))
	strMessage = Request.Form("PmMessage")
	strBuddyName = Trim(Mid(Request.Form("Buddy"), 1, 15))
End If



'If this is a reply to a pm then get the details from the db
If Request.QueryString("code") = "reply" Then

	'Read in the pm mesage number to reply to
	lngMessageID = CLng(Request.QueryString("pm"))

	'Get the pm from the database

	'Initlise the sql statement
	strSQL = "SELECT " & strDbTable & "PMMessage.*, " & strDbTable & "Author.Username "
	strSQL = strSQL & "FROM " & strDbTable & "Author INNER JOIN " & strDbTable & "PMMessage ON " & strDbTable & "Author.Author_ID = " & strDbTable & "PMMessage.From_ID "
	strSQL = strSQL & "WHERE " & strDbTable & "PMMessage.PM_ID=" & lngMessageID & " AND " & strDbTable & "PMMessage.Author_ID=" & lngLoggedInUserID & ";"

	'Query the database
	rsCommon.Open strSQL, adoCon

	'Read in the date of the reply pm
	dtmReplyPMDate = CDate(rsCommon("PM_Message_Date"))

	'Read in the username to be the pm is a reply to
	strBuddyName = rsCommon("Username")

	'Set up the pm title
	strTopicSubject = Replace(rsCommon("PM_Tittle"), "RE: ", "")
	strTopicSubject = "RE: " & strTopicSubject

	'Build up the reply pm
	strMessage = "-- " & strTxtPreviousPrivateMessage & " --"
	strMessage = strMessage & vbCrLf & "[B]" & strTxtSentBy & " :[/B] " & strBuddyName
	strMessage = strMessage & vbCrLf & "[B]" & strTxtSent & " :[/B] " & DateFormat(dtmReplyPMDate, saryDateTimeData) & " at " & TimeFormat(dtmReplyPMDate, saryDateTimeData) & vbCrLf & vbCrLf

	'Read in the pm from the recordset
	strMessage = strMessage & rsCommon("PM_Message")

	'Convert the orginal PM back to forum codes
	strMessage = EditPostConvertion (strMessage)

	'Place a couple of carridge returns infront of the reply pm
	strMessage = vbCrLf & vbCrLf & vbCrLf & strMessage

	'Close recordset
	rsCommon.Close
End If
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Private Messenger: Send New Message</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

<%
'If Gecko Madis API (RTE) need to strip default input from the API
If RTEenabled = "Gecko" Then Response.Write("	//For Gecko Madis API (RTE)" & vbCrLf & "	if (document.frmAddMessage.message.value.indexOf('<br>') > -1 && document.frmAddMessage.message.value.length == 8) document.frmAddMessage.message.value = '';" & vbCrLf)

%>
	//Check for a member name
	if ((document.frmAddMessage.member.value=="") && (document.frmAddMessage.selectMember.value=="")){
		errorMsg += "\n\t<% = strTxtNoToUsernameErrorMsg %>";
	}

	//Check for a subject
	if (document.frmAddMessage.subject.value==""){
		errorMsg += "\n\t<% = strTxtNoPMSubjectErrorMsg %>";
	}

	//Check for message
	if (document.frmAddMessage.message.value==""){
		errorMsg += "\n\t<% = strTxtNoPMErrorMsg %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	//Reset the submition action
	document.frmAddMessage.action = "pm_post_message.asp"
	document.frmAddMessage.target = "_self";
<% 
If RTEenabled() <> "false" AND blnRTEEditor AND blnWYSIWYGEditor Then Response.Write(vbCrLf & "	frmAddMessage.Submit.disabled=true;")
%>

	return true;
}
</script>

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr>
  <td align="left" class="heading"><% = strTxtPrivateMessenger %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtPrivateMessenger %><br /></td>
  </tr>
</table>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr>
  <td width="60%"><span class="lgText"><img src="<% = strImagePath %>subject_folder.gif" alt="<% = strTxtSubjectFolder %>" align="absmiddle"> <% = strTxtPrivateMessenger & ": " & strTxtSendNewMessage %></span></td>
  <td align="right" width="40%" nowrap="nowarp"><a href="pm_inbox.asp" target="_self"><img src="<% = strImagePath %>inbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtInbox %>" border="0"></a><a href="pm_outbox.asp" target="_self"><img src="<% = strImagePath %>outbox.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtOutbox %>" border="0"></a><a href="pm_buddy_list.asp" target="_self"><img src="<% = strImagePath %>buddy_list.gif" alt="<% = strTxtPrivateMessenger & " " & strTxtBuddyList %>" border="0"></a><a href="pm_new_message_form.asp" target="_self"><img src="<% = strImagePath %>new_private_message.gif" alt="<% = strTxtNewPrivateMessage %>" border="0"></a></td>
 </tr>
</table>
<%

'See if the users browser is RTE enabled
If RTEenabled() <> "false" AND blnRTEEditor = True AND blnWYSIWYGEditor = True Then

	'If we are editing the post then we need to pass the edited message to the Iframe
	If Request.QueryString("code") = "edit" Then Session("PmMessage") = strMessage

	'Open the message form for RTE enabled browsers
	%><!--#include file="includes/RTE_message_form_inc.asp" --><%
Else
	'Open up the mesage form for non RTE enabled browsers
	%><!--#include file="includes/message_form_inc.asp" --><%
End If

'Reset server variables
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

%><br />
<div align="center">
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->