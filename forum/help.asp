<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/help_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Set the buffer to true
Response.Buffer = True

'Declare variables
Dim intForumID

%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>Forum Help</title>

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
</script>
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<a name="top"></a>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
 <tr> 
  <td align="left" class="heading"><% = strTxtForumHelp %></td>
</tr>
 <tr> 
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtForumHelp %><br /></td>
  </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtChooseAHelpTopic %></td>
    </tr>
    <tr> 
     <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr>
           <td class="bold"><% = strTxtLoginAndRegistration %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ1"><% = strTxtWhyCantILogin %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ2"><% = strTxtDoINeedToRegister %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ3"><% = strTxtLostPasswords %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ4"><% = strTxtIRegisteredInThePastButCantLogin %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><% = strTxtUserPreferencesAndForumSettings %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ5"><% = strTxtHowDoIChangeMyForumSettings %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ6"><% = strTxtForumTimesAndDates %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ7"><% = strTxtWhatDoesMyRankIndicate %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ8"><% = strTxtCanIChangeMyRank %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><% = strTxtPostingIssues %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ9"><% = strTxtHowPostMessageInTheForum %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ10"><% = strTxtHowDeletePosts %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ11"><% = strTxtHowEditPosts %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ12"><% = strTxtHowSignaturToMyPost %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ13"><% = strTxtHowCreatePoll %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ14"><% = strTxtWhyNotViewForum %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ15"><% = strTxtInternetExplorerWYSIWYGPosting %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><% = strTxtMessageFormatting %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ16"><% = strTxtWhatForumCodes %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ17"><% = strTxtCanIUseHTML %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ18"><% = strTxtWhatEmoticons %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ19"><% = strTxtCanPostImages %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ20"><% = strTxtWhatClosedTopics %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><% = strTxtUsergroups %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ21"><% = strTxtWhatForumAdministrators %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ22"><% = strTxtWhatForumModerators %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ23"><% = strTxtWhatUsergroups %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><% = strTxtPrivateMessaging %></td>
          </tr>
          <tr> 
           <td><a href="#FAQ24"><% = strTxtIPrivateMessages %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ25"><% = strTxtIPrivateMessagesToSomeUsers %></a></td>
          </tr>
          <tr> 
           <td><a href="#FAQ26"><% = strTxtHowCanPreventSendingPrivateMessages %></a></td>
          </tr>
         </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</td>
 </tr>
</table>
<br>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtLoginAndRegistration %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ1"></a><% = strTxtWhyCantILogin %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ1 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ2"></a><% = strTxtDoINeedToRegister %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ2 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ3"></a><% = strTxtLostPasswords %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ3 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ4"></a><% = strTxtIRegisteredInThePastButCantLogin %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ4 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtUserPreferencesAndForumSettings %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ5"></a><% = strTxtHowDoIChangeMyForumSettings %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ5 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ6"></a><% = strTxtForumTimesAndDates %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ6 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ7"></a><% = strTxtWhatDoesMyRankIndicate %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ7 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ8"></a><% = strTxtCanIChangeMyRank %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ8 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtPostingIssues %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ9"></a><% = strTxtHowPostMessageInTheForum %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ9 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ10"></a><% = strTxtHowDeletePosts %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ10 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ11"></a><% = strTxtHowEditPosts %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ11 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ12"></a><% = strTxtHowSignaturToMyPost %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ12 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ13"></a><% = strTxtHowCreatePoll %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ13 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ14"></a><% = strTxtWhyNotViewForum %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ14 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ15"></a><% = strTxtInternetExplorerWYSIWYGPosting %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ15 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtMessageFormatting %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ16"></a><% = strTxtWhatForumCodes %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ16 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ17"></a><% = strTxtCanIUseHTML %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ17 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ18"></a><% = strTxtWhatEmoticons %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ18 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ19"></a><% = strTxtCanPostImages %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ19 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ20"></a><% = strTxtWhatClosedTopics %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ20 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtUsergroups %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ21"></a><% = strTxtWhatForumAdministrators %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ21 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ22"></a><% = strTxtWhatForumModerators %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ22 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ23"></a><% = strTxtWhatUsergroups %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ23 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr> 
  <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr> 
     <td bgcolor="<% = strTableBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" height="14" bgcolor="<% = strTableBgColour %>">
       <tr> 
        <td bgcolor="<% = strTableTitleColour %>" height="2" class="tHeading" background="<% = strTableTitleBgImage %>"><% = strTxtPrivateMessaging %></td>
       </tr>
       <tr> 
        <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="text">
          <tr> 
           <td class="bold"><a name="FAQ24"></a><% = strTxtIPrivateMessages %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ24 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ25"></a><% = strTxtIPrivateMessagesToSomeUsers %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ25 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
          <tr> 
           <td>&nbsp;</td>
          </tr>
          <tr> 
           <td class="bold"><a name="FAQ26"></a><% = strTxtHowCanPreventSendingPrivateMessages %></td>
          </tr>
          <tr> 
           <td class="text"><% = strTxtFAQ26 %></td>
          </tr>
          <tr> 
           <td><a href="#top" target="_self" class="smLink"><% = strTxtBackToTop %></a></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="4" align="center">
 <tr>
  <form>
   <td>
    <!-- #include file="includes/forum_jump_inc.asp" -->
   </td>
  </form>
 </tr>
</table>
<div align="center"><br /><%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

	
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******


'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
</div>
<!-- #include file="includes/footer.asp" -->