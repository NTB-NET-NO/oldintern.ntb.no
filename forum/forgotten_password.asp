<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<!--#include file="functions/functions_hash1way.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


Response.Buffer = True 


'Dimension variables
Dim objCDOMail			'Holds the CDO mail object
Dim objJMail			'Holds the Jmail object
Dim strUsername			'Holds the users username
Dim strPassword			'Holds the usres password
Dim strEmailAddress		'Holds the users e-mail address
Dim strReturnPage		'Holds the page to return to 
Dim blnInvalidUsername 		'Set to true if the username entered does not exsit
Dim blnInvalidEmail 		'Set to true if the user has not given there e-mail address	
Dim blnEmailSent		'Set to true if the e-mail has been sent
Dim strEmailBody		'Holds the body of the e-mail message	
Dim strSubject			'Holds the subject of the e-mail
Dim strSalt			'Holds the salt value for the password
Dim strEncyptedPassword		'Holds the encrypted password
Dim strUserCode			'Holds the user code for the user


'Intialise variables
blnInvalidUsername = False
blnInvalidEmail = False
blnEmailSent = False

'If e-mail notify is not turned on then close the window
If blnEmail = False Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect "default.asp"
End If

'Read in the users details from the form
strUsername = Trim(Mid(Request.Form("name"), 1, 15))
strEmailAddress = Trim(Mid(Request.Form("email"), 1, 60))

'Take out parts of the username that are not permitted
strUsername = disallowedMemberNames(strUserName)

'Replace harmful SQL quotation marks with doubles
strUsername = formatSQLInput(strUsername)
strEmailAddress = formatSQLInput(strEmailAddress)

'Remove single quotes as they should not be in email addresses
strEmailAddress = Replace(strEmailAddress, "'", "", 1, -1, 1)
   
   
'If a username has been entered check that the password is correct
If NOT strUsername = "" Then
	
	'Initalise the strSQL variable with an SQL statement to query the database
	strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Password, " & strDbTable & "Author.User_code, " & strDbTable & "Author.Salt, " & strDbTable & "Author.Author_email "
	strSQL = strSQL & "FROM " & strDbTable & "Author "
	strSQL = strSQL & "WHERE " & strDbTable & "Author.Username = '" & strUsername & "' AND " & strDbTable & "Author.Author_email = '" & strEmailAddress & "';"
	
	'Set the cursor type property of the record set to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	
	
	'If the query has returned a value to the recordset then generate new password and send it to the user in an email
	If NOT rsCommon.EOF Then
	
		'Read in the users email address from the recordset
		strEmailAddress = rsCommon("Author_email")
		
		'If there is a password in the db to send to change the password and email the user
		If NOT strEmailAddress = "" Then
			
			'Genrate a new user code for the user
			strUserCode = userCode(rsCommon("Username"))
			
			'Generate a new password using an 8 character long hex values
			strPassword = hexValue(8)
			
			'If pass is to be encrypted then do so
			If blnEncryptedPasswords Then
				
				'Create a salt value for the new password
				strSalt = getSalt(8)
				
				'Concatenate salt value to the password
				strEncyptedPassword = LCase(strPassword) & strSalt
				
				'Encrypt the password
				strEncyptedPassword = HashEncode(strEncyptedPassword) 
			
			'Else the password is not to be encrypted
			Else
				strEncyptedPassword = LCase(strPassword)
			End If
			
			
			'Save new password back to the database with the salt
			rsCommon.Fields("Password") = strEncyptedPassword
			rsCommon.Fields("Salt") = strSalt	
			rsCommon.Fields("User_code") = strUserCode		
			
			'Update the database with the new password
			rsCommon.Update
			
		
		
			'Initailise the e-mail body variable with the body of the e-mail
			strEmailBody = strTxtHi
			strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtEmailPasswordRequest & " " & strMainForumName & "."
			strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtEmailPasswordRequest2 & " " & strPassword
			strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtEmailPasswordRequest3
			strEmailBody = strEmailBody & vbCrLf & vbCrLf & "   " & strForumPath
			
			'Initalise the subject of the e-mail
			strSubject = strTxtForumLostPasswordRequest
			
			'Send the e-mail using the Send Mail function created on the send_mail_function.inc file
			blnEmailSent = SendMail(strEmailBody, decodeString(strUsername), decodeString(strEmailAddress), strWebsiteName, decodeString(strForumEmailAddress), strSubject, strMailComponent, false)
			
		Else
			'Set the Invalid e-mail variable to True
			blnInvalidEmail = True	
		End If
	
	
	Else
		'Set the Invalid username variable to True
		blnInvalidUsername = True		
		
	End If
	
	'Clean up
	rsCommon.Close
End If
	


'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>  
<html>
<head>
<title>Forgotten Password</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
		
<!-- Check the from is filled in correctly before submitting -->
<script  language="JavaScript">
<!-- Hide from older browsers...

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";
	
	//Check for a Username
	if (document.frmMailPass.name.value==""){
	
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";
	
		alert(msg + "\n\t<% = strTxtErrorUsername %>");
		document.frmMailPass.name.focus();
		return false;
	}
	
	return true
}
// -->
</script>
<!--#include file="includes/skin_file.asp" -->
</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<div align="center" class="heading"><% = strTxtForgottenPassword %></div><br /><%

'If the user has entered a username that does not exsit then display an error message
If blnInvalidUsername = True Then
%>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
    <td align="center" class="error"><% = strTxtNoRecordOfUsername %><br /><% = strTxtPleaseTryAgain %></td>
    </tr>
  </table><%
  
'If there is no e-mail address for the user then display an error message
ElseIf blnInvalidEmail = True Then
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center"> 
      <p class="text"><% = strTxtNoEmailAddressInProfile %><br />
        <br /><% = strTxtReregisterForForum %><br /><br /><br /></p>
      </td>
  </tr>
</table><%

'If the password has been e-mailed to the user then let them know
ElseIf blnEmailSent = True Then
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
    <td align="center"><span class="text"><% = strTxtPasswordEmailToYou %> 
      </span><br /><br /><br />
    </td>
    </tr>
  </table><%
  
Else
%>
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      
    <td align="center" class="text"><% = strTxtPleaseEnterYourUsername %></td>
    </tr>
  </table><%
  
End If

If blnInvalidEmail = False AND blnEmailSent = False Then
%>
<form method="post" name="frmMailPass" action="forgotten_password.asp" onSubmit="return CheckForm();">
  <br />
  <table width="390" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% = strTableBorderColour %>" height="30">
    <tr> 
      <td height="2" width="483" align="center"> 
        <table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" height="26"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr> 
                  <td align="right" width="30%"><span class="text"><% = strTxtUsername %>:</span>&nbsp;&nbsp;</td>
                  <td width="70%"> 
                    <input type="text" name="name" size="15" maxlength="15" value="<% = strUsername %>">
                  </td>
                </tr>
                <tr> 
                  <td align="right" width="30%"><span class="text"><% = strTxtEmail %>:</span>&nbsp;&nbsp;</td>
                  <td width="70%"> 
                    <input type="text" name="email" size="30" maxlength="60" value="<% = strEmailAddress %>">
                  </td>
                </tr>
                <tr> 
                  <td align="right" width="30%">&nbsp;</td>
                  <td width="70%">
                    <input type="submit" name="Submit" value="<% = strTxtEmailPassword %>">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" height="51" class="text"><% = strTxtValidEmailRequired %></td>
    </tr>
  </table><%
  
End If

%><table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
  <tr>
    <td align="center"><a href="JavaScript:onClick=window.close()"><% = strTxtCloseWindow %></a>
    <br /><br />
<% 
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then 
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If 
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
    </td>
  </tr>
</table>
</body>
</html>