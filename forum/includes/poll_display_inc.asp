 <%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'Declare variables
Dim strPollQuestion		'Holds the poll question
Dim intPollChoiceNumber		'Holds the poll choice number
Dim strPollChoice		'Holds the poll choice
Dim lngPollChoiceVotes		'Holds the choice number of votes
Dim lngTotalPollVotes		'Holds the total number of votes
Dim dblPollVotePercentage	'Holds the vote percentage for the vote choice
Dim lngLastVoteUserID		'Holds the IP address of the voter
Dim blnAlreadyVoted		'Set to true if the user has already voted
Dim blnMultipleVotes		'set to true if multiple votes are allowed


'Initlise variables
blnAlreadyVoted = False


'Get the poll from the database

'Initalise the strSQL variable with an SQL statement to query the database get the thread details
If strDatabaseType = "SQLServer" Then
	strSQL = "EXECUTE " & strDbProc & "PollDetails @lngPollID = " & lngPollID
Else
	strSQL = "SELECT  " & strDbTable & "Poll.*, " & strDbTable & "PollChoice.* "
	strSQL = strSQL & "FROM " & strDbTable & "Poll INNER JOIN " & strDbTable & "PollChoice ON " & strDbTable & "Poll.Poll_ID = " & strDbTable & "PollChoice.Poll_ID "
	strSQL = strSQL & "WHERE (((" & strDbTable & "Poll.Poll_ID)=" & lngPollID & "));"
End If

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a poll then display it
If NOT rsCommon.EOF Then

	'Read in the poll question
	strPollQuestion = rsCommon("Poll_question")

	'Read the last voters ID
	lngLastVoteUserID = rsCommon("Author_ID")

	'See if multiple votes are allowed
	blnMultipleVotes = CBool(rsCommon("Multiple_votes"))
	
	'See if this is a poll only
	blnPollNoReply = CBool(rsCommon("Reply"))

	'If multiple votes are not allowed see if the user has voted before
	If blnMultipleVotes = False Then

		'Check to see if the last voted ID matches the ID of the current user unless the user is using the guest account
		If lngLastVoteUserID = lngLoggedInUserID AND lngLoggedInUserID <> 2 Then blnAlreadyVoted = True

		'Check the user has not already voted by reading in a cookie from there system
		'Read in the Poll ID number of the last poll the user has voted in
		If CInt(Request.Cookies(strCookieName)("PID" & lngPollID)) = lngPollID Then blnAlreadyVoted = True
	End If

	'Loop through and get the total number of votes
	Do While NOT rsCommon.EOF

		'Get the total number of votes
		lngTotalPollVotes = lngTotalPollVotes + rsCommon("Votes")

		'Move to the next record
        	rsCommon.MoveNext
        Loop

        'Go back to the begining of the recordset
        rsCommon.MoveFirst

%><!-- Start Poll -->
 <table width="<% = strTablePollVariableWidth %>" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTablePollBorderColour %>" align="center">
 <tr>
  <form name="frmPoll" method="post" action="poll_cast_vote.asp">
   <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTablePollBgColour %>">
     <tr>
      <td bgcolor="<% = strTablePollBgColour %>"> <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="<% = strTablePollBgColour %>">
        <tr>
         <td colspan="4" background="<% = strTablePollTitleBgImage %>" bgcolor="<% = strTablePollTitleColour %>" class="tHeading"><% = strTxtPollQuestion %>: <% = strPollQuestion %></td>
        </tr>
        <tr><%

'Display the vote choice slection column if the user CAN vote in this poll
If blnVote = True AND blnForumLocked = False AND blnTopicLocked = False AND blnActiveMember = True AND blnAlreadyVoted = False Then

	%>
         <td width="3%" align="center" background="<% = strTablePollColumnHeadingBgImage %>" bgcolor="<% = strTablePollColumnHeadingColour %>" class="tiHeading"><% = strTxtVote %></td><%
End If
%>
         <td width="47%" background="<% = strTablePollColumnHeadingBgImage %>" bgcolor="<% = strTablePollColumnHeadingColour %>" class="tiHeading" nowrap="nowrap"><% = strTxtPollChoice %></td>
        <td width="6%" align="center" nowrap="nowrap" background="<% = strTablePollColumnHeadingBgImage %>" bgcolor="<% = strTablePollColumnHeadingColour %>" class="tiHeading"><% = strTxtVotes %></td>
         <td width="47%" background="<% = strTablePollColumnHeadingBgImage %>" bgcolor="<% = strTablePollColumnHeadingColour %>" class="tiHeading"><% = strTxtPollStatistics %></td>
        </tr><%

 	'Loop through the Poll Choices
 	Do While NOT rsCommon.EOF

 		'Read in the poll details
 		intPollChoiceNumber = Cint(rsCommon("Choice_ID"))
 		strPollChoice = rsCommon("Choice")
 		lngPollChoiceVotes = CLng(rsCommon("Votes"))

		'If there are no votes yet then format the percent by 0 otherwise an overflow error will happen
		If lngTotalPollVotes = 0 Then
			dblPollVotePercentage = FormatPercent(0, 2)

		'Else read in the the percentage of votes cast for the vote choice
		Else
			dblPollVotePercentage = FormatPercent((lngPollChoiceVotes / lngTotalPollVotes), 2)
		End If

 		'Calculate the row colour
 		intRecordLoopCounter = intRecordLoopCounter + 1

        %>
        <tr><%

		'Display the vote radio buttons if the user CAN vote in this poll
		If blnVote = True AND blnForumLocked = False AND blnTopicLocked = False AND blnActiveMember = True AND blnAlreadyVoted = False Then

	%>
         <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePollEvenRowColour) Else Response.Write(strTablePollOddRowColour) %>" background="<% = strTablePollBgImage %>" align="center"><input type="radio" name="voteChoice" value="<% = intPollChoiceNumber %>" id="P<% = intPollChoiceNumber %>"></td><%

        	End If

        %>
         <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePollEvenRowColour) Else Response.Write(strTablePollOddRowColour) %>" background="<% = strTablePollBgImage %>" class="text"><label for="P<% = intPollChoiceNumber %>"><% = strPollChoice %></label></td>
         <td align="center" background="<% = strTablePollBgImage %>" bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePollEvenRowColour) Else Response.Write(strTablePollOddRowColour) %>" class="text"><% = lngPollChoiceVotes %></td>
         <td bgcolor="<% If (intRecordLoopCounter MOD 2 = 0 ) Then Response.Write(strTablePollEvenRowColour) Else Response.Write(strTablePollOddRowColour) %>" background="<% = strTablePollBgImage %>" class="smText" nowrap="nowrap"><img src="<% = strImagePath %>bar_graph_image.gif" width="<% = CInt(Replace(CStr(dblPollVotePercentage), "%", "", 1, -1, 1)) * 2 %>" height="11" align="middle"> [<% = dblPollVotePercentage %>]</td>
        </tr><%

        	'Move to the next record
        	rsCommon.MoveNext
        Loop

        %>
        <tr align="center">
         <td colspan="4" background="<% = strTableBgImage %>" bgcolor="<% = strTablePollBottomRowColour %>" class="text"><%

'Display either text msg if the user can NOT vote or a button if they can

'If the forum is locked display a locked forum meesage
If blnForumLocked = True OR  blnTopicLocked = True Then

	Response.Write(strTxtThisTopicIsClosedNoNewVotesAccepted)

'Else the user can not vote or they are not an active member of the forum
ElseIf blnActiveMember = False OR blnVote = False Then

	Response.Write(strsTxYouCanNotNotVoteInThisPoll)

'Else the user has already voted in this poll and multiple votes are not permitted
ElseIf blnAlreadyVoted = True Then

	Response.Write(strTxtYouHaveAlreadyVotedInThisPoll)

'Else display vote button
Else
%>
          <input type="hidden" name="PID" value="<% = lngPollID %>">
          <input type="hidden" name="TID" value="<% = lngTopicID %>">
          <input type="hidden" name="FID" value="<% = intForumID %>">
          <input type="hidden" name="PN" value="<% = intTopicPageNumber %>">
          <input type="hidden" name="TPN" value="<% = intRecordPositionPageNum %>">
          <input type="submit" name="Submit" value="<% = strTxtCastMyVote %>"><%
End If

%>
         </td>
        </tr>
       </table></td>
     </tr>
    </table></td>
  </form>
 </tr>
</table>
<br />
<!-- End Poll --><%

End If

'Clean up
rsCommon.Close


'Display a msg letting the user know if there vote has been cast or not
Select Case Request.QueryString("RN")
	Case "1"
		Response.Write("<script  language=""JavaScript"">")
		Response.Write("alert('" & strTxtThankYouForCastingYourVote & "');")
		Response.Write("</script>")
	Case "2"
		Response.Write("<script  language=""JavaScript"">")
		Response.Write("alert('" & strTxtYouDidNotSelectAChoiceForYourVote & "');")
		Response.Write("</script>")
End Select
%>