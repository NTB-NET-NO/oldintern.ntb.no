<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


%>
<html>
<head>
<title>Forum Administration Menu</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->
     	
<link href="includes/default_style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" background="images/main_bg.gif" text="#000000">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    
  <td height="2" align="center" class="tiText"> <span class="heading">Forum Administration Menu</span><br><font size="2">
   <a href="../default.asp" target="_top">Return to the Main Forum</a>
   <%

'If this is the main forum admin let him change the admin username and password
If lngLoggedInUserID = 1 Then %>
   <br />
   <br />
   For security it is highly recommended that you <a href="change_admin_username.asp" target="_self">change the Admin Username and Password</a> to stop others messing up your Forums!<%
	 
	 End If
	 %></td>
  </tr>
</table>
<br />
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td > <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> <table width="100%" border="0" cellpadding="7" cellspacing="0">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText"><b>Forum Administration</b></span><b><br />
         </b>The following pages are to help you administer the forum</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="view_forums.asp" target="_self">Forum Administration</a><br />
         Create, Amend, Delete any forum's and forum categories, alter forum details, set forum permissions, lock forums, password protect forums, etc.</td>
       </tr>
       <tr>
        <td bgcolor="#F5F5FA" class="text"><a href="close_forums.asp" target="_self">Lock Forums</a><br />
         From this option you can Lock the Forums so that no-one can post, register, login. etc. on the forum. Useful for forum maintenance.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><p><a href="http://www.webwizforums.com/update.asp?v=<% = Server.URLEncode(strVersion) %>" target="_blank">Check for updates</a><br />
          Check for updates to the Web Wiz Forums application version <% = strVersion %>.
         </td>
         <tr> 
        <td bgcolor="#F5F5FA" class="text"><p><a href="http://www.webwizforums.com" target="_blank">About</a><br />
          About Web Wiz Forums.
         </td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td > <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText"><b>Forum User Group</b></span><b> Administration<br />
         </b>The following pages are to help you administer forum User Groups</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="view_groups.asp" target="_self">Group Administration</a><br />
         Create, Amend, Delete, change the details, etc. of forum User Groups.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="group_perm_forum.asp" target="_self">Group Permissions Administration</a><br />
         From this option you can configure permissions on forums for User Groups, set permissions for forum moderation, entry, posting, creating polls, etc.</td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td > <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText"><b>Membership</b></span><b> Administration<br />
         </b>The following pages are to help you administer Forum User Memberships</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="select_members.asp" target="_self">Membership Administration</a><br />
         Administer members accounts, make them moderators, change status, delete members, suspend accounts, etc.</td>
       </tr>
       <tr>
        <td bgcolor="#F5F5FA" class="text"><a href="find_user.asp" target="_self">Member Permissions Administration</a><br />
         From this option you can configure permissions on forums for Members, set permissions for forum moderation, entry, posting, creating polls, etc.</td>
       </tr>
       <tr>
        <td bgcolor="#F5F5FA" class="text"><a href="add_member.asp" target="_self">Add New Member </a><br />
From this option you can Add a New Forum Member. </td>
       </tr>
       <tr>
        <td bgcolor="#F5F5FA" class="text"><a href="change_username.asp" target="_self">Change Username </a><br />
From this option you can change the username of your forum members. </td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="suspend_registration.asp" target="_self">Suspend New Registrations</a><br />
         From this option you can Suspend New Users from Registering to use the forum.</td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td > <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText">General Forum Admin</span><br />
         The following pages are to help you setup, configure and administer your board</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="forum_configure.asp" target="_self">Forum Configuration</a><br />
         Configure general options to customise the way the forum functions and looks.</td>
       </tr>
       <tr><%

'If this is the main forum admin let him change the admin username and password
If lngLoggedInUserID = 1 Then %>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="change_admin_username.asp" target="_self">Change Admin Username and Password</a><br />
         Definitely recommended for higher Forum security!</td>
       </tr>
       <%
End If

%>   
        <td bgcolor="#F5F5FA" class="text"><a href="date_time_configure.asp" target="_self">Date and Time Settings</a><br />
         Change the format of dates and times in the forum or change time/date settings to your local settings if the server is in a foreign country to your own.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="email_notify_configure.asp" target="_self"> Email Setup and Configuration </a><br />
         Configure email settings and enable email features such as email notification, email account activation, etc.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="member_mailier.asp" target="_self">Mass Email Members</a><br />
         From this option you can send emails to all members or members of specific User Group.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="upload_configure.asp" target="_self">File, Image and Avatar Upload Setup and Configuration </a><br />
         Allow users to upload images and files in their posts, also setup Avatar uploading.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="spam_configure.asp" target="_self">Anti-Spam Configuration</a><br />
         Configure the Anti-Spam settings so you don't get members spamming the forum with 1,000's of unwanted and abusive posts in minutes.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="bad_word_filter_configure.asp" target="_self">Configure the Bad Word Filter</a><br />
         Remove or add new swear words to the bad word filter.</td>
       </tr><%
       
'If this is an access database show the compact and repair feature
If strDatabaseType = "Access" Then %>      
<tr>
        <td bgcolor="#F5F5FA" class="text">
<p><a href="compact_access_db.asp">Compact and Repair Access Database</a><br>
          Form here you can compact and repair your Forums Access database to increase performance.</p>
         </td>
       </tr><%

End If %>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="statistics.asp" target="_self">Forum Statistics</a><br />
         Displays a list of forum statistics.</td>
       </tr><%

'If the forum is using an SQL server then display stats oabout the server link
If strDatabaseType = "SQLServer" Then %>
       <tr>
        <td bgcolor="#F5F5FA" class="text">
<p><a href="sql_server_db_stats.asp">MS SQL Server Database Statistics</a><br>
          Displays statistics about the MS SQL Server Database you are using for your forum.</p>
         </td>
       </tr><%
End If %>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td > <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText"><b>Ban Administration</b></span><b><br />
         </b>The following pages are to help you control who uses the forums</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="ip_blocking.asp" target="_self">IP Address Banning</a><br />
         Use this option to Ban IP addresses and Ranges.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="email_domain_blocking.asp" target="_self">Email Address Banning</a><br />
         Use this option to Ban Email address and Email Domains from being registered on the board.</td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td> 
   <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr> 
     <td> 
      <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="text"><span class="lgText">Forum Clearout and Archive</span><br />
         The following pages are to clear out the database if you find it is getting a little full and slowing down and to archive topics</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="resync_forum_post_count.asp" target="_self">Re-sync Topic and Post Count</a><br>
         Re-sync the Topic and Post Count display for the forum's</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="archive_topics_form.asp" target="_self">Batch Lock Old Topics</a><br />
         Batch lock old Topics allows you to batch lock Topics that haven't been posted in for sometime.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_posts_form.asp" target="_self">Batch Delete Topics</a><br />
         Clean out the Forum Database by batch deleting topics that have not been posted in for sometime.</td>
       </tr>
       <tr>
        <td bgcolor="#F5F5FA" class="text"><a href="batch_move_topics_form.asp" target="_self">Batch Move Topics</a><br />
         Batch move Topics from one forum to another.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_pm_form.asp" target="_self">Batch Delete Private Messages</a><br />
         Clean out the Forum Database by batch deleting old Private Messages.</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="batch_delete_members_form.asp" target="_self">Batch Delete Members</a><br />
         Clean out the Forum Database by batch deleting Members who have never posted.</td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br />
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#000000">
 <tr> 
  <td> 
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
    <tr> 
     <td> 
      <table width="100%" border="0" cellspacing="0" cellpadding="7">
       <tr> 
        <td bgcolor="#CCCEE6" class="lgText">Removing the Web Wiz Forums links</td>
       </tr>
       <tr> 
        <td bgcolor="#F5F5FA" class="text"><a href="remove_link_buttons.asp">Remove Powered By Web Wiz Forums links</a><br />
         Remove the Powered by Web Wiz Forums links from the forum application pages. </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<div align="center"><br />
 <br />
 <table width="98%" border="0" cellspacing="0" cellpadding="1" bgcolor="#000000">
  <tr> 
   <td width="986"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#EFEF#F5F5FA">
     <tr> 
      <td width="100%" height="186" align="center" bgcolor="#EAEAF4" class="text">I have spent many 1000's of unpaid hours in development and support this and the other applications available for free from 
       Web Wiz Guide. 
       <p class="text">If you like using this application then please help support the development and update of this and future applications from Web Wiz Guide by purchasing a link removal key for Web 
        Wiz Forums. For more info click on the link below:-<br />
        <br />
        <a href="remove_link_buttons.asp">Remove Powered By Web Wiz Forums links</a><br />
        <br />
        The Web Wiz Forums application remains free and you may still use it as much as you like both privately and commercially, the purchasing the link removal key is only a request to help me cover some 
        of the costs involved.<br />
        <br />
        For more info contact: -<br />
        <a href="mailto:purchase@webwizguide.info">purchase@webwizguide.info</a><br />
        Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom. </p>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 <br />
 <a href="http://www.webwizforums.com" target="_blank" class="boldLink">Check for updates to Web Wiz Forums</a><br />
 <br />
 <a href="http://www.webwizguide.info/asp/sample_scripts/default.asp" target="_blank">Other Free ASP Applications from Web Wiz Guide</a><br />
 </div>
</body>
</html>
<%
'Reset Server Objects
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
