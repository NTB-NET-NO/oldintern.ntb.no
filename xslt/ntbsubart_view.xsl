<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>

<!-- 
	Stylesheet for displaying subarticles for the feature section
-->

<xsl:template match="/nitf">
<html>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<body class='viewBody'>

	<div><p></p></div>

	<div class="viewHeadline"><xsl:value-of select="body/body.head/hedline/hl1"/></div>
	
	<xsl:apply-templates select="body/body.content"/>
	
	<xsl:if test="body/body.content/media[@media-type='image' and not(@class)]">
		<hr size="1" />
		<table>
			<xsl:apply-templates select="body/body.content/media[@media-type='image' and not(@class)]"/>
		</table>
	</xsl:if>

</body>
</html>
</xsl:template>


<!-- Templates -->

<xsl:template match="body/body.content">
	<xsl:choose>
	<xsl:when test="p">
		<xsl:apply-templates select="p | hl2 | table | br"/>
	</xsl:when>	
	<xsl:otherwise>
		<div class='viewNews'><xsl:value-of select="."/></div>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="p[.!='']">
	<!-- Normal paragraphs -->
	<div class="viewNews"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[.='']">
	<!-- Empty paragraphs -->
	<br/>
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
	<!-- Paragraph of "ingress" -->
	<div class="viewIngress"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[@innrykk='true']">
	<!-- Paragraph of "Br�dtekst innrykk" -->
	<div class="viewIndent"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[@style='tabellkode']">
	<!-- Paragraph of "tabellkode" -->
	<div class="viewTableHead"><xsl:value-of select="."/></div>
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
	<!-- Normal paragraphs -->
	<div class="viewNews"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="hl2">
	<!-- Mellomtittel -->
	<div class="viewHl2"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="table">
	<!-- Tabeller -->
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>

<xsl:template match="body/body.content/media[@media-type='image' and not(@class)]">
	<!-- Template for Scanpix media	-->
	<tr>
		<td>
			<a target="_blank"><xsl:attribute name="href">picture.aspx?Type=L&amp;SpCode=<xsl:value-of select="media-reference/@source"/></xsl:attribute>
			<img border="0"><xsl:attribute name="src">http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_<xsl:value-of select="media-reference/@source"/></xsl:attribute></img>
			</a>
		</td>
		<td valign="top">
			<div class="viewIngress">Bilde: <xsl:value-of select="media-reference/@source"/></div>
			<div class="viewNews"><xsl:value-of select="media-caption"/></div>
			<div class="viewNews"><a target="_blank"><xsl:attribute name="href">picture.aspx?Type=H&amp;SpCode=<xsl:value-of select="media-reference/@source"/></xsl:attribute>Last ned h�yoppl�selig</a></div>
		</td>
	</tr>	
</xsl:template>

</xsl:stylesheet>