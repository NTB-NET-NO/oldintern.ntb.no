<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************




'Set the response buffer to true
Response.Buffer = True

%>
<html>
<head>
<title>Administer Forums</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<link href="includes/default_style.css" rel="stylesheet" type="text/css">
</head>
<body  background="images/main_bg.gif" bgcolor="#FFFFFF" text="#000000">
<div align="center"><span class="heading">Administer Forums</span><br />
 <a href="admin_menu.asp" target="_self">Return to the the Administration Menu</a></div>
<form name="form1" method="post" action="update_forum_order.asp">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td align="center" class="text">From here you can add, delete, edit, or lock Categories and Forums.<br>
    <br>
    Click on Forum name or Category to Amend Details.<br /> <br />
    Select the order you would like the forums to be in from the Order drop down list and click on the Update Order button
 </table>
 <br />
 <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000" align="center">
  <tr>
   <td> <table width="100%" border="0" cellspacing="1" cellpadding="3" align="center">
     <tr>
      <td bgcolor="#CCCEE6" class="lgText" height="12"><b><span style="font-size: 12px;">Forum</span></b></td>
      <td bgcolor="#CCCEE6" class="lgText" width="9%" height="12" align="center"><b><span style="font-size: 12px;">Lock</span></b></td>
      <td bgcolor="#CCCEE6" class="lgText" width="9%" height="12" align="center"><b><span style="font-size: 12px;">Delete</span></b></td>
      <td bgcolor="#CCCEE6" class="lgText" width="10%" height="12" align="center"><b><span style="font-size: 12px;">Order</span></b></td>
     </tr><%

'Dimension variables
Dim rsForum 			'Holds the Recordset for the forum details
Dim strCategory			'Holds the categories
Dim intCatID			'Holds the category ID number
Dim intForumID			'Holds the forum ID number
Dim strForumName		'Holds the forum name
Dim strForumDiscription		'Holds the forum description
Dim blnForumLocked		'Set to true if the forum is locked
Dim intLoop			'Holds the number of times round in the Loop Counter
Dim intNumOfForums		'Holds the number of forums
Dim intForumOrder		'Holds the order number of the forum
Dim intNumOfCategories		'Holds the number of categories
Dim intCatOrder			'Holds the order number of the category


'Read the various categories from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Category.* FROM " & strDbTable & "Category ORDER BY " & strDbTable & "Category.Cat_order ASC;"

'Set the curson type to 1 so we can count the number of records returned
rsCommon.CursorType = 1

'Query the database
rsCommon.Open strSQL, adoCon

'Check there are categories to display
If rsCommon.EOF Then

	'If there are no categories to display then display the appropriate error message
	Response.Write vbCrLf & "<td bgcolor=""#FFFFFF"" colspan=""4""><span class=""text"">There are no Categories to display. <a href=""category_details.asp?mode=new"">Click here to create a Forum Category</a></span></td>"

'Else there the are categories so write the HTML to display categories and the forum names and a discription
Else
	'Create a recordset to get the forum details
	Set rsForum = Server.CreateObject("ADODB.Recordset")

	'Get the number of categories
	intNumOfCategories = rsCommon.RecordCount

	'Loop round to read in all the categories in the database
	Do While NOT rsCommon.EOF

		'Get the category name from the database
		strCategory = rsCommon("Cat_name")
		intCatID = CInt(rsCommon("Cat_ID"))
		intCatOrder = CInt(rsCommon("Cat_order"))


		'Display the category name
		
		%>
     <tr bgcolor="#EAEAF4"> 
      <td colspan="2"><a href="category_details.asp?mode=edit&CatID=<% = intCatID %>" target="_self"><b> 
       <% = strCategory %>
       </b></a></td>
      <td align="center"><a href="delete_category.asp?CatID=<% = intCatID %>" onClick="return confirm('Are you sure you want to Delete this Category?\n\nWARNING: Deleting this category will permanently  remove all Forum(s) in this Category and all the Posts!')"><img src="images/delete_icon.gif" width="15" height="16" border="0" alt="Delete"></a></td>
      <td align="center"> 
       <select name="catOrder<% = intCatID %>"><%
           'loop round to display the number of forums for the order select list
           For intLoop = 1 to intNumOfCategories
		Response.Write("<option value=""" & intLoop & """ ")

			'If the loop number is the same as the order number make this one selected
			If intCatOrder = intLoop Then
				Response.Write("selected")
			End If

		Response.Write(">" & intLoop & "</option>")
           Next
           %>
       </select> </td>
     </tr><%
		'Read the various forums from the database
		'Initalise the strSQL variable with an SQL statement to query the database
		strSQL = "SELECT " & strDbTable & "Forum.* FROM " & strDbTable & "Forum WHERE " & strDbTable & "Forum.Cat_ID = " & intCatID & " ORDER BY " & strDbTable & "Forum.Forum_Order ASC;"

		rsForum.CursorType = 1

		'Query the database
		rsForum.Open strSQL, adoCon

		'Check there are forum's to display
		If rsForum.EOF Then

			'If there are no forum's to display then display the appropriate error message
			Response.Write vbCrLf & "<td bgcolor=""#FFFFFF"" colspan=""4""><span class=""text"">There are no Forum's to display. <a href=""forum_details.asp?mode=new"">Click here to create a Forum</a></span></td>"

		'Else there the are forum's to write the HTML to display it the forum names and a discription
		Else

			'Get the number of categories
			intNumOfForums = rsForum.RecordCount

			'Loop round to read in all the forums in the database
			Do While NOT rsForum.EOF

				'Read in forum details from the database
				intForumID = CInt(rsForum("Forum_ID"))
				strForumName = rsForum("Forum_name")
				strForumDiscription = rsForum("Forum_description")
				intForumOrder = CInt(rsForum("Forum_order"))
				blnForumLocked = CBool(rsForum("Locked"))

				'Write the HTML of the forum descriptions and hyperlinks to the forums%>
     <tr bgcolor="#F5F5FA"> 
      <td bgcolor="#F5F5FA" class="text"><a href="forum_details.asp?mode=edit&FID=<% = intForumID %>" target="_self"> 
       <% = strForumName %>
       </a><br />
       <span class="smText"><% = strForumDiscription %></span></td>
      <td width="9%" align="center" class="text"> 
       <%

		            	'If the forum is locked and the user is admin let them unlock it
				If blnForumLocked = True Then
				  	Response.Write ("	<a href=""../lock_forum.asp?code=2&mode=UnLock&FID=" & intForumID & """ OnClick=""return confirm('Are you sure you want to Un-Lock this Forum?')""><img src=""images/forum_locked_icon.gif"" width=""11"" height=""14"" border=""0"" align=""baseline"" alt=""Un-Lock Forum""></a>")
				'If the forum is not lovked and this is the admin then let them lock it
				ElseIf blnForumLocked = False Then
				  	Response.Write ("	<a href=""../lock_forum.asp?code=2&mode=Lock&FID=" & intForumID & """ OnClick=""return confirm('Are you sure you want to Lock this Forum?')""><img src=""images/forum_unlock_icon.gif"" width=""15"" height=""14"" border=""0"" align=""baseline"" alt=""Lock Forum""></a>")
				End If

               %>
      </td>
      <td width="9%" align="center" class="text"> <a href="delete_forum.asp?FID=<% = intForumID %>" OnClick="return confirm('Are you sure you want to Delete this Forum?\n\nWARNING: Deleting this forum will permanently  remove all Posts in this Forum!')"><img src="images/delete_icon.gif" width="15" height="16" border="0" alt="Delete"></a></td>
      <td width="10%" class="text"  align="center"> 
       <select name="forumOrder<% = intForumID %>"><%

           'loop round to display the number of forums for the order select list
           For intLoop = 1 to intNumOfForums

		Response.Write("<option value=""" & intLoop & """ ")

			'If the loop number is the same as the order number make this one selected
			If intForumOrder = intLoop Then
				Response.Write("selected")
			End If

		Response.Write(">" & intLoop & "</option>")
           Next
           %>
       </select> </td>
     </tr><%


				'Move to the next database record
				rsForum.MoveNext
			'Loop back round for next forum
			Loop
		End If

		'Close recordsets
		rsForum.Close

		'Move to the next database record
		rsCommon.MoveNext
	'Loop back round for next category
	Loop
End If

Set rsForum = Nothing
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing
%>
    </table></td>
  </tr>
 </table>
 <div align="center">
  <table width="98%" border="0" cellspacing="0" cellpadding="3">
   <tr align="right"> 
    <td width="100%"><input type="submit" name="Submit" value="Update Order"></td>
   </tr>
  </table>
 </div>
</form>
<div align="center">
 <table width="98%" border="0" cellspacing="0" cellpadding="3">
  <tr align="center"> 
   <td width="50%"><form action="category_details.asp?mode=new" method="post" name="form2" target="_self">
     <input type="submit" name="Submit" value="Create New Forum Category"> 
    </form>
   </td>
   <td width="50%"><form action="forum_details.asp?mode=new" method="post" name="form3" target="_self">
     <input type="submit" name="Submit" value="Create New Forum">
    </form>
   </td>
  </tr>
 </table>
</div>
</body>
</html>