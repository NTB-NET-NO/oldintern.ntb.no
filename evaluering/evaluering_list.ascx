<%@ Control Language="vb" AutoEventWireup="false" Inherits="intranett.evaluering_list" CodeFile="evaluering_list.ascx.vb" %>

<!-- Lists items from the forum -->

<asp:datalist CSSClass="artikkel_tekst" id="dlItems" runat="server" CellPadding="0" CellSpacing="0"
	RepeatLayout="Table">
	<ItemTemplate>
			<%# dateheader%>
			<%# Format(dr("start_date"), "dd.MM.yyyy") %> <!-- br -->
			<b><A href='view_evitem.aspx?FID=<%= FID %>&TID=<%# dr("topic_id") %>' target="evaluering_main" >
				<%# dr("Subject") %>
			</A></b>
			<!--br>( <i>
			<asp:Label ID="name" Runat="server" Visible=True>
			</asp:Label>
			</i> )
			-->
			<br><br>
	</ItemTemplate>
</asp:datalist>
