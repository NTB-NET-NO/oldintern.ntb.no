<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="language_files/admin_language_file_inc.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim rsSelectForum	'Holds the db recordset
Dim lngTopicID 		'Holds the topic ID number to return to
Dim intForumID		'Holds the forum ID number
Dim strBlockIP		'Holds the IP address to block
Dim strBlockedIPList	'Holds the IP addresses in the blocked list
Dim lngBlockedIPID	'Holds the ID number of the blcoked db record
Dim laryCheckedIPAddrID	'Holds the array of IP addresses to be ditched




'Read in the message ID number to be deleted
lngTopicID = CLng(Request("TID"))


'If the person is not an admin or a moderator then send them away
If lngTopicID = "" Then
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	'Redirect
	Response.Redirect("default.asp")
End If




'Initliase the SQL query to get the topic details from the database
strSQL = "SELECT " & strDbTable & "Topic.* "
strSQL = strSQL & "FROM " & strDbTable & "Topic "
strSQL = strSQL & "WHERE " & strDbTable & "Topic.Topic_ID = " & lngTopicID & ";"

'Query the database
rsCommon.Open strSQL, adoCon

'If there is a record returened read in the forum ID
If NOT rsCommon.EOF Then
	intForumID = CInt(rsCommon("Forum_ID"))
End If

'Close rs
rsCommon.Close



'Call the moderator function and see if the user is a moderator (if not logged in as an admin)
If blnAdmin = False Then blnModerator = isModerator(intForumID, intGroupID)


'Only run the following lines if this is a moderator or an admin
If blnAdmin OR blnModerator Then

	'Run through till all checked IP addresses are deleted
	For each laryCheckedIPAddrID in Request.Form("chkDelete")
	
	
		'Here we use the less effiecient ADO to delete from the database this way we can throw in a requery while we wait for slow old MS Access to catch up
	
		'Delete the IP address from the database	
		strSQL = "SELECT * FROM " & strDbTable & "BanList WHERE " & strDbTable & "BanList.Ban_ID="  & CInt(laryCheckedIPAddrID) & ";"
		
		With rsCommon		
			'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
			.CursorType = 2
			
			'Set the Lock Type for the records so that the record set is only locked when it is updated
			.LockType = 3
			
			'Query the database
			.Open strSQL, adoCon
			
			'Delete from the db
			If NOT .EOF Then .Delete
			
			'Requery
			.Requery
			
			'Close the recordset
			.Close
		End With
		
	Next
	
	
	
	'Read in all the blocked IP address from the database
	
	'Initalise the strSQL variable with an SQL statement to query the database to count the number of topics in the forums
	strSQL = "SELECT " & strDbTable & "BanList.Ban_ID, " & strDbTable & "BanList.IP FROM " & strDbTable & "BanList WHERE " & strDbTable & "BanList.IP Is Not Null;"
	
	'Set the cursor	type property of the record set	to Dynamic so we can navigate through the record set
	rsCommon.CursorType = 2
	
	'Set the Lock Type for the records so that the record set is only locked when it is updated
	rsCommon.LockType = 3
	
	'Query the database
	rsCommon.Open strSQL, adoCon
	
	
	
	'If this is a post back then  update the database
	If Request.Form("IP") <> "" Then
	
		'Read in the IP address to block
		strBlockIP = Trim(Mid(Request.Form("IP"), 1, 30))
	
		'Update the recordset
		With rsCommon
		
			.AddNew
	
			'Update	the recorset
			.Fields("IP") = strBlockIP
	
			'Update db
			.Update
	
			'Re-run the query as access needs time to catch up
			.ReQuery
	
		End With
	End If
End If
%>
<html>
<head>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />
<title>IP Blocking</title>

<!-- The Web Wiz Guide ASP forum is written by Bruce Corkhill �2001-2004
    	 If you want your forum then goto http://www.webwizforums.com -->

<script language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a subject
	if (document.frmIPadd.IP.value==""){
		errorMsg += "\n\t<% = strTxtErrorIPEmpty %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
</script>

<!--#include file="includes/skin_file.asp" -->

</head>
<body bgcolor="<% = strBgColour %>" text="<% = strTextColour %>" background="<% = strBgImage %>" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" OnLoad="self.focus();">
<div align="center" class="heading"><% = strTxtIPBlocking %></div>
    <br /><%

'If there is no topic info returned by the rs then display an error message
If blnAdmin = False AND blnModerator = False Then

	'Close the rs
	rsCommon.Close

	Response.Write("<div align=""center"">")
	Response.Write("<span class=""lgText"">" & strTxtAccessDenied & "</span><br /><br /><br />")
	Response.Write("</div>")

'Else display a form to allow updating of the topic
Else

'Display the IP blocked list	
%>
<form name="frmIPList" method="post" action="pop_up_IP_blocking.asp">
 <table width="350" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td>
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="100%">
     <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>">
      <td colspan="2" bgcolor="<% = strTableTitleColour %>" background="<% = strTableTitleBgImage %>" class="tHeading"><% = strTxtBlockedIPList %></td>
     </tr><%
	'Display the IP blcok list
	If rsCommon.EOF Then 
		
		'Disply no entires forun
		Response.Write("<td colspan=""2"" align=""center"" bgcolor=""" & strTableColour & """ background=""" & strTableBgImage & """ class=""bold"">" & strTxtYouHaveNoBlockedIpAddesses & "</td>")
	
	'Else disply the IP block list
	Else
	
		'Loop through the recordset
		Do While NOT rsCommon.EOF
	
     			'Read in the topic details
     			lngBlockedIPID = CLng(rsCommon("Ban_ID"))
			strBlockedIPList = rsCommon("IP")
     
     %>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td width="3%" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="checkbox" name="chkDelete" value="<% = lngBlockedIPID %>"></td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strBlockedIPList %></td>
     </tr><%
     

			'Move to the next record in the recordset
			rsCommon.MoveNext
		Loop
	End If

'Reset Server Objects
rsCommon.Close
%>
          </select></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" align="center">
      <td valign="top" colspan="2" background="<% = strTableBgImage %>">
        <input type="hidden" name="TID" value="<% = lngTopicID %>">
        <input type="submit" name="Submit" value="<% = strTxtRemoveIP %>">
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
</form>
<form name="frmIPadd" method="post" action="pop_up_IP_blocking.asp" onSubmit="return CheckForm();">
   <table width="350" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" align="center">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<% = strTableBgColour %>">
    <tr>
     <td bgcolor="<% = strTableBgColour %>">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<% = strTableBgColour %>">
    <tr>
   <td>
    <table border="0" align="center" cellpadding="4" cellspacing="1" width="350">
     <tr align="left" bgcolor="<% = strTableColour %>" background="<% = strTableTitleBgImage %>">
      <td colspan="2" bgcolor="<% = strTableTitleColour %>" background="<% = strTableTitleBgImage %>" class="tHeading"><% = strTxtBlockIPAddressOrRange %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td colspan="2" align="center" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="smText"><% = strTxtBlockIPRangeWhildcardDescription %></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>">
      <td align="right" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" class="text"><% = strTxtIpAddressRange %>:</td>
      <td bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>"><input type="text" name="IP" size="20" maxlength="30" value="<% = Server.HTMLEncode(Request.QueryString("IP")) %>"/></td>
     </tr>
     <tr bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" align="center">
      <td valign="top" colspan="2" background="<% = strTableBgImage %>">
        <input type="hidden" name="TID" value="<% = lngTopicID %>">
        <input type="submit" name="Submit" value="<% = strTxtBlockIPAddressRange %>">
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
 </td>
  </tr>
 </table>
   </form><%

End If

%>
  <table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr>
      <td align="center">
        <a href="JavaScript:onClick=window.close();"><% = strTxtCloseWindow %></a>
      </td>
    </tr>
  </table>
<br /><br />   
<div align="center">
<%

'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
%>
</div>
</body>
</html>