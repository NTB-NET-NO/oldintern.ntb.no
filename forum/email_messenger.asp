<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_send_mail.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True


'Dimension variables
Dim lngToUserID		'Holds the user ID of who the email is to
Dim strToUser		'Holds the user name of the person the email is to
Dim intForumID		'Holds the forum ID
Dim blnShowEmail	'set to true if the user allws emailing to them
Dim strToEmail		'Holds the email address of who the email is to
Dim strFromEmail	'Holds the email address of who the email is from
Dim blnEmailSent	'Set to true if the email has been sent
Dim strEmailBody
Dim strSubject


'Get who the email is to
lngToUserID = CLng(Request("SEID"))

'If there is no recopinet for the email then send em to homepage
If Request("SEID") = "" OR blnEmailMessenger = False OR blnEmail = False Then
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing

	Response.Redirect("default.asp")
End If


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	
	'Clean up
	Set rsCommon = Nothing
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If


'Initlise variables
blnEmailSent = False

'Get the email address and name of the person the email is to be sent to

'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Author.Username, " & strDbTable & "Author.Author_email, " & strDbTable & "Author.Show_email "
strSQL = strSQL & "FROM " & strDbTable & "Author "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & lngToUserID

'Query the database
rsCommon.Open strSQL, adoCon

'Read in the details from the user
If NOT rsCommon.EOF Then

	strToUser = rsCommon("Username")
	strToEmail = rsCommon("Author_email")
	blnShowEmail = CBool(rsCommon("Show_email"))
End If

'Clean up
rsCommon.Close


'Get the email of who the email is from
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "SELECT " & strDbTable & "Author.Author_email "
strSQL = strSQL & "FROM " & strDbTable & "Author "
strSQL = strSQL & "WHERE " & strDbTable & "Author.Author_ID = " & lngLoggedInUserID

'Query the database
rsCommon.Open strSQL, adoCon

'Read in the details from the user
If NOT rsCommon.EOF Then

	strFromEmail = rsCommon("Author_email")
End If

'Clean up
rsCommon.Close


'If this is a post back send the mail
If Request.Form("postBack") Then
	
	'Check the session ID to stop spammers using the email form
	Call checkSessionID(Request.Form("sessionID"))

	'Initilalse the body of the email message
	strEmailBody = strTxtHi & " " & strToUser & ","
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtTheFollowingEmailHasBeenSentToYouBy & " " & strLoggedInUsername & " " & strTxtFromYourAccountOnThe & " " & strMainForumName & "."
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtIfThisMessageIsAbusive & ": - "
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strForumEmailAddress
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtIncludeThisEmailAndTheFollowing & ": - " & "BBS=" & strMainForumName & ";ID=" & lngLoggedInUserID & ";USR= " & strLoggedInUsername & ";"
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtReplyToEmailSetTo & " " & strLoggedInUsername & "."
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & strTxtMessageSent & ": -"
	strEmailBody = strEmailBody & vbCrLf & "---------------------------------------------------------------------------------------"
	strEmailBody = strEmailBody & vbCrLf & vbCrLf & Request.Form("message")

	'Inititlaise the subject of the e-mail
	strSubject = Request.Form("subject")

	'Send the e-mail using the Send Mail function created on the send_mail_function.inc file
	blnEmailSent = SendMail(strEmailBody, strToUser, strToEmail, strLoggedInUsername, strFromEmail, strSubject, strMailComponent, false)

	'If the user wants a copy of the email as well send em one
	If Request.Form("mySelf") Then
		Call SendMail(strEmailBody, strLoggedInUsername, strFromEmail, strLoggedInUsername, strFromEmail, strSubject, strMailComponent, false)
	End If

End If
%>
<html>
<head>
<title>Email Messenger</title>
<meta name="copyright" content="Copyright (C) 2001-2004 Bruce Corkhill" />

<!-- Web Wiz Forums ver. <% = strVersion %> is written and produced by Bruce Corkhill �2001-2004
     	If you want your own FREE Forum then goto http://www.webwizforums.com -->

<!-- Check the from is filled in correctly before submitting -->
<script language="JavaScript">

//Function to check form is filled in correctly before submitting
function CheckForm () {

	var errorMsg = "";

	//Check for a subject
	if (document.frmEmailMsg.subject.value==""){
		errorMsg += "\n\t<% = strTxtErrorTopicSubject %>";
	}

	//Check for message
	if (document.frmEmailMsg.message.value==""){
		errorMsg += "\n\t<% = strTxtNoMessageError %>";
	}

	//If there is aproblem with the form then display an error
	if (errorMsg != ""){
		msg = "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine1 %>\n";
		msg += "<% = strTxtErrorDisplayLine2 %>\n";
		msg += "<% = strTxtErrorDisplayLine %>\n\n";
		msg += "<% = strTxtErrorDisplayLine3 %>\n";

		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}

	return true;
}
</script>

<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/navigation_buttons_inc.asp" -->
<table width="<% = strTableVariableWidth %>" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
  <td align="left" class="heading"><% = strTxtEmailMessenger %></td>
</tr>
 <tr>
  <td align="left" width="71%" class="bold"><img src="<% = strImagePath %>open_folder_icon.gif" border="0" align="absmiddle">&nbsp;<a href="default.asp" target="_self" class="boldLink"><% = strMainForumName %></a><% = strNavSpacer %><% = strTxtEmailMessenger %><br /></td>
  </tr>
</table>
 <%
'Clean up
Set rsCommon = Nothing
adoCon.Close
Set adoCon = Nothing

'If the users account is suspended then let them know
If blnActiveMember = False Then
	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtForumMemberSuspended & "</span><br /><br /><br /><br /><br /></div>"

'Else if the user is not logged in so let them know to login
ElseIf intGroupID = 2 Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtMustBeRegistered & "</span><br /><br />"
	Response.Write vbCrLf & "<a href=""registration_rules.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "register.gif""  alt=""" & strTxtRegister & """ border=""0"" align=""absmiddle""></a>&nbsp;&nbsp;<a href=""login_user.asp?FID=" & intForumID & """ target=""_self""><img src=""" & strImagePath & "login.gif""  alt=""" & strTxtLogin & """ border=""0"" align=""absmiddle""></a><br /><br /><br /><br /></div>"

'If the email has been sent display the appropriate message
ElseIf blnEmailSent Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtYourEmailHasBeenSentTo & " " & strToUser & "</span><br /><br /><a href=""default.asp"" target=""_self"">" & strTxtReturnToDiscussionForum & "</a><br /><br /><br /><br /><br /></div>"

'Else If the to user doesn't have an email address then don't the user can not send to them
ElseIf isNull(strToEmail) Or strToEmail = "" Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtYouCanNotEmail & " " & strToUser & ", " & strTxtTheyDontHaveAValidEmailAddr & "</span><br /><br /><br /><br /><br /></div>"

'Else If the current user doesn't have a valid email address in their profile then they can't send an email
ElseIf isNull(strFromEmail) OR strFromEmail = "" Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtYouCanNotEmail & " " & strToUser & ", " & strTxtYouDontHaveAValidEmailAddr & "</span><br /><br /><br /><br /><br /></div>"

'Else If the to user has choosen to hide their email address
ElseIf blnShowEmail = False AND blnAdmin = False Then

	Response.Write vbCrLf & "<div align=""center""><br /><br /><span class=""text"">" & strTxtYouCanNotEmail & " " & strToUser & ", " & strTxtTheyHaveChoosenToHideThierEmailAddr & "</span><br /><br /><br /><br /><br /></div>"

'Else show the form so the person can be emailed
Else

%><br />
<form method="post" name="frmEmailMsg" action="email_messenger.asp" onSubmit="return CheckForm();" onReset="return confirm('<% = strResetFormConfirm %>');">
 <table width="610" border="0" cellspacing="0" cellpadding="1" bgcolor="<% = strTableBorderColour %>" height="230" align="center">
  <tr>
   <td height="66" width="967">
    <table width="610" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="<% = strTableColour %>" background="<% = strTableBgImage %>" height="201">
     <tr>
      <td height="199">
       <table width="610" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr align="left">
         <td colspan="2" height="31" class="text">*<% = strTxtRequiredFields %></td>
        </tr>
        <tr>
         <td align="right" width="15%" class="text"><% = strTxtRecipient %>:</td>
         <td width="70%" class="bold"><% = strToUser %></td>
        </tr>
        <tr>
         <td align="right" width="15%" class="text"><% = strTxtSubjectFolder %>*:</td>
         <td width="70%"> <input type="text" name="subject" size="30" maxlength="41"></td>
        </tr>
        <tr>
         <td valign="top" align="right" width="15%" class="text"><% = strTxtMessage %>*:<br /><br />
         <span class="smText"><% = strTxtNoHTMLorForumCodeInEmailBody %></span></td>
         <td width="70%" valign="top"><textarea name="message" cols="57" rows="12"></textarea>
         </td>
         <tr>
         <td align="right" width="15%" class="text">&nbsp;</td>
         <td width="70%" class="text">&nbsp;<input type="checkbox" name="mySelf" value="True"><% = strTxtSendACopyOfThisEmailToMyself %></td>
        </tr>
        </tr>
         <td><input name="SEID" type="hidden" id="to" value="<% = lngToUserID %>"><input name="postBack" type="hidden" id="postBack" value="true">&nbsp;</td>
        <td width="70%" align="left">
           <input type="hidden" name="sessionID" value="<% = Session.SessionID %>" />
	   <input type="submit" name="Submit" value="<% = strTxtSendEmail %>">
           <input type="reset" name="Reset" value="<% = strTxtResetForm %>">
        </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</form><%

End If
%>
<br />
<div align="center">
<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Powered by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums</a> version " & strVersion & "</span>")
	Else
  		Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""" & strImagePath & "web_wiz_guide.gif"" border=""0"" alt=""Powered by Web Wiz Forums version " & strVersion & """></a>")
	End If

	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2004 <a href=""http://www.webwizguide.info"" target=""_blank"" style=""font-size:10px"">Web Wiz Guide</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 4) & " " & strTxtSeconds & "</span>"
%>
<!-- #include file="includes/footer.asp" -->