<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Set the response buffer to true as we maybe redirecting
Response.Buffer = True 
Set rsCommon = Nothing


'If the user is user is using a banned IP redirect to an error page
If bannedIP() Then
	
	'Clean up
	adoCon.Close
	Set adoCon = Nothing
	
	'Redirect
	Response.Redirect("insufficient_permission.asp?M=IP")

End If

'dimension variables
Dim lngMemberID	'Holds the member Id to delete
Dim strReturn	'Holds the return page mode



'Intilise variables
strReturn = "UPD"


'Read in the member ID to delete
lngMemberID = CLng(Request.QueryString("MID"))


'If this is the forum admin and the ID number passed across is numeric then delete the member
If blnAdmin = True AND isNumeric(lngMemberID) Then
	
	'Make sure we are not trying to delete the admin or geust account
	If lngMemberID > 2 Then
	
		'Delete the members buddy list
		'Initalise the strSQL variable with an SQL statement
		strSQL = "DELETE FROM " & strDbTable & "BuddyList WHERE (Author_ID ="  & lngMemberID & ") OR (Buddy_ID ="  & lngMemberID & ")"
		
		'Write to database
		adoCon.Execute(strSQL)	
		
		
		'Delete the members private msg's
		strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (Author_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)	
		
		
		'Delete the members private msg's
		strSQL = "DELETE FROM " & strDbTable & "PMMessage WHERE (From_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Set all the users private messages to Guest account
		strSQL = "UPDATE " & strDbTable & "PMMessage SET From_ID=2 WHERE (From_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Set all the users posts to the Guest account
		strSQL = "UPDATE " & strDbTable & "Thread SET Author_ID=2 WHERE (Author_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
				
		
		'Delete the user from the email notify table
		strSQL = "DELETE FROM " & strDbTable & "EmailNotify WHERE (Author_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Delete the user from forum permissions table
		strSQL = "DELETE FROM " & strDbTable & "Permissions WHERE (Author_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		
		'Finally we can now delete the member from the forum
		strSQL = "DELETE FROM " & strDbTable & "Author WHERE (Author_ID ="  & lngMemberID & ")"
			
		'Write to database
		adoCon.Execute(strSQL)
		
		'Return page mode
		strReturn = "DEL"
	End If	
End If

'Reset main server variables
adoCon.Close
Set adoCon = Nothing

'Return to forum
Response.Redirect("register_confirm.asp?TP=" & strReturn & "&FID=0")
%>