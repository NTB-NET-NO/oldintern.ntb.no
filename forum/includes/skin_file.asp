<%
'Default White/Silver Skin

'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Guide - Web Wiz Forums
'**                                                              
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.                                
'**
'**  This program is free software; you can modify (at your own risk) any part of it 
'**  under the terms of the License that accompanies this software and use it both 
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the 
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even 
'**  if it is modified or reverse engineered in whole or in part without express 
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**   
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER 
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program; 
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**    
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************


'Global on Each Page
'---------------------------------------------------------------------------------
Const strBgColour = "#FFFFFF"			'Forum page colour
Const strBgImage = "forum_images/main_bg.gif"				'Forum bacground image path
Const strTextColour = "#000000"			'Text colour
Const strNavSpacer = " : "			'Navigation sepporator in bread crumb navigation



'Avatar Size
'---------------------------------------------------------------------------------
Const intAvatarHeight = 64	'Avatar hieght
Const intAvatarWidth = 64	'Avatar width



'Table colours
'---------------------------------------------------------------------------------
Const strTableColour = "#F4F4FB"		'Table colour
Const strTableBgImage = ""			'Table backgroud image path
Const strTableBgColour = "#FFFFFF"		'Table backgroud colour
Const strTableBorderColour = "#999999"		'Table border colour
Const strTableVariableWidth = "98%"		'Variable table size

Const strTableTitleColour = "#A6AAD3"		'Table title colour
Const strTableTitleBgImage = "forum_images/table_bg_image.gif"	'Table backgroud image path

Const strTableTitleColour2 = "#E6E7F2"		'Colour of second title if more than one title in a table
Const strTableTitleBgImage2 = ""		'Background image path if more than one title in a table

Const strTableEvenRowColour = "#F4F4FB"		'Even table row colour
Const strTableOddRowColour = "#F8F8FC"		'Odd table row colour

Const strTableBottomRowColour = "#E6E7F2"	'This is the bottom row colour for things like buttons on forms


'Posts table colours (forum_posts.asp)
'---------------------------------------------------------------------------------
Const strTablePostsColour = "#FBFBF6"		'Posts table colour
Const strTablePostsBgImage = ""			'Posts table backgroud image path
Const strTablePostsBgColour = "#FFFFFF"		'Posts table backgroud colour
Const strTablePostsBorderColour = "#999999"	'Posts table border colour
Const strTablePostsVariableWidth = "98%"	'Posts variable table size

Const strTablePostsTitleColour = "#A6AAD3"	'Posts table title colour
Const strTablePostsTitleBgImage = "forum_images/table_bg_image.gif" 'Posts table backgroud image path

Const strTablePostsEvenRowColour = "#F4F4FB"	'Posts even post background colour
Const strTablePostsOddRowColour = "#F8F8FC"	'Posts odd posts backgroud colour
Const strTablePostsSideEvenRowColour = "#F4F4FB"'Posts even side post background colour
Const strTablePostsSideOddRowColour = "#F8F8FC"	'Posts odd side posts backgroud colour

Const strTablePostsSeporatorColour = "#E1E2F0"	'Colour of the table row that sepporates the posts

Dim strTableQuoteBorderColour
strTableQuoteBorderColour = "#999999"	'Quoted Posts table border colour (This is quotes within a post)
Dim strTableQuoteColour
strTableQuoteColour = "#FFFFFF"		'Quoted Posts table colour




'Poll table colours (poll_display_inc.asp)
'---------------------------------------------------------------------------------
Const strTablePollColour = "#FBFBF6"		'Poll table colour
Const strTablePollBgImage = ""			'Poll table backgroud image path
Const strTablePollBgColour = "#FFFFFF"		'Poll table backgroud colour
Const strTablePollBorderColour = "#999999"	'Poll table border colour
Const strTablePollVariableWidth = "98%"		'Poll variable table size

Const strTablePollTitleColour = "#A6AAD3"	'Poll table title colour
Const strTablePollTitleBgImage = "forum_images/table_bg_image.gif"	'Poll table backgroud image path

Const strTablePollColumnHeadingColour = "#E6E7F2"	'Poll second title row colour
Const strTablePollColumnHeadingBgImage = ""	'Poll second title row backgroud image path

Const strTablePollEvenRowColour = "#F4F4FB"	'Poll even post background colour
Const strTablePollOddRowColour = "#F8F8FC"	'Poll odd posts backgroud colour

Const strTablePollBottomRowColour = "#E6E7F2"	'Poll bottom row colour, the one displaying the cast vote button and vote status




'Private Msg display box (pm_show_message.asp)
'---------------------------------------------------------------------------------
Const strTablePMBgColour = "#FFFFFF"		'PM table colour
Const strTablePMBgImage = ""			'PM table backgroud image path
Const strTablePMBoxBgColour = "#F4F4FB"		'PM table backgroud colour
Const strTablePMBoxSideBgColour = "#F4F4FB"	'PM table side backgroud colour

Const strTablePMTitleColour = "#A6AAD3"		'PM table title colour
Const strTablePMTitleBgImage = "forum_images/table_bg_image.gif"	'PM table backgroud image path




'Profile table colours (poll_display_inc.asp)
'---------------------------------------------------------------------------------
Const strTableProfileColour = "#F4F4FB"		'Profile table colour
Const strTableProfileBgImage = ""		'Profile table backgroud image path
Const strTableProfileBgColour = "#FFFFFF"	'Profile table backgroud colour
Const strTableProfileBorderColour = "#999999"	'Profile table border colour
Const strTableRowProfileColour = "#F4F4FB"	'Profile row backgroud colour

Const strTableProfileTitleColour = "#A6AAD3"	'Profile table title colour
Const strTableProfileTitleBgImage = "forum_images/table_bg_image.gif"	'Profile table backgroud image path





'Misc
'---------------------------------------------------------------------------------
Const strIETextBoxColour = "#FFFFFF"	'Colour of the RTE WYSIWYG Post Editor Box
Const strImagePath = "forum_images/"	'Path to the forum images




'Cascading Style Sheet Link
'---------------------------------------------------------------------------------
Response.Write(vbCrLf & "<link href=""includes/default_style.css"" rel=""stylesheet"" type=""text/css"" />")

%>