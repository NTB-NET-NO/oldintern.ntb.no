<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Rich Text Editor
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************

'RTE Toolbar 1


Response.Write(vbCrLf & "             <span id=""ToolBar1"">")

'Font Style
If blnFontStyle Then 
	Response.Write(vbCrLf & "             <select id=""selectTxtBlock"" name=""selectTxtBlock"" onChange=""FormatText('formatblock', selectTxtBlock.options[selectTxtBlock.selectedIndex].value); document.frmAddMessage.selectTxtBlock.options[0].selected = true;"">" & _
	vbCrLf & "	      <option value=""0"" selected>-- " & strTxtFontStyle & " --</option>" & _
	vbCrLf & "	      <option value=""<p>"">Normal</option>" & _
	vbCrLf & "	      <option value=""<p>"">Paragraph</option>" & _
	vbCrLf & "	      <option value=""<h1>"">Heading 1 <h1></option>" & _
	vbCrLf & "	      <option value=""<h2>"">Heading 2 <h2></option>" & _
	vbCrLf & "	      <option value=""<h3>"">Heading 3 <h3></option>" & _
	vbCrLf & "	      <option value=""<h4>"">Heading 4 <h4></option>" & _
	vbCrLf & "	      <option value=""<h5>"">Heading 5 <h5></option>" & _
	vbCrLf & "	      <option value=""<h6>"">Heading 6 <h6></option>" & _
	vbCrLf & "	      <option value=""<address>"">Address <ADDR></option>" & _
	vbCrLf & "	      <option value=""<pre>"">Formatted <pre></option>" & _
	vbCrLf & "             </select>")
End If

'Font Type
If blnFontType Then
	Response.Write(vbCrLf & "             <select id=""selectFont"" name=""selectFont"" onChange=""FormatText('fontname', selectFont.options[selectFont.selectedIndex].value); document.frmAddMessage.selectFont.options[0].selected = true;"">" & _
	vbCrLf & "              <option value=""0"" selected>-- " & strTxtFontTypes & " --</option>" & _
	vbCrLf & "              <option value=""Arial, Helvetica, sans-serif"">Arial</option>" & _
	vbCrLf & "              <option value=""Times New Roman, Times, serif"">Times</option>" & _
	vbCrLf & "              <option value=""Courier New, Courier, mono"">Courier New</option>" & _
	vbCrLf & "              <option value=""Verdana, Arial, Helvetica, sans-serif"">Verdana</option>" & _
	vbCrLf & "             </select>")
End If

'Font Size
If blnFontSize Then
	Response.Write(vbCrLf & "             <select id=""selectFontSize"" name=""selectFontSize"" onChange=""FormatText('fontsize', selectFontSize.options[selectFontSize.selectedIndex].value); document.frmAddMessage.selectFontSize.options[0].selected = true;"">" & _
	vbCrLf & "              <option value=""0"" selected>-- " & strTxtFontSizes & " --</option>" & _
	vbCrLf & "              <option value=""1"">1</option>" & _
	vbCrLf & "              <option value=""2"">2</option>" & _
	vbCrLf & "              <option value=""3"">3</option>" & _
	vbCrLf & "              <option value=""4"">4</option>" & _
	vbCrLf & "              <option value=""5"">5</option>" & _
	vbCrLf & "              <option value=""6"">6</option>" & _
	vbCrLf & "              <option value=""7"">7</option>" & _
	vbCrLf & "             </select>")
End If


If blnBold Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_bold.gif"" align=""absmiddle"" title=""" & strTxtBold & """ onClick=""FormatText('bold', '')"" style=""cursor: pointer;"">")
If blnItalic Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_italic.gif""  align=""absmiddle"" title=""" & strTxtItalic & """ onClick=""FormatText('italic', '')"" style=""cursor: pointer;""> ")
If blnUnderline Then Response.Write(vbCrLf & "             <img src=""" & strImagePath & "post_button_underline.gif"" align=""absmiddle"" title=""" & strTxtUnderline & """ onClick=""FormatText('underline', '')"" style=""cursor: pointer;"">&nbsp; ")            
If blnTextColour Then Response.Write(vbCrLf & "             <img id=""forecolor"" src=""" & strImagePath & "post_button_colour_pallete.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtTextColour & """ onClick=""FormatText('forecolor', '')"" style=""cursor: pointer;"">")
If (RTEenabled = "winIE" OR RTEenabled = "winIE5") AND blnTextBackgroundColour Then Response.Write(vbCrLf & "             <img id=""backcolor"" src=""" & strImagePath & "post_button_fill.gif"" align=""absmiddle"" border=""0"" title=""" & strTxtBackgroundColour & """ onClick=""FormatText('backcolor', '')"" style=""cursor: pointer;"">&nbsp;")        
If blnImageUpload Then Response.Write(vbCrLf & "                <a href=""javascript:openWin('upload_images.asp?MSG=RTE&FID=" & intForumID & "','images','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=150')""><img src=""" & strImagePath & "post_button_image_upload.gif"" align=""absmiddle"" alt=""" & strTxtImageUpload & """ border=""0""></a>")
If blnAttachments Then Response.Write(vbCrLf & "                <a href=""javascript:openWin('upload_files.asp?MSG=RTE&FID=" & intForumID & "','files','toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=400,height=150')""><img src=""" & strImagePath & "post_button_file_upload.gif"" align=""absmiddle"" alt=""" & strTxtFileUpload & """ border=""0""></a>")

Response.Write("              </span>")
%>