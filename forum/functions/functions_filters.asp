<%
'****************************************************************************************
'**  Copyright Notice
'**
'**  Web Wiz Guide - Web Wiz Forums
'**
'**  Copyright 2001-2004 Bruce Corkhill All Rights Reserved.
'**
'**  This program is free software; you can modify (at your own risk) any part of it
'**  under the terms of the License that accompanies this software and use it both
'**  privately and commercially.
'**
'**  All copyright notices must remain in tacked in the scripts and the
'**  outputted HTML.
'**
'**  You may use parts of this program in your own private work, but you may NOT
'**  redistribute, repackage, or sell the whole or any part of this program even
'**  if it is modified or reverse engineered in whole or in part without express
'**  permission from the author.
'**
'**  You may not pass the whole or any part of this application off as your own work.
'**
'**  All links to Web Wiz Guide and powered by logo's must remain unchanged and in place
'**  and must remain visible when the pages are viewed unless permission is first granted
'**  by the copyright holder.
'**
'**  This program is distributed in the hope that it will be useful,
'**  but WITHOUT ANY WARRANTY; without even the implied warranty of
'**  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
'**  WARRANTIES WHETHER EXPRESSED OR IMPLIED.
'**
'**  You should have received a copy of the License along with this program;
'**  if not, write to:- Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom.
'**
'**
'**  No official support is available for this program but you may post support questions at: -
'**  http://www.webwizguide.info/forum
'**
'**  Support questions are NOT answered by e-mail ever!
'**
'**  For correspondence or non support questions contact: -
'**  info@webwizguide.info
'**
'**  or at: -
'**
'**  Web Wiz Guide, PO Box 4982, Bournemouth, BH8 8XP, United Kingdom
'**
'****************************************************************************************



'**********************************************
'***  Check HTML input for malicious code *****
'**********************************************

'Check images function
Private Function checkHTML(ByVal strMessageInput)

	Dim strTempHTMLMessage		'Temporary message store
	Dim lngMessagePosition		'Holds the message position
	Dim intHTMLTagLength		'Holds the length of the HTML tags
	Dim strHTMLMessage		'Holds the HTML message
	Dim strTempMessageInput		'Temp store for the message input
	Dim lngLoopCounter		'Loop counter
	
	'Include the array of disallowed HTML tags
	%><!--#include file="disllowed_HTML_tags_inc.asp" --><%
	
	
	'Strip scripting
	strMessageInput = Replace(strMessageInput, "<script language=""javascript"">", "", 1, -1, 1)
	strMessageInput = Replace(strMessageInput, "<script language=javascript>", "", 1, -1, 1)
	strMessageInput = Replace(strMessageInput, "<script language=""vbscript"">", "", 1, -1, 1)
	strMessageInput = Replace(strMessageInput, "<script language=vbscript>", "", 1, -1, 1)
	strMessageInput = Replace(strMessageInput, "</script>", "", 1, -1, 1)


	'Place the message input into a temp store
	strTempMessageInput = strMessageInput

	'Loop through each character in the post message
	For lngMessagePosition = 1 to CLng(Len(strMessageInput))

		'If this is the end of the message then save some process time and jump out the loop
		If Mid(strMessageInput, lngMessagePosition, 1) = "" Then Exit For
		
		'If an HTML tag is found then move to the end of it so that we can strip the HTML tag and check it for malicious code
		If Mid(strMessageInput, lngMessagePosition, 1) = "<" Then

			'Get the length of the HTML tag
			intHTMLTagLength = (InStr(lngMessagePosition, strMessageInput, ">", 1) - lngMessagePosition)

			'Place the HTML tag back into the temporary message store
			strHTMLMessage = Mid(strMessageInput, lngMessagePosition, intHTMLTagLength + 1)

			'Place the HTML tag into a temporay variable store to be stripped of malcious code
			strTempHTMLMessage = strHTMLMessage


			'***** Filter Hyperlinks *****
			
			'If this is an hyperlink tag then check it for malicious code
			If InStr(1, strTempHTMLMessage, "href", 1) <> 0 Then

				'Turn < and > into forum codes so they aren't stripped when checking links
				strTempHTMLMessage = Replace(strTempHTMLMessage, "<", "**/**", 1, -1, 1)
				strTempHTMLMessage = Replace(strTempHTMLMessage, ">", "**\**", 1, -1, 1)

				'Call the format link function to strip malicious codes
				strTempHTMLMessage = formatLink(strTempHTMLMessage)

				'Turn **/** and **\** back from forum codes
				strTempHTMLMessage = Replace(strTempHTMLMessage, "**/**", "<", 1, -1, 1)
				strTempHTMLMessage = Replace(strTempHTMLMessage, "**\**", ">", 1, -1, 1)

				'Format link tag
				strTempHTMLMessage = Replace(strTempHTMLMessage, ">", " target=""_blank"">", 1, -1, 1)

			End If


			'***** Filter Image Tags *****

			'If this is an Image tag then check it for malicious code
			If InStr(1, strTempHTMLMessage, "img", 1) <> 0 AND InStr(1, strTempHTMLMessage, "src", 1) <> 0 Then

				'Turn < and > into forum codes so they aren't stripped when checking links
				strTempHTMLMessage = Replace(strTempHTMLMessage, "<", "**/**", 1, -1, 1)
				strTempHTMLMessage = Replace(strTempHTMLMessage, ">", "**\**", 1, -1, 1)

				'Call the check images function to strip malicious codes
				strTempHTMLMessage = checkImages(strTempHTMLMessage)

				'Turn **/** and **\** back from forum codes
				strTempHTMLMessage = Replace(strTempHTMLMessage, "**/**", "<", 1, -1, 1)
				strTempHTMLMessage = Replace(strTempHTMLMessage, "**\**", ">", 1, -1, 1)

				'Format image tag
				strTempHTMLMessage = Replace(strTempHTMLMessage, ">", " border=""0"">", 1, -1, 1)
				
			End If


			'***** Filter Unwanted HTML Tags *****

			'If this is not an image or a link then cut all unwanted HTML out of the HTML tag
			If InStr(1, strTempHTMLMessage, "href", 1) = 0 AND InStr(1, strTempHTMLMessage, "img", 1) = 0 Then

				'Loop through the array of disallowed HTML tags
				For lngLoopCounter = LBound(saryHTMLtags) To UBound(saryHTMLtags)
					strTempHTMLMessage = Replace(strTempHTMLMessage, saryHTMLtags(lngLoopCounter), "", 1, -1, 1)
				Next
			End If



			'***** Format Unwanted HTML Tags *****
			
			'Strip out malicious code from the HTML that may have not been stripped but trying to sneak through in a hyperlink or image src
			strTempHTMLMessage = formatInput(strTempHTMLMessage)


			'Place the new fromatted HTML tag back into the message post
			strTempMessageInput = Replace(strTempMessageInput, strHTMLMessage, strTempHTMLMessage, 1, -1, 1)
			
		End If
	Next

	'Return the function
	checkHTML = strTempMessageInput
End Function





'******************************************
'***  Check Images for malicious code *****
'******************************************

'Check images function
Private Function checkImages(ByVal strInputEntry)

	Dim strImageFileExtension	'Holds the file extension of the image
	Dim saryImageTypes		'Array holding allowed image types in the forum
	Dim intExtensionLoopCounter	'Holds the loop counter for the array
	Dim blnImageExtOK		'Set to true if the image extension is OK

	'If there is no . in the link then there is no extenison and so can't be an image
	If inStr(1, strInputEntry, ".", 1) = 0 Then

		strInputEntry = ""

	'Else remove malicious code and check the extension is an image extension
	Else

		'Initiliase variables
		blnImageExtOK = false
		
		'Get the file extension
		strImageFileExtension = LCase(Mid(strInputEntry, InStrRev(strInputEntry, "."), 4))

		'Get the image types allowed in the forum
		strImageTypes = strImageTypes & ";gif;jpg;jpe;bmp;png"
		
		'Place the image types into an array
		saryImageTypes = Split(Trim(strImageTypes), ";")
		
		'Loop through all the allowed extensions and see if the image has one
		For intExtensionLoopCounter = 0 To UBound(saryImageTypes)
		
			'Reformat extension to check
			saryImageTypes(intExtensionLoopCounter) = "." & Trim(Mid(saryImageTypes(intExtensionLoopCounter), 1, 3))
			
			'Check to see if the image extension is allowed
			If saryImageTypes(intExtensionLoopCounter) = strImageFileExtension Then blnImageExtOK = true	
		Next
		
		'If the image extension is not OK then strip it from the image link
		If blnImageExtOK = false Then strInputEntry = Replace(strInputEntry, strImageFileExtension, "", 1, -1, 1)

		'Call the format link function to strip malicious codes
		strInputEntry = formatLink(strInputEntry)


		'Chop out any querystring question marks that maybe in the image link
		strInputEntry = Replace(strInputEntry, "?", "", 1, -1, 1)
	End If

	'Return
	checkImages = strInputEntry
End Function





'********************************************
'*** 		 Format Links 		*****
'********************************************

'Format links funtion
Private Function formatLink(ByVal strInputEntry)

	'Remove malisous charcters from links and images
	strInputEntry = Replace(strInputEntry, "document.cookie", ".", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "javascript:", "javascript ", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "vbscript:", "vbscript ", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "javascript :", "javascript ", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "vbscript :", "vbscript ", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "[", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "]", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "(", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, ")", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "{", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "}", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "<", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, ">", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "|", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "script", "&#115;cript", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "SCRIPT", "&#083;CRIPT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Script", "&#083;cript", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "script", "&#083;cript", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "object", "&#111;bject", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "OBJECT", "&#079;BJECT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Object", "&#079;bject", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "object", "&#079;bject", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "applet", "&#097;pplet", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "APPLET", "&#065;PPLET", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Applet", "&#065;pplet", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "applet", "&#065;pplet", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "embed", "&#101;mbed", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "EMBED", "&#069;MBED", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Embed", "&#069;mbed", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "embed", "&#069;mbed", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "document", "&#100;ocument", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "DOCUMENT", "&#068;OCUMENT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Document", "&#068;ocument", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "document", "&#068;ocument", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "cookie", "&#099;ookie", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "COOKIE", "&#067;OOKIE", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Cookie", "&#067;ookie", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "cookie", "&#067;ookie", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "event", "&#101;vent", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "EVENT", "&#069;VENT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Event", "&#069;vent", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "event", "&#069;vent", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "on", "&#111;n", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "ON", "&#079;N", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "On", "&#079;n", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "on", "&#111;n", 1, -1, 1)

	'Return
	formatLink = strInputEntry
End Function






'******************************************
'***  		Format user input     *****
'******************************************

'Format user input function
Private Function formatInput(ByVal strInputEntry)

	'Get rid of malicous code in the message
	strInputEntry = Replace(strInputEntry, "</script>", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "<script language=""javascript"">", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "<script language=javascript>", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "script", "&#115;cript", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "SCRIPT", "&#083;CRIPT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Script", "&#083;cript", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "script", "&#083;cript", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "object", "&#111;bject", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "OBJECT", "&#079;BJECT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Object", "&#079;bject", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "object", "&#079;bject", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "applet", "&#097;pplet", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "APPLET", "&#065;PPLET", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Applet", "&#065;pplet", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "applet", "&#065;pplet", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "embed", "&#101;mbed", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "EMBED", "&#069;MBED", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Embed", "&#069;mbed", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "embed", "&#069;mbed", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "event", "&#101;vent", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "EVENT", "&#069;VENT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Event", "&#069;vent", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "event", "&#069;vent", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "document", "&#100;ocument", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "DOCUMENT", "&#068;OCUMENT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Document", "&#068;ocument", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "document", "&#068;ocument", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "cookie", "&#099;ookie", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "COOKIE", "&#067;OOKIE", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Cookie", "&#067;ookie", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "cookie", "&#067;ookie", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "form", "&#102;orm", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "FORM", "&#070;ORM", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Form", "&#070;orm", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "form", "&#070;orm", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "iframe", "i&#102;rame", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "IFRAME", "I&#070;RAME", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Iframe", "I&#102;rame", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "iframe", "i&#102;rame", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "textarea", "&#116;extarea", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "TEXTAREA", "&#84;EXTAREA", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "Textarea", "&#84;extarea", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "textarea", "&#84;extarea", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "on", "&#111;n", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "ON", "&#079;N", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "On", "&#079;n", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "on", "&#111;n", 1, -1, 1)


	'Reformat a few bits
	strInputEntry = Replace(strInputEntry, "<STR&#079;NG>", "<strong>", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "<str&#111;ng>", "<strong>", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "</STR&#079;NG>", "</strong>", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "</str&#111;ng>", "</strong>", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "f&#111;nt", "font", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "F&#079;NT", "FONT", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "F&#111;nt", "Font", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "f&#079;nt", "font", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "f&#111;nt", "font", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "m&#111;no", "mono", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "M&#079;NO", "MONO", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "M&#079;no", "Mono", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "m&#079;no", "mono", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "m&#111;no", "mono", 1, -1, 1)

	'Return
	formatInput = strInputEntry
End Function






'********************************************
'*** 		 Format SQL input	*****
'********************************************

'Format SQL Query funtion
Private Function formatSQLInput(ByVal strInputEntry)

	'Remove malisous charcters from links and images
	strInputEntry = Replace(strInputEntry, "<", "&lt;")
	strInputEntry = Replace(strInputEntry, ">", "&gt;")
	strInputEntry = Replace(strInputEntry, "[", "&#091;")
	strInputEntry = Replace(strInputEntry, "]", "&#093;")
	strInputEntry = Replace(strInputEntry, """", "", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "=", "&#061;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "'", "''", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "select", "sel&#101;ct", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "join", "jo&#105;n", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "union", "un&#105;on", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "where", "wh&#101;re", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "insert", "ins&#101;rt", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "delete", "del&#101;te", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "update", "up&#100;ate", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "like", "lik&#101;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "drop", "dro&#112;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "create", "cr&#101;ate", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "modify", "mod&#105;fy", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "rename", "ren&#097;me", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "alter", "alt&#101;r", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "cast", "ca&#115;t", 1, -1, 1)

	'Return
	formatSQLInput = strInputEntry
End Function





'*********************************************
'***  		Strip all tags		 *****
'*********************************************

'Remove all tags for text only display (mainly for subject lines)
Private Function removeAllTags(ByVal strInputEntry)

	'Remove all HTML scripting tags etc. for plain text output
	strInputEntry = Replace(strInputEntry, "&", "&amp;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "<", "&lt;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, ">", "&gt;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, "'", "&#146;", 1, -1, 1)
	strInputEntry = Replace(strInputEntry, """", "&quot;", 1, -1, 1)

	'Return
	removeAllTags = strInputEntry
End Function





'******************************************
'***  Non-Alphanumeric Character Strip ****
'******************************************

'Function to strip non alphanumeric characters
Private Function characterStrip(strTextInput)

	'Dimension variable
	Dim intLoopCounter 	'Holds the loop counter
	
	'Loop through the ASCII characters
	For intLoopCounter = 0 to 47
		strTextInput = Replace(strTextInput, CHR(intLoopCounter), "", 1, -1, 0) 
	Next
	
	'Loop through the ASCII characters numeric characters to lower-case characters
	For intLoopCounter = 91 to 96
		strTextInput = Replace(strTextInput, CHR(intLoopCounter), "", 1, -1, 0) 
	Next
	
	'Loop through the extended ASCII characters
	For intLoopCounter = 58 to 64
		strTextInput = Replace(strTextInput, CHR(intLoopCounter), "", 1, -1, 0) 
	Next
	
	'Loop through the extended ASCII characters
	For intLoopCounter = 123 to 255
		strTextInput = Replace(strTextInput, CHR(intLoopCounter), "", 1, -1, 0) 
	Next
	
	
	'Return the string
	characterStrip = strTextInput
	
End Function





'**********************************************
'*** 		 Strip HTML 		  *****
'**********************************************

'Remove HTML function
Private Function removeHTML(ByVal strMessageInput)

	Dim lngMessagePosition		'Holds the message position
	Dim intHTMLTagLength		'Holds the length of the HTML tags
	Dim strHTMLMessage		'Holds the HTML message
	Dim strTempMessageInput		'Temp store for the message input


	'Place the message input into a temp store
	strTempMessageInput = strMessageInput

	'Loop through each character in the post message
	For lngMessagePosition = 1 to CLng(Len(strMessageInput))

		'If this is the end of the message then save some process time and jump out the loop
		If Mid(strMessageInput, lngMessagePosition, 1) = "" Then Exit For
		
		'If an HTML tag is found then jump to the end so we can strip it
		If Mid(strMessageInput, lngMessagePosition, 1) = "<" Then

			'Get the length of the HTML tag
			intHTMLTagLength = (InStr(lngMessagePosition, strMessageInput, ">", 1) - lngMessagePosition)
			
			'If the end of the HTML string is in error then set it to the number of characters being passed
			If intHTMLTagLength < 0 Then intHTMLTagLength = CLng(Len(strTempMessageInput))

			'Place the HTML tag back into the temporary message store
			strHTMLMessage = Mid(strMessageInput, lngMessagePosition, intHTMLTagLength + 1)


			'Strip the HTML from the temp message store
			strTempMessageInput = Replace(strTempMessageInput, strHTMLMessage, "", 1, -1, 0)
			
		End If
	Next
	
	'Replace a few characters in the remaining text
	strTempMessageInput = Replace(strTempMessageInput, "<", "&lt;", 1, -1, 1)
	strTempMessageInput = Replace(strTempMessageInput, ">", "&gt;", 1, -1, 1)
	strTempMessageInput = Replace(strTempMessageInput, "'", "&#039;", 1, -1, 1)
	strTempMessageInput = Replace(strTempMessageInput, """", "&#034;", 1, -1, 1)
	strTempMessageInput = Replace(strTempMessageInput, "&nbsp;", "", 1, -1, 1)

	'Return the function
	removeHTML = strTempMessageInput
End Function





'******************************************
'***     Split long text strings	***
'******************************************

'Function to strip out long words, long rows of chars, and long text lines from text
Private Function removeLongText(ByVal strMessageInput)

	Dim lngMessagePosition		'Holds the message position
	Dim intHTMLTagLength		'Holds the length of the HTML tags
	Dim strHTMLMessage		'Holds the HTML message
	Dim strTempMessageText		'Temp store for the message input
	Dim strTempPlainTextWord	'Holds the plain text word
	Dim saryPlainTextWord		'Array holding the plain text words
	Dim sarySplitTextWord()		'Array holding the plain text word that has been split
	Dim lngSplitPlainTextWordLoop	'Loop counter for looping through the pain text split word
	Dim strTempOutputMessage	'Outputted string
	Dim intWordSartPos		'Holds the location in the word to start the split
	Dim saryHTMLlinks()		'Holds links from the message and thier corrisponding code
	Dim strHTMLlinksCode		'Holds the code that is replaced the links with
	Dim lngLoopCounter		'loop counter to count the number of HTML links in meesage
	Dim blnHTMLlink			'Set to true if there is a link in the message body
	Dim strTempFlashMsg		'Temp store for the falsh forum code
	Dim lngStartPos
	Dim lngEndPos
	Const intMaxWordLength = 60	'Holds the max word lentgh (can't be below 22 or will mess up the link code placed into messages)
	
	
	'Initliase variables
	lngLoopCounter = 0
	blnHTMLlink = False
	
	'Place the message input into a temp store
	strTempMessageText = strMessageInput
	strTempOutputMessage = strMessageInput



	'****** Remove flash forum code so it's not changed *******
	
	'Loop through all the codes in the message and convert them to formated flash links
	Do While InStr(1, strTempMessageText, "[FLASH", 1) > 0 AND InStr(1, strTempMessageText, "[/FLASH]", 1) > 0

		'Get the Flash BBcode from the message
		lngStartPos = InStr(1, strTempMessageText, "[FLASH", 1)
		lngEndPos = InStr(lngStartPos, strTempMessageText, "[/FLASH]", 1) + 8

		'Make sure the end position is not in error
		If lngEndPos < lngStartPos Then lngEndPos = lngStartPos + 6

		'Get the original Flash BBcode from the message
		strTempFlashMsg = Trim(Mid(strTempMessageText, lngStartPos, lngEndPos-lngStartPos))
		
		'Add 1 to the loop counter
		lngLoopCounter = lngLoopCounter + 1
				
		'Redim the array, use preserve to keep the old array parts
		ReDim Preserve saryHTMLlinks(2,lngLoopCounter)
				
		'Create a code to replace the link with in original string
		strHTMLlinksCode = " **/**WWFflash00" & lngLoopCounter & "**\** "
				
		'Place the code and the original link into the array
		saryHTMLlinks(1,lngLoopCounter) = strHTMLlinksCode
		saryHTMLlinks(2,lngLoopCounter) = strHTMLMessage
		
		'Rpleace the HTML tag with the new code that is saved in array
		strTempMessageText = Replace(strTempMessageText, strTempFlashMsg, strHTMLlinksCode, 1, -1, 0)
			
		'A link is found so set the link found variable to true
		blnHTMLlink = True
		
		strTempMessageText = Replace(strTempMessageText, strTempFlashMsg, Replace(strTempFlashMsg, "[", "&#91;", 1, -1, 1), 1, -1, 1)
	Loop




	'****** Strip HTML from message *******
	
	'Loop through each character in the post message
	For lngMessagePosition = 1 to CLng(Len(strMessageInput))

		'If this is the end of the message then save some process time and jump out the loop
		If Mid(strMessageInput, lngMessagePosition, 1) = "" Then Exit For
		
		'If an HTML tag is found then jump to the end so we can strip it
		If Mid(strMessageInput, lngMessagePosition, 1) = "<" Then

			'Get the length of the HTML tag
			intHTMLTagLength = (InStr(lngMessagePosition, strMessageInput, ">", 1) - lngMessagePosition)
			
			'If the end of the HTML string is in error then set it to the number of characters being passed
			If intHTMLTagLength < 0 Then intHTMLTagLength = CLng(Len(strTempMessageText))

			'Place the HTML tag back into the temporary message store
			strHTMLMessage = Mid(strMessageInput, lngMessagePosition, intHTMLTagLength + 1)
			
			
			'****** Remove links so they aren't changed *******
			
			'See if the HTML tag is a link, if so replace with a code so it is not changed when spliting long chars
			If InStr(1, strHTMLMessage, "href", 1) Then
				
				'Add 1 to the loop counter
				lngLoopCounter = lngLoopCounter + 1
				
				'Redim the array, use preserve to keep the old array values
				ReDim Preserve saryHTMLlinks(2,lngLoopCounter)
				
				'Create a code to replace the link with in original string
				strHTMLlinksCode = " **/**WWFlink00" & lngLoopCounter & "**\** "
				
				'Place the code and the original link into the array
				saryHTMLlinks(1,lngLoopCounter) = strHTMLlinksCode
				saryHTMLlinks(2,lngLoopCounter) = strHTMLMessage
				
				'Rpleace the HTML tag with the new code that is saved in array
				strTempOutputMessage = Replace(strTempOutputMessage, strHTMLMessage, strHTMLlinksCode, 1, -1, 0)
			
				'A link is found so set the link found variable to true
				blnHTMLlink = True
			End If
			'***************************************************
	
			'Strip the HTML
			strTempMessageText = Replace(strTempMessageText, strHTMLMessage, " ", 1, -1, 0)
		End If
	Next
	
	
	
	'****** Strip Forum Codes from message *******
	
	'Loop through each character in the post message
	For lngMessagePosition = 1 to CLng(Len(strMessageInput))

		'If this is the end of the message then save some process time and jump out the loop
		If Mid(strMessageInput, lngMessagePosition, 1) = "" Then Exit For
		
		'If an BBCode tag is found then jump to the end so we can strip it
		If Mid(strMessageInput, lngMessagePosition, 1) = "[" Then

			'Get the length of the BBCode tag
			intHTMLTagLength = (InStr(lngMessagePosition, strMessageInput, "]", 1) - lngMessagePosition)
			
			'If the end of the BBCode string is in error then set it to the number of characters being passed
			If intHTMLTagLength < 0 Then intHTMLTagLength = CLng(Len(strTempMessageText))

			'Place the BBCode tag back into the temporary message store
			strHTMLMessage = Mid(strMessageInput, lngMessagePosition, intHTMLTagLength + 1)
	
			'Strip the BBCode
			strTempMessageText = Replace(strTempMessageText, strHTMLMessage, " ", 1, -1, 0)
		End If
	Next
	


	'****** Check for and remove long strings *******
	
	'Now we have just the text (no HTML) in plain text variable see if any of the text strings in it are over 30 chars in length
	saryPlainTextWord = Split(Trim(strTempMessageText), " ")
	
	'Loop through all the words till they are shortened
	For lngLoopCounter = 0 To UBound(saryPlainTextWord)
		
		'If the text string length is more than the max word length then place spaces in the text string
		If Len(saryPlainTextWord(lngLoopCounter)) > intMaxWordLength Then
			
			'Redim the array (don't use preserve as we want to loose the last data in the array)
			Redim sarySplitTextWord(CInt(Len(saryPlainTextWord(lngLoopCounter))/intMaxWordLength+1))
			
			'Initiliase variable
			intWordSartPos = 1
			
			'Loop through all the splits in the word
			For lngSplitPlainTextWordLoop = 1 To UBound(sarySplitTextWord)
			
				'Place the split word into the array
				sarySplitTextWord(lngSplitPlainTextWordLoop) = Mid(saryPlainTextWord(lngLoopCounter), intWordSartPos, intMaxWordLength)

				'Add max word length to the start position
				intWordSartPos = intWordSartPos + intMaxWordLength
			Next
			
			'Place the split up long text string back together in one variable with spaces at the max word length
			strTempPlainTextWord = Join(sarySplitTextWord, " ")
			
			'Place the split up word back into the orginal message
			strTempOutputMessage = Replace(strTempOutputMessage, saryPlainTextWord(lngLoopCounter), strTempPlainTextWord, 1, -1, 0)
		End If
	Next
	
	
	
	'****** Replace links so they aren't changed ******
	
	'Place back all the links into the message in place of the codes placed in for them
	If blnHTMLlink Then
		'Loop through each of the changed links
		For lngLoopCounter = 1 To Ubound(saryHTMLlinks,2)
		
			'Strip line carridge returns for MAC users
			saryHTMLlinks(2,lngLoopCounter) = Replace(saryHTMLlinks(2,lngLoopCounter), vbCrLf, "", 1, -1, 0) 
		
			'Replace the code with the link
			strTempOutputMessage = Replace(strTempOutputMessage, saryHTMLlinks(1,lngLoopCounter), saryHTMLlinks(2,lngLoopCounter), 1, -1, 0)
		
		Next
	End If
	

	'Return the function
	removeLongText = strTempOutputMessage
End Function





'*********************************************
'***  	   Decode HTML encoding		 *****
'*********************************************

'Decode encoded strings
Private Function decodeString(ByVal strInputEntry)

	'Decode HTML character entities

	strInputEntry = Replace(strInputEntry, "&#097;", "a", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#098;", "b", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#099;", "c", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#100;", "d", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#101;", "e", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#102;", "f", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#103;", "g", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#104;", "h", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#105;", "i", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#106;", "j", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#107;", "k", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#108;", "l", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#109;", "m", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#110;", "n", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#111;", "o", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#112;", "p", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#113;", "q", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#114;", "r", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#115;", "s", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#116;", "t", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#117;", "u", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#118;", "v", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#119;", "w", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#120;", "x", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#121;", "y", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#122;", "z", 1, -1, 0)

	strInputEntry = Replace(strInputEntry, "&#065;", "A", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#066;", "B", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#067;", "C", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#068;", "D", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#069;", "E", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#070;", "F", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#071;", "G", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#072;", "H", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#073;", "I", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#074;", "J", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#075;", "K", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#076;", "L", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#077;", "M", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#078;", "N", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#079;", "O", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#080;", "P", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#081;", "Q", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#082;", "R", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#083;", "S", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#084;", "T", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#085;", "U", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#086;", "V", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#087;", "W", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#088;", "X", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#089;", "Y", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#090;", "Z", 1, -1, 0)
	

	strInputEntry = Replace(strInputEntry, "&#048;", "0", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#049;", "1", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#050;", "2", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#051;", "3", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#052;", "4", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#053;", "5", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#054;", "6", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#055;", "7", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#056;", "8", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#057;", "9", 1, -1, 0)
	
	
	strInputEntry = Replace(strInputEntry, "&#061;", "=", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&lt;", "<", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&gt;", ">", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&amp;", "&", 1, -1, 0)
	strInputEntry = Replace(strInputEntry, "&#146;", "'", 1, -1, 1)

	'Return
	decodeString = strInputEntry
End Function
%>